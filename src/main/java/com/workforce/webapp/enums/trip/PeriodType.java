package com.workforce.webapp.enums.trip;

public enum PeriodType {
    DAILY, WEEKLY, MONTHLY;
}

