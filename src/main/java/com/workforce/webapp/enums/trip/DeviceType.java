package com.workforce.webapp.enums.trip;

public enum DeviceType {
    EXECUTIVE_DEVICE_MOBILE, TRACKING_DEVICE;
}
