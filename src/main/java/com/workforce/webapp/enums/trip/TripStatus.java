package com.workforce.webapp.enums.trip;

public enum TripStatus {
    CREATED, COMPLETED, IN_PROGRESS, REJECTED, ACCEPTED, PENDING, CANCELLED, CHECK_IN, CHECK_OUT, ALLOCATED, ON_HOLD;
}
