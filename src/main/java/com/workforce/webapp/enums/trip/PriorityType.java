package com.workforce.webapp.enums.trip;

public enum PriorityType {
    HIGH, MEDIUM, LOW;
}
