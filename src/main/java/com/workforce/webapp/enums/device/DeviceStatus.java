package com.workforce.webapp.enums.device;

public enum DeviceStatus {

    REQUESTED, APPROVED, ACCEPTED, COMPLETED, ENROLLED, ASSIGNED, ACTIVE, DE_ACTIVE, EXPIRED, CANCELLED;
}
