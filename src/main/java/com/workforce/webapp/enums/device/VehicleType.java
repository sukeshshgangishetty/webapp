package com.workforce.webapp.enums.device;

public enum VehicleType {
    TRUCK, BUS, CAR, BIKE, OTHER;
}
