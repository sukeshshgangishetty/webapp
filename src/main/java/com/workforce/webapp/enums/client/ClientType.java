package com.workforce.webapp.enums.client;

public enum ClientType {
    B2B, B2C, C2C;
}
