package com.workforce.webapp.enums.client;

public enum RequestType {
    UPDATE, DELETE, CREATE;
}
