package com.workforce.webapp.enums.client;

public enum Designation {
    SUPERVISOR, MANAGER, SALESMAN;
}
