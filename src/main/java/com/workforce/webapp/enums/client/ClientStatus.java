package com.workforce.webapp.enums.client;

public enum ClientStatus {
    ACTIVE, INACTIVE;
}
