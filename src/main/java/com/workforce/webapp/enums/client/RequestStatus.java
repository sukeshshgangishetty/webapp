package com.workforce.webapp.enums.client;

public enum RequestStatus {
    CREATED, PENDING, COMPLETED;
}
