package com.workforce.webapp.dto;

public enum FilterType {

    DATE, TEXT, DROPDOWN, DATERANGE
}
