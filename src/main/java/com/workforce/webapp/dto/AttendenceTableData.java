package com.workforce.webapp.dto;

import java.util.List;

public class AttendenceTableData {

    int attendanceMonth;

    int attendanceYear;

    List<AttendenceData> attendancedata;

    public int getAttendanceMonth() {
        return attendanceMonth;
    }

    public void setAttendanceMonth(int attendanceMonth) {
        this.attendanceMonth = attendanceMonth;
    }

    public int getAttendanceYear() {
        return attendanceYear;
    }

    public void setAttendanceYear(int attendanceYear) {
        this.attendanceYear = attendanceYear;
    }

    public List<AttendenceData> getAttendancedata() {
        return attendancedata;
    }

    public void setAttendancedata(List<AttendenceData> attendancedata) {
        this.attendancedata = attendancedata;
    }

    public AttendenceTableData(int attendanceMonth, int attendanceYear, List<AttendenceData> attendancedata) {
        super();
        this.attendanceMonth = attendanceMonth;
        this.attendanceYear = attendanceYear;
        this.attendancedata = attendancedata;
    }

    public AttendenceTableData() {
        super();
    }


}
