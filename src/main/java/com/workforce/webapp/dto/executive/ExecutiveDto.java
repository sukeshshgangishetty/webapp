package com.workforce.webapp.dto.executive;

import com.workforce.webapp.model.Address;
import com.workforce.webapp.model.Organization;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class ExecutiveDto {

    private String name;

    private String phone;

    private String imgLogo;

    private Address address;

    private String email;

    private Long joinedDate;

    private String vehicleNumber;

    private String pfNumber;

    private String esiNumber;

    private Long dob;

    private String aadhaarNumber;

    private String panCardNumber;

    private String qualification;

    private String remarks;
}
