package com.workforce.webapp.dto;

public class EmployeeMeta {

    private String name;
    private String role;
    private String image;
    private long empid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getEmpid() {
        return empid;
    }

    public void setEmpid(long empid) {
        this.empid = empid;
    }

    public EmployeeMeta(String name, String role, String image, long empid) {
        super();
        this.name = name;
        this.role = role;
        this.image = image;
        this.empid = empid;
    }

    public EmployeeMeta() {
        super();
    }


}
