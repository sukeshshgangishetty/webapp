package com.workforce.webapp.dto;

public class DeviceMappingDto {


    long orgid;

    long deviceid;

    long ddid;

    String vehicleNo;

    public long getOrgid() {
        return orgid;
    }

    public void setOrgid(long orgid) {
        this.orgid = orgid;
    }

    public long getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(long deviceid) {
        this.deviceid = deviceid;
    }


    public long getDdid() {
        return ddid;
    }

    public void setDdid(long ddid) {
        this.ddid = ddid;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }


    public DeviceMappingDto(long orgid, long deviceid, String vehicleNo) {
        super();
        this.orgid = orgid;
        this.deviceid = deviceid;
        this.vehicleNo = vehicleNo;
    }

    public DeviceMappingDto(long orgid, long deviceid) {
        this.orgid = orgid;
        this.deviceid = deviceid;
    }

    public DeviceMappingDto() {

    }


}
