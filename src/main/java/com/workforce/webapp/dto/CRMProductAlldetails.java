package com.workforce.webapp.dto;

import java.util.List;

import com.workforce.webapp.model.CRMCoupon;
import com.workforce.webapp.model.CRMProduct;
import com.workforce.webapp.model.CRMProductAddOn;
import com.workforce.webapp.model.CRMProductPlan;

public class CRMProductAlldetails {

    private CRMProduct details;

    private List<CRMProductAddOn> addons;

    private List<CRMProductPlan> plans;

    private List<CRMCoupon> coupons;

    public CRMProduct getDetails() {
        return details;
    }

    public void setDetails(CRMProduct details) {
        this.details = details;
    }

    public List<CRMProductAddOn> getAddons() {
        return addons;
    }

    public void setAddons(List<CRMProductAddOn> addons) {
        this.addons = addons;
    }

    public List<CRMProductPlan> getPlans() {
        return plans;
    }

    public void setPlans(List<CRMProductPlan> plans) {
        this.plans = plans;
    }

    public List<CRMCoupon> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<CRMCoupon> coupons) {
        this.coupons = coupons;
    }

    public CRMProductAlldetails(CRMProduct details, List<CRMProductAddOn> addons, List<CRMProductPlan> plans,
                                List<CRMCoupon> coupons) {
        super();
        this.details = details;
        this.addons = addons;
        this.plans = plans;
        this.coupons = coupons;
    }

    public CRMProductAlldetails() {
        super();
    }


}
