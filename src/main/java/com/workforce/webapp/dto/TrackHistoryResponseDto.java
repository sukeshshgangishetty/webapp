package com.workforce.webapp.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.workforce.webapp.model.TrackDetails;


public class TrackHistoryResponseDto {

    @JsonFormat(pattern = "dd-MM-yyyy")
    Date date;

    List<TrackDetails> tracks;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<TrackDetails> getTracks() {
        return tracks;
    }

    public void setTracks(List<TrackDetails> tracks) {
        this.tracks = tracks;
    }

    public TrackHistoryResponseDto(Date date, List<TrackDetails> tracks) {
        this.date = date;
        this.tracks = tracks;
    }

    public TrackHistoryResponseDto() {
    }


}
