package com.workforce.webapp.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Response implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 9135667998821289159L;

    long timestamp = new Date().getTime();

    @JsonInclude(Include.NON_DEFAULT)
    int status;

    String error;

    String exception;

    String message;

    String path;

    Object filters;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    Object data;

    public String getMessage() {
        return this.message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getFilters() {
        return filters;
    }

    public void setFilters(Object filters) {
        this.filters = filters;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Response(HttpStatus status, String error, String exception, String message, String path) {
        this.status = status.value();
        this.error = error;
        this.exception = exception;
        this.message = message;
        this.path = path;
        this.data = null;
    }

    public Response(String message) {
        this.message = message;
    }

    public Response(String message, Object data) {
        this.message = message;
        this.data = data;
    }

    public Response(String message, HttpStatus status) {
        this.message = message;
        this.status = status.value();
    }

    public Response(String message, Object data, HttpStatus ok) {
        super();
        this.status = ok.value();
        this.message = message;
        this.data = data;
    }

    public Response(String message, Object data, Object filters, HttpStatus ok) {
        super();
        this.status = ok.value();
        this.message = message;
        this.data = data;
        this.filters = filters;
    }

    public Response() {
        this.message = "Unknown";
    }

}
