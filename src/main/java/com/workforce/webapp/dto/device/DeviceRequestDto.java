package com.workforce.webapp.dto.device;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class DeviceRequestDto {

    private Integer deviceCount;

    private Long orgId;

    private String comment;

    private Boolean quotationRequired;

    private Long requiredBy;
}
