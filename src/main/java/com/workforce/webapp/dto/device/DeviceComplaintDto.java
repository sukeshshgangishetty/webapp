package com.workforce.webapp.dto.device;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class DeviceComplaintDto {

    private String subject;

    private String description;

    private String emailId;

}
