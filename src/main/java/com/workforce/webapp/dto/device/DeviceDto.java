package com.workforce.webapp.dto.device;

import com.workforce.webapp.enums.device.DeviceStatus;
import com.workforce.webapp.model.Location;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class DeviceDto {

    private String imei;

    private String deviceNumber;

    private Long lastRenewal;

    private Long nextRenewal;

}
