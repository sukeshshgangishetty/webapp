package com.workforce.webapp.dto.device;

import com.workforce.webapp.enums.device.VehicleType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class DeviceDetailDto {

    private String driName;

    private String vehicleNo;

    private String driLicNo;

    private String driPhone;

    private Double maxSpeed;

    private Double millage;

    private VehicleType vehicleType;

    private String pastDriver;
}
