package com.workforce.webapp.dto;

public class ReportingMangerMeta {

    long empId;
    String name;
    com.workforce.webapp.model.Department Department;

    public long getEmpId() {
        return empId;
    }

    public void setEmpId(long empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ReportingMangerMeta(long empId, String name, com.workforce.webapp.model.Department department) {
        this.empId = empId;
        this.name = name;
        Department = department;
    }

    public com.workforce.webapp.model.Department getDepartment() {
        return Department;
    }

    public void setDepartment(com.workforce.webapp.model.Department department) {
        Department = department;
    }

    public ReportingMangerMeta() {
        super();
    }

}
