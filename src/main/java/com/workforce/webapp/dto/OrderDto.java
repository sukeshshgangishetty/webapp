package com.workforce.webapp.dto;

import java.util.List;

import com.workforce.webapp.model.OrderDetails;
import com.workforce.webapp.model.OrderParticulars;

public class OrderDto {
    OrderDetails order;
    List<OrderParticulars> particulars;

    public OrderDetails getOrder() {
        return order;
    }

    public void setOrder(OrderDetails order) {
        this.order = order;
    }

    public List<OrderParticulars> getParticulars() {
        return particulars;
    }

    public void setParticulars(List<OrderParticulars> particulars) {
        this.particulars = particulars;
    }

    public OrderDto(OrderDetails order, List<OrderParticulars> particulars) {
        super();
        this.order = order;
        this.particulars = particulars;
    }

    public OrderDto() {
        super();
    }

    @Override
    public String toString() {
        return "OrderDto [order=" + order + ", particulars=" + particulars + "]";
    }

}
