package com.workforce.webapp.dto;

import com.workforce.webapp.model.CurrentTrack;
import com.workforce.webapp.model.TrackDetails;

public class TrackDetailsDto {

    private String name;
    private CurrentTrack currentTrack;
    private TrackDetails track;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the currentTrack
     */
    public CurrentTrack getCurrentTrack() {
        return currentTrack;
    }

    /**
     * @param currentTrack the currentTrack to set
     */
    public void setCurrentTrack(CurrentTrack currentTrack) {
        this.currentTrack = currentTrack;
    }

    /**
     * @return the track
     */
    public TrackDetails getTrack() {
        return track;
    }

    /**
     * @param track the track to set
     */
    public void setTrack(TrackDetails track) {
        this.track = track;
    }

    public TrackDetailsDto(String name, CurrentTrack currentTrack, TrackDetails track) {
        super();
        this.name = name;
        this.currentTrack = currentTrack;
        this.track = track;
    }

    public TrackDetailsDto() {
        super();
    }


}
