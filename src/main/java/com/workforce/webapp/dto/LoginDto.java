package com.workforce.webapp.dto;

public class LoginDto {

    String un;
    String ps;
    String ft;
    String imei, model, manufacturer, osversion;


    public String getUn() {
        return un;
    }


    public void setUn(String un) {
        this.un = un;
    }


    public String getPs() {
        return ps;
    }


    public void setPs(String ps) {
        this.ps = ps;
    }


    public String getFt() {
        return ft;
    }


    public void setFt(String ft) {
        this.ft = ft;
    }


    public LoginDto(String un, String ps, String ft) {
        this.un = un;
        this.ps = ps;
        this.ft = ft;
    }


    public String getImei() {
        return imei;
    }


    public void setImei(String imei) {
        this.imei = imei;
    }


    public String getModel() {
        return model;
    }


    public void setModel(String model) {
        this.model = model;
    }


    public String getManufacturer() {
        return manufacturer;
    }


    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }


    public String getOsversion() {
        return osversion;
    }


    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }


    public LoginDto() {
    }


    @Override
    public String toString() {
        return "{un:" + un + ", ps:" + ps + ", ft:" + ft + ", imei:" + imei + ", model:" + model + ", manufacturer:"
                + manufacturer + ", osversion:" + osversion + "}";
    }


}
