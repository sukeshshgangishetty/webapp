package com.workforce.webapp.dto;

import java.util.Date;

public class TrackHistoryRequestDto {

    long modelid;
    int ttype;
    Date from = null;
    Date to = null;

    public long getModelid() {
        return modelid;
    }

    public void setModelid(long modelid) {
        this.modelid = modelid;
    }

    public int getTtype() {
        return ttype;
    }

    public void setTtype(int ttype) {
        this.ttype = ttype;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public TrackHistoryRequestDto(long modelid, int ttype) {
        this.modelid = modelid;
        this.ttype = ttype;
        this.from = null;
        this.to = null;
    }

    public TrackHistoryRequestDto() {
        this.from = null;
        this.to = null;
    }

    @Override
    public String toString() {
        return "TrackHistoryRequestDto [modelid=" + modelid + ", ttype=" + ttype + ", from=" + from + ", to=" + to
                + "]";
    }


}
