package com.workforce.webapp.dto.client;

import com.workforce.webapp.enums.client.RequestType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class ClientRequestDto {

    private String title;

    private String description;

    private RequestType requestType;
}
