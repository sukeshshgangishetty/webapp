package com.workforce.webapp.dto.client;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class KycDetailDto {

    private String name;

    private Long dob;

    private String panCard;

    private String aadhaarNo;

}
