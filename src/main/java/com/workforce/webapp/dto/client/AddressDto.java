package com.workforce.webapp.dto.client;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class AddressDto {

    private String line1;

    private String line2;

    private String city;

    private String state;

    private String zipcode;

    private Boolean isBilling;
}
