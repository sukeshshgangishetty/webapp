package com.workforce.webapp.dto.client;

import com.workforce.webapp.enums.client.Department;
import com.workforce.webapp.enums.client.Designation;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class OwnerDetailDto {

    private String name;

    private String description;

    private String phoneNo;

    private String emailId;

    private Long dob;

    private String department;

    private String designation;

    private Boolean isPrimary;
}
