package com.workforce.webapp.dto.client;

import com.workforce.webapp.model.Location;
import com.workforce.webapp.model.client.Category;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class ClientDto {

    private String name;

    private String description;

    private String phoneNo;

    private String landlineNo;

    private String email;

    private Location location;

    private Long incorporatedAt;

    private String gstNo;

    private Long orgId;

    private Long assignedTo;

    private Double openingBalance;

    private Integer noOfEmp;

    private Double outstanding;

    private Double creditLimit;

    private Integer repayPeriod;

    private ClientRequestDto clientRequest;

    private Category category;

    List<OwnerDetailDto> ownerDetails;
}
