package com.workforce.webapp.dto.client;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class ContactDetailDto {

    private String websiteUrl;

    private String fbUrl;

    private String twitterUrl;

    private String instaUrl;

    private String linkedinUrl;

    private String gPlusUrl;

}
