package com.workforce.webapp.dto.client;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class AccountDetailDto {

    private String name;

    private String bankName;

    private String accNo;

    private String ifsc;

    private String branch;

    private String accType;
}
