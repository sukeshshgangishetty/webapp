package com.workforce.webapp.dto.client;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class CategoryDto {

    private String name;

    private String description;
}
