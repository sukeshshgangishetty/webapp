package com.workforce.webapp.dto;

import com.workforce.webapp.model.Employee;

public class EmployeeDetails {

    Employee details;
    ReportingMangerMeta reportingManger;

    public Employee getDetails() {
        return details;
    }

    public void setDetails(Employee details) {
        this.details = details;
    }

    public ReportingMangerMeta getReportingManger() {
        return reportingManger;
    }

    public void setReportingManger(ReportingMangerMeta reportingManger) {
        this.reportingManger = reportingManger;
    }

    public EmployeeDetails(Employee details, ReportingMangerMeta reportingManger) {
        super();
        this.details = details;
        this.reportingManger = reportingManger;
    }

    public EmployeeDetails(ReportingMangerMeta reportingManger) {
        this.reportingManger = reportingManger;
    }

    public EmployeeDetails() {
    }


}
