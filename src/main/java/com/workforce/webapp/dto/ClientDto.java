package com.workforce.webapp.dto;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.model.ClientAccoutStatus;
import com.workforce.webapp.model.Location;


public class ClientDto {


    private Long id;

    private String clientname;


    private String phone;
    private String mail;

    private Location location;

    private String image;


    @JsonIgnore
    private long orgId;

    private String companyName;

    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date incorporatedAt;

    private String officeNumber;

    private String remarks;

    private String gstNo;

    private long assignedTo;

    private long updateState;

    private double openingBalance;

    private double closingBalance;

    private int orderCount;

    private int paymentsCount;

    private double totalOrderAmount;

    private double totalPaymentAmount;

    private ClientAccoutStatus accStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getOrgId() {
        return orgId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Date getIncorporatedAt() {
        return incorporatedAt;
    }

    public void setIncorporatedAt(Date incorporatedAt) {
        this.incorporatedAt = incorporatedAt;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public long getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(long assignedTo) {
        this.assignedTo = assignedTo;
    }

    public long getUpdateState() {
        return updateState;
    }

    public void setUpdateState(long updateState) {
        this.updateState = updateState;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public int getPaymentsCount() {
        return paymentsCount;
    }

    public void setPaymentsCount(int paymentsCount) {
        this.paymentsCount = paymentsCount;
    }


    public double getTotalOrderAmount() {
        return totalOrderAmount;
    }

    public void setTotalOrderAmount(double totalOrderAmount) {
        this.totalOrderAmount = totalOrderAmount;
    }

    public double getTotalPaymentAmount() {
        return totalPaymentAmount;
    }

    public void setTotalPaymentAmount(double totalPaymentAmount) {
        this.totalPaymentAmount = totalPaymentAmount;
    }


    public ClientAccoutStatus getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(ClientAccoutStatus accStatus) {
        this.accStatus = accStatus;
    }

    public ClientDto() {

    }

    public ClientDto(Long id, String clientname, String phone, String mail, Location location, String image, long orgId,
                     String companyName, Date incorporatedAt, String officeNumber, String remarks, String gstNo, long assignedTo,
                     long updateState, double openingBalance) {
        this.id = id;
        this.clientname = clientname;
        this.phone = phone;
        this.mail = mail;
        this.location = location;
        this.image = image;
        this.orgId = orgId;
        this.companyName = companyName;
        this.incorporatedAt = incorporatedAt;
        this.officeNumber = officeNumber;
        this.remarks = remarks;
        this.gstNo = gstNo;
        this.assignedTo = assignedTo;
        this.updateState = updateState;
        this.openingBalance = openingBalance;
    }


    public void setFromClient(Client client) {

        this.id = client.getId();
        this.clientname = client.getClientname();
        this.phone = client.getPhone();
        this.mail = client.getMail();
        this.location = client.getLocation();
        this.image = client.getImage();
        this.orgId = client.getOrgId();
        this.companyName = client.getCompanyName();
        this.incorporatedAt = client.getIncorporatedAt();
        this.officeNumber = client.getOfficeNumber();
        this.remarks = client.getRemarks();
        this.gstNo = client.getGstNo();
        this.assignedTo = client.getAssignedTo();
        this.updateState = client.getUpdateState();
        this.openingBalance = client.getOpeningBalance();

    }


}
