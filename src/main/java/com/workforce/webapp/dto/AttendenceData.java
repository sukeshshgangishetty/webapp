package com.workforce.webapp.dto;

import java.util.List;

import com.workforce.webapp.model.AttendanceRecord;

public class AttendenceData {

    private EmployeeMeta employee;
    private double presentdays;
    private List<AttendanceRecord> data;

    public EmployeeMeta getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeMeta employee) {
        this.employee = employee;
    }

    public double getPresentdays() {
        return presentdays;
    }

    public void setPresentdays(double presentdays) {
        this.presentdays = presentdays;
    }

    public List<AttendanceRecord> getData() {
        return data;
    }

    public void setData(List<AttendanceRecord> data) {
        this.data = data;
    }

    public AttendenceData(EmployeeMeta employee, double presentdays, List<AttendanceRecord> data) {
        super();
        this.employee = employee;
        this.presentdays = presentdays;
        this.data = data;
    }

    public AttendenceData() {
        super();
    }


}
