package com.workforce.webapp.dto.trip;

import com.workforce.webapp.enums.trip.PeriodType;
import com.workforce.webapp.enums.trip.PriorityType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class TripDto {

    private String title;

    private String description;

    private PriorityType priorityType;

    private PeriodType period;

    private List<TripPointDto> tripPoints;

}
