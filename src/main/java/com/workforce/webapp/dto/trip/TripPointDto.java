package com.workforce.webapp.dto.trip;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class TripPointDto {

    private String tripPointId;

    private PointDto point;

    private String expTym;

    private Integer sequence;

    private String purpose;
}
