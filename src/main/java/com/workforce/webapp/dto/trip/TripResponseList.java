package com.workforce.webapp.dto.trip;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class TripResponseList {

    List<TripResponse> tripResponses;
}
