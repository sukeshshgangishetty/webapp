package com.workforce.webapp.dto.trip;

import com.workforce.webapp.model.Address;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class ExecutiveResponse {

    private Long exeId;

    private String name;

    private String phone;

    private Address address;

    private String schAllocId;
}
