package com.workforce.webapp.dto.trip;

import com.workforce.webapp.enums.trip.PointType;
import com.workforce.webapp.model.Location;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class PointDto {

    private String id;

    private String name;

    private PointType type;

    private Location locCurrent;

    private String clientId;
}
