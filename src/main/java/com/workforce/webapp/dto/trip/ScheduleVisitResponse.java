package com.workforce.webapp.dto.trip;

import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.Location;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class ScheduleVisitResponse {

    private String id;

    private TripPointDto tripPoint;

    private Long checkIn;

    private Long checkOut;

    private String comment;

    private Double distance;

    private Long totTym;

    private TripStatus status;

    private Integer sequence;

    private Location location;
}
