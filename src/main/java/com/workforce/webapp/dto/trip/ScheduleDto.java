package com.workforce.webapp.dto.trip;

import com.workforce.webapp.enums.trip.DeviceType;
import com.workforce.webapp.model.trip.TripPoint;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class ScheduleDto {

    private String id;

    private Long expStrTym;

    private Long expEndTym;

    private String name;

    private List<Long> assignedTo;

    private DeviceType assignedType;

    private String tripId;

    private List<TripPointDto> tripPoints;

    private Boolean changeTripSeq;

}
