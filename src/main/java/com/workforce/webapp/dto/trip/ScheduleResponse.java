package com.workforce.webapp.dto.trip;

import com.workforce.webapp.enums.trip.DeviceType;
import com.workforce.webapp.enums.trip.TripStatus;
import io.swagger.models.auth.In;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class ScheduleResponse {

    private String id;

    private Double totDis;

    private Long expStrTym;

    private Long expEndTym;

    private Long actStrTym;

    private Long actEndTym;

    private Long totTym;

    private TripStatus status;

    private Integer totAlloc;

    private String exeAllocId;

    private Integer totVisits;

    private Integer comVisits;

    private String name;

    private String description;

    private DeviceType assignedType;
}
