package com.workforce.webapp.dto.trip;

import com.workforce.webapp.model.trip.ScheduleVisit;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class ScheduleAllocResponse {

    private List<ExecutiveResponse> executives;

    private String name;

    private Long totTym;

    private Double totDis;

    private Integer comVisits;

    private String schAllocId;

    private List<ScheduleVisitResponse> scheduleVisits;

}
