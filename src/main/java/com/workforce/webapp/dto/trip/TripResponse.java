package com.workforce.webapp.dto.trip;

import com.workforce.webapp.enums.trip.PeriodType;
import com.workforce.webapp.enums.trip.PriorityType;
import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.trip.Schedule;
import com.workforce.webapp.model.trip.TripPoint;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class TripResponse {
    private String id;

    private String title;

    private String description;

    private PriorityType priorityType;

    private PeriodType period;

    private Integer count;

    private TripStatus status;

    private Integer totVisits;

    private Integer totSchedules;

    private Integer comSchedules;

    private String createdBy;

    private Date createdAt;

    private Schedule upcomingSchedule;

    List<ScheduleResponse> schedules;

    List<ExecutiveResponse> executiveResponses;

    List<TripPoint> tripPoints;
}
