package com.workforce.webapp.dto;

import java.util.List;

import com.workforce.webapp.model.AttendanceActivity;
import com.workforce.webapp.model.AttendanceRecord;

public class AttendenceDataDto {

    private AttendanceRecord attendence;

    private List<AttendanceActivity> activities;

    public AttendanceRecord getAttendence() {
        return attendence;
    }

    public void setAttendence(AttendanceRecord attendence) {
        this.attendence = attendence;
    }

    public List<AttendanceActivity> getActivities() {
        return activities;
    }

    public void setActivities(List<AttendanceActivity> activities) {
        this.activities = activities;
    }

    public AttendenceDataDto(AttendanceRecord attendence, List<AttendanceActivity> activities) {
        super();
        this.attendence = attendence;
        this.activities = activities;
    }

    public AttendenceDataDto() {
        super();
    }

}
