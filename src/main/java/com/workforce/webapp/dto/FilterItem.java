package com.workforce.webapp.dto;

public class FilterItem {

    String fieldName;
    FilterType type;
    Object extra;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public FilterType getType() {
        return type;
    }

    public void setType(FilterType type) {
        this.type = type;
    }

    public Object getExtra() {
        return extra;
    }

    public void setExtra(Object extra) {
        this.extra = extra;
    }

    public FilterItem(String fieldName, FilterType type, Object extra) {
        super();
        this.fieldName = fieldName;
        this.type = type;
        this.extra = extra;
    }

    public FilterItem(String fieldName, FilterType type) {
        super();
        this.fieldName = fieldName;
        this.type = type;
    }


}
