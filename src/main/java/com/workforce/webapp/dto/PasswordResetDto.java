package com.workforce.webapp.dto;

public class PasswordResetDto {
    String phone;

    int otp;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public PasswordResetDto(String phone, int otp) {
        super();
        this.phone = phone;
        this.otp = otp;
    }

    public PasswordResetDto() {
        super();
    }


}
