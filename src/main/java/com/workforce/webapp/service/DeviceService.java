package com.workforce.webapp.service;

import com.workforce.webapp.dto.device.DeviceComplaintDto;
import com.workforce.webapp.dto.device.DeviceDetailDto;
import com.workforce.webapp.dto.device.DeviceDto;
import com.workforce.webapp.dto.device.DeviceRequestDto;
import com.workforce.webapp.enums.device.DeviceStatus;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.model.device.Device;
import com.workforce.webapp.model.device.DeviceComplaint;
import com.workforce.webapp.model.device.DeviceDetail;
import com.workforce.webapp.model.device.DeviceRequest;
import com.workforce.webapp.repository.device.DeviceComplaintRepository;
import com.workforce.webapp.repository.device.DeviceDetailRepository;
import com.workforce.webapp.repository.device.DeviceRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.repository.device.DeviceRequestRepository;
import com.workforce.webapp.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Component
public class DeviceService {

    private @Autowired
    DeviceRepository deviceRepository;

    private @Autowired
    DeviceDetailRepository deviceDetailRepository;

    private @Autowired
    UserDetailsRepository userRepository;

    private @Autowired
    OrganizationRepository organizationRepository;

    private @Autowired
    DeviceRequestRepository deviceRequestRepository;

    private @Autowired
    DeviceComplaintRepository deviceComplaintRepository;

    @Transactional
    public Device addOrUpdateDevice(String token, String deviceId, DeviceDto deviceDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.TRACKINGDEVICE_VENDOR)) {
            Device device = null;
            if (!Util.isNullOrEmpty(deviceId)) {
                device = updateDevice(deviceId, deviceDto, user);
            } else {
                if (user.getRole().equals(Userrole.TRACKINGDEVICE_VENDOR))
                    device = requestToEntity(null, deviceDto, user);
            }
            return deviceRepository.save(device);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private Device updateDevice(String deviceId, DeviceDto deviceDto, UserDetails user) {
        Device device = deviceRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Device not Found"));
        device = requestToEntity(device, deviceDto, user);
        return device;
    }
    /*
            if(!Util.isNullOrEmpty(deviceDto.getOrgId())){
        if(Util.isNullOrEmpty(device.getOrgId()) || (!Util.isNullOrEmpty(device.getOrgId()) && !device.getOrgId().equals(deviceDto.getOrgId()))) {
            Organization organization = organizationRepository.findById(deviceDto.getOrgId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Organisation not Found"));
            device.setOrgId(organization.getId());
            device.setStatus(DeviceStatus.ASSIGNED);
        }
    }*/

    /* if (!Util.isNullOrEmpty(deviceDto.getOrgId())) {
         Organization organization = organizationRepository.findById(deviceDto.getOrgId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Organisation not Found"));
         device.setOrgId(organization.getId());
         device.setStatus(DeviceStatus.ASSIGNED);
     }
 */
    private Device requestToEntity(Device entity, DeviceDto request, UserDetails user) {
        if (Util.isNullOrEmpty(entity)) {
            entity = new Device();
        }
        if (user.getRole().equals(Userrole.TRACKINGDEVICE_VENDOR)) {
            entity.setImei(request.getImei());
            entity.setDeviceNumber(request.getDeviceNumber());
        }
        entity.setLastRenewal(request.getLastRenewal());
        entity.setNextRenewal(request.getNextRenewal());
        entity.setStatus(DeviceStatus.ENROLLED);
        return entity;
    }


    @Transactional
    public List<Device> getAllDevices(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            return deviceRepository.findAll();
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public List<DeviceDetail> getAllDeviceByOrg(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            return deviceDetailRepository.findAllByOrgId(user.getEntityKey());
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public DeviceDetail getAllDeviceDetailById(String token, String devDetId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            DeviceDetail deviceDetail = deviceDetailRepository.findById(devDetId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "DeviceDetail not Found"));
            return deviceDetail;
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    @Transactional
    public void assignOrgToDevice(String token, String deviceId, Long orgId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            Device device = deviceRepository.findById(deviceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Device not Found"));
            Organization organization = organizationRepository.findById(orgId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Organization not Found"));
            device.setOrgId(orgId);
            device.setStatus(DeviceStatus.ASSIGNED);
            deviceRepository.save(device);
            DeviceDetail deviceDetail = new DeviceDetail();
            deviceDetail.setDevice(device);
            deviceDetail.setOrgId(orgId);
            deviceDetailRepository.save(deviceDetail);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public DeviceDetail addDeviceDetail(String token, String devDetId, DeviceDetailDto deviceDetailDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            DeviceDetail device = deviceDetailRepository.findById(devDetId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Device Detail not Found"));
            if (Objects.equals(user.getEntityKey(), device.getOrgId())) {
                device = requestToEntity(device, deviceDetailDto);
                return deviceDetailRepository.save(device);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device is not from the requested Organisation");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private DeviceDetail requestToEntity(DeviceDetail entity, DeviceDetailDto request) {
        entity.setDriLicNo(request.getDriLicNo());
        entity.setDriName(request.getDriName());
        entity.setDriPhone(request.getDriPhone());
        entity.setMillage(request.getMillage());
        entity.setVehicleNo(request.getVehicleNo());
        entity.setVehicleType(request.getVehicleType());
        entity.setMaxSpeed(request.getMaxSpeed());
        return entity;
    }

    @Transactional
    public DeviceRequest deviceRequest(String token, String requestId, DeviceRequestDto deviceRequestDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            if (Util.isNullOrEmpty(requestId)) {
                DeviceRequest deviceRequest = requestToEntity(null, deviceRequestDto);
                deviceRequest.setOrgId(user.getEntityKey());
                return deviceRequestRepository.save(deviceRequest);
            } else {
                DeviceRequest deviceRequest = deviceRequestRepository.findById(requestId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Request not Found"));
                if (Objects.equals(user.getEntityKey(), deviceRequest.getOrgId())) {
                    deviceRequest = requestToEntity(deviceRequest, deviceRequestDto);
                    return deviceRequestRepository.save(deviceRequest);
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The given request is from the other organisation");
                }
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private DeviceComplaint requestToEntity(DeviceComplaint entity, DeviceComplaintDto request) {
        if (Util.isNullOrEmpty(entity))
            entity = new DeviceComplaint();

        entity.setDescription(request.getDescription());
        entity.setEmailId(request.getEmailId());
        entity.setSubject(request.getSubject());
        entity.setStatus(DeviceStatus.REQUESTED);
        return entity;
    }


    @Transactional
    public DeviceComplaint deviceComplaint(String token, String devDetId, String complaintId, DeviceComplaintDto deviceComplaintDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            if (Util.isNullOrEmpty(complaintId) && !Util.isNullOrEmpty(devDetId)) {
                DeviceDetail deviceDetail = deviceDetailRepository.findById(devDetId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Device Detail not Found"));
                if (deviceDetail.getOrgId().equals(user.getEntityKey())) {
                    DeviceComplaint deviceComplaint = requestToEntity(null, deviceComplaintDto);
                    deviceComplaint.setOrgId(user.getEntityKey());
                    deviceComplaint.setDevice(deviceDetail.getDevice());
                    return deviceComplaintRepository.save(deviceComplaint);
                } else {
                    throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The request device is not assigned to requested organisation");
                }
            } else {
                DeviceComplaint deviceComplaint = deviceComplaintRepository.findById(complaintId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Request not Found"));
                if (Objects.equals(user.getEntityKey(), deviceComplaint.getOrgId())) {
                    deviceComplaint = requestToEntity(deviceComplaint, deviceComplaintDto);
                    return deviceComplaintRepository.save(deviceComplaint);
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The given request is from the other organisation");
                }
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private DeviceRequest requestToEntity(DeviceRequest entity, DeviceRequestDto request) {
        if (Util.isNullOrEmpty(entity))
            entity = new DeviceRequest();

        entity.setDeviceCount(request.getDeviceCount());
        entity.setComment(request.getComment());
        entity.setRequiredBy(request.getRequiredBy());
        entity.setQuotationRequired(request.getQuotationRequired());
        return entity;
    }


    @Transactional
    public DeviceComplaint cancelDeviceComplaint(String token, String complaintId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            DeviceComplaint deviceComplaint = deviceComplaintRepository.findById(complaintId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Request not Found"));
            if (Objects.equals(user.getEntityKey(), deviceComplaint.getOrgId())) {
                deviceComplaint.setStatus(DeviceStatus.CANCELLED);
                return deviceComplaintRepository.save(deviceComplaint);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The given request is from the other organisation");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public List<DeviceComplaint> getAllDevCompByOrg(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            return deviceComplaintRepository.findAllByOrgId(user.getEntityKey());
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public List<DeviceComplaint> getAllDevCompByDevId(String token, String devDetId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            DeviceDetail deviceDetail = deviceDetailRepository.findById(devDetId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Device not found"));
            if (deviceDetail.getOrgId().equals(user.getEntityKey()))
                return deviceComplaintRepository.findAllByDevice(deviceDetail.getDevice());
            else
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested device is not from the requested organization");
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public DeviceComplaint getDevCompByCompId(String token, String complaintId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            DeviceComplaint deviceComplaint = deviceComplaintRepository.findById(complaintId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Complaint Not found"));
            if (deviceComplaint.getOrgId().equals(user.getEntityKey()))
                return deviceComplaint;
            else
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested complaint is not from the requested organization");
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    @Transactional
    public List<DeviceRequest> getAllDevReqByOrg(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            return deviceRequestRepository.findAllByOrgId(user.getEntityKey());
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    @Transactional
    public DeviceRequest getDevReqByReqId(String token, String requestId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            DeviceRequest deviceRequest = deviceRequestRepository.findById(requestId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Device Request Not found"));
            if (deviceRequest.getOrgId().equals(user.getEntityKey()))
                return deviceRequest;
            else
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The Device Request is not from the requested organization");
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    @Transactional
    public DeviceRequest cancelDeviceRequest(String token, String requestId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            DeviceRequest deviceRequest = deviceRequestRepository.findById(requestId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Request not Found"));
            if (Objects.equals(user.getEntityKey(), deviceRequest.getOrgId())) {
                deviceRequest.setStatus(DeviceStatus.CANCELLED);
                return deviceRequestRepository.save(deviceRequest);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The given request is from the other organisation");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


}
