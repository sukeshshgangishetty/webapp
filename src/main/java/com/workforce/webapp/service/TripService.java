package com.workforce.webapp.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.workforce.webapp.enums.trip.PointType;
import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.mapper.TripMapper;
import com.workforce.webapp.model.*;
import com.workforce.webapp.model.client.ClientEntity;
import com.workforce.webapp.model.trip.*;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.device.DeviceRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.repository.client.ClientNewRepository;
import com.workforce.webapp.repository.trip.*;
import com.workforce.webapp.utils.RestUtils;
import com.workforce.webapp.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.*;

@Component
public class TripService {

    private @Autowired
    TripRepository tripRepository;

    private @Autowired
    ExecutiveRepository executiveRepository;

    private @Autowired
    TripPointRepository tripPointRepository;

    private @Autowired
    PointRepository pointRepository;

    private @Autowired
    DeviceRepository deviceRepository;

    private @Autowired
    ScheduleVisitRepository scheduleVisitRepository;

    private @Autowired
    ScheduleRepository scheduleRepository;

    private @Autowired
    ScheduleAllocationRepository scheduleAllocationRepository;

    private @Autowired
    UserDetailsRepository userRepository;

    private @Autowired
    ClientNewRepository clientRepository;

    @Transactional
    public ResponseEntity getAllTrips(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                return ResponseEntity.ok(mapTripResponseList(tripRepository.findAll()));
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return ResponseEntity.ok(mapTripResponseList(tripRepository.findAllByCreatedBy(token)));
            } else {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private TripResponseList mapTripResponseList(List<Trip> trips) {
        TripResponseList tripResponseList = new TripResponseList();
        List<TripResponse> tripResponses = new ArrayList<>();
        for (Trip trip : trips) {
            TripResponse tripResponse = new TripResponse();
            tripResponse.setId(trip.getId());
            tripResponse.setTitle(trip.getTitle());
            tripResponse.setPriorityType(trip.getPriorityType());
            tripResponse.setCount(trip.getCount());
            tripResponse.setTotSchedules(scheduleRepository.countAllByTripId(trip.getId()));
            tripResponse.setComSchedules(scheduleRepository.countAllByTripIdAndStatus(trip.getId(), TripStatus.COMPLETED));
            tripResponse.setUpcomingSchedule(scheduleRepository.findTopByTripIdAndStatusAndExpStrTymAfter(trip.getId(), TripStatus.ALLOCATED, new Date().getTime()));
            tripResponse.setPeriod(trip.getPeriod());
            tripResponse.setStatus(trip.getStatus());
            tripResponse.setDescription(trip.getDescription());
            List<Long> ids = scheduleRepository.getExeIdsFromAlloc(null, trip.getId());
            List<Long> listWithoutDuplicates = Lists.newArrayList(Sets.newHashSet(ids));
            tripResponse.setExecutiveResponses(mapExecutiveResponse(listWithoutDuplicates));
            tripResponses.add(tripResponse);
        }
        tripResponseList.setTripResponses(tripResponses);
        return tripResponseList;
    }

    @Transactional
    public TripResponse getTripById(String token, String tripId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            Trip trip = tripRepository.findById(tripId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "TripNotFound"));
            List<Schedule> schedules = scheduleRepository.findAllByTripId(tripId);
            List<Long> exeIds = new ArrayList<>();
            List<ScheduleResponse> scheduleResponses = new ArrayList<>();
            if (!schedules.isEmpty()) {
                for (Schedule schedule : schedules) {
                    List<Long> ids = scheduleRepository.getExeIdsFromAlloc(schedule.getId(), null);
                    scheduleResponses.add(mapScheduleResponse(schedule, null));
                    if (ids.size() > 0) {
                        exeIds.addAll(ids);
                    }
                }
            }
            List<Long> listWithoutDuplicates = Lists.newArrayList(Sets.newHashSet(exeIds));
            return mapTripResponse(trip, scheduleResponses, listWithoutDuplicates);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private ScheduleResponse mapScheduleResponse(Schedule schedule, ScheduleAllocation scheduleAllocation) {
        ScheduleResponse scheduleResponse = new ScheduleResponse();
        if (!Util.isNullOrEmpty(scheduleAllocation)) {
            scheduleResponse.setStatus(scheduleAllocation.getStatus());
        } else {
            scheduleResponse.setStatus(schedule.getStatus());
        }
        scheduleResponse.setTotAlloc(schedule.getTotAlloc());
        scheduleResponse.setId(schedule.getId());
        scheduleResponse.setName(schedule.getName());
        scheduleResponse.setTotVisits(schedule.getTrip().getTripPoints().size());
        scheduleResponse.setDescription(schedule.getTrip().getDescription());
        scheduleResponse.setExpStrTym(schedule.getExpStrTym());
        scheduleResponse.setExpEndTym(schedule.getExpEndTym());
        scheduleResponse.setAssignedType(schedule.getAssignedType());
        if (!Util.isNullOrEmpty(scheduleAllocation)) {
            scheduleResponse.setExeAllocId(scheduleAllocation.getId());
            Map<TripStatus, Integer> scheduleVisitStats = scheduleRepository.getScheduleVisitStats(scheduleAllocation.getId());
            scheduleResponse.setComVisits(scheduleVisitStats.get(TripStatus.COMPLETED));
            scheduleResponse.setActStrTym(scheduleAllocation.getActStrTym());
            scheduleResponse.setActEndTym(scheduleAllocation.getActEndTym());
        }
        return scheduleResponse;
    }

    private TripResponse mapTripResponse(Trip trip, List<ScheduleResponse> scheduleResponses, List<Long> exeIds) {
        TripResponse tripResponse = new TripResponse();
        tripResponse.setTitle(trip.getTitle());
        tripResponse.setTotVisits(trip.getTripPoints().size());
        tripResponse.setDescription(trip.getDescription());
        tripResponse.setId(trip.getId());
        tripResponse.setCount(trip.getCount());
        tripResponse.setCreatedBy(trip.getCreatedBy());
        tripResponse.setCreatedAt(trip.getCreatedDate());
        tripResponse.setPriorityType(trip.getPriorityType());
        tripResponse.setPeriod(trip.getPeriod());
        tripResponse.setSchedules(scheduleResponses);
        tripResponse.setTripPoints(trip.getTripPoints());
        tripResponse.setExecutiveResponses(mapExecutiveResponse(exeIds));
        return tripResponse;
    }

    private List<ExecutiveResponse> mapExecutiveResponse(List<Long> exeIds) {
        List<ExecutiveResponse> executiveResponses = new ArrayList<>();
        for (Long id : exeIds) {
            Executive executive = executiveRepository.getOne(id);
            ExecutiveResponse executiveResponse = new ExecutiveResponse();
            executiveResponse.setExeId(executive.getExeId());
            executiveResponse.setAddress(executive.getAddress());
            executiveResponse.setName(executive.getName());
            executiveResponse.setPhone(executive.getName());
            executiveResponses.add(executiveResponse);
        }
        return executiveResponses;
    }

    @Transactional
    public ResponseEntity getScheduleAllocByOrg(String token, String scheduleId, Long exeId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            if (!Util.isNullOrEmpty(exeId)) {
                return ResponseEntity.ok(getScheduleAlloc(exeId));
            } else {
                List<ScheduleAllocation> scheduleAllocations = scheduleAllocationRepository.findAllByScheduleId(scheduleId);
                if (!scheduleAllocations.isEmpty()) {
                    return ResponseEntity.ok(mapScheduleAllocResponse(scheduleAllocations));
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There is no Allocation for the particular schedule");
                }
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public List<ScheduleResponse> getScheduleAllocByExc(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.USER_EXECUTIVE)) {
            return getScheduleAlloc(user.getEntityKey());
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private List<ScheduleResponse> getScheduleAlloc(Long exeId) {
        List<ScheduleAllocation> scheduleAllocations = scheduleAllocationRepository.findAllByAssignedTo(exeId);
        if (!scheduleAllocations.isEmpty()) {
            List<ScheduleResponse> scheduleResponses = new ArrayList<>();
            for (ScheduleAllocation scheduleAllocation : scheduleAllocations)
                scheduleResponses.add(mapScheduleResponse(scheduleAllocation.getSchedule(), scheduleAllocation));
            return scheduleResponses;
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "There is no Allocation for the particular executive");
        }
    }


    private ScheduleAllocResponse mapScheduleAllocResponse(List<ScheduleAllocation> scheduleAllocations) {
        ScheduleAllocResponse scheduleAllocResponse = new ScheduleAllocResponse();
        List<ExecutiveResponse> executiveResponses = new ArrayList<>();
        String name = null;
        String schAllocId = null;
        Long totTym = null;
        Double totDis = null;
        for (ScheduleAllocation scheduleAllocation : scheduleAllocations) {
            Executive executive = executiveRepository.getOne(scheduleAllocation.getAssignedTo());
            ExecutiveResponse executiveResponse = new ExecutiveResponse();
            executiveResponse.setName(executive.getName());
            executiveResponse.setExeId(executive.getExeId());
            executiveResponse.setPhone(executive.getPhone());
            executiveResponse.setSchAllocId(scheduleAllocation.getId());
            executiveResponses.add(executiveResponse);
            name = executive.getName();
            schAllocId = scheduleAllocation.getId();
            if (!Util.isNullOrEmpty(scheduleAllocation.getTotTym()))
                totTym = scheduleAllocation.getTotTym();
            totDis = scheduleAllocation.getTotDis();
        }
        scheduleAllocResponse.setTotTym(totTym);
        scheduleAllocResponse.setTotDis(totDis);
        scheduleAllocResponse.setSchAllocId(schAllocId);
        scheduleAllocResponse.setExecutives(executiveResponses);
        scheduleAllocResponse.setName(name);

        List<ScheduleVisit> scheduleVisits = scheduleVisitRepository.findAllByScheduleAllocationId(schAllocId);

        scheduleAllocResponse.setScheduleVisits(mapScheduleVisitResponse(scheduleVisits));
        Map<TripStatus, Integer> scheduleVisitStats = scheduleRepository.getScheduleVisitStats(schAllocId);
        scheduleAllocResponse.setComVisits(scheduleVisitStats.get(TripStatus.COMPLETED));
        return scheduleAllocResponse;
    }

    @Transactional
    public List<ScheduleResponse> getAllocByExeIdAndTripId(String token, String tripId, Long exeId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                List<ScheduleResponse> scheduleResponses = new ArrayList<>();
                List<ScheduleAllocation> scheduleAllocations = scheduleRepository.getAllocByExeIdAndTripId(exeId, tripId);
                for (ScheduleAllocation scheduleAllocation : scheduleAllocations) {
                    scheduleResponses.add(mapScheduleResponse(scheduleAllocation.getSchedule(), scheduleAllocation));
                }
                return scheduleResponses;
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User dont have the right to get Trip Details");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public ResponseEntity getScheduleVisit(String token, String scheduleAllocId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            return ResponseEntity.ok(mapScheduleVisitResponse(scheduleVisitRepository.findAllByScheduleAllocationId(scheduleAllocId)));
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private List<ScheduleVisitResponse> mapScheduleVisitResponse(List<ScheduleVisit> scheduleVisits) {
        List<ScheduleVisitResponse> scheduleVisitResponses = new ArrayList<>();
        if (!scheduleVisits.isEmpty()) {
            for (ScheduleVisit scheduleVisit : scheduleVisits) {
                ScheduleVisitResponse scheduleVisitResponse = new ScheduleVisitResponse();
                if (!Util.isNullOrEmpty(scheduleVisit.getCheckIn()))
                    scheduleVisitResponse.setCheckIn(scheduleVisit.getCheckIn());

                if (!Util.isNullOrEmpty(scheduleVisit.getCheckOut()))
                    scheduleVisitResponse.setCheckOut(scheduleVisit.getCheckOut());

                if (!Util.isNullOrEmpty(scheduleVisit.getTotTym()))
                    scheduleVisitResponse.setTotTym(scheduleVisit.getTotTym());


                scheduleVisitResponse.setTripPoint(getTripPoint(scheduleVisit.getTripPoint()));
                scheduleVisitResponse.setComment(scheduleVisit.getComment());
                scheduleVisitResponse.setDistance(scheduleVisit.getDistance());
                scheduleVisitResponse.setSequence(scheduleVisit.getSequence());
                scheduleVisitResponse.setLocation(scheduleVisit.getLocation());
                scheduleVisitResponse.setStatus(scheduleVisit.getStatus());
                scheduleVisitResponse.setId(scheduleVisit.getId());
                scheduleVisitResponses.add(scheduleVisitResponse);
            }
        }
        return scheduleVisitResponses;
    }

    private TripPointDto getTripPoint(String tripPointJson) {
        Gson gson = new Gson();
        TripPoint tripPoint = gson.fromJson(tripPointJson, TripPoint.class);
        return entityToResponse(tripPoint);
    }


    @Transactional
    public Trip createTrip(String token, Trip trip, TripDto tripDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return tripRepository.save(setTrip(token, trip, tripDto));
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User dont have to right to make trip");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private Trip setTrip(String token, Trip trip, TripDto tripDto) {
        trip = TripMapper.requestToEntity(trip, tripDto);
        trip.setTripPoints(addTripPoints(trip, tripDto));
        trip.setStatus(TripStatus.CREATED);
        trip.setCreatedBy(token);
        return trip;
    }

    private List<TripPoint> addTripPoints(Trip trip, TripDto tripDto) {
        List<TripPoint> tripPoints = new ArrayList<>();
        if (tripDto.getTripPoints().size() > 1) {
            for (TripPointDto tripPointDto : tripDto.getTripPoints()) {
                TripPoint tripPoint = setTripPoint(trip, tripPointDto);
                if (!tripPoints.stream().anyMatch(e -> e.getPoint().getId().contains(tripPoint.getPoint().getId()))) {
                    tripPoints.add(tripPoint);
                }
            }
            trip.getTripPoints().clear();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Points should be more than one");
        }
        return tripPoints;
    }

    private TripPoint setTripPoint(Trip trip, TripPointDto tripPointDto) {
        Point point = getPoint(tripPointDto.getPoint());
        if (!Util.isNullOrEmpty(trip.getTripPoints()) && trip.getTripPoints().size() > 1 && !Util.isNullOrEmpty(tripPointDto.getTripPointId())) {
            for (TripPoint tripPoint : trip.getTripPoints()) {
                if (tripPoint.getId().equals(tripPointDto.getTripPointId())) {
                    return mapTripPoint(trip, tripPoint, tripPointDto, point);
                }
            }
        } else {
            return mapTripPoint(trip, null, tripPointDto, point);
        }
        return null;
    }

    private TripPoint mapTripPoint(Trip trip, TripPoint tripPoint, TripPointDto tripPointDto, Point point) {
        if (Util.isNullOrEmpty(tripPoint))
            tripPoint = new TripPoint();

        tripPoint.setTrip(trip);
        tripPoint.setPurpose(tripPointDto.getPurpose());
        tripPoint.setPoint(point);
        tripPoint.setExpTym(tripPointDto.getExpTym());
        tripPoint.setSequence(tripPointDto.getSequence());
        return tripPoint;
    }

    /*private void setExpTym(TripPoint tripPoint,TripPointDto tripPointDto){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date date = null;
        try {
            date = format.parse(tripPointDto.getExpTym());
            Long tym = date.getTime();
            tripPoint.setExpTym(Util.timestampToDate(tym));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }*/

    private Point getPoint(PointDto pointDto) {
        Point point = null;
        if (Util.isNullOrEmpty(pointDto.getId())) {
            if (pointDto.getType().equals(PointType.CLIENT)) {
                ClientEntity client = clientRepository.findById(pointDto.getClientId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "There is no Client with the given Client Id"));
                point = pointRepository.findByClientId(pointDto.getClientId());
            } else {
                point = pointRepository.findByNameAndType(pointDto.getName(), PointType.VISIT);
            }
        } else {
            point = pointRepository.getOne(pointDto.getId());
        }
        if (Util.isNullOrEmpty(point)) {
            point = TripMapper.requestToEntity(null, pointDto);
            return pointRepository.save(point);
        } else {
            return point;
        }
    }


    @Transactional
    public Trip updateTrip(String token, String tripId, TripDto tripDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            Trip entity = tripRepository.findById(tripId)
                    .orElseThrow(() ->
                            new ResponseStatusException(HttpStatus.BAD_REQUEST, "Trip not Found"));
            if (entity.getCreatedBy().equals(token) || user.getRole().equals(Userrole.ORGANIZATION)) {
                //entity.getTripPoints().clear();
                return createTrip(token, entity, tripDto);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User dont have the right to update the trip");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public List<Point> getAllPoints() {
        List<Point> entities = pointRepository.findAll();
        return entities;
    }


    @Transactional
    public ScheduleDto createTripSchedule(String token, Schedule schedule, ScheduleDto scheduleDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            return validateTrip(user, token, schedule, scheduleDto);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    public ScheduleDto validateTrip(UserDetails user, String token, Schedule schedule, ScheduleDto scheduleDto) {
        Optional<Trip> trip = tripRepository.findById(scheduleDto.getTripId());
        if (trip.isPresent()) {
            Schedule entity = validateUserAndRole(user, token, schedule, scheduleDto, trip.get());
            scheduleDto.setId(entity.getId());
            return scheduleDtoResponse(scheduleDto, trip.get());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There is no given trip");
        }
    }

    private ScheduleDto scheduleDtoResponse(ScheduleDto scheduleDto, Trip trip) {
        if (Util.isNullOrEmpty(scheduleDto.getTripPoints()) && !scheduleDto.getChangeTripSeq()) {
            List<TripPointDto> tripPointDtos = new ArrayList<>();
            for (TripPoint tripPoint : trip.getTripPoints()) {
                tripPointDtos.add(entityToResponse(tripPoint));
            }
            scheduleDto.setTripPoints(tripPointDtos);
            return scheduleDto;
        } else {
            return scheduleDto;
        }
    }

    private TripPointDto entityToResponse(TripPoint tripPoint) {
        TripPointDto tripPointDto = new TripPointDto();
        Point point = tripPoint.getPoint();
        tripPointDto.setTripPointId(tripPoint.getId());
        tripPointDto.setPurpose(tripPoint.getPurpose());
        tripPointDto.setSequence(tripPoint.getSequence());
        tripPointDto.setExpTym(tripPoint.getExpTym());
        PointDto pointDto = new PointDto();
        pointDto.setId(point.getId());
        pointDto.setName(point.getName());
        pointDto.setClientId(point.getClientId());
        pointDto.setType(point.getType());
        pointDto.setLocCurrent(point.getLocCurrent());
        tripPointDto.setPoint(pointDto);
        return tripPointDto;
    }

    public Schedule validateUserAndRole(UserDetails user, String token, Schedule schedule, ScheduleDto scheduleDto, Trip trip) {
        if (user.getRole().equals(Userrole.ORGANIZATION)) {
            return scheduleRepository.save(setSchedule(schedule, scheduleDto, trip));
        } else if (user.getRole().equals(Userrole.USER_EXECUTIVE) && trip.getCreatedBy().equals(token)) {
            if (scheduleDto.getAssignedTo().size() == 1 && scheduleDto.getAssignedTo().get(0).equals(user.getEntityKey())) {
                return scheduleRepository.save(setSchedule(schedule, scheduleDto, trip));
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User dont have the access to create the trip schedule for another");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User dont have the access to create the  trip schedule");
        }
    }

    public Schedule setSchedule(Schedule schedule, ScheduleDto scheduleDto, Trip trip) {
        schedule = TripMapper.requestToEntity(schedule, scheduleDto);
        trip.setStatus(TripStatus.ALLOCATED);
        schedule.setStatus(TripStatus.ALLOCATED);
        schedule.setName(scheduleDto.getName());
        schedule.setTrip(trip);
        schedule.setAssignedType(scheduleDto.getAssignedType());
        schedule.setTotAlloc(scheduleDto.getAssignedTo().size());
        schedule.setScheduleAllocations(setScheduleAllocations(schedule, scheduleDto, trip));
        return schedule;
    }


    private List<ScheduleAllocation> setScheduleAllocations(Schedule schedule, ScheduleDto scheduleDto, Trip trip) {
        List<ScheduleAllocation> scheduleAllocations = new ArrayList<>();
        List<ScheduleVisit> visits = getScheduleVisit(scheduleDto, trip);

        for (Long id : scheduleDto.getAssignedTo()) {
            if (executiveValidation(id) /*|| deviceValidation(id)*/) {
                ScheduleAllocation scheduleAllocation = mapScheduleAlloc(schedule, scheduleDto, visits, id);
                scheduleAllocations.add(scheduleAllocation);
            }
        }
        return scheduleAllocations;
    }

    private List<ScheduleVisit> getScheduleVisit(ScheduleDto scheduleDto, Trip trip) {
        if (!trip.getTripPoints().isEmpty()) {
            return getVisits(scheduleDto, trip.getTripPoints());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There is no trip points for the given trip");
        }
    }

    private ScheduleAllocation mapScheduleAlloc(Schedule schedule, ScheduleDto scheduleDto, List<ScheduleVisit> visits, Long id) {
        ScheduleAllocation scheduleAllocation = new ScheduleAllocation();
        scheduleAllocation.setSchedule(schedule);
        scheduleAllocation.setAssignedTo(id);
        //scheduleAllocation.setAssignedType(scheduleDto.getAssignedType());
        scheduleAllocation.setScheduleVisits(visits);
        scheduleAllocation.setStatus(TripStatus.ALLOCATED);
        scheduleAllocation.setTotVisit(visits.size());
        for (ScheduleVisit visit : visits) {
            visit.setScheduleAllocation(scheduleAllocation);
        }
        return scheduleAllocation;
    }


    private List<ScheduleVisit> getVisits(ScheduleDto scheduleDto, List<TripPoint> tripPoints) {
        List<ScheduleVisit> scheduleVisits = new ArrayList<>();
        for (TripPoint tripPoint : tripPoints) {
            if (!Util.isNullOrEmpty(scheduleDto.getTripPoints()) && scheduleDto.getTripPoints().size() > 0 && scheduleDto.getChangeTripSeq()) {
                for (TripPointDto tripPointDto : scheduleDto.getTripPoints()) {
                    if (tripPoint.getId().equals(tripPointDto.getTripPointId())) {
                        scheduleVisits.add(map(tripPoint, tripPointDto));
                        changeTripSequence(scheduleDto, tripPoint, tripPointDto);
                    }
                }
            } else {
                scheduleVisits.add(map(tripPoint, null));
            }
        }
        return scheduleVisits;
    }

    private void changeTripSequence(ScheduleDto scheduleDto, TripPoint tripPoint, TripPointDto tripPointDto) {
        if (scheduleDto.getChangeTripSeq()) {
            if (tripPoint.getSequence() != tripPointDto.getSequence()) {
                tripPoint.setSequence(tripPointDto.getSequence());
                tripPointRepository.save(tripPoint);
            }
        }
    }

    private ScheduleVisit map(TripPoint tripPoint, TripPointDto tripPointDto) {
        ScheduleVisit scheduleVisit = new ScheduleVisit();
        scheduleVisit.setTripPoint(getTripPointJson(tripPoint));
        scheduleVisit.setStatus(TripStatus.ALLOCATED);
        scheduleVisit.setLocation(new Location());
        if (!Util.isNullOrEmpty(tripPointDto)) {
            scheduleVisit.setSequence(tripPointDto.getSequence());
        } else {
            scheduleVisit.setSequence(tripPoint.getSequence());
        }
        return scheduleVisit;
    }

    private String getTripPointJson(TripPoint tripPoint) {
        TripPoint entity = new TripPoint();
        entity.setPoint(tripPoint.getPoint());
        entity.setId(tripPoint.getId());
        entity.setSequence(tripPoint.getSequence());
        entity.setPurpose(tripPoint.getPurpose());
        Gson gson = new Gson();
        return gson.toJson(entity);
    }

    private Boolean executiveValidation(Long id) {
        Executive executive = executiveRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
        return true;
    }

    private Boolean deviceValidation(Long id) {
        ///  Device device = deviceRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Device Not Found"));;
        return true;
    }

    private Trip loadTrip(String tripId) {
        Trip trip = new Trip();
        trip.setId(tripId);
        return trip;
    }

    @Transactional
    public void scheduleTripStatus(String token, String scheduleAllocId, TripStatus status) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            ScheduleAllocation scheduleAllocation = scheduleAllocationRepository.findById(scheduleAllocId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Schedule Allocation not Found"));
            validateScheduleTrip(user, scheduleAllocation, status);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private void validateScheduleTrip(UserDetails user, ScheduleAllocation scheduleAllocation, TripStatus status) {
        if (user.getRole().equals(Userrole.USER_EXECUTIVE) && scheduleAllocation.getAssignedTo().equals(user.getEntityKey())) {
            if (status.equals(TripStatus.REJECTED)) {
                scheduleAllocation.getScheduleVisits().clear();
                scheduleAllocation.setStatus(status);
                scheduleStatusUpdate(scheduleAllocation);
            } else if (status.equals(TripStatus.IN_PROGRESS)) {
                scheduleAllocation.setStatus(status);
                scheduleAllocation.setActStrTym(new Date().getTime());
                Schedule schedule = scheduleAllocation.getSchedule();
                schedule.setStatus(TripStatus.IN_PROGRESS);
                scheduleRepository.save(schedule);
            } else if (status.equals(TripStatus.ACCEPTED)) {
                scheduleAllocation.setStatus(status);
            } else if (status.equals(TripStatus.COMPLETED)) {
                scheduleAllocStatusUpdate(scheduleAllocation);
                //scheduleAllocation.setActEndTym(new Date());
                //scheduleAllocation.setStatus(status);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Status");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Trip is not allocated to the requested user");
        }
    }

    @Transactional
    public void scheduleVisitStatus(String token, String scheduleVisitId, TripStatus status, Location location) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            ScheduleVisit scheduleVisit = scheduleVisitRepository.findById(scheduleVisitId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Schedule visit Not Found"));
            if (scheduleVisit.getScheduleAllocation().getStatus().equals(TripStatus.IN_PROGRESS)) {
                scheduleVisitStatusUpdate(user, scheduleVisit, status, location);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Schedule is not in progress");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private void scheduleVisitStatusUpdate(UserDetails user, ScheduleVisit scheduleVisit, TripStatus status, Location location) {
        if (scheduleVisit.getScheduleAllocation().getAssignedTo().equals(user.getEntityKey())) {
            scheduleVisit.setLocation(location);
            if (status.equals(TripStatus.CHECK_IN)) {
                scheduleVisit.setCheckIn(new Date().getTime());
                scheduleVisit.setStatus(TripStatus.IN_PROGRESS);
            } else if (status.equals(TripStatus.CHECK_OUT)) {
                if (!Util.isNullOrEmpty(scheduleVisit.getCheckIn())) {
                    scheduleVisit.setCheckOut(new Date().getTime());
                    scheduleVisit.setStatus(TripStatus.COMPLETED);
                    Long totTym = new Date().getTime() - scheduleVisit.getCheckIn();
                    scheduleVisit.setTotTym(totTym);
                    if (scheduleVisit.getSequence() == 0) {
                        scheduleVisit.setDistance(0.0);
                    } else {
                        scheduleVisit.setDistance(setDistance(scheduleVisit, location));
                    }
                    // scheduleAllocStatusUpdate(scheduleVisit);
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Without CheckIn You can't be checkOut");
                }
            } else if (status.equals(TripStatus.CANCELLED)) {
                scheduleVisit.setStatus(TripStatus.CANCELLED);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Status");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Scheduled Visit is not allocated to the requested user");
        }
    }

    public Double setDistance(ScheduleVisit scheduleVisit, Location location) {
        ScheduleVisit entity = scheduleVisitRepository.findBySequenceAndScheduleAllocationId(scheduleVisit.getSequence() - 1, scheduleVisit.getScheduleAllocation().getId());
        if (entity.getStatus().equals(TripStatus.CANCELLED)) {
            if (entity.getSequence() == 0) {
                return 0.0;
            } else {
                setDistance(entity, location);
            }
        } else {
            return RestUtils.distance(entity.getLocation().getLat(), location.getLat(), entity.getLocation().getLng(), location.getLng());
        }
        return 0.0;
    }

    private void scheduleAllocStatusUpdate(ScheduleAllocation scheduleAllocation) {
        Map<TripStatus, Integer> scheduleVisitStats = scheduleRepository.getScheduleVisitStats(scheduleAllocation.getId());
        //ScheduleAllocation scheduleAllocation = scheduleVisit.getScheduleAllocation();
        if (scheduleVisitStats.containsKey(TripStatus.COMPLETED) && scheduleVisitStats.get(TripStatus.COMPLETED).equals(scheduleAllocation.getTotVisit())) {
            scheduleAllocation.setStatus(TripStatus.COMPLETED);
            //scheduleAllocation.setTotDis(scheduleVisit.getDistance());
            scheduleAllocation.setComVisit(scheduleVisitStats.get(TripStatus.COMPLETED));
            scheduleAllocation.setActEndTym(new Date().getTime());
            Long totTym = scheduleAllocation.getActEndTym() - scheduleAllocation.getActStrTym();
            scheduleAllocation.setTotTym(totTym);
            scheduleAllocationRepository.save(scheduleAllocation);
            scheduleStatusUpdate(scheduleAllocation);
        } else {
            if (scheduleVisitStats.containsKey(TripStatus.COMPLETED))
                scheduleAllocation.setComVisit(scheduleVisitStats.get(TripStatus.COMPLETED));
            else
                scheduleAllocation.setComVisit(0);
        }
    }

    private void scheduleStatusUpdate(ScheduleAllocation scheduleAllocation) {
        Map<TripStatus, Integer> scheduleAllocStatus = scheduleRepository.getScheduleAllocStats(scheduleAllocation.getSchedule().getId(), null);
        Schedule schedule = scheduleAllocation.getSchedule();
        if (scheduleAllocStatus.containsKey(TripStatus.COMPLETED) && scheduleAllocStatus.get(TripStatus.COMPLETED).equals(scheduleAllocation.getSchedule().getTotAlloc())) {
            schedule.setStatus(TripStatus.COMPLETED);
        } else if (scheduleAllocStatus.containsKey(TripStatus.REJECTED) && scheduleAllocStatus.get(TripStatus.REJECTED).equals(scheduleAllocation.getSchedule().getTotAlloc())) {
            schedule.setStatus(TripStatus.REJECTED);
        }
        schedule.setComAlloc(scheduleAllocStatus.get(TripStatus.COMPLETED));
        scheduleRepository.save(schedule);
    }


    @Transactional
    public ScheduleDto updateTripSchedule(String token, String scheduleId, ScheduleDto scheduleDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            Schedule schedule = scheduleRepository.findById(scheduleId)
                    .orElseThrow(() ->
                            new ResponseStatusException(HttpStatus.BAD_REQUEST, "Schedule not Found"));

            if (schedule.getCreatedBy().equals(token)) {
                if (new Date().getTime() >= schedule.getExpStrTym()) {
                    throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Trip Already Started, Can't Update the trip");
                } else {
                    schedule.getScheduleAllocations().clear();
                }
            }
            return createTripSchedule(token, schedule, scheduleDto);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public void delete(String scheduleId) {
        Optional<Schedule> tripSchedule = scheduleRepository.findById(scheduleId);
        if (tripSchedule.isPresent()) {
            scheduleRepository.delete(tripSchedule.get());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Trip Schedule Not Found");
        }
    }


    @Transactional
    public ResponseEntity getTripsStats(Long exeId) {
        Map<TripStatus, Integer> tripsStats = null;
        if (Util.isNullOrEmpty(exeId)) {
            tripsStats = scheduleRepository.getTripsStats();
            return ResponseEntity.ok(tripsStats);
        } else {
            tripsStats = scheduleRepository.getScheduleAllocStats(null, exeId);
            return ResponseEntity.ok(tripsStats);
        }
    }
}








