package com.workforce.webapp.service;

import com.google.gson.Gson;
import com.workforce.webapp.enums.client.RequestStatus;
import com.workforce.webapp.enums.client.RequestType;
import com.workforce.webapp.model.*;
import com.workforce.webapp.model.client.*;
import com.workforce.webapp.model.client.Address;
import com.workforce.webapp.model.trip.Schedule;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.repository.client.*;
import com.workforce.webapp.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Component
public class ClientService {

    private @Autowired
    UserDetailsRepository userRepository;

    private @Autowired
    ClientNewRepository clientRepository;

    private @Autowired
    ExecutiveRepository executiveRepository;

    private @Autowired
    ClientRequestRepository clientRequestRepository;

    private @Autowired
    ContactDetailRepository contactDetailRepository;

    private @Autowired
    KycDetailRepository kycDetailRepository;

    private @Autowired
    CategoryRepository categoryRepository;

    private @Autowired
    AddressRepository addressRepository;

    private @Autowired
    AccountDetailRepository accountDetailRepository;

    @Transactional
    public List<Category> getAllCategories(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                return categoryRepository.getAllByOrgId(user.getEntityKey());
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
                return categoryRepository.getAllByOrgId(executive.getOrg().getId());
            } else {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User Dont have the right to get categories");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public Category getCategoryById(String token, String categoryId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                return categoryRepository.getByIdAndOrgId(categoryId, user.getEntityKey());
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
                return categoryRepository.getByIdAndOrgId(categoryId, executive.getOrg().getId());
            } else {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User Dont have the right to get categories");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public ResponseEntity createCategory(String token, String categoryId, CategoryDto categoryDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                if (!Util.isNullOrEmpty(categoryId)) {
                    return ResponseEntity.ok(updateCategory(categoryId, categoryDto, user));
                } else {
                    return ResponseEntity.ok(createCategory(categoryDto, user));
                }
            }/*else if(user.getRole().equals(Userrole.USER_EXECUTIVE)){
                Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(()->new ResponseStatusException(HttpStatus.BAD_REQUEST,"Executive Not Found"));
                if(category.getOrgId().equals(executive.getOrg().getId())){
                    return ResponseEntity.ok(categoryRepository.save(category));
                }else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The executive organisation and the given organisation is not same");
                }
            }*/ else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User dont have to right to create client");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private Category updateCategory(String categoryId, CategoryDto categoryDto, UserDetails user) {
        Category entity = categoryRepository.findById(categoryId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Category not found"));
        if (entity.getOrgId().equals(user.getEntityKey())) {
            entity = mapCategory(entity, categoryDto);
            return categoryRepository.save(entity);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The category is not from the requested organisation");
        }
    }

    private Category createCategory(CategoryDto categoryDto, UserDetails user) {
        Category entity = mapCategory(null, categoryDto);
        entity.setOrgId(user.getEntityKey());
        return categoryRepository.save(entity);
    }

    private Category mapCategory(Category category, CategoryDto categoryDto) {
        if (Util.isNullOrEmpty(category)) {
            category = new Category();
        }
        category.setDescription(categoryDto.getDescription());
        category.setName(categoryDto.getName());
        return category;
    }


    @Transactional
    public void deleteCategory(String token, String categoryId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Category entity = categoryRepository.findById(categoryId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Category not found"));
                categoryRepository.delete(entity);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User dont have to right to create client");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public List<ClientResponse> getAllClient(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                return mapClientResponse(clientRepository.findAllByOrgId(user.getEntityKey()));
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
                return mapClientResponse(clientRepository.findAllByOrgId(executive.getOrg().getId()));
            } else {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User dont have the right to fetch the information");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public ClientEntity getClientById(String token, String clientId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                return clientRepository.findByIdAndOrgId(clientId, user.getEntityKey());
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
                return clientRepository.findByIdAndOrgId(clientId, executive.getOrg().getId());
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User dont have the right to fetch the information");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private List<ClientResponse> mapClientResponse(List<ClientEntity> clientEntities) {
        List<ClientResponse> responses = new ArrayList<>();
        if (clientEntities.size() > 0) {
            for (ClientEntity entity : clientEntities) {
                ClientResponse response = new ClientResponse();
                response.setLocation(entity.getLocation());
                response.setDescription(entity.getDescription());
                response.setAssignedTo(entity.getAssignedTo());
                response.setEmail(entity.getEmail());
                response.setId(entity.getId());
                response.setCreditLimit(entity.getCreditLimit());
                response.setGstNo(entity.getGstNo());
                if (!Util.isNullOrEmpty(entity.getIncorporatedAt()))
                    response.setIncorporatedAt(entity.getIncorporatedAt());
                response.setOrgId(entity.getOrgId());
                response.setName(entity.getName());
                response.setOutstanding(entity.getOutstanding());
                response.setPhoneNo(entity.getPhoneNo());
                response.setLandlineNo(entity.getLandlineNo());
                response.setIsActive(entity.getIsActive());
                for (OwnerDetail ownerDetail : entity.getOwnerDetails()) {
                    if (ownerDetail.getIsPrimary()) {
                        response.setOwnerDetail(ownerDetail);
                    }
                }
                responses.add(response);
            }
        }
        return responses;
    }


    @Transactional
    public ResponseEntity createClient(String token, ClientEntity client, ClientDto clientDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                return ResponseEntity.ok(validateClient(client, clientDto, user));
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return ResponseEntity.ok(createClientRequest(clientDto, user, null));
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User dont have to right to create client");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public ResponseEntity updateClient(String token, String clientId, ClientDto clientDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user)) {
            ClientEntity entity = clientRepository.findById(clientId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Client not Found"));
            if (user.getRole().equals(Userrole.ORGANIZATION) && entity.getOrgId().equals(user.getEntityKey())) {
                entity.getOwnerDetails().clear();
                return ResponseEntity.ok(validateClient(entity, clientDto, user));
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return ResponseEntity.ok(updateClientByExe(entity, clientDto, user));
            } else {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User dont have the right to update the client");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private ClientRequest updateClientByExe(ClientEntity entity, ClientDto clientDto, UserDetails user) {
        Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
        if (Objects.equals(executive.getOrg().getId(), entity.getOrgId())) {
            return createClientRequest(clientDto, user, entity);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The Executive which is requesting to update the given client are not from the given organisation");
        }
    }

    public ClientEntity validateClient(ClientEntity client, ClientDto clientDto, UserDetails user) {
        if (!Util.isNullOrEmpty(clientDto.getAssignedTo())) {
            Executive executive = executiveRepository.findById(clientDto.getAssignedTo()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
            if (Objects.equals(user.getEntityKey(), executive.getOrg().getId())) {
                return saveClient(client, clientDto, user);
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The assigning executive is not from the given organisation");
            }
        } else {
            return saveClient(client, clientDto, user);
        }
    }

    private ClientEntity saveClient(ClientEntity client, ClientDto clientDto, UserDetails user) {
        client = requestToEntity(client, clientDto);
        if (!Util.isNullOrEmpty(clientDto.getCategory()) && !Util.isNullOrEmpty(clientDto.getCategory().getId())) {
            Category category = categoryRepository.findById(clientDto.getCategory().getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "CategoryId not found"));
            client.setCategory(category);
        } else {
            client.setCategory(null);
        }
        client.setOrgId(user.getEntityKey());
        client.setOwnerDetails(setOwnerDetails(client, clientDto, Userrole.ORGANIZATION));
        return clientRepository.save(client);
    }

    public ClientRequest createClientRequest(ClientDto clientDto, UserDetails user, ClientEntity entity) {
        Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
        if (!Util.isNullOrEmpty(clientDto.getAssignedTo())) {
            Executive assignedTo = executiveRepository.findById(clientDto.getAssignedTo()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive to which you are assigning client is not Found"));
            if (Objects.equals(executive.getOrg().getId(), assignedTo.getOrg().getId())) {
                return mapClientRequest(entity, clientDto, executive, getJson(clientDto, executive));
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requesting executive or the assigning executive is not from the given organisation");
            }
        } else {
            return mapClientRequest(entity, clientDto, executive, getJson(clientDto, executive));
        }
    }

    private String getJson(ClientDto clientDto, Executive executive) {
        ClientEntity client = null;
        client = requestToEntity(client, clientDto);
        client.setOrgId(executive.getOrg().getId());
        client.setCategory(clientDto.getCategory());
        client.setOwnerDetails(setOwnerDetails(client, clientDto, Userrole.USER_EXECUTIVE));
        Gson gson = new Gson();
        return gson.toJson(client);
    }

    private ClientRequest mapClientRequest(ClientEntity clientEntity, ClientDto clientDto, Executive executive, String clientJson) {
        ClientRequest clientRequest = null;
        if (!Util.isNullOrEmpty(clientDto.getClientRequest())) {
            clientRequest = requestToEntity(null, clientDto.getClientRequest());
            clientRequest.setClientData(clientJson);
            clientRequest.setOrgId(executive.getOrg().getId());
            if (!Util.isNullOrEmpty(clientDto))
                clientRequest.setClientEntity(clientEntity);
            return clientRequestRepository.save(clientRequest);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ClientRequest is Empty");
        }
    }


    public Category load(String categoryId) {
        Category category = new Category();
        category.setId(categoryId);
        return category;
    }


    private List<OwnerDetail> setOwnerDetails(ClientEntity clientEntity, ClientDto clientDto, Userrole userrole) {
        List<OwnerDetail> ownerDetails = new ArrayList<>();
        if (clientDto.getOwnerDetails().size() > 0) {
            for (OwnerDetailDto ownerDetailDto : clientDto.getOwnerDetails()) {
                OwnerDetail ownerDetail = requestToEntity(null, ownerDetailDto);
                if (userrole.equals(Userrole.ORGANIZATION))
                    ownerDetail.setClientEntity(clientEntity);
                ownerDetails.add(ownerDetail);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Owner should not be empty");
        }
        return ownerDetails;
    }


    public ClientEntity loadClientEntity(String clientId) {
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setId(clientId);
        return clientEntity;
    }

    public ClientEntity requestToEntity(ClientEntity entity, ClientDto request) {
        if (Util.isNullOrEmpty(entity)) {
            entity = new ClientEntity();
        }
        entity.setDescription(request.getDescription());
        entity.setName(request.getName());
        entity.setPhoneNo(request.getPhoneNo());
        entity.setLandlineNo(request.getLandlineNo());
        entity.setEmail(request.getEmail());
        if (!Util.isNullOrEmpty(request.getLocation()))
            entity.setLocation(request.getLocation());
        entity.setIncorporatedAt(request.getIncorporatedAt());
        entity.setCreditLimit(request.getCreditLimit());
        entity.setOpeningBalance(request.getOpeningBalance());
        entity.setOutstanding(request.getOutstanding());
        entity.setNoOfEmp(request.getNoOfEmp());
        entity.setRepayPeriod(request.getRepayPeriod());
        entity.setGstNo(request.getGstNo());
        entity.setAssignedTo(request.getAssignedTo());
        entity.setIsActive(true);
        return entity;
    }

    public ClientRequest requestToEntity(ClientRequest entity, ClientRequestDto request) {
        if (Util.isNullOrEmpty(entity)) {
            entity = new ClientRequest();
        }
        entity.setTitle(request.getTitle());
        entity.setDescription(request.getDescription());
        entity.setRequestType(request.getRequestType());
        entity.setStatus(RequestStatus.CREATED);
        return entity;
    }


    public OwnerDetail requestToEntity(OwnerDetail entity, OwnerDetailDto request) {
        if (Util.isNullOrEmpty(entity)) {
            entity = new OwnerDetail();
        }
        entity.setDescription(request.getDescription());
        entity.setName(request.getName());
        entity.setDepartment(request.getDepartment());
        entity.setDesignation(request.getDesignation());
        if (!Util.isNullOrEmpty(request.getDob()))
            entity.setDob(request.getDob());
        entity.setEmailId(request.getEmailId());
        entity.setPhoneNo(request.getPhoneNo());
        entity.setIsPrimary(request.getIsPrimary());
        return entity;
    }

    @Transactional
    public void clientRequestStatus(String token, String requestId, RequestStatus status) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            ClientRequest clientRequest = clientRequestRepository.findById(requestId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Request not Found"));
            if (clientRequest.getOrgId().equals(user.getEntityKey())) {
                if (status.equals(RequestStatus.COMPLETED)) {
                    Gson gson = new Gson();
                    ClientDto clientDto = gson.fromJson(clientRequest.getClientData(), ClientDto.class);
                    if (clientRequest.getRequestType().equals(RequestType.CREATE)) {
                        clientRequest.setClientEntity(saveClient(null, clientDto, user));
                        clientRequest.setStatus(RequestStatus.COMPLETED);
                    } else if (clientRequest.getRequestType().equals(RequestType.UPDATE)) {
                        updateClient(token, clientRequest.getClientEntity().getId(), clientDto);
                        clientRequest.setStatus(RequestStatus.COMPLETED);
                    }
                }
            } else {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The requested organisation has not access to change the given client request");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    @Transactional
    public ResponseEntity addClientContactDetail(String token, String clientId, ContactDetailDto contactDetailDto, String status) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            ClientEntity client = clientRepository.findById(clientId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Client Not Found"));
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return ResponseEntity.ok(addClientContactDetailByExe(client, contactDetailDto, user, status));
            } else {
                return ResponseEntity.ok(addClientContactDetailByOrg(client, contactDetailDto, user, status));
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    private ContactDetail addClientContactDetailByExe(ClientEntity client, ContactDetailDto contactDetailDto, UserDetails user, String status) {
        Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
        if (client.getOrgId().equals(executive.getOrg().getId())) {
            return saveContactDetail(client, contactDetailDto, status);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive is not from the same organisation");
        }
    }

    private ContactDetail addClientContactDetailByOrg(ClientEntity client, ContactDetailDto contactDetailDto, UserDetails user, String status) {
        if (client.getOrgId().equals(user.getEntityKey())) {
            return saveContactDetail(client, contactDetailDto, status);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested client if not from your organisation");
        }
    }

    private ContactDetail saveContactDetail(ClientEntity client, ContactDetailDto contactDetailDto, String status) {
        ContactDetail contactDetail = requestToEntity(client.getContactDetail(), contactDetailDto);
        contactDetail.setClientEntity(client);
        return contactDetailRepository.save(contactDetail);
    }

    public ContactDetail requestToEntity(ContactDetail entity, ContactDetailDto request) {
        if (Util.isNullOrEmpty(entity)) {
            entity = new ContactDetail();
        }
        entity.setFbUrl(request.getFbUrl());
        entity.setGPlusUrl(request.getGPlusUrl());
        entity.setInstaUrl(request.getInstaUrl());
        entity.setTwitterUrl(request.getTwitterUrl());
        entity.setLinkedinUrl(request.getLinkedinUrl());
        entity.setWebsiteUrl(request.getWebsiteUrl());
        return entity;
    }

    public KYCDetail requestToEntity(KYCDetail entity, KycDetailDto request) {
        if (Util.isNullOrEmpty(entity)) {
            entity = new KYCDetail();
        }
        entity.setDob(request.getDob());
        entity.setAadhaarNo(request.getAadhaarNo());
        entity.setName(request.getAadhaarNo());
        entity.setPanCard(request.getPanCard());
        return entity;
    }


    @Transactional
    public ResponseEntity addClientKYCDetail(String token, String clientId, KycDetailDto kycDetailDto, String status) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            ClientEntity client = clientRepository.findById(clientId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Client Not Found"));
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return ResponseEntity.ok(addClientKYCDetailByExe(client, kycDetailDto, user));
            } else {
                return ResponseEntity.ok(addClientKYCDetailByOrg(client, kycDetailDto, user));
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private KYCDetail addClientKYCDetailByExe(ClientEntity client, KycDetailDto kycDetailDto, UserDetails user) {
        Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
        if (client.getOrgId().equals(executive.getOrg().getId())) {
            return saveKycDetail(client, kycDetailDto);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive is not from the same organisation");
        }
    }

    private KYCDetail addClientKYCDetailByOrg(ClientEntity client, KycDetailDto kycDetailDto, UserDetails user) {
        if (client.getOrgId().equals(user.getEntityKey())) {
            return saveKycDetail(client, kycDetailDto);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested client if not from your organisation");
        }
    }

    private KYCDetail saveKycDetail(ClientEntity client, KycDetailDto kycDetailDto) {
        KYCDetail kycDetail = requestToEntity(client.getKycDetail(), kycDetailDto);
        kycDetail.setClientEntity(client);
        return kycDetailRepository.save(kycDetail);
    }

    @Transactional
    public ResponseEntity addClientAddress(String token, String clientId, AddressDto addressDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            ClientEntity client = clientRepository.findById(clientId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Client Not Found"));
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return ResponseEntity.ok(addClientAddressByExe(client, null, addressDto, user));
            } else {
                return ResponseEntity.ok(addClientAddressByOrg(client, null, addressDto, user));
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private Address addClientAddressByExe(ClientEntity client, Address address, AddressDto addressDto, UserDetails user) {
        Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
        if (client.getOrgId().equals(executive.getOrg().getId())) {
            return saveAddress(client, address, addressDto);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive is not from the same organisation");
        }
    }

    private Address addClientAddressByOrg(ClientEntity client, Address address, AddressDto addressDto, UserDetails user) {
        if (client.getOrgId().equals(user.getEntityKey())) {
            return saveAddress(client, address, addressDto);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested client if not from your organisation");
        }
    }

    @Transactional
    public ResponseEntity updateClientAddress(String token, String addId, AddressDto addressDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            Address entity = addressRepository.findById(addId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Address Not found"));
            ClientEntity client = entity.getClientEntity();
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return ResponseEntity.ok(addClientAddressByExe(client, entity, addressDto, user));
            } else {
                return ResponseEntity.ok(addClientAddressByOrg(client, entity, addressDto, user));
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public ResponseEntity addressPrimaryStatus(String token, String addId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            Address entity = addressRepository.findById(addId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Address Not found"));
            ClientEntity client = entity.getClientEntity();
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
                if (client.getOrgId().equals(executive.getOrg().getId())) {
                    return ResponseEntity.ok(setIsPrimary(client, entity));
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive is not from the same organisation");
                }
            } else {
                if (client.getOrgId().equals(user.getEntityKey())) {
                    return ResponseEntity.ok(setIsPrimary(client, entity));
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested client if not from your organisation");
                }
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private Address setIsPrimary(ClientEntity client, Address entity) {
        if (entity.getIsBilling()) {
            Address address = addressRepository.findByClientEntityIdAndIsBillingTrueAndIsPrimaryTrue(client.getId());
            if (!Util.isNullOrEmpty(address)) {
                address.setIsPrimary(false);
            }
            entity.setIsPrimary(true);
        } else {
            Address address = addressRepository.findByClientEntityIdAndIsBillingFalseAndIsPrimaryTrue(client.getId());
            if (!Util.isNullOrEmpty(address)) {
                address.setIsPrimary(false);
            }
            entity.setIsPrimary(true);
        }
        return addressRepository.save(entity);
    }


    @Transactional
    public ResponseEntity accDetailPrimaryStatus(String token, String accId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            AccountDetail entity = accountDetailRepository.findById(accId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Address Not found"));
            ClientEntity client = entity.getClientEntity();
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
                if (client.getOrgId().equals(executive.getOrg().getId())) {
                    return ResponseEntity.ok(setIsPrimary(client, entity));
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive is not from the same organisation");
                }
            } else {
                if (client.getOrgId().equals(user.getEntityKey())) {
                    return ResponseEntity.ok(setIsPrimary(client, entity));
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested client if not from your organisation");
                }
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private AccountDetail setIsPrimary(ClientEntity client, AccountDetail entity) {
        AccountDetail accountDetail = accountDetailRepository.findByClientEntityIdAndIsPrimaryTrue(client.getId());
        if (!Util.isNullOrEmpty(accountDetail)) {
            accountDetail.setIsPrimary(false);
        }
        entity.setIsPrimary(true);
        return accountDetailRepository.save(entity);
    }

    private Address saveAddress(ClientEntity client, Address entity, AddressDto addressDto) {
        Address address = requestToEntity(entity, addressDto);
        address.setClientEntity(client);
        if (client.getAddresses().size() > 0) {
            address.setIsPrimary(false);
        } else {
            address.setIsPrimary(true);
        }
        return addressRepository.save(address);
    }


    private Address requestToEntity(Address entity, AddressDto request) {
        if (Util.isNullOrEmpty(entity))
            entity = new Address();

        entity.setCity(request.getCity());
        entity.setIsBilling(request.getIsBilling());
        entity.setLine1(request.getLine1());
        entity.setLine2(request.getLine2());
        entity.setState(request.getState());
        entity.setZipcode(request.getZipcode());
        return entity;
    }

    @Transactional
    public ResponseEntity addClientAccDetail(String token, String clientId, AccountDetailDto accountDetailDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            ClientEntity client = clientRepository.findById(clientId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Client Not Found"));
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return ResponseEntity.ok(addClientAccDetailByExe(client, null, accountDetailDto, user));
            } else {
                return ResponseEntity.ok(addClientAccDetailByOrg(client, null, accountDetailDto, user));
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    private AccountDetail addClientAccDetailByExe(ClientEntity client, AccountDetail accountDetail, AccountDetailDto accountDetailDto, UserDetails user) {
        Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
        if (client.getOrgId().equals(executive.getOrg().getId())) {
            return saveAccDetail(client, accountDetail, accountDetailDto);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive is not from the same organisation");
        }
    }

    private AccountDetail addClientAccDetailByOrg(ClientEntity client, AccountDetail accountDetail, AccountDetailDto accountDetailDto, UserDetails user) {
        if (client.getOrgId().equals(user.getEntityKey())) {
            return saveAccDetail(client, accountDetail, accountDetailDto);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested client if not from your organisation");
        }
    }

    private AccountDetail saveAccDetail(ClientEntity client, AccountDetail entity, AccountDetailDto accountDetailDto) {
        AccountDetail accountDetail = requestToEntity(entity, accountDetailDto);
        accountDetail.setClientEntity(client);
        if (client.getAccountDetails().size() > 0) {
            accountDetail.setIsPrimary(false);
        } else {
            accountDetail.setIsPrimary(true);
        }
        return accountDetailRepository.save(accountDetail);
    }

    private AccountDetail requestToEntity(AccountDetail entity, AccountDetailDto request) {
        if (Util.isNullOrEmpty(entity))
            entity = new AccountDetail();

        entity.setAccNo(request.getAccNo());
        entity.setAccType(request.getAccType());
        entity.setBankName(request.getBankName());
        entity.setBranch(request.getBranch());
        entity.setIfsc(request.getIfsc());
        entity.setName(request.getName());
        return entity;
    }

    @Transactional
    public ResponseEntity updateClientAccDetails(String token, String addId, AccountDetailDto accountDetailDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            AccountDetail entity = accountDetailRepository.findById(addId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Account Detail Not found"));
            ClientEntity client = entity.getClientEntity();
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                return ResponseEntity.ok(addClientAccDetailByExe(client, entity, accountDetailDto, user));
            } else {
                return ResponseEntity.ok(addClientAccDetailByOrg(client, entity, accountDetailDto, user));
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public void deleteAddress(String token, String addId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            Address entity = addressRepository.findById(addId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Address Not found"));
            ClientEntity client = entity.getClientEntity();
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                deleteClientAddressByExe(client, entity, user);
            } else {
                deleteClientAddressByOrg(client, entity, user);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    private void deleteClientAddressByExe(ClientEntity client, Address address, UserDetails user) {
        Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
        if (client.getOrgId().equals(executive.getOrg().getId())) {
            addressRepository.delete(address);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive is not from the same organisation");
        }
    }

    private void deleteClientAddressByOrg(ClientEntity client, Address address, UserDetails user) {
        if (client.getOrgId().equals(user.getEntityKey())) {
            addressRepository.delete(address);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested client if not from your organisation");
        }
    }


    @Transactional
    public void deleteAccDetail(String token, String addId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE))) {
            AccountDetail entity = accountDetailRepository.findById(addId).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Address Not found"));
            ClientEntity client = entity.getClientEntity();
            if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                deleteClientAccDetailByExe(client, entity, user);
            } else {
                deleteClientAccDetailByOrg(client, entity, user);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    private void deleteClientAccDetailByExe(ClientEntity client, AccountDetail accountDetail, UserDetails user) {
        Executive executive = executiveRepository.findById(user.getEntityKey()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive Not Found"));
        if (client.getOrgId().equals(executive.getOrg().getId())) {
            accountDetailRepository.delete(accountDetail);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Executive is not from the same organisation");
        }
    }

    private void deleteClientAccDetailByOrg(ClientEntity client, AccountDetail accountDetail, UserDetails user) {
        if (client.getOrgId().equals(user.getEntityKey())) {
            accountDetailRepository.delete(accountDetail);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The requested client if not from your organisation");
        }
    }
}
