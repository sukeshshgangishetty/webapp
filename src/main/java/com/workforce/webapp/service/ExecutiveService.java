package com.workforce.webapp.service;

import com.workforce.webapp.dto.executive.ExecutiveDto;
import com.workforce.webapp.model.*;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.utils.RestUtils;
import com.workforce.webapp.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.Objects;

@Component
public class ExecutiveService {

    private @Autowired
    UserDetailsRepository userRepository;

    private @Autowired
    ExecutiveRepository executiveRepository;

    @Transactional
    public Executive createExecutive(String token, Long exeId, ExecutiveDto executiveDto) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            if (Util.isNullOrEmpty(exeId)) {
                return addExecutive(executiveDto, user);
            } else {
                return updateExecutive(executiveDto, user, exeId);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    private Executive addExecutive(ExecutiveDto executiveDto, UserDetails user) {
        UserDetails existingExe = userRepository.findByPhone(executiveDto.getPhone());
        if (Util.isNullOrEmpty(existingExe)) {
            return saveExecutive(null, executiveDto, user);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PhoneNo Already Exists");
        }
    }

    private Executive updateExecutive(ExecutiveDto executiveDto, UserDetails user, Long exeId) {
        Executive entity = executiveRepository.findById(exeId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Executive Not Found"));
        if (Objects.equals(entity.getOrg().getId(), user.getEntityKey())) {
            if (entity.getPhone().equals(executiveDto.getPhone())) {
                return saveExecutive(entity, executiveDto, user);
            } else {
                UserDetails existExe = userRepository.findByPhone(executiveDto.getPhone());
                if (Util.isNullOrEmpty(existExe)) {
                    return saveExecutive(entity, executiveDto, user);
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "PhoneNo Already Exists");
                }
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The requested executive are not from the same requested organisation");
        }
    }

    private Executive saveExecutive(Executive entity, ExecutiveDto executiveDto, UserDetails user) {
        Executive executive = requestToEntity(entity, executiveDto);
        executive.setOrg(load(user.getEntityKey()));
        executiveRepository.save(executive);
        UserDetails newExe = null;
        if (!Util.isNullOrEmpty(entity))
            newExe = addUser(executive, "UPDATE");
        else
            newExe = addUser(executive, "SAVE");

        String msg = "Dear " + newExe.getUsername()
                + ", use your phone number as username and password for eorbapp @"
                + " https://bit.ly/2J9ui1Q -Thanks, Call @ 8886668273, Neemiit";

        SmsServiceImpl.sendSms(newExe.getPhone(), msg);
        return executive;
    }

    private Organization load(Long orgId) {
        Organization organization = new Organization();
        organization.setId(orgId);
        return organization;
    }

    private UserDetails addUser(Executive executive, String status) {
        if (status.equals("UPDATE")) {
            UserDetails user = userRepository.findByEntityKeyAndRole(executive.getExeId(), Userrole.USER_EXECUTIVE);
            return mapUser(user, executive);
        } else {
            return mapUser(null, executive);
        }
    }

    private UserDetails mapUser(UserDetails user, Executive executive) {
        if (Util.isNullOrEmpty(user)) {
            user = new UserDetails();
        }
        user.setRole(Userrole.USER_EXECUTIVE);
        user.setUsername(executive.getName());
        user.setPhone(executive.getPhone());
        user.setEntityKey(executive.getExeId());
        user.setStatus(UserStatus.PENDING_SELF_ACTIVATION);
        user.setLogin(executive.getPhone());
        user.setPassword(RestUtils.md5(executive.getPhone()));
        user.setEmail(executive.getEmail());
        String userActivation = "";
        while (userActivation.equals("")) {
            userActivation = RestUtils.getActivationKey();
            if (userRepository.findByActivationKey(userActivation) == null) {
                System.out.println("Activation key=" + userActivation);
                user.setActivationKey(userActivation);
            } else {
                userActivation = "";
            }
        }
        return userRepository.save(user);
    }


    private Executive requestToEntity(Executive entity, ExecutiveDto request) {
        if (Util.isNullOrEmpty(entity))
            entity = new Executive();

        entity.setAadhaarNumber(request.getAadhaarNumber());
        entity.setEmail(request.getEmail());
        entity.setAddress(request.getAddress());
        entity.setImgLogo(request.getImgLogo());
        entity.setName(request.getName());
        entity.setDob(request.getDob());
        entity.setPhone(request.getPhone());
        entity.setEsiNumber(request.getEsiNumber());
        entity.setPanCardNumber(request.getPanCardNumber());
        entity.setQualification(request.getQualification());
        entity.setPfNumber(request.getPfNumber());
        entity.setVehicleNumber(request.getVehicleNumber());
        entity.setRemarks(request.getRemarks());
        entity.setJoinedDate(request.getJoinedDate());
        return entity;
    }


    @Transactional
    public ResponseEntity getAllExecs(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            return ResponseEntity.ok(executiveRepository.findByOrgId(user.getEntityKey()));
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

    @Transactional
    public Executive getExecById(String token, Long exeId) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.ORGANIZATION)) {
            Executive executive = executiveRepository.findByExeId(exeId);
            if (Objects.equals(user.getEntityKey(), executive.getOrg().getId())) {
                return executive;
            } else {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Organisation requested for different organisation executive");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }


    @Transactional
    public Executive getExeProfile(String token) {
        UserDetails user = userRepository.findByToken(token);
        if (!Util.isNullOrEmpty(user) && user.getRole().equals(Userrole.USER_EXECUTIVE)) {
            return executiveRepository.findByExeId(user.getEntityKey());
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is UnAuthorized");
        }
    }

}
