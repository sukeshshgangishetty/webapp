package com.workforce.webapp.mapper;

import com.workforce.webapp.dto.trip.PointDto;
import com.workforce.webapp.dto.trip.ScheduleDto;
import com.workforce.webapp.dto.trip.TripDto;
import com.workforce.webapp.model.trip.Point;
import com.workforce.webapp.model.trip.Schedule;
import com.workforce.webapp.model.trip.Trip;
import com.workforce.webapp.utils.Util;

public class TripMapper {

    public static Trip requestToEntity(Trip entity, TripDto request) {
        if (Util.isNullOrEmpty(entity)) {
            entity = new Trip();
        }

        entity.setDescription(request.getDescription());
        entity.setPeriod(request.getPeriod());
        entity.setPriorityType(request.getPriorityType());
        entity.setTitle(request.getTitle());

        return entity;
    }

    public static Point requestToEntity(Point entity, PointDto request) {
        if (Util.isNullOrEmpty(entity)) {
            entity = new Point();
        }
        entity.setLocCurrent(request.getLocCurrent());
        entity.setClientId(request.getClientId());
        entity.setName(request.getName());
        entity.setType(request.getType());
        return entity;
    }

    public static Schedule requestToEntity(Schedule entity, ScheduleDto request) {
        if (Util.isNullOrEmpty(entity)) {
            entity = new Schedule();
        }
        entity.setExpEndTym(request.getExpEndTym());
        entity.setExpStrTym(request.getExpStrTym());
        return entity;
    }

}
