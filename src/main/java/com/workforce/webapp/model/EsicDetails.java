package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class EsicDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long esicId;

    private String insName;

    private String empCode;

    @ManyToOne
    NomineeDetails nominee;

    private String branchOfc;

    private String dispensary;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dob;

    @JsonIgnore
    @Column(name = "createdAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt = null;

    @JsonIgnore
    @Column(name = "updatedAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt = null;

    public long getEsicId() {
        return esicId;
    }

    public void setEsicId(long esicId) {
        this.esicId = esicId;
    }

    public String getInsName() {
        return insName;
    }

    public void setInsName(String insName) {
        this.insName = insName;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public NomineeDetails getNominee() {
        return nominee;
    }

    public void setNominee(NomineeDetails nominee) {
        this.nominee = nominee;
    }

    public String getBranchOfc() {
        return branchOfc;
    }

    public void setBranchOfc(String branchOfc) {
        this.branchOfc = branchOfc;
    }

    public String getDispensary() {
        return dispensary;
    }

    public void setDispensary(String dispensary) {
        this.dispensary = dispensary;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "EsicDetails [esicId=" + esicId + ", insName=" + insName + ", empCode=" + empCode + ", nominee="
                + nominee + ", branchOfc=" + branchOfc + ", dispensary=" + dispensary + ", dob=" + dob + ", createdAt="
                + createdAt + ", updatedAt=" + updatedAt + "]";
    }


}
