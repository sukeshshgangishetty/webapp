package com.workforce.webapp.model;

public enum ExpenseStatus {

    ADDED, APPROVED, REJECTED, ONHOLD, REFUNDED, OTHERS

}
