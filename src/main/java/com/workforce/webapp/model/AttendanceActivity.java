package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class AttendanceActivity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long attActId;

    private AttendanceAction action;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date actionAt;

    private AttendanceSource source;

    @Temporal(TemporalType.TIME)
    @JsonFormat(pattern = "HH:mm:ss")
    private Date actionTime;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date updatedAt;

    private String reason;

    private long attendenceRecId;

    public long getAttActId() {
        return attActId;
    }

    public void setAttActId(long attActId) {
        this.attActId = attActId;
    }

    public AttendanceAction getAction() {
        return action;
    }

    public void setAction(AttendanceAction action) {
        this.action = action;
    }

    public Date getActionAt() {
        return actionAt;
    }

    public void setActionAt(Date actionAt) {
        this.actionAt = actionAt;
    }

    public AttendanceSource getSource() {
        return source;
    }

    public void setSource(AttendanceSource source) {
        this.source = source;
    }

    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getAttendenceRecId() {
        return attendenceRecId;
    }

    public void setAttendenceRecId(long attendenceRecId) {
        this.attendenceRecId = attendenceRecId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public AttendanceActivity(long attActId, AttendanceAction action, Date actionAt, AttendanceSource source,
                              Date actionTime, Date createdAt, Date updatedAt) {
        super();
        this.attActId = attActId;
        this.action = action;
        this.actionAt = actionAt;
        this.source = source;
        this.actionTime = actionTime;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public AttendanceActivity(AttendanceAction action, Date actionAt, AttendanceSource source, Date actionTime) {
        super();
        this.action = action;
        this.actionAt = actionAt;
        this.source = source;
        this.actionTime = actionTime;
    }

    public AttendanceActivity() {
        super();
    }

    @Override
    public String toString() {
        return "AttendanceActivity [attActId=" + attActId + ", action=" + action + ", actionAt=" + actionAt
                + ", source=" + source + ", actionTime=" + actionTime + ", createdAt=" + createdAt + ", updatedAt="
                + updatedAt + ", attendenceRecId=" + attendenceRecId + "]";
    }

}
