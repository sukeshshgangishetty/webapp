package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class AttendanceRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long attRecId;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date attOn;

    @Temporal(TemporalType.TIME)
    @JsonFormat(pattern = "HH:mm:ss")
    private Date punchIn;


    @Temporal(TemporalType.TIME)
    @JsonFormat(pattern = "HH:mm:ss")
    private Date punchOut;


    private double production;

    @Temporal(TemporalType.TIME)
    @JsonFormat(pattern = "HH:mm:ss")
    private Date breakTime;


    private double overTime;

    private AttendenceCurrentStatus currStatus;

    @ManyToOne
    @JsonIgnore
    private Employee emp;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date updatedAt;

    private boolean actionRequired;

    private String actionMessage;

    @ManyToOne
    @JsonIgnore
    private UserDetails addedBy;

    private AttendanceNotation notation;

    private AttendanceIndication indication;

    public long getAttRecId() {
        return attRecId;
    }

    public void setAttRecId(long attRecId) {
        this.attRecId = attRecId;
    }

    public Date getAttOn() {
        return attOn;
    }

    public void setAttOn(Date attOn) {
        this.attOn = attOn;
    }

    public Date getPunchIn() {
        return punchIn;
    }

    public void setPunchIn(Date punchIn) {
        this.punchIn = punchIn;
    }

    public Date getPunchOut() {
        return punchOut;
    }

    public void setPunchOut(Date punchOut) {
        this.punchOut = punchOut;
    }

    public double getProduction() {
        return production;
    }

    public void setProduction(double production) {
        this.production = production;
    }

    public Date getBreakTime() {
        return breakTime;
    }

    public void setBreakTime(Date breakTime) {
        this.breakTime = breakTime;
    }

    public double getOverTime() {
        return overTime;
    }

    public void setOverTime(double overTime) {
        this.overTime = overTime;
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UserDetails getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(UserDetails addedBy) {
        this.addedBy = addedBy;
    }

    public AttendanceNotation getNotation() {
        return notation;
    }

    public void setNotation(AttendanceNotation notation) {
        this.notation = notation;
    }

    public AttendanceIndication getIndication() {
        return indication;
    }

    public void setIndication(AttendanceIndication indication) {
        this.indication = indication;
    }


    public boolean isActionRequired() {
        return actionRequired;
    }

    public void setActionRequired(boolean actionRequired) {
        this.actionRequired = actionRequired;
    }


    public String getActionMessage() {
        return actionMessage;
    }

    public void setActionMessage(String actionMessage) {
        this.actionMessage = actionMessage;
    }

    public AttendanceRecord(long attRecId, Date attOn, Date punchIn, Date punchOut, double production, Date breakTime,
                            double overTime, Employee emp, Date createdAt, Date updatedAt, UserDetails addedBy,
                            AttendanceNotation notation, AttendanceIndication indication) {
        super();
        this.attRecId = attRecId;
        this.attOn = attOn;
        this.punchIn = punchIn;
        this.punchOut = punchOut;
        this.production = production;
        this.breakTime = breakTime;
        this.overTime = overTime;
        this.emp = emp;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.addedBy = addedBy;
        this.notation = notation;
        this.indication = indication;
    }

    public AttendanceRecord(Date attOn, Date punchIn, Date punchOut, double production, Date breakTime, double overTime,
                            Employee emp, UserDetails addedBy, AttendanceNotation notation, AttendanceIndication indication) {
        super();
        this.attOn = attOn;
        this.punchIn = punchIn;
        this.punchOut = punchOut;
        this.production = production;
        this.breakTime = breakTime;
        this.overTime = overTime;
        this.emp = emp;
        this.addedBy = addedBy;
        this.notation = notation;
        this.indication = indication;
    }

    public AttendanceRecord() {
        super();
    }

    public AttendenceCurrentStatus getCurrStatus() {
        return currStatus;
    }

    public void setCurrStatus(AttendenceCurrentStatus currStatus) {
        this.currStatus = currStatus;
    }


}
