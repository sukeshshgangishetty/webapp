package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Gratuity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long graId;

    @ManyToOne
    NomineeDetails nominee;

    private String shareVal;

    @JsonIgnore
    @Column(name = "createdAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt = null;

    @JsonIgnore
    @Column(name = "updatedAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt = null;

    public long getGraId() {
        return graId;
    }

    public void setGraId(long graId) {
        this.graId = graId;
    }

    public NomineeDetails getNominee() {
        return nominee;
    }

    public void setNominee(NomineeDetails nominee) {
        this.nominee = nominee;
    }

    public String getShareVal() {
        return shareVal;
    }

    public void setShareVal(String shareVal) {
        this.shareVal = shareVal;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Gratuity [graId=" + graId + ", nominee=" + nominee + ", shareVal=" + shareVal + ", createdAt="
                + createdAt + ", updatedAt=" + updatedAt + "]";
    }


}
