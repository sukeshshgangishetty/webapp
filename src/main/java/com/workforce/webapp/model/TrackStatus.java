package com.workforce.webapp.model;

public enum TrackStatus {

    TRACK_STARTED, TRACK_STOPPED, TRACK_STOPPED_FORCIBLY
}
