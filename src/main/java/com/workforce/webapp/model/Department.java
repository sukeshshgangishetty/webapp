package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Department {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long departmentid;

    private String departmentName;


    private String address;


    private String branch;

    @ManyToOne
    private Employee hod;

    private String buildingName;


    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    public long getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(long departmentid) {
        this.departmentid = departmentid;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Employee getHod() {
        return hod;
    }

    public void setHod(Employee hod) {
        this.hod = hod;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }


    public Department(long departmentid, String departmentName, String address, String branch, Employee hod,
                      String buildingName, Date createdAt) {
        super();
        this.departmentid = departmentid;
        this.departmentName = departmentName;
        this.address = address;
        this.branch = branch;
        this.hod = hod;
        this.buildingName = buildingName;
        this.createdAt = createdAt;
    }

    public Department(long departmentid, String departmentName, Date createdAt) {
        super();
        this.departmentid = departmentid;
        this.departmentName = departmentName;
        this.createdAt = createdAt;
    }

    public Department() {
        super();
    }


}
