package com.workforce.webapp.model;

public enum LeaveStatus {

    APPLIED, APPROVED, REJECTED, ONHOLD, CANCELLED

}
