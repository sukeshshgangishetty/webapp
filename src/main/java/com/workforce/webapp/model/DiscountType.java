package com.workforce.webapp.model;

public enum DiscountType {

    PRODUCT, PRODUCT_CATGORY, ORDER_VALUE, PAYMENT_MODE
}
