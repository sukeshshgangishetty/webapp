package com.workforce.webapp.model;

public enum DispatchListStatus {

    CREATED, DISPATCHED, ON_HOLD, COMPLETED, VALIDATED, VALIDATION_PENDING, OTHERS
}
