package com.workforce.webapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@Entity
public class OrderParticulars {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @JsonIgnore
    @ManyToOne
    private OrderDetails order;


    @ManyToOne
    PriceList pricelist;


    double qty;

    @JsonInclude(Include.NON_DEFAULT)
    double amount;

    @JsonInclude(Include.NON_DEFAULT)
    double rate;

    double tax;

    public OrderDetails getOrder() {
        return order;
    }

    public void setOrder(OrderDetails order) {
        this.order = order;
    }


    public double getQty() {
        return qty;
    }

    public PriceList getPricelist() {
        return pricelist;
    }

    public void setPricelist(PriceList pricelist) {
        this.pricelist = pricelist;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    @Override
    public String toString() {
        return "OrderParticulars [product=" + pricelist + ", qty=" + qty + ", amount=" + amount + ", rate=" + rate + "]";
    }

    public OrderParticulars(OrderDetails order, PriceList pricelist, double qty, double amount, double rate) {
        super();
        this.pricelist = pricelist;
        this.qty = qty;
        this.amount = amount;
        this.rate = rate;
    }

    public OrderParticulars(OrderDetails order, double qty, long id) {
        super();
        this.order = order;
        this.qty = qty;
        this.id = id;
    }


    public OrderParticulars() {
        super();
        // TODO Auto-generated constructor stub
    }


}
