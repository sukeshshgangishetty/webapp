package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class SalaryStructureItems {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long ssItemId;

    @ManyToOne
    private PayrollItem item;


    private double value;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date updatedAt;

    public long getSsItemId() {
        return ssItemId;
    }

    public void setSsItemId(long ssItemId) {
        this.ssItemId = ssItemId;
    }

    public PayrollItem getItem() {
        return item;
    }

    public void setItem(PayrollItem item) {
        this.item = item;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public SalaryStructureItems(long ssItemId, PayrollItem item, double value, Date createdAt, Date updatedAt) {
        super();
        this.ssItemId = ssItemId;
        this.item = item;
        this.value = value;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public SalaryStructureItems() {
        super();
    }

    public SalaryStructureItems(PayrollItem item) {
        super();
        this.item = item;
    }

    public SalaryStructureItems(String item) {
        super();
    }


}
