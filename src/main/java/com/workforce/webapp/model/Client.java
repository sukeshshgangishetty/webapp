package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String clientname;

    @NotBlank
    private String phone;

    @NotBlank
    private String mail;

    private Location location;

    private String image;

    @JsonIgnore
    private long orgId;

    private String companyName;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date incorporatedAt;

    private String officeNumber;

    private String remarks;

    @ManyToOne
    private UserDetails addedBy;

    private String gstNo;

    private long assignedTo;

    private long updateState;

    private double openingBalance;

    private ClientAccoutStatus accStatus;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the clientname
     */
    public String getClientname() {
        return clientname;
    }

    /**
     * @param clientname the clientname to set
     */
    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the orgId
     */
    public long getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Date getIncorporatedAt() {
        return incorporatedAt;
    }

    public void setIncorporatedAt(Date incorporatedAt) {
        this.incorporatedAt = incorporatedAt;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public UserDetails getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(UserDetails addedBy) {
        this.addedBy = addedBy;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public long getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(long assignedTo) {
        this.assignedTo = assignedTo;
    }

    public long getUpdateState() {
        return updateState;
    }

    public void setUpdateState(long updateState) {
        this.updateState = updateState;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public ClientAccoutStatus getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(ClientAccoutStatus accStatus) {
        this.accStatus = accStatus;
    }


    public Client(String clientname, String phone, String mail, Location location, String image, long orgId) {
        super();
        this.clientname = clientname;
        this.phone = phone;
        this.mail = mail;
        this.location = location;
        this.image = image;
        this.orgId = orgId;
    }

    public Client(String clientname, String phone, String mail, Location location, String image, long orgId,
                  String companyName, Date incorporatedAt, String officeNumber, String remarks) {
        super();
        this.clientname = clientname;
        this.phone = phone;
        this.mail = mail;
        this.location = location;
        this.image = image;
        this.orgId = orgId;
        this.companyName = companyName;
        this.incorporatedAt = incorporatedAt;
        this.officeNumber = officeNumber;
        this.remarks = remarks;
    }

    public Client() {
        super();
    }

    @Override
    public String toString() {
        return "{\"id\":" + id + ", clientname\":" + clientname + ", phone\":" + phone + ", mail\":" + mail
                + ", location\":" + location + ", image\":" + image + ", orgId\":" + orgId + ", companyName\":"
                + companyName + ", incorporatedAt\":" + incorporatedAt + ", officeNumber\":" + officeNumber
                + ", remarks\":" + remarks + ", addedBy\":" + addedBy + ", gstNo\":" + gstNo + ", assignedTo\":"
                + assignedTo + ", updateState\":" + updateState + "}";
    }

}
