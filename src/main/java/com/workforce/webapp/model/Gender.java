package com.workforce.webapp.model;

public enum Gender {

    MALE, FEMALE, OTHER
}
