package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class UploadDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long upId;

    private String passportPhoto;

    private IdProofType idProofType;

    private String idFront;

    private String idBack;

    private String addrFront;

    private String addrBack;

    private String bankDoc;

    private String resume;

    private String tenthSheet;

    private String interSheet;

    private String ugSheet;

    private String pgSheet;

    private String offerLetter;

    private String paySlip;

    private String relievingLetter;

    private String prevOffLetter;

    @JsonIgnore
    @Column(name = "createdAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt = null;

    @JsonIgnore
    @Column(name = "updatedAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt = null;

    public long getUpId() {
        return upId;
    }

    public void setUpId(long upId) {
        this.upId = upId;
    }

    public String getPassportPhoto() {
        return passportPhoto;
    }

    public void setPassportPhoto(String passportPhoto) {
        this.passportPhoto = passportPhoto;
    }

    public IdProofType getIdProofType() {
        return idProofType;
    }

    public void setIdProofType(IdProofType idProofType) {
        this.idProofType = idProofType;
    }

    public String getIdFront() {
        return idFront;
    }

    public void setIdFront(String idFront) {
        this.idFront = idFront;
    }

    public String getIdBack() {
        return idBack;
    }

    public void setIdBack(String idBack) {
        this.idBack = idBack;
    }

    public String getAddrFront() {
        return addrFront;
    }

    public void setAddrFront(String addrFront) {
        this.addrFront = addrFront;
    }

    public String getAddrBack() {
        return addrBack;
    }

    public void setAddrBack(String addrBack) {
        this.addrBack = addrBack;
    }

    public String getBankDoc() {
        return bankDoc;
    }

    public void setBankDoc(String bankDoc) {
        this.bankDoc = bankDoc;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getTenthSheet() {
        return tenthSheet;
    }

    public void setTenthSheet(String tenthSheet) {
        this.tenthSheet = tenthSheet;
    }

    public String getInterSheet() {
        return interSheet;
    }

    public void setInterSheet(String interSheet) {
        this.interSheet = interSheet;
    }

    public String getUgSheet() {
        return ugSheet;
    }

    public void setUgSheet(String ugSheet) {
        this.ugSheet = ugSheet;
    }

    public String getPgSheet() {
        return pgSheet;
    }

    public void setPgSheet(String pgSheet) {
        this.pgSheet = pgSheet;
    }

    public String getOfferLetter() {
        return offerLetter;
    }

    public void setOfferLetter(String offerLetter) {
        this.offerLetter = offerLetter;
    }

    public String getPaySlip() {
        return paySlip;
    }

    public void setPaySlip(String paySlip) {
        this.paySlip = paySlip;
    }

    public String getRelievingLetter() {
        return relievingLetter;
    }

    public void setRelievingLetter(String relievingLetter) {
        this.relievingLetter = relievingLetter;
    }

    public String getPrevOffLetter() {
        return prevOffLetter;
    }

    public void setPrevOffLetter(String prevOffLetter) {
        this.prevOffLetter = prevOffLetter;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "UploadDetails [upId=" + upId + ", passportPhoto=" + passportPhoto + ", idProofType=" + idProofType
                + ", idFront=" + idFront + ", idBack=" + idBack + ", addrFront=" + addrFront + ", addrBack=" + addrBack
                + ", bankDoc=" + bankDoc + ", resume=" + resume + ", tenthSheet=" + tenthSheet + ", interSheet="
                + interSheet + ", ugSheet=" + ugSheet + ", pgSheet=" + pgSheet + ", offerLetter=" + offerLetter
                + ", paySlip=" + paySlip + ", relievingLetter=" + relievingLetter + ", prevOffLetter=" + prevOffLetter
                + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
    }

}
