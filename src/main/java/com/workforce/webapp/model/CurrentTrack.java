package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class CurrentTrack {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("trackkey")
    private long ctid;

    @NotNull
    private TrackStatus tstat = TrackStatus.TRACK_STARTED;

    @NotNull
    private int cttype;

    @JsonProperty("exeid")
    @NotNull
    private long etyky;

    @NotNull
    private Date startedat = new Date();

    @NotNull
    private double kmtr = 0;

    private Date endedat;

    private Location currentLoc = new Location(0, 0, "");

    private int ttype;

    private double currbat;


    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date trackUpdatedAt;


    private String imei;


    private String manufacturer;

    private String osversion;

    private String model;

    @JsonProperty("distanceTravelled")
    double disTra;

    double speed;


    /**
     * @return the ctid
     */
    public long getCtid() {
        return ctid;
    }

    /**
     * @param ctid the ctid to set
     */
    public void setCtid(long ctid) {
        this.ctid = ctid;
    }

    /**
     * @return the tstat
     */
    public TrackStatus getTstat() {
        return tstat;
    }

    /**
     * @param tstat the tstat to set
     */
    public void setTstat(TrackStatus tstat) {
        this.tstat = tstat;
    }

    /**
     * @return the cttype
     */
    public int getCttype() {
        return cttype;
    }

    /**
     * @param cttype the cttype to set
     */
    public void setCttype(int cttype) {
        this.cttype = cttype;
    }

    /**
     * @return the etyky
     */
    public long getEtyky() {
        return etyky;
    }

    /**
     * @param etyky the etyky to set
     */
    public void setEtyky(long etyky) {
        this.etyky = etyky;
    }

    /**
     * @return the startedat
     */
    public Date getStartedat() {
        return startedat;
    }

    /**
     * @param startedat the startedat to set
     */
    public void setStartedat(Date startedat) {
        this.startedat = startedat;
    }

    /**
     * @return the kmtr
     */
    public double getKmtr() {
        return kmtr;
    }

    /**
     * @param kmtr the kmtr to set
     */
    public void setKmtr(double kmtr) {
        this.kmtr = kmtr;
    }

    /**
     * @return the endedat
     */
    public Date getEndedat() {
        return endedat;
    }

    /**
     * @param endedat the endedat to set
     */
    public void setEndedat(Date endedat) {
        this.endedat = endedat;
    }


    public Location getCurrentLoc() {
        return currentLoc;
    }

    public void setCurrentLoc(Location currentLoc) {
        this.currentLoc = currentLoc;
    }


    public int getTtype() {
        return ttype;
    }

    public void setTtype(int ttype) {
        this.ttype = ttype;
    }


    public double getCurrbat() {
        return currbat;
    }

    public void setCurrbat(double currbat) {
        this.currbat = currbat;
    }


    public Date getTrackUpdatedAt() {
        return trackUpdatedAt;
    }

    public void setTrackUpdatedAt(Date trackUpdatedAt) {
        this.trackUpdatedAt = trackUpdatedAt;
    }


    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getDisTra() {
        return disTra;
    }

    public void setDisTra(double disTra) {
        this.disTra = disTra;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public CurrentTrack(int cttype, long etyky) {
        super();
        this.cttype = cttype;
        this.etyky = etyky;
        this.currbat = -1;

    }

    public CurrentTrack() {
        super();
    }


}
