package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class EpfDetails {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long epfId;

    private String uanNumber;

    private String prevPfMemId;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date doe;

    private String schemeCerNum;

    private String penPayOrdNum;

    private boolean internationalWorker;

    private String originCountry;

    private String passportNum;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date validFrom;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date validTo;

    @JsonIgnore
    @Column(name = "createdAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt = null;

    @JsonIgnore
    @Column(name = "updatedAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt = null;

    public long getEpfId() {
        return epfId;
    }

    public void setEpfId(long epfId) {
        this.epfId = epfId;
    }

    public String getUanNumber() {
        return uanNumber;
    }

    public void setUanNumber(String uanNumber) {
        this.uanNumber = uanNumber;
    }

    public String getPrevPfMemId() {
        return prevPfMemId;
    }

    public void setPrevPfMemId(String prevPfMemId) {
        this.prevPfMemId = prevPfMemId;
    }

    public Date getDoe() {
        return doe;
    }

    public void setDoe(Date doe) {
        this.doe = doe;
    }

    public String getSchemeCerNum() {
        return schemeCerNum;
    }

    public void setSchemeCerNum(String schemeCerNum) {
        this.schemeCerNum = schemeCerNum;
    }

    public String getPenPayOrdNum() {
        return penPayOrdNum;
    }

    public void setPenPayOrdNum(String penPayOrdNum) {
        this.penPayOrdNum = penPayOrdNum;
    }

    public boolean isInternationalWorker() {
        return internationalWorker;
    }

    public void setInternationalWorker(boolean internationalWorker) {
        this.internationalWorker = internationalWorker;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "EpfDetails [epfId=" + epfId + ", uanNumber=" + uanNumber + ", prevPfMemId=" + prevPfMemId + ", doe="
                + doe + ", schemeCerNum=" + schemeCerNum + ", penPayOrdNum=" + penPayOrdNum + ", internationalWorker="
                + internationalWorker + ", originCountry=" + originCountry + ", passportNum=" + passportNum
                + ", validFrom=" + validFrom + ", validTo=" + validTo + ", createdAt=" + createdAt + ", updatedAt="
                + updatedAt + "]";
    }


}
