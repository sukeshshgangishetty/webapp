package com.workforce.webapp.model.device;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.enums.device.VehicleType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Entity
public class DeviceDetail {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String devDetId;

    private String driName;

    private String vehicleNo;

    private String driLicNo;

    private String driPhone;

    private Double maxSpeed;

    private Double millage;

    @OneToOne
    @JsonIgnore
    private Device device;

    @Enumerated(EnumType.STRING)
    private VehicleType vehicleType;

    private String pastDriver;

    private Long orgId;

}
