package com.workforce.webapp.model.device;

import javax.persistence.*;

import com.workforce.webapp.enums.device.DeviceStatus;
import com.workforce.webapp.model.*;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import com.sun.istack.NotNull;
import org.hibernate.annotations.GenericGenerator;

@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Entity
public class Device extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String deviceId;

    @Column(unique = true)
    private String imei;

    @NotNull
    private String deviceNumber;

    private Long lastRenewal;

    private Long nextRenewal;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DeviceStatus status;

    private Long orgId;

    private Location locCurrent = new Location();

    private Long lastActive;

	/*@OneToOne(mappedBy = "trackingDevice", cascade = CascadeType.ALL)
	private CurrentTrack currentTrack;*/

}
