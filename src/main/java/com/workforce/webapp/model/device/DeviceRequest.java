package com.workforce.webapp.model.device;

import com.workforce.webapp.enums.device.DeviceStatus;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class DeviceRequest {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private Integer deviceCount;

    private Long orgId;

    private String comment;

    private Boolean quotationRequired;

    private Long requiredBy;

    private DeviceStatus status;

}
