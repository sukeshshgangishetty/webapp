package com.workforce.webapp.model.device;

import com.workforce.webapp.enums.device.DeviceStatus;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class DeviceComplaint {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private Long orgId;

    private String subject;

    private String description;

    private String emailId;

    private DeviceStatus status;

    @ManyToOne
    @JoinColumn(name = "device_id")
    private Device device;

}
