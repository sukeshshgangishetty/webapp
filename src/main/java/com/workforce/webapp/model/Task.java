package com.workforce.webapp.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long tid;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    @ManyToOne
    private Client client;

    private String others;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date expstartDate;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date expEndDate;

    private String address;

    @Column(nullable = false)
    private TaskPriority priority;

    @ElementCollection
    private Collection<String> attachments;

    private TaskAccessibility accessibility;


    private boolean needReminder;

    private TaskStatus status;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date reminderTime;

    @ManyToOne
    private Organization org;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date startedAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date endedAt;

    @ElementCollection
    private Collection<Long> executives;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @UpdateTimestamp
    Date updatedAt;

    public long getTid() {
        return tid;
    }

    public void setTid(long tid) {
        this.tid = tid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public Date getExpstartDate() {
        return expstartDate;
    }

    public void setExpstartDate(Date expstartDate) {
        this.expstartDate = expstartDate;
    }

    public Date getExpEndDate() {
        return expEndDate;
    }

    public void setExpEndDate(Date expEndDate) {
        this.expEndDate = expEndDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public Collection<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(Collection<String> attachments) {
        this.attachments = attachments;
    }

    public TaskAccessibility getAccessibility() {
        return accessibility;
    }

    public void setAccessibility(TaskAccessibility accessibility) {
        this.accessibility = accessibility;
    }

    public boolean isNeedReminder() {
        return needReminder;
    }

    public void setNeedReminder(boolean needReminder) {
        this.needReminder = needReminder;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public Date getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(Date reminderTime) {
        this.reminderTime = reminderTime;
    }

    public Organization getOrg() {
        return org;
    }

    public void setOrg(Organization org) {
        this.org = org;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Date endedAt) {
        this.endedAt = endedAt;
    }

    public Collection<Long> getExecutives() {
        return executives;
    }

    public void setExecutives(Collection<Long> executives) {
        this.executives = executives;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Task(long tid, String title, String description, Client client, String others, Date expstartDate,
                Date expEndDate, String address, TaskPriority priority, Collection<String> attachments,
                TaskAccessibility accessibility, boolean needReminder, TaskStatus status, Date reminderTime,
                Organization org, Date createdAt, Date startedAt, Date endedAt, Collection<Long> executives,
                Date updatedAt) {
        super();
        this.tid = tid;
        this.title = title;
        this.description = description;
        this.client = client;
        this.others = others;
        this.expstartDate = expstartDate;
        this.expEndDate = expEndDate;
        this.address = address;
        this.priority = priority;
        this.attachments = attachments;
        this.accessibility = accessibility;
        this.needReminder = needReminder;
        this.status = status;
        this.reminderTime = reminderTime;
        this.org = org;
        this.createdAt = createdAt;
        this.startedAt = startedAt;
        this.endedAt = endedAt;
        this.executives = executives;
        this.updatedAt = updatedAt;
    }

    public Task(String title, String description, Client client, String others, Date expstartDate, Date expEndDate,
                String address, TaskPriority priority, Collection<String> attachments, TaskAccessibility accessibility,
                boolean needReminder, TaskStatus status, Date reminderTime, Organization org, Date createdAt,
                Date startedAt, Date endedAt, Collection<Long> executives, Date updatedAt) {
        super();
        this.title = title;
        this.description = description;
        this.client = client;
        this.others = others;
        this.expstartDate = expstartDate;
        this.expEndDate = expEndDate;
        this.address = address;
        this.priority = priority;
        this.attachments = attachments;
        this.accessibility = accessibility;
        this.needReminder = needReminder;
        this.status = status;
        this.reminderTime = reminderTime;
        this.org = org;
        this.createdAt = createdAt;
        this.startedAt = startedAt;
        this.endedAt = endedAt;
        this.executives = executives;
        this.updatedAt = updatedAt;
    }

    public Task() {
        // TODO Auto-generated constructor stub
        super();

    }


}
