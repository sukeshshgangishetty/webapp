package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
public class OTP {

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long Id;

    String phone;

    int value;

    private Date generatedat;

    int otpfor;

    private String purpose;


    public Date getGeneratedat() {
        return generatedat;
    }

    public void setGeneratedat(Date generatedat) {
        this.generatedat = generatedat;
    }

    /**
     * @return the id
     */
    public long getId() {
        return Id;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getOtpfor() {
        return otpfor;
    }

    public void setOtpfor(int otpfor) {
        this.otpfor = otpfor;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public OTP(String phone, int value) {
        super();
        this.phone = phone;
        this.value = value;
        this.purpose = "org reg";
    }

    public OTP() {
        super();
    }


    public OTP(String phone, int value, int otpFor, String purpose) {
        super();
        this.phone = phone;
        this.value = value;
        this.otpfor = otpfor;
        this.purpose = purpose;
    }

    @Override
    public String toString() {
        return "{\"Id\":" + Id + ", phone\":" + phone + ", oTP\":" + value + ", generated\":" + generatedat + ", purpose\":"
                + purpose + "}";
    }

}
