package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class FamilyDetails {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long fdId;

    private String fatherName;

    private String fatherAadharNumber;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date fatherDob;

    private String motherName;

    private String motherAadharNumber;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date motherDob;

    private boolean residingWithU;

    private String spouseAadharNumber;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date spouseDob;


    @JsonIgnore
    @Column(name = "createdAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt = null;

    @JsonIgnore
    @Column(name = "updatedAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt = null;

    public long getFdId() {
        return fdId;
    }

    public void setFdId(long fdId) {
        this.fdId = fdId;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getFatherAadharNumber() {
        return fatherAadharNumber;
    }

    public void setFatherAadharNumber(String fatherAadharNumber) {
        this.fatherAadharNumber = fatherAadharNumber;
    }

    public Date getFatherDob() {
        return fatherDob;
    }

    public void setFatherDob(Date fatherDob) {
        this.fatherDob = fatherDob;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getMotherAadharNumber() {
        return motherAadharNumber;
    }

    public void setMotherAadharNumber(String motherAadharNumber) {
        this.motherAadharNumber = motherAadharNumber;
    }

    public Date getMotherDob() {
        return motherDob;
    }

    public void setMotherDob(Date motherDob) {
        this.motherDob = motherDob;
    }

    public boolean isResidingWithU() {
        return residingWithU;
    }

    public void setResidingWithU(boolean residingWithU) {
        this.residingWithU = residingWithU;
    }

    public String getSpouseAadharNumber() {
        return spouseAadharNumber;
    }

    public void setSpouseAadharNumber(String spouseAadharNumber) {
        this.spouseAadharNumber = spouseAadharNumber;
    }

    public Date getSpouseDob() {
        return spouseDob;
    }

    public void setSpouseDob(Date spouseDob) {
        this.spouseDob = spouseDob;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "FamilyDetails [fdId=" + fdId + ", fatherName=" + fatherName + ", fatherAadharNumber="
                + fatherAadharNumber + ", fatherDob=" + fatherDob + ", motherName=" + motherName
                + ", motherAadharNumber=" + motherAadharNumber + ", motherDob=" + motherDob + ", residingWithU="
                + residingWithU + ", spouseAadharNumber=" + spouseAadharNumber + ", spouseDob=" + spouseDob
                + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
    }


}
