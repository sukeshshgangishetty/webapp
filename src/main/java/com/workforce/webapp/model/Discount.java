package com.workforce.webapp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Discount implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5145889276096825077L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long dsid;


    DiscountType disType;

    long helpId;

    boolean isPer;

    double value;

    double minOrderValue;

    double maxDiscount;

    int maxOrders;

    int turnsForCustomer;

    boolean clubbed;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    Date applicableFrom;

    @JsonIgnore
    @ManyToOne
    Organization org;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @CreationTimestamp
    Date createdAt;

    public long getDsid() {
        return dsid;
    }

    public void setDsid(long dsid) {
        this.dsid = dsid;
    }

    public DiscountType getDisType() {
        return disType;
    }

    public void setDisType(DiscountType disType) {
        this.disType = disType;
    }

    public long getHelpId() {
        return helpId;
    }

    public void setHelpId(long helpId) {
        this.helpId = helpId;
    }

    public boolean isPer() {
        return isPer;
    }

    public void setPer(boolean isPer) {
        this.isPer = isPer;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getMinOrderValue() {
        return minOrderValue;
    }

    public void setMinOrderValue(double minOrderValue) {
        this.minOrderValue = minOrderValue;
    }

    public double getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(double maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public int getMaxOrders() {
        return maxOrders;
    }

    public void setMaxOrders(int maxOrders) {
        this.maxOrders = maxOrders;
    }

    public int getTurnsForCustomer() {
        return turnsForCustomer;
    }

    public void setTurnsForCustomer(int turnsForCustomer) {
        this.turnsForCustomer = turnsForCustomer;
    }

    public boolean isClubbed() {
        return clubbed;
    }

    public void setClubbed(boolean clubbed) {
        this.clubbed = clubbed;
    }

    public Organization getOrg() {
        return org;
    }

    public void setOrg(Organization org) {
        this.org = org;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Discount(long dsid, DiscountType disType, long helpId, boolean isPer, double value, double minOrderValue,
                    double maxDiscount, int maxOrders, int turnsForCustomer, boolean clubbed, Organization org,
                    Date createdAt) {
        super();
        this.dsid = dsid;
        this.disType = disType;
        this.helpId = helpId;
        this.isPer = isPer;
        this.value = value;
        this.minOrderValue = minOrderValue;
        this.maxDiscount = maxDiscount;
        this.maxOrders = maxOrders;
        this.turnsForCustomer = turnsForCustomer;
        this.clubbed = clubbed;
        this.org = org;
        this.createdAt = createdAt;
    }

    public Discount(DiscountType disType, long helpId, boolean isPer, double value, double minOrderValue,
                    double maxDiscount, int maxOrders, int turnsForCustomer, boolean clubbed, Organization org,
                    Date createdAt) {
        super();
        this.disType = disType;
        this.helpId = helpId;
        this.isPer = isPer;
        this.value = value;
        this.minOrderValue = minOrderValue;
        this.maxDiscount = maxDiscount;
        this.maxOrders = maxOrders;
        this.turnsForCustomer = turnsForCustomer;
        this.clubbed = clubbed;
        this.org = org;
        this.createdAt = createdAt;
    }

    public Discount() {
        super();
    }

    @Override
    public String toString() {
        return "Discount [dsid=" + dsid + ", disType=" + disType + ", helpId=" + helpId + ", isPer=" + isPer
                + ", value=" + value + ", minOrderValue=" + minOrderValue + ", maxDiscount=" + maxDiscount
                + ", maxOrders=" + maxOrders + ", turnsForCustomer=" + turnsForCustomer + ", clubbed=" + clubbed
                + ", org=" + org + ", createdAt=" + createdAt + "]";
    }


}
