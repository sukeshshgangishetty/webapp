package com.workforce.webapp.model;

public enum TaskAccessibility {
    GROUP, INDIVIDUAL, OTHER

}
