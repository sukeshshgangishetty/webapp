package com.workforce.webapp.model;

public enum AttendenceCurrentStatus {

    AVAILABLE, NOT_AVAILABLE

}
