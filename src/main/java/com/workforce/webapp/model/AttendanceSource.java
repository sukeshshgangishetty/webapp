package com.workforce.webapp.model;

public enum AttendanceSource {

    BIOMETRIC, WEB, MOBILE, MANUAL, OTHERS
}
