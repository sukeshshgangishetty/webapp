package com.workforce.webapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class LeaveBalance {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long balanceid;

    @ManyToOne
    private LeaveTypes leaveType;

    private double leaveBalance;

    private double totalLeaves;

    @ManyToOne
    @JsonIgnore
    private Employee employee;

    public long getBalanceid() {
        return balanceid;
    }

    public void setBalanceid(long balanceid) {
        this.balanceid = balanceid;
    }

    public LeaveTypes getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(LeaveTypes leaveType) {
        this.leaveType = leaveType;
    }

    public double getLeaveBalance() {
        return leaveBalance;
    }

    public void setLeaveBalance(double leaveBalance) {
        this.leaveBalance = leaveBalance;
    }

    public double getTotalLeaves() {
        return totalLeaves;
    }

    public void setTotalLeaves(double totalLeaves) {
        this.totalLeaves = totalLeaves;
    }

    public LeaveBalance(long balanceid, LeaveTypes leaveType, double leaveBalance, double totalLeaves) {
        super();
        this.balanceid = balanceid;
        this.leaveType = leaveType;
        this.leaveBalance = leaveBalance;
        this.totalLeaves = totalLeaves;
    }

    public LeaveBalance(LeaveTypes leaveType, double leaveBalance, double totalLeaves) {
        super();
        this.leaveType = leaveType;
        this.leaveBalance = leaveBalance;
        this.totalLeaves = totalLeaves;
    }


    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public LeaveBalance(LeaveTypes leaveType, double leaveBalance, double totalLeaves, Employee employee) {
        super();
        this.leaveType = leaveType;
        this.leaveBalance = leaveBalance;
        this.totalLeaves = totalLeaves;
        this.employee = employee;
    }

    public LeaveBalance() {
        super();
    }

}
