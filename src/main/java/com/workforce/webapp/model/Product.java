package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Null;

import org.hibernate.annotations.CreationTimestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

@Entity
public class Product {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long pid;

    private String name;

    /*@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "catname")
    @JsonIdentityReference(alwaysAsId = true)
    */
    @NotNull
    @ManyToOne
    private ProductCategory pcat;


    private String units;

    private String partNumber;

    private String hsnCode;

    private double tax;

    private boolean taxable;

    @JsonIgnore
    @ManyToOne
    private UserDetails user;

    @CreationTimestamp
    @Column(updatable = false)
    @JsonFormat(pattern = "dd-MM-YYYY HH:mm:ss")
    private Date createdat;

    @ManyToOne
    @JsonIgnore
    Organization org;

    private double qtyPerCase;


    @Null
    private String image;


    public Organization getOrg() {
        return org;
    }

    public void setOrg(Organization org) {
        this.org = org;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getPid() {
        return pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductCategory getPcat() {
        return pcat;
    }

    public void setPcat(ProductCategory pcat) {
        this.pcat = pcat;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public Date getCreatedat() {
        return createdat;
    }

    public void setCreatedat(Date createdat) {
        this.createdat = createdat;
    }

    public double getQtyPerCase() {
        return qtyPerCase;
    }

    public void setQtyPerCase(double qtyPerCase) {
        this.qtyPerCase = qtyPerCase;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public boolean isTaxable() {
        return taxable;
    }

    public void setTaxable(boolean taxable) {
        this.taxable = taxable;
    }

    public UserDetails getUser() {
        return user;
    }


    public void setUser(UserDetails user) {
        this.user = user;
    }


    public void setPid(long pid) {
        this.pid = pid;
    }

    public Product(String name, ProductCategory pcat, double price, String units, UserDetails user, Date createdat,
                   double qtyPerCase, Organization org) {
        super();
        this.name = name;
        this.pcat = pcat;
        this.units = units;
        this.user = user;
        this.createdat = createdat;
        this.qtyPerCase = qtyPerCase;
        this.org = org;
    }

    public Product() {
        super();
    }

    public Product(String name, ProductCategory pcat, double price, String units, double qtyPerCase, Organization org) {
        super();
        this.name = name;
        this.pcat = pcat;
        this.units = units;
        this.qtyPerCase = qtyPerCase;
        this.org = org;
    }

    public Product(String name, ProductCategory pcat, double price, String units, String partNumber, String hsnCode,
                   double tax, boolean taxable, UserDetails user, Date createdat, Organization org, double qtyPerCase, String image) {
        super();
        this.name = name;
        this.pcat = pcat;
        this.units = units;
        this.partNumber = partNumber;
        this.hsnCode = hsnCode;
        this.tax = tax;
        this.taxable = taxable;
        this.user = user;
        this.createdat = createdat;
        this.org = org;
        this.qtyPerCase = qtyPerCase;
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product [pid=" + pid + ", name=" + name + ", pcat=" + pcat + ", units=" + units
                + ", partNumber=" + partNumber + ", hsnCode=" + hsnCode + ", tax=" + tax + ", taxable=" + taxable
                + ", user=" + user + ", createdat=" + createdat + ", org=" + org + ", qtyPerCase=" + qtyPerCase
                + ", image=" + image + "]";
    }

    public String[] getValuesArray(String upUrl) {
        String[] values = new String[11];
        values[0] = "" + this.pid;
        values[1] = "" + this.name;
        values[2] = "" + this.pcat.getCatname();
        values[4] = "" + this.units;
        values[5] = "" + this.tax;
        values[6] = "" + this.taxable;
        values[7] = "" + this.qtyPerCase;
        values[8] = "" + this.image;
        values[9] = "" + this.partNumber;
        values[10] = "" + this.hsnCode;
        return values;
    }

}


//caa4c80a40ea0cc561fe596fc4cf4427


