package com.workforce.webapp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.Nullable;

@Entity
public class UserDetails implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userid;

    // @Pattern(regexp = "^[a-z0-9]*$",message="Invalid login pattern")
    @Size(min = 1, max = 50)
    @JsonIgnore
    @Column(length = 50, unique = true, nullable = false)
    private String login = "k";

    @JsonIgnore
    // /@Size(min = 60, max = 60)
    @Column(length = 60)
    private String password = "";

    @Size(max = 50)
    @Column(name = "username", length = 50)
    private String username;

    @Size(max = 100)
    @Column(length = 100)
    private String email;

    @Size(max = 20)
    @Column(length = 20)
    private String phone;

    @JsonIgnore
    // retained for backwards compatibility. Can be removed once data is fixed.
    @Column(nullable = false)
    private boolean activated = false;

    @JsonIgnore
    // @Size(max = 20)
    @Column(name = "activation_key", length = 32)
    private String activationKey;

    @JsonIgnore
    @Size(max = 20)
    @Column(name = "reset_key", length = 32)
    private String resetKey;

    @JsonIgnore
    @Column(name = "reset_date", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date resetDate = null;

    @JsonIgnore
    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @Enumerated(EnumType.STRING)
    private Userrole role;

    long entityKey;

    @JsonIgnore
    @Column(name = "token", length = 32)
    private String token;

    @JsonIgnore
    @Column(name = "tokenGeneratedAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date tokenGeneratedAt = null;

    @JsonIgnore
    @Size(max = 255)
    @Nullable
    private String fbTok;

    private boolean reportingManager;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    public UserStatus getStatus() {
        return status;
    }
    // 08f820316b7f4442884bd4c237b1218e

    public void setStatus(UserStatus status) {
        this.status = status;
        // For backwards compatibility
        if (status == UserStatus.ACTIVE) {
            activated = true;
        } else {
            activated = false;
        }
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setLastName(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean getActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public Date getResetDate() {
        return resetDate;
    }

    public void setResetDate(Date resetDate) {
        this.resetDate = resetDate;
    }

    /**
     * @return the userid
     */
    public long getUserid() {
        return userid;
    }

    /**
     * @param userid the userid to set
     */
    public void setUserid(long userid) {
        this.userid = userid;
    }

    /**
     * @return the role
     */
    public Userrole getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Userrole role) {
        this.role = role;
    }

    /**
     * @return the entityKey
     */
    public long getEntityKey() {
        return entityKey;
    }

    /**
     * @param entityKey the entityKey to set
     */
    public void setEntityKey(long entityKey) {
        this.entityKey = entityKey;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserDetails user = (UserDetails) o;

        if (!this.login.equals(user.login)) {
            return false;
        }

        return true;
    }

    /**
     * @return the tokenGeneratedAt
     */
    public Date getTokenGeneratedAt() {
        return tokenGeneratedAt;
    }

    /**
     * @param tokenGeneratedAt the tokenGeneratedAt to set
     */
    public void setTokenGeneratedAt() {
        this.tokenGeneratedAt = new Date();
    }

    public String getFbTok() {
        return fbTok;
    }

    public void setFbTok(String fbTok) {
        this.fbTok = fbTok;
    }

    public void setTokenGeneratedAt(Date tokenGeneratedAt) {
        this.tokenGeneratedAt = tokenGeneratedAt;
    }



    /*
     * @Override public String toString() { return "User{" + "login='" + login +
     * '\'' + ", password='" + password + '\'' + ", username='" + username +
     * '\'' + ", email='" + email + '\'' + ", activated='" + activated + '\'' +
     * ", activationKey='" + activationKey + '\'' + ", token='" + token + '\'' +
     * ", Role='" + role + '\'' + "}"; }
     */

    public boolean isReportingManager() {
        return reportingManager;
    }

    public void setReportingManager(boolean reportingManager) {
        this.reportingManager = reportingManager;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getLangKey() {
        // TODO Auto-generated method stub
        return "en";
    }

    @Override
    public String toString() {
        return "User [userid=" + userid + ", login=" + login + ", password=" + password + ", username=" + username
                + ", email=" + email + ", phone=" + phone + ", activated=" + activated + ", activationKey="
                + activationKey + ", resetKey=" + resetKey + ", resetDate=" + resetDate + ", status=" + status
                + ", role=" + role + ", entityKey=" + entityKey + ", token=" + token + ", tokenGeneratedAt="
                + tokenGeneratedAt + ", fbTok=" + fbTok + "]";
    }
}
