package com.workforce.webapp.model;

public enum RegularizationType {

    MISS_PUNCH, UNABLE_TO_PUNCH, FORGOT_TO_PUNCH, OTHERS
}
