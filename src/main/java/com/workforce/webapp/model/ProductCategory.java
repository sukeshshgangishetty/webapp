package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
public class ProductCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long catid;

    String catname;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    @JsonFormat(pattern = "dd-MM-YYYY HH:mm:ss")
    Date createddate;


    @CreatedBy
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "userid")
    @JsonIdentityReference(alwaysAsId = true)
    @ManyToOne
    @JsonIgnore
    UserDetails user;


    @ManyToOne
    @JsonIgnore
    Organization org;


    public long getCatid() {
        return catid;
    }

    public String getCatname() {
        return catname;
    }


    public void setCatname(String catname) {
        this.catname = catname;
    }


    public Date getCreateddate() {
        return createddate;
    }


    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }


    public UserDetails getUser() {
        return user;
    }


    public void setUser(UserDetails user) {
        this.user = user;
    }


    public Organization getOrg() {
        return org;
    }


    public void setOrg(Organization org) {
        this.org = org;
    }


    public ProductCategory(String catname, Date createddate, UserDetails user, Organization org) {
        super();
        this.catname = catname;
        this.createddate = createddate;
        this.user = user;
        this.org = org;
    }


    public ProductCategory() {
        super();
    }

    @Override
    public String toString() {
        return "ProductCategory [catid=" + catid + ", catname=" + catname + ", createddate=" + createddate + ", user="
                + user + ", org=" + org + "]";
    }


}
