package com.workforce.webapp.model;

public enum AttendanceAction {

    PUNCH_IN, PUNCH_OUT, OTHERS
}
