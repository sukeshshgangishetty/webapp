package com.workforce.webapp.model;

public enum AttendanceIndication {

    LATE_LOGIN, EARLY_LOGOUT, OVERTIME, ON_TIME, OTHERS

}
