package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class CRMProductPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long crmpplanId;

    private String name;

    @Column(unique = true)
    private String planCode;

    @ManyToOne
    private CRMProduct product;

    private String unitName;

    private double price;

    private int billingMonths;

    private int freeTrial;

    private double setupfee;


    private String planDesp;


    public CRMProduct getProduct() {
        return product;
    }

    public void setProduct(CRMProduct product) {
        this.product = product;
    }

    public long getCrmpplanId() {
        return crmpplanId;
    }

    public void setCrmpplanId(long crmpplanId) {
        this.crmpplanId = crmpplanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getBillingMonths() {
        return billingMonths;
    }

    public void setBillingMonths(int billingMonths) {
        this.billingMonths = billingMonths;
    }

    public int getFreeTrial() {
        return freeTrial;
    }

    public void setFreeTrial(int freeTrial) {
        this.freeTrial = freeTrial;
    }

    public double getSetupfee() {
        return setupfee;
    }

    public void setSetupfee(double setupfee) {
        this.setupfee = setupfee;
    }

    public String getPlanDesp() {
        return planDesp;
    }

    public void setPlanDesp(String planDesp) {
        this.planDesp = planDesp;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;


}
