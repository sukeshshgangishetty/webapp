package com.workforce.webapp.model;

public enum OrderType {

    EXECUTIVE_PLACED, CLIENT_SELF_PLACED, OTHERS
}
