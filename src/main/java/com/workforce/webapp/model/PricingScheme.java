package com.workforce.webapp.model;

public enum PricingScheme {

    UNIT, VOLUME, TIER, PACKAGE

}
