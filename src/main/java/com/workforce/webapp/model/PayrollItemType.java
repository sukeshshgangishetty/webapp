package com.workforce.webapp.model;

public enum PayrollItemType {

    EARNING, DEDUCTIONS, COMPANY_EXPENSES, OTHERS
}
