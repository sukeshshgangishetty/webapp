package com.workforce.webapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
@Entity
public class DispatchListItem {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @JsonIgnore
    @ManyToOne
    private DispatchList dplist;


    @ManyToOne
    PriceList pricelist;

    double qty;

    long ordersInvolved;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DispatchList getDplist() {
        return dplist;
    }

    public void setDplist(DispatchList dplist) {
        this.dplist = dplist;
    }

    public PriceList getPricelist() {
        return pricelist;
    }

    public void setPricelist(PriceList pricelist) {
        this.pricelist = pricelist;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public long getOrdersInvolved() {
        return ordersInvolved;
    }

    public void setOrdersInvolved(long ordersInvolved) {
        this.ordersInvolved = ordersInvolved;
    }

    public DispatchListItem(DispatchList dplist, PriceList pricelist, double qty, long ordersInvolved) {
        super();
        this.dplist = dplist;
        this.pricelist = pricelist;
        this.qty = qty;
        this.ordersInvolved = ordersInvolved;
    }

    public DispatchListItem(long id, DispatchList dplist, PriceList pricelist, double qty, long ordersInvolved) {
        super();
        this.id = id;
        this.dplist = dplist;
        this.pricelist = pricelist;
        this.qty = qty;
        this.ordersInvolved = ordersInvolved;
    }

    public DispatchListItem() {
        super();
    }

}
	

