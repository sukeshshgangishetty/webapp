package com.workforce.webapp.model;

public enum IdProofType {

    AADHAR, PASSPORT, VOTER, ELECTRICITYBILL
}
