package com.workforce.webapp.model;

public enum LeaveNotation {

    FULL_DAY, FIRST_HALF, SECOND_HALF, OTHERS
}
