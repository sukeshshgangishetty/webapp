package com.workforce.webapp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.istack.Nullable;


@Entity
public class CrmUser implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3961062082702112073L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long crmusid;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, updatable = false)
    private String phone;

    private String imglogo = "";


    @Column(nullable = false)
    private Address address;

    private String email;


    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date joinedDate;

    @Nullable
    private String vehicleNumber;


    private String pfNumber;


    private String esiNumber;

    @Nullable
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dob;


    @Nullable
    @Column(length = 12)
    private String aadhaarNumber;

    @Nullable
    private String panCardNumber;

    @Nullable
    private String qualification;

    @Nullable
    private String remarks;

    public String getImglogo() {
        return imglogo;
    }


    public void setImglogo(String imglogo) {
        this.imglogo = imglogo;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public Date getJoinedDate() {
        return joinedDate;
    }


    public void setJoinedDate(Date joinedDate) {
        this.joinedDate = joinedDate;
    }


    public String getVehicleNumber() {
        return vehicleNumber;
    }


    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }


    public String getPfNumber() {
        return pfNumber;
    }


    public void setPfNumber(String pfNumber) {
        this.pfNumber = pfNumber;
    }


    public String getEsiNumber() {
        return esiNumber;
    }


    public void setEsiNumber(String esiNumber) {
        this.esiNumber = esiNumber;
    }


    public Date getDob() {
        return dob;
    }


    public void setDob(Date dob) {
        this.dob = dob;
    }


    public String getAadhaarNumber() {
        return aadhaarNumber;
    }


    public void setAadhaarNumber(String aadhaarNumber) {
        this.aadhaarNumber = aadhaarNumber;
    }


    public String getPanCardNumber() {
        return panCardNumber;
    }


    public void setPanCardNumber(String panCardNumber) {
        this.panCardNumber = panCardNumber;
    }


    public String getQualification() {
        return qualification;
    }


    public void setQualification(String qualification) {
        this.qualification = qualification;
    }


    public String getRemarks() {
        return remarks;
    }


    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }


    public long getCrmusid() {
        return crmusid;
    }


    public void setCrmusid(long crmusid) {
        this.crmusid = crmusid;
    }


    public CrmUser(String name, String phone, Address address) {
        super();

        this.name = name;
        this.phone = phone;
        this.address = address;
    }


    public CrmUser(String name, String phone, String imglogo, Address address, String email,
                   Date joinedDate, String vehicleNumber, String pfNumber, String esiNumber, Date dob, String aadhaarNumber,
                   String panCardNumber, String qualification, String remarks) {
        super();
        this.name = name;
        this.phone = phone;
        this.imglogo = imglogo;
        this.address = address;
        this.email = email;
        this.joinedDate = joinedDate;
        this.vehicleNumber = vehicleNumber;
        this.pfNumber = pfNumber;
        this.esiNumber = esiNumber;
        this.dob = dob;
        this.aadhaarNumber = aadhaarNumber;
        this.panCardNumber = panCardNumber;
        this.qualification = qualification;
        this.remarks = remarks;
    }


    public CrmUser() {
        super();
    }

    public String[] getValuesArray(String upUrl) {
        String[] values = new String[19];
        values[0] = "" + this.crmusid;
        values[1] = "" + this.name;
        values[2] = "" + this.phone;
        values[3] = "" + this.email;
        values[4] = "" + this.imglogo;
        values[5] = this.address != null ? "" + this.address.getHno() : "";
        values[6] = this.address != null ? "" + this.address.getStreetname() : "";
        values[7] = this.address != null ? "" + this.address.getCity() : "";
        values[8] = this.address != null ? "" + this.address.getState() : "";
        values[9] = this.address != null ? "" + this.address.getPincode() : "";
        values[10] = "" + this.joinedDate;
        values[11] = "" + this.vehicleNumber;
        values[12] = "" + this.pfNumber;
        values[13] = "" + this.esiNumber;
        values[14] = "" + this.dob;
        values[15] = "" + this.aadhaarNumber;
        values[16] = "" + this.panCardNumber;
        values[17] = "" + this.qualification;
        values[18] = "" + this.remarks;
        return values;
    }


    @Override
    public String toString() {
        return "CRMUser [crmuserid=" + crmusid + ", name=" + name + ", phone=" + phone + ", imglogo=" + imglogo
                + ", address=" + address + ", email=" + email + ", joinedDate=" + joinedDate
                + ", vehicleNumber=" + vehicleNumber + ", pfNumber=" + pfNumber + ", esiNumber=" + esiNumber + ", dob="
                + dob + ", aadhaarNumber=" + aadhaarNumber + ", panCardNumber=" + panCardNumber + ", qualification="
                + qualification + ", remarks=" + remarks + "]";
    }


}
