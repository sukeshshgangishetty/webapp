package com.workforce.webapp.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class SubTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long subTaskId;

    @Column(nullable = false)
    private String title;

    private String description;

    @ManyToOne
    private Client client;

    private String other;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date expectedStartTime;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date expectedendTime;

    private TaskPriority priority;

    @ElementCollection
    private Set<String> attachments;


    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @CreationTimestamp
    private Date createdAt;

    @ManyToOne
    @JsonIgnore
    private Task task;

    public long getSubTaskId() {
        return subTaskId;
    }

    public void setSubTaskId(long subTaskId) {
        this.subTaskId = subTaskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Date getExpectedStartTime() {
        return expectedStartTime;
    }

    public void setExpectedStartTime(Date expectedStartTime) {
        this.expectedStartTime = expectedStartTime;
    }

    public Date getExpectedendTime() {
        return expectedendTime;
    }

    public void setExpectedendTime(Date expectedendTime) {
        this.expectedendTime = expectedendTime;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    public void setPriority(TaskPriority priority) {
        this.priority = priority;
    }

    public Set<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<String> attachments) {
        this.attachments = attachments;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public SubTask(long subTaskId, String title, String description, Client client, String other,
                   Date expectedStartTime, Date expectedendTime, TaskPriority priority, Set<String> attachments,
                   Date createdAt, Task task) {
        super();
        this.subTaskId = subTaskId;
        this.title = title;
        this.description = description;
        this.client = client;
        this.other = other;
        this.expectedStartTime = expectedStartTime;
        this.expectedendTime = expectedendTime;
        this.priority = priority;
        this.attachments = attachments;
        this.createdAt = createdAt;
        this.task = task;
    }

    public SubTask(String title, String description, Client client, String other, Date expectedStartTime,
                   Date expectedendTime, TaskPriority priority, Set<String> attachments, Date createdAt, Task task) {
        super();
        this.title = title;
        this.description = description;
        this.client = client;
        this.other = other;
        this.expectedStartTime = expectedStartTime;
        this.expectedendTime = expectedendTime;
        this.priority = priority;
        this.attachments = attachments;
        this.createdAt = createdAt;
        this.task = task;
    }

    public SubTask() {
        super();
    }


}
