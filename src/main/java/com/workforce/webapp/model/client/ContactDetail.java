package com.workforce.webapp.model.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class ContactDetail extends BaseEntity {

    @Id
    @Column(name = "client_id")
    private String id;

    private String websiteUrl;

    private String fbUrl;

    private String twitterUrl;

    private String instaUrl;

    private String linkedinUrl;

    private String gPlusUrl;

    @OneToOne
    @MapsId
    @JoinColumn(name = "client_id")
    @JsonIgnore
    private ClientEntity clientEntity;

}
