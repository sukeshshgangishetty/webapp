package com.workforce.webapp.model.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class Category extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private String name;

    private String description;

    private Long orgId;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "category",
            orphanRemoval = true)
    @JsonIgnore
    private List<ClientEntity> clientEntities = new ArrayList<>();

    @PreRemove
    public void checkReviewAssociationBeforeRemoval() {
        if (!this.clientEntities.isEmpty()) {
            throw new RuntimeException("Can't remove a Category that has Clients.");
        }
    }


}
