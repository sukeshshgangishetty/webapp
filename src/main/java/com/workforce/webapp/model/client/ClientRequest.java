package com.workforce.webapp.model.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.enums.client.RequestStatus;
import com.workforce.webapp.enums.client.RequestType;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class ClientRequest extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private String title;

    private String description;

    @Enumerated(EnumType.STRING)
    private RequestType requestType;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id")
    @JsonIgnore
    private ClientEntity clientEntity;

    private Long orgId;

    @Column(columnDefinition = "text")
    private String clientData;

    @Enumerated(EnumType.STRING)
    private RequestStatus status;
}
