package com.workforce.webapp.model.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class KYCDetail extends BaseEntity {

    @Id
    @Column(name = "client_id")
    private String id;

    private String name;

    private Long dob;

    private String panCard;

    private String aadhaarNo;

    private Boolean isVerified;

    private Boolean isPanVerified;

    private Boolean isAadhaarVerified;

    @OneToOne
    @MapsId
    @JoinColumn(name = "client_id")
    @JsonIgnore
    private ClientEntity clientEntity;
}
