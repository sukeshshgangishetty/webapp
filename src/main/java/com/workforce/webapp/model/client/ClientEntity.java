package com.workforce.webapp.model.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.model.Location;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class ClientEntity extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private String name;

    private String description;

    private String phoneNo;

    private String landlineNo;

    private String email;

    @Column(nullable = true)
    private Location location;

    private Long incorporatedAt;

    private String gstNo;

    private Long orgId;

    private Long assignedTo;

    private Double openingBalance;

    private Integer noOfEmp;

    private Double outstanding;

    private Double creditLimit;

    private Integer repayPeriod;

    private Boolean isActive;

    @OneToOne(mappedBy = "clientEntity", cascade = CascadeType.ALL)
    private ContactDetail contactDetail;

    @OneToOne(mappedBy = "clientEntity", cascade = CascadeType.ALL)
    private KYCDetail kycDetail;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "clientEntity",
            orphanRemoval = true)
    private List<OwnerDetail> ownerDetails = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "clientEntity",
            orphanRemoval = true)
    private List<Address> addresses = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "clientEntity",
            orphanRemoval = true)
    private List<AccountDetail> accountDetails = new ArrayList<>();

    public List<OwnerDetail> getOwnerDetails() {
        return ownerDetails;
    }

    public void setOwnerDetails(List<OwnerDetail> ownerDetails) {
        this.ownerDetails.addAll(ownerDetails);
    }

}
