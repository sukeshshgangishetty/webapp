package com.workforce.webapp.model;

public enum TaskPriority {
    HIGH, MEDIUM, LOW, OTHERS

}
