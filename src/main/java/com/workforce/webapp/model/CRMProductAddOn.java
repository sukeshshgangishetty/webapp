package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class CRMProductAddOn {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long crmpaddOn;

    private String name;

    @Column(unique = true)
    private String addonCode;

    private String unitName;

    private PriceIntervel priceIntervel;

    private PricingScheme pricingScheme;

    private String addonDesp;

    @ManyToOne
    private CRMProduct product;


    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @UpdateTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public long getCrmpaddOn() {
        return crmpaddOn;
    }

    public void setCrmpaddOn(long crmpaddOn) {
        this.crmpaddOn = crmpaddOn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddonCode() {
        return addonCode;
    }

    public void setAddonCode(String addonCode) {
        this.addonCode = addonCode;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public PriceIntervel getPriceIntervel() {
        return priceIntervel;
    }

    public void setPriceIntervel(PriceIntervel priceIntervel) {
        this.priceIntervel = priceIntervel;
    }

    public PricingScheme getPricingScheme() {
        return pricingScheme;
    }

    public void setPricingScheme(PricingScheme pricingScheme) {
        this.pricingScheme = pricingScheme;
    }

    public String getAddonDesp() {
        return addonDesp;
    }

    public void setAddonDesp(String addonDesp) {
        this.addonDesp = addonDesp;
    }

    public CRMProduct getProduct() {
        return product;
    }

    public void setProduct(CRMProduct product) {
        this.product = product;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }


}
