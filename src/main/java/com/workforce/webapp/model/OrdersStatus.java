package com.workforce.webapp.model;

public enum OrdersStatus {
    ADDED, ACCEPTED, REJECTED, ON_HOLD, ADDED_TO_DISPATCH, OTHERS
}
