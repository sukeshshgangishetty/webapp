package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Organization {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;


    @Column(name = "orgname", nullable = false, updatable = false)
    @NotNull
    private String orgname;


    @Column(name = "contactname", nullable = false)
    @NotNull
    private String contactname;


    @Column(name = "contactemail")
    private String contactemail;

    @Column(name = "contactphone", updatable = false, unique = true)
    @NotNull
    private String contactphone;


    @Column(name = "logo")
    private String logoUrl;


    @Column(name = "location", nullable = false)
    @NotNull
    private String location;

    private OrganizationStatus status;


    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @JsonIgnore
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss ")
    @Column(updatable = false)
    private Date regat;


    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public String getOrgname() {
        return orgname;
    }


    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }


    public String getContactname() {
        return contactname;
    }


    public void setContactname(String contactname) {
        this.contactname = contactname;
    }


    public String getContactemail() {
        return contactemail;
    }


    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }


    public String getContactphone() {
        return contactphone;
    }


    public void setContactphone(String contactphone) {
        this.contactphone = contactphone;
    }


    public String getLogoUrl() {
        return logoUrl;
    }


    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }


    public String getLocation() {
        return location;
    }


    public void setLocation(String location) {
        this.location = location;
    }


    public Date getRegat() {
        return regat;
    }


    public void setRegat(Date regat) {
        this.regat = regat;
    }


    public OrganizationStatus getStatus() {
        return status;
    }


    public void setStatus(OrganizationStatus status) {
        this.status = status;
    }


    public Organization(long id, String orgname, String contactname, String contactemail, String contactphone,
                        String logoUrl, String location, Date regat) {
        super();
        this.id = id;
        this.orgname = orgname;
        this.contactname = contactname;
        this.contactemail = contactemail;
        this.contactphone = contactphone;
        this.logoUrl = logoUrl;
        this.location = location;
        this.regat = regat;
    }

    public Organization(String orgname, String contactname, String contactemail, String contactphone,
                        String logoUrl, String location, Date regat) {
        super();
        this.orgname = orgname;
        this.contactname = contactname;
        this.contactemail = contactemail;
        this.contactphone = contactphone;
        this.logoUrl = logoUrl;
        this.location = location;
        this.regat = regat;
    }

    public Organization() {
        super();
    }


    @Override
    public String toString() {
        return "Organization [id=" + id + ", orgname=" + orgname + ", contactname=" + contactname + ", contactemail="
                + contactemail + ", contactphone=" + contactphone + ", logoUrl=" + logoUrl + ", location=" + location
                + ", regat=" + regat + "]";
    }


}
