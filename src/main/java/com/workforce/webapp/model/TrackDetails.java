package com.workforce.webapp.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class TrackDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long tid;


    @NotNull
    @JsonIgnore
    private long entky;

    @NotNull
    @JsonIgnore
    private long ctrk;


    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "MM/dd/yyyy HH:mm:ss")
    @Column(nullable = false, updatable = false)
    Date updatedAt = new Date();

    //@OneToMany

    /**
     * @return the ctrk
     */
    public long getCtrk() {
        return ctrk;
    }


    /**
     * @param ctrk the ctrk to set
     */
    public void setCtrk(long ctrk) {
        this.ctrk = ctrk;
    }

    @NotNull
    private Location loc;


    private double bat;


    /**
     * @return the tid
     */
    public long getTid() {
        return tid;
    }


    /**
     * @param tid the tid to set
     */
    public void setTid(long tid) {
        this.tid = tid;
    }


    /**
     * @return the entky
     */
    public long getEntky() {
        return entky;
    }


    /**
     * @param entky the entky to set
     */
    public void setEntky(long entky) {
        this.entky = entky;
    }


    /**
     * @return the loc
     */
    public Location getLoc() {
        return loc;
    }


    /**
     * @param loc the loc to set
     */
    public void setLoc(Location loc) {
        this.loc = loc;
    }


    /**
     * @return the bat
     */
    public double getBat() {
        return bat;
    }


    /**
     * @param bat the bat to set
     */
    public void setBat(double bat) {
        this.bat = bat;
    }


    public TrackDetails(long entky, Location loc, double bat, long ctrk) {
        super();
        this.entky = entky;
        this.loc = loc;
        this.bat = bat;
        this.ctrk = ctrk;
    }

    public TrackDetails() {
        super();

    }


}
