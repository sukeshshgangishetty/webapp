package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class AttendanceRegularization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long attRegId;

    private AttendanceAction action;

    @ManyToOne
    private Employee emp;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date regularizationDate;


    @Temporal(TemporalType.TIME)
    @JsonFormat(pattern = "HH:mm:ss")
    private Date actionTime;

    private RegularizationType type;

    private String comments;


    private RegularizationAction regAction;

    private long refId;

    @ManyToOne
    private UserDetails regularizedBy;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;


    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date updatedAt;


    public Date getRegularizationDate() {
        return regularizationDate;
    }


    public void setRegularizationDate(Date regularizationDate) {
        this.regularizationDate = regularizationDate;
    }


    public long getAttRegId() {
        return attRegId;
    }


    public void setAttRegId(long attRegId) {
        this.attRegId = attRegId;
    }


    public AttendanceAction getAction() {
        return action;
    }


    public void setAction(AttendanceAction action) {
        this.action = action;
    }


    public Employee getEmp() {
        return emp;
    }


    public void setEmp(Employee emp) {
        this.emp = emp;
    }


    public Date getActionTime() {
        return actionTime;
    }


    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }


    public RegularizationType getType() {
        return type;
    }


    public void setType(RegularizationType type) {
        this.type = type;
    }


    public String getComments() {
        return comments;
    }


    public void setComments(String comments) {
        this.comments = comments;
    }


    public Date getCreatedAt() {
        return createdAt;
    }


    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }


    public Date getUpdatedAt() {
        return updatedAt;
    }


    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }


    public RegularizationAction getRegAction() {
        return regAction;
    }


    public void setRegAction(RegularizationAction regAction) {
        this.regAction = regAction;
    }


    public long getRefId() {
        return refId;
    }


    public void setRefId(long refId) {
        this.refId = refId;
    }


    public UserDetails getRegularizedBy() {
        return regularizedBy;
    }


    public void setRegularizedBy(UserDetails regularizedBy) {
        this.regularizedBy = regularizedBy;
    }


    public AttendanceRegularization(long attRegId, AttendanceAction action, Employee emp, Date actionTime,
                                    RegularizationType type, String comments, Date createdAt, Date updatedAt) {
        super();
        this.attRegId = attRegId;
        this.action = action;
        this.emp = emp;
        this.actionTime = actionTime;
        this.type = type;
        this.comments = comments;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }


    public AttendanceRegularization(AttendanceAction action, Employee emp, Date actionTime, RegularizationType type,
                                    String comments) {
        super();
        this.action = action;
        this.emp = emp;
        this.actionTime = actionTime;
        this.type = type;
        this.comments = comments;
    }


    public AttendanceRegularization() {
        super();
    }


}
