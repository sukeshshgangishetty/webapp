package com.workforce.webapp.model;

public enum ClientAccoutStatus {

    NO_ACCOUNT, ACCOUNT_IN_ACTIVE, ACCOUNT_IN_DEACTIVE
}
