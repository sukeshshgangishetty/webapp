package com.workforce.webapp.model;

public enum OrganizationStatus {
    ACTIVE, INACTIVE, SUSPENDED
}
