package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class EmployeeOboardingDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long empObDetId;

    @OneToOne
    private Employee emp;

    @OneToOne
    private PersonalDetails personalDetails;

    @OneToOne
    private EducationalDetails educationDetails;

    @OneToOne
    private ExperienceDetails experianceDetails;

    @OneToOne
    private KycDetails kycDetails;

    @OneToOne
    private EpfDetails epfDetails;

    @OneToOne
    private NomineeDetails nomineeDetails;

    @OneToOne
    private FamilyDetails familyDetails;

    @OneToOne
    private EsicDetails esicDetails;

    @OneToOne
    private Gratuity gratuity;

    @OneToOne
    private UploadDetails uploadDetails;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date updatedAt;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }


    public FamilyDetails getFamilyDetails() {
        return familyDetails;
    }

    public void setFamilyDetails(FamilyDetails familyDetails) {
        this.familyDetails = familyDetails;
    }

    public long getEmpObDetId() {
        return empObDetId;
    }

    public void setEmpObDetId(long empObDetId) {
        this.empObDetId = empObDetId;
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }

    public PersonalDetails getPersonalDetails() {
        return personalDetails;
    }

    public void setPersonalDetails(PersonalDetails personalDetails) {
        this.personalDetails = personalDetails;
    }

    public EducationalDetails getEducationDetails() {
        return educationDetails;
    }

    public void setEducationDetails(EducationalDetails educationDetails) {
        this.educationDetails = educationDetails;
    }

    public ExperienceDetails getExperianceDetails() {
        return experianceDetails;
    }

    public void setExperianceDetails(ExperienceDetails experianceDetails) {
        this.experianceDetails = experianceDetails;
    }

    public KycDetails getKycDetails() {
        return kycDetails;
    }

    public void setKycDetails(KycDetails kycDetails) {
        this.kycDetails = kycDetails;
    }

    public EpfDetails getEpfDetails() {
        return epfDetails;
    }

    public void setEpfDetails(EpfDetails epfDetails) {
        this.epfDetails = epfDetails;
    }

    public NomineeDetails getNomineeDetails() {
        return nomineeDetails;
    }

    public void setNomineeDetails(NomineeDetails nomineeDetails) {
        this.nomineeDetails = nomineeDetails;
    }

    public EsicDetails getEsicDetails() {
        return esicDetails;
    }

    public void setEsicDetails(EsicDetails esicDetails) {
        this.esicDetails = esicDetails;
    }

    public Gratuity getGratuity() {
        return gratuity;
    }

    public void setGratuity(Gratuity gratuity) {
        this.gratuity = gratuity;
    }

    public UploadDetails getUploadDetails() {
        return uploadDetails;
    }

    public void setUploadDetails(UploadDetails uploadDetails) {
        this.uploadDetails = uploadDetails;
    }

}
