package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class CRMProduct {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long crmpid;

    private String name;

    private String descp;

    private String notificationMailId;

    private String redirectUrl;

    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public long getCrmpid() {
        return crmpid;
    }

    public void setCrmpid(long crmpid) {
        this.crmpid = crmpid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public String getNotificationMailId() {
        return notificationMailId;
    }

    public void setNotificationMailId(String notificationMailId) {
        this.notificationMailId = notificationMailId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public CRMProduct(long crmpid, String name, String descp, String notificationMailId, String redirectUrl,
                      Date createdAt, Date updatedAt) {
        super();
        this.crmpid = crmpid;
        this.name = name;
        this.descp = descp;
        this.notificationMailId = notificationMailId;
        this.redirectUrl = redirectUrl;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public CRMProduct() {
        super();
    }


}
