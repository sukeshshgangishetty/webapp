package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class ExpensesCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long expCatId;

    private String categoryName;


    private String description;

    private ExpenseCategoryStatus status;


    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;


    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date updatedAt;

    @ManyToOne
    private UserDetails createdBy;


    public long getExpCatId() {
        return expCatId;
    }


    public void setExpCatId(long expCatId) {
        this.expCatId = expCatId;
    }


    public String getCategoryName() {
        return categoryName;
    }


    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public ExpenseCategoryStatus getStatus() {
        return status;
    }


    public void setStatus(ExpenseCategoryStatus status) {
        this.status = status;
    }


    public Date getCreatedAt() {
        return createdAt;
    }


    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }


    public Date getUpdatedAt() {
        return updatedAt;
    }


    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }


    public UserDetails getCreatedBy() {
        return createdBy;
    }


    public void setCreatedBy(UserDetails createdBy) {
        this.createdBy = createdBy;
    }


    public ExpensesCategory(String categoryName, String description, ExpenseCategoryStatus status,
                            UserDetails createdBy) {
        super();
        this.categoryName = categoryName;
        this.description = description;
        this.status = status;
        this.createdBy = createdBy;
    }


    public ExpensesCategory() {
        super();
    }


}
