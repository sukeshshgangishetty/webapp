package com.workforce.webapp.model;

public enum TrackDeviceStatus {

    ENROLLED, ASSIGNED, ACTIVE, DE_ACTIVE, EXPIRED
}
