package com.workforce.webapp.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {

    @Column(name = "hno")
    private String hno;
    @Column(name = "streetname")
    private String streetname;
    @Column(name = "city")
    private String city;
    @Column(name = "state")
    private String state;
    @Column(name = "pincode")
    private String pincode;


    public String getHno() {
        return hno;
    }

    public void setHno(String hno) {
        this.hno = hno;
    }

    public String getStreetname() {
        return streetname;
    }

    public void setStreetname(String streetname) {
        this.streetname = streetname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public Address(String hno, String streetname, String city, String state, String pincode) {
        super();
        this.hno = hno;
        this.streetname = streetname;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
    }

    public Address(String streetname, String city, String state, String pincode) {
        super();
        this.streetname = streetname;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
    }


    public Address() {

    }

}
