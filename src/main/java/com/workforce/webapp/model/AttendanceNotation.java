package com.workforce.webapp.model;

public enum AttendanceNotation {

    PRESENT, ABSENT, LEAVE, HOLIDAY, COMP_OFF, FIRST_HALF, SECOND_HALF, OTHERS, ACTION_REQUIRED
}
