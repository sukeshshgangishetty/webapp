package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class DispatchList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long dispId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdAt;

    long totalItems;


    long totalOrders;

    String listName;

    DispatchListStatus dpstatus;

    @JsonIgnore
    @ManyToOne
    Organization org;

    public long getDispId() {
        return dispId;
    }

    public void setDispId(long dispId) {
        this.dispId = dispId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }

    public long getTotalOrders() {
        return totalOrders;
    }

    public void setTotalOrders(long totalOrders) {
        this.totalOrders = totalOrders;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }


    public Organization getOrg() {
        return org;
    }

    public void setOrg(Organization org) {
        this.org = org;
    }


    public DispatchListStatus getDpstatus() {
        return dpstatus;
    }

    public void setDpstatus(DispatchListStatus dpstatus) {
        this.dpstatus = dpstatus;
    }

    public DispatchList(Date createdAt, long totalItems, long totalOrders, String listName, DispatchListStatus dpstatus,
                        Organization org) {
        super();
        this.createdAt = createdAt;
        this.totalItems = totalItems;
        this.totalOrders = totalOrders;
        this.listName = listName;
        this.dpstatus = dpstatus;
        this.org = org;
    }


    public DispatchList(Date createdAt, long totalItems, long totalOrders, String listName) {
        super();
        this.createdAt = createdAt;
        this.totalItems = totalItems;
        this.totalOrders = totalOrders;
        this.listName = listName;
    }


    public DispatchList(long dispId, Date createdAt, long totalItems, long totalOrders, String listName,
                        DispatchListStatus dpstatus, Organization org) {
        super();
        this.dispId = dispId;
        this.createdAt = createdAt;
        this.totalItems = totalItems;
        this.totalOrders = totalOrders;
        this.listName = listName;
        this.dpstatus = dpstatus;
        this.org = org;
    }

    public DispatchList(long dispId, Date createdAt, long totalItems, long totalOrders, String listName) {
        super();
        this.dispId = dispId;
        this.createdAt = createdAt;
        this.totalItems = totalItems;
        this.totalOrders = totalOrders;
        this.listName = listName;
    }

    public DispatchList() {
        super();
    }


}
