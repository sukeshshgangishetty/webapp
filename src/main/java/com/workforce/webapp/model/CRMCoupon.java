package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class CRMCoupon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long crmcouponId;

    private String name;

    @Column(unique = true)
    private String couponCode;


    private double discount;

    private CRMDiscountType discType;

    private CRMDiscountRedemptionType redemptionType;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date validupTo;

    private int maximumRedemption;

    @ManyToOne
    private CRMProduct product;


    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;


    public CRMProduct getProduct() {
        return product;
    }

    public void setProduct(CRMProduct product) {
        this.product = product;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getCrmcouponId() {
        return crmcouponId;
    }

    public void setCrmcouponId(long crmcouponId) {
        this.crmcouponId = crmcouponId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public CRMDiscountType getDiscType() {
        return discType;
    }

    public void setDiscType(CRMDiscountType discType) {
        this.discType = discType;
    }

    public CRMDiscountRedemptionType getRedemptionType() {
        return redemptionType;
    }

    public void setRedemptionType(CRMDiscountRedemptionType redemptionType) {
        this.redemptionType = redemptionType;
    }

    public Date getValidupTo() {
        return validupTo;
    }

    public void setValidupTo(Date validupTo) {
        this.validupTo = validupTo;
    }

    public int getMaximumRedemption() {
        return maximumRedemption;
    }

    public void setMaximumRedemption(int maximumRedemption) {
        this.maximumRedemption = maximumRedemption;
    }

    public CRMCoupon(long crmcouponId, String name, String couponCode, double discount, CRMDiscountType discType,
                     CRMDiscountRedemptionType redemptionType, Date validupTo, int maximumRedemption) {
        super();
        this.crmcouponId = crmcouponId;
        this.name = name;
        this.couponCode = couponCode;
        this.discount = discount;
        this.discType = discType;
        this.redemptionType = redemptionType;
        this.validupTo = validupTo;
        this.maximumRedemption = maximumRedemption;
    }

    public CRMCoupon() {
        super();
    }


}
