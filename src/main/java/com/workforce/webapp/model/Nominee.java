package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Nominee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long noId;

    private String name;

    private String age;

    @JsonIgnore
    @Column(name = "createdAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt = null;

    @JsonIgnore
    @Column(name = "updatedAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt = null;

    public long getNoId() {
        return noId;
    }

    public void setNoId(long noId) {
        this.noId = noId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "Nominee [noId=" + noId + ", name=" + name + ", age=" + age + ", createdAt=" + createdAt + ", updatedAt="
                + updatedAt + "]";
    }


}
