package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ExperienceDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long eduId;

    private long expYear;

    private long expMonth;

    private String cmpName;

    private String mode;

    private String postion;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date doj;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dor;

    private String referenceName;

    private String refPhoneNumber;

    private String reason;

    @JsonIgnore
    @Column(name = "createdAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt = null;

    @JsonIgnore
    @Column(name = "updatedAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt = null;

    public long getEduId() {
        return eduId;
    }

    public void setEduId(long eduId) {
        this.eduId = eduId;
    }

    public long getExpYear() {
        return expYear;
    }

    public void setExpYear(long expYear) {
        this.expYear = expYear;
    }

    public long getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(long expMonth) {
        this.expMonth = expMonth;
    }

    public String getCmpName() {
        return cmpName;
    }

    public void setCmpName(String cmpName) {
        this.cmpName = cmpName;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getPostion() {
        return postion;
    }

    public void setPostion(String postion) {
        this.postion = postion;
    }

    public Date getDoj() {
        return doj;
    }

    public void setDoj(Date doj) {
        this.doj = doj;
    }

    public Date getDor() {
        return dor;
    }

    public void setDor(Date dor) {
        this.dor = dor;
    }

    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public String getRefPhoneNumber() {
        return refPhoneNumber;
    }

    public void setRefPhoneNumber(String refPhoneNumber) {
        this.refPhoneNumber = refPhoneNumber;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "ExperienceDetails [eduId=" + eduId + ", expYear=" + expYear + ", expMonth=" + expMonth
                + ", cmpName=" + cmpName + ", mode=" + mode + ", postion=" + postion + ", doj=" + doj + ", dor="
                + dor + ", referenceName=" + referenceName + ", refPhoneNumber=" + refPhoneNumber + ", reason="
                + reason + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
    }

}
