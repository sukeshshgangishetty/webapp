package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CollectionId;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class LeaveTypes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ltId;

    @Column(unique = true)
    private String typename;

    @NonNull
    private int defaultValue;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;

    private double monthlyLimit;

    private boolean recurring;

    public long getLtId() {
        return ltId;
    }

    public void setLtId(long ltId) {
        this.ltId = ltId;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public int getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(int defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public double getMonthlyLimit() {
        return monthlyLimit;
    }

    public void setMonthlyLimit(double monthlyLimit) {
        this.monthlyLimit = monthlyLimit;
    }

    public boolean isRecurring() {
        return recurring;
    }

    public void setRecurring(boolean recurring) {
        this.recurring = recurring;
    }

    public LeaveTypes(long ltId, String typename, int defaultValue, Date createdAt, double monthlyLimit,
                      boolean recurring) {
        super();
        this.ltId = ltId;
        this.typename = typename;
        this.defaultValue = defaultValue;
        this.createdAt = createdAt;
        this.monthlyLimit = monthlyLimit;
        this.recurring = recurring;
    }

    public LeaveTypes(String typename, int defaultValue, Date createdAt, double monthlyLimit, boolean recurring) {
        super();
        this.typename = typename;
        this.defaultValue = defaultValue;
        this.createdAt = createdAt;
        this.monthlyLimit = monthlyLimit;
        this.recurring = recurring;
    }

    public LeaveTypes() {
        super();
    }


}
