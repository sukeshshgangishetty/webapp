package com.workforce.webapp.model.trip;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;


@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class TripPoint extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @ManyToOne
    @JoinColumn(name = "trip_id", nullable = false)
    @JsonIgnore
    private Trip trip;

    @ManyToOne
    @JoinColumn(name = "point_id", nullable = false)
    private Point point;

    private String expTym;

    private Integer sequence;

    private String purpose;

}
