package com.workforce.webapp.model.trip;

import com.workforce.webapp.enums.trip.PointType;
import com.workforce.webapp.model.Location;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class Point extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private String name;

    @Enumerated(EnumType.STRING)
    private PointType type;

    @Embedded
    private Location locCurrent;

    private String clientId;

}
