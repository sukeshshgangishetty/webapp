package com.workforce.webapp.model.trip;

import com.workforce.webapp.enums.trip.PeriodType;
import com.workforce.webapp.enums.trip.PriorityType;
import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class Trip extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private String title;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Enumerated(EnumType.STRING)
    private PriorityType priorityType;

    @Enumerated(EnumType.STRING)
    private PeriodType period;

    private Integer count;

    @Enumerated(EnumType.STRING)
    private TripStatus status;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "trip",
            orphanRemoval = true)
    private List<TripPoint> tripPoints = new ArrayList<>();

    public List<TripPoint> getTripPoints() {
        return tripPoints;
    }

    public void setTripPoints(List<TripPoint> tripPoints) {
        this.tripPoints.addAll(tripPoints);
    }

}
