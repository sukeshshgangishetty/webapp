package com.workforce.webapp.model.trip;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.Location;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class ScheduleVisit extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

   /* @ManyToOne
    @JoinColumn(name = "trip_point_id", nullable = false)*/

    @Column(nullable = false, columnDefinition = "text")
    private String tripPoint;

    private Long checkIn;

    private Long checkOut;

    private String comment;

    private Double distance;

    private Long totTym;

    @Enumerated(EnumType.STRING)
    private TripStatus status;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "schedule_allocation_id", nullable = false)
    @JsonIgnore
    private ScheduleAllocation scheduleAllocation;

    private Integer sequence;

    private Location location;

}
