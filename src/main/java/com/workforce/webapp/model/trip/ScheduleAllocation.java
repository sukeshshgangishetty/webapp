package com.workforce.webapp.model.trip;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.enums.trip.DeviceType;
import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class ScheduleAllocation extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private Long actStrTym;

    private Long actEndTym;

    private Long assignedTo;


    @Enumerated(EnumType.STRING)
    private TripStatus status;

    private Integer totVisit = 0;

    private Integer comVisit = 0;

    private Double totDis;

    private Long totTym;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "schedule_id", nullable = false)
    @JsonIgnore
    private Schedule schedule;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "scheduleAllocation",
            orphanRemoval = true)
    private List<ScheduleVisit> scheduleVisits = new ArrayList<>();

    public List<ScheduleVisit> getScheduleVisits() {
        return scheduleVisits;
    }

    public void setScheduleVisits(List<ScheduleVisit> scheduleVisits) {
        this.scheduleVisits.addAll(scheduleVisits);
    }
}
