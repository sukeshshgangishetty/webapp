package com.workforce.webapp.model.trip;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workforce.webapp.enums.trip.DeviceType;
import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Entity
public class Schedule extends BaseEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    private String name;

    private Long expStrTym;

    private Long expEndTym;

    private Double totDis;

    @Enumerated(EnumType.STRING)
    private DeviceType assignedType;

    private Long totTym;

    @Enumerated(EnumType.STRING)
    private TripStatus status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trip_id", nullable = false)
    @JsonIgnore
    private Trip trip;

    private Integer totAlloc = 0;

    private Integer comAlloc = 0;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "schedule",
            orphanRemoval = true)
    private List<ScheduleAllocation> scheduleAllocations = new ArrayList<>();


    public List<ScheduleAllocation> getScheduleAllocations() {
        return scheduleAllocations;
    }

    public void setScheduleAllocations(List<ScheduleAllocation> tripPoints) {
        this.scheduleAllocations.addAll(tripPoints);
    }

}
