package com.workforce.webapp.model;

public enum PaymentMode {

    CASH, ONLINE, CHEQUE, UPI, OTHERS
}
