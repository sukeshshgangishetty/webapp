package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class PayrollItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long payrollItemId;

    private String name;

    private PayrollItemType type;

    private double matcherPersentage;

    private double grossPayValue;


    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public long getPayrollItemId() {
        return payrollItemId;
    }

    public void setPayrollItemId(long payrollItemId) {
        this.payrollItemId = payrollItemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PayrollItemType getType() {
        return type;
    }

    public void setType(PayrollItemType type) {
        this.type = type;
    }

    public double getMatcherPersentage() {
        return matcherPersentage;
    }

    public void setMatcherPersentage(double matcherPersentage) {
        this.matcherPersentage = matcherPersentage;
    }

    public double getGrossPayValue() {
        return grossPayValue;
    }

    public void setGrossPayValue(double grossPayValue) {
        this.grossPayValue = grossPayValue;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PayrollItem(long payrollItemId, String name, PayrollItemType type, double matcherPersentage,
                       double grossPayValue, Date createdAt, Date updatedAt) {
        super();
        this.payrollItemId = payrollItemId;
        this.name = name;
        this.type = type;
        this.matcherPersentage = matcherPersentage;
        this.grossPayValue = grossPayValue;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public PayrollItem() {
        super();
    }


}
