package com.workforce.webapp.model;

public enum UserStatus {

    ACTIVE, BLOCKED, INACTIVE, PENDING_SELF_ACTIVATION, PENDING_SUPERVISOR_ACTIVATION

}
