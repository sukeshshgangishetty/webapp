package com.workforce.webapp.model;

public enum CRMDiscountRedemptionType {

    ONE_TIME, FOREVER, LIMITED

}
