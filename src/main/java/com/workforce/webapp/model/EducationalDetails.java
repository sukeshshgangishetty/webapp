package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class EducationalDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long edId;

    private String highestQualification;

    private String degree;

    private String institueName;

    private String borarOrUniversity;

    private String year;

    private double percentage;


    @JsonIgnore
    @Column(name = "createdAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt = null;

    @JsonIgnore
    @Column(name = "updatedAt", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt = null;

    public long getEdId() {
        return edId;
    }

    public void setEdId(long edId) {
        this.edId = edId;
    }

    public String getHighestQualification() {
        return highestQualification;
    }

    public void setHighestQualification(String highestQualification) {
        this.highestQualification = highestQualification;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getInstitueName() {
        return institueName;
    }

    public void setInstitueName(String institueName) {
        this.institueName = institueName;
    }

    public String getBorarOrUniversity() {
        return borarOrUniversity;
    }

    public void setBorarOrUniversity(String borarOrUniversity) {
        this.borarOrUniversity = borarOrUniversity;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "EducationalDetails [edId=" + edId + ", highestQualification=" + highestQualification + ", degree="
                + degree + ", institueName=" + institueName + ", borarOrUniversity=" + borarOrUniversity + ", year="
                + year + ", percentage=" + percentage + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
    }


}
