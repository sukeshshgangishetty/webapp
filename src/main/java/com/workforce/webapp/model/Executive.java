package com.workforce.webapp.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.Nullable;
import com.workforce.webapp.model.audit.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class Executive extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long exeId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String phone;

    private String imgLogo;

    @Column(nullable = false)
    private Address address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id", nullable = false)
    @JsonIgnore
    private Organization org;

    private String email;

    private Long joinedDate;

    @Nullable
    private String vehicleNumber;


    private String pfNumber;


    private String esiNumber;

    private Long dob;

    @Nullable
    @Column(length = 12)
    private String aadhaarNumber;

    @Nullable
    private String panCardNumber;

    @Nullable
    private String qualification;

    @Nullable
    private String remarks;

}
