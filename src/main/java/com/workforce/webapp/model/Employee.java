package com.workforce.webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Employee {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long empId;

    private String fName;

    private String LName;

    private String uName;

    private String email;

    @Column(unique = true)
    private String phone;

    private String password;

    @Column(unique = true)
    private String comempid;

    private String joiningDate;

    private long reportingEmpId;

    @ManyToOne
    Employee reportingEmp;


    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @ManyToOne
    private Department department;

    private String designation;

    public Employee(long entityKey) {
        // TODO Auto-generated constructor stub
        this.empId = entityKey;
    }

    public long getEmpId() {
        return empId;
    }

    public void setEmpId(long empId) {
        this.empId = empId;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getLName() {
        return LName;
    }

    public void setLName(String lName) {
        LName = lName;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getComempid() {
        return comempid;
    }

    public void setComempid(String comempid) {
        this.comempid = comempid;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }


    public Employee getReportingEmp() {
        return reportingEmp;
    }

    public void setReportingEmp(Employee reportingEmp) {
        this.reportingEmp = reportingEmp;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public long getReportingEmpId() {
        return reportingEmpId;
    }

    public void setReportingEmpId(long reportingEmpId) {
        this.reportingEmpId = reportingEmpId;
    }

    public Employee(long empId, String fName, String lName, String uName, String email, String phone, String password,
                    String comempid, String joiningDate, long reportingEmpId, Date createdAt, Department department,
                    String designation) {
        super();
        this.empId = empId;
        this.fName = fName;
        LName = lName;
        this.uName = uName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.comempid = comempid;
        this.joiningDate = joiningDate;
        this.reportingEmpId = reportingEmpId;
        this.createdAt = createdAt;
        this.department = department;
        this.designation = designation;
    }


    @Override
    public String toString() {
        return "Employee [empId=" + empId + ", fName=" + fName + ", LName=" + LName + ", uName=" + uName + ", email="
                + email + ", phone=" + phone + ", password=" + password + ", comempid=" + comempid + ", joiningDate="
                + joiningDate + ", reportingEmpId=" + reportingEmpId + ", reportingEmp=" + reportingEmp + ", createdAt="
                + createdAt + ", department=" + department + ", designation=" + designation + "]";
    }

    public Employee() {
        super();
    }


}
