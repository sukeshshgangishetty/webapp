package com.workforce.webapp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author pc-2
 */
@Entity
public class LeaveApplication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long leaveid;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date leaveFrom;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date leaveTo;

    private LeaveStatus status;

    @ManyToOne
    private LeaveTypes leaveType;

    @ManyToOne
    private Employee reportingManager;

    @ManyToOne
    private Employee appliedBy;

    @ManyToOne
    private UserDetails createdBy;

    @ElementCollection
    private List<String> attachments;

    private double availableLeaves;

    private String reason;

    private LeaveNotation notation;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;

    public long getLeaveid() {
        return leaveid;
    }

    public void setLeaveid(long leaveid) {
        this.leaveid = leaveid;
    }

    public Date getLeaveFrom() {
        return leaveFrom;
    }

    public void setLeaveFrom(Date leaveFrom) {
        this.leaveFrom = leaveFrom;
    }

    public Date getLeaveTo() {
        return leaveTo;
    }

    public void setLeaveTo(Date leaveTo) {
        this.leaveTo = leaveTo;
    }

    public LeaveStatus getStatus() {
        return status;
    }

    public void setStatus(LeaveStatus status) {
        this.status = status;
    }

    public LeaveTypes getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(LeaveTypes leaveType) {
        this.leaveType = leaveType;
    }

    public Employee getReportingManager() {
        return reportingManager;
    }

    public void setReportingManager(Employee reportingManager) {
        this.reportingManager = reportingManager;
    }

    public Employee getAppliedBy() {
        return appliedBy;
    }

    public void setAppliedBy(Employee appliedBy) {
        this.appliedBy = appliedBy;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public double getAvailableLeaves() {
        return availableLeaves;
    }

    public void setAvailableLeaves(double availableLeaves) {
        this.availableLeaves = availableLeaves;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public LeaveNotation getNotation() {
        return notation;
    }

    public void setNotation(LeaveNotation notation) {
        this.notation = notation;
    }

    public LeaveApplication(long leaveid, Date leaveFrom, Date leaveTo, LeaveStatus status, LeaveTypes leaveType,
                            Employee reportingManager, Employee appliedBy, List<String> attachments, long availableLeaves,
                            String reason, Date createdAt) {
        super();
        this.leaveid = leaveid;
        this.leaveFrom = leaveFrom;
        this.leaveTo = leaveTo;
        this.status = status;
        this.leaveType = leaveType;
        this.reportingManager = reportingManager;
        this.appliedBy = appliedBy;
        this.attachments = attachments;
        this.availableLeaves = availableLeaves;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public LeaveApplication(Date leaveFrom, Date leaveTo, LeaveStatus status, LeaveTypes leaveType,
                            Employee reportingManager, Employee appliedBy, List<String> attachments, long availableLeaves,
                            String reason, Date createdAt) {
        super();
        this.leaveFrom = leaveFrom;
        this.leaveTo = leaveTo;
        this.status = status;
        this.leaveType = leaveType;
        this.reportingManager = reportingManager;
        this.appliedBy = appliedBy;
        this.attachments = attachments;
        this.availableLeaves = availableLeaves;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public LeaveApplication() {
        super();
    }

    public UserDetails getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserDetails createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "LeaveApplication [leaveid=" + leaveid + ", leaveFrom=" + leaveFrom + ", leaveTo=" + leaveTo
                + ", status=" + status + ", leaveType=" + leaveType + ", reportingManager=" + reportingManager
                + ", appliedBy=" + appliedBy + ", createdBy=" + createdBy + ", attachments=" + attachments
                + ", availableLeaves=" + availableLeaves + ", reason=" + reason + ", notation=" + notation
                + ", createdAt=" + createdAt + "]";
    }

}
