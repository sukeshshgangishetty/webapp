package com.workforce.webapp.model;

public enum ExpenseCategoryStatus {

    ACTIVE, IN_ACTIVE, DELETED, OTHERS

}
