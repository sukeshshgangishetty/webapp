package com.workforce.webapp.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class OrderDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long orderid;


    @ManyToOne
    Client orderto;

    @ManyToOne
    UserDetails orderCollected;

//	@ElementCollection
//	@OneToMany(cascade = CascadeType.ALL, 
    //        mappedBy = "order", orphanRemoval = true)
    //private Set<OrderParticulars> orderParticulars;

    @JsonFormat(pattern = "dd-MM-yyy HH:mm:ss")
    @CreationTimestamp
    Date createdat;

    @JsonFormat(pattern = "dd-MM-yyy HH:mm:ss")
    @CreationTimestamp
    Date approvedat;

    double total;

    OrderType oType;

    @ManyToOne
    Organization org;

    String remarks;

    @JsonProperty("status")
    OrdersStatus orstatus;

    String clientSign;

    public String getRemarks() {
        return remarks;
    }


    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }


    public long getOrderid() {
        return orderid;
    }


    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }


    public Client getOrderto() {
        return orderto;
    }


    public void setOrderto(Client orderto) {
        this.orderto = orderto;
    }


    public UserDetails getOrderCollected() {
        return orderCollected;
    }


    public void setOrderCollected(UserDetails orderCollected) {
        this.orderCollected = orderCollected;
    }


	/*public Set<OrderParticulars> getOrderParticulars() {
		return orderParticulars;
	}


	public void setOrderParticulars(Set<OrderParticulars> orderParticulars) {
		this.orderParticulars = orderParticulars;
	}*/


    public Date getCreatedat() {
        return createdat;
    }


    public void setCreatedat(Date createdat) {
        this.createdat = createdat;
    }


    public double getTotal() {
        return total;
    }


    public void setTotal(double total) {
        this.total = total;
    }


    public Organization getOrg() {
        return org;
    }


    public void setOrg(Organization org) {
        this.org = org;
    }


    public String getClientSign() {
        return clientSign;
    }


    public void setClientSign(String clientSign) {
        this.clientSign = clientSign;
    }


    public Date getApprovedat() {
        return approvedat;
    }


    public void setApprovedat(Date approvedat) {
        this.approvedat = approvedat;
    }


    public OrdersStatus getOrstatus() {
        return orstatus;
    }


    public void setOrstatus(OrdersStatus orstatus) {
        this.orstatus = orstatus;
    }


    public OrderType getoType() {
        return oType;
    }


    public void setoType(OrderType oType) {
        this.oType = oType;
    }


    @Override
    public String toString() {
        return "Order2 [orderid=" + orderid + ", orderto=" + orderto + ", orderCollected=" + orderCollected
                + ", createdat=" + createdat + ", total=" + total + ", org=" + org + ", remarks=" + remarks
                + ", status=" + orstatus + "]";
    }


    public OrderDetails(Client orderto, UserDetails orderCollected, Set<OrderParticulars> orderParticulars, Date createdat,
                        double total, Organization org) {
        super();
        this.orderto = orderto;
        this.orderCollected = orderCollected;
        //this.orderParticulars = orderParticulars;
        this.createdat = createdat;
        this.total = total;
        this.org = org;
        this.orstatus = OrdersStatus.ADDED;
    }


    public OrderDetails() {
        super();
    }


}
