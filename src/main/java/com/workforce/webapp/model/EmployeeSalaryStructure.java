package com.workforce.webapp.model;

import java.util.Date;
import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class EmployeeSalaryStructure {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long essId;

    @OneToOne
    Employee emp;

    double netSalary;

    @ElementCollection
    Set<SalaryStructureItems> salaryItems;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date createdAt;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date updatedAt;


    @CreatedBy
    @ManyToOne
    private UserDetails addedBy;


    public long getEssId() {
        return essId;
    }


    public void setEssId(long essId) {
        this.essId = essId;
    }


    public Employee getEmp() {
        return emp;
    }


    public void setEmp(Employee emp) {
        this.emp = emp;
    }


    public Set<SalaryStructureItems> getSalaryItems() {
        return salaryItems;
    }


    public void setSalaryItems(Set<SalaryStructureItems> salaryItems) {
        this.salaryItems = salaryItems;
    }


    public Date getCreatedAt() {
        return createdAt;
    }


    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }


    public Date getUpdatedAt() {
        return updatedAt;
    }


    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }


    public UserDetails getAddedBy() {
        return addedBy;
    }


    public void setAddedBy(UserDetails addedBy) {
        this.addedBy = addedBy;
    }


    public double getNetSalary() {
        return netSalary;
    }


    public void setNetSalary(double netSalary) {
        this.netSalary = netSalary;
    }


    public EmployeeSalaryStructure(long essId, Employee emp, Set<SalaryStructureItems> salaryItems, Date createdAt,
                                   Date updatedAt, UserDetails addedBy) {
        super();
        this.essId = essId;
        this.emp = emp;
        this.salaryItems = salaryItems;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.addedBy = addedBy;
    }


    public EmployeeSalaryStructure(Employee emp, Set<SalaryStructureItems> salaryItems, Date createdAt, Date updatedAt,
                                   UserDetails addedBy) {
        super();
        this.emp = emp;
        this.salaryItems = salaryItems;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.addedBy = addedBy;
    }


    public EmployeeSalaryStructure(long essId, Employee emp, Set<SalaryStructureItems> salaryItems,
                                   UserDetails addedBy) {
        super();
        this.essId = essId;
        this.emp = emp;
        this.salaryItems = salaryItems;
        this.addedBy = addedBy;
    }


    public EmployeeSalaryStructure() {
        super();
    }


}
