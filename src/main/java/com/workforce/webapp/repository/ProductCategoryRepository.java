package com.workforce.webapp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.ProductCategory;


public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {


    List<ProductCategory> findAllByOrg(Organization org);

    Optional<ProductCategory> findOneByCatidAndOrg(long id, Organization org);

    Optional<ProductCategory> findByCatidAndOrg(Long id, Organization org);

    Optional<ProductCategory> findByCatnameAndOrg(String name, Organization org);

    ProductCategory findOneByCatid(long id);

}
