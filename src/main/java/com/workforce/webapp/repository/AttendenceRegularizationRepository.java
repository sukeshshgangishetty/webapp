package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.AttendanceActivity;
import com.workforce.webapp.model.AttendanceRegularization;

public interface AttendenceRegularizationRepository extends JpaRepository<AttendanceRegularization, Long> {

    List<AttendanceRegularization> findByEmpEmpId(long empid);

}
