package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.SubTask;
import com.workforce.webapp.model.Task;

public interface SubTaskRepository extends JpaRepository<SubTask, Long> {

    List<SubTask> findByTask(Task task);

}
