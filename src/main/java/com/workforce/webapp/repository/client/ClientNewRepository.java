package com.workforce.webapp.repository.client;

import com.workforce.webapp.model.client.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientNewRepository extends JpaRepository<ClientEntity, String> {

    List<ClientEntity> findAllByOrgId(Long orgId);

    ClientEntity findByIdAndOrgId(String id, Long orgId);

}
