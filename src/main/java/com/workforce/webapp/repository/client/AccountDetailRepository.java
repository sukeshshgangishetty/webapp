package com.workforce.webapp.repository.client;

import com.workforce.webapp.model.client.AccountDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountDetailRepository extends JpaRepository<AccountDetail, String> {
    AccountDetail findByClientEntityIdAndIsPrimaryTrue(String clientId);
}
