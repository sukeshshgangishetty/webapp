package com.workforce.webapp.repository.client;

import com.workforce.webapp.model.client.ContactDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactDetailRepository extends JpaRepository<ContactDetail, String> {
}
