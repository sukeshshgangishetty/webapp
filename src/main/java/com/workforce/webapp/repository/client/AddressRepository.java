package com.workforce.webapp.repository.client;

import com.workforce.webapp.model.client.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, String> {
    Address findByClientEntityIdAndIsBillingTrueAndIsPrimaryTrue(String clientId);

    Address findByClientEntityIdAndIsBillingFalseAndIsPrimaryTrue(String clientId);
}
