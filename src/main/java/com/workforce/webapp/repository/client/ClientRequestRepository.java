package com.workforce.webapp.repository.client;

import com.workforce.webapp.model.client.ClientRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRequestRepository extends JpaRepository<ClientRequest, String> {
}
