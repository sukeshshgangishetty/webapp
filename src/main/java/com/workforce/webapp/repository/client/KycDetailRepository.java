package com.workforce.webapp.repository.client;

import com.workforce.webapp.model.client.KYCDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KycDetailRepository extends JpaRepository<KYCDetail, String> {
}
