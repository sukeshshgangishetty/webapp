package com.workforce.webapp.repository.client;

import com.workforce.webapp.model.client.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, String> {

    List<Category> getAllByOrgId(Long orgId);

    Category getByIdAndOrgId(String id, Long orgId);
}
