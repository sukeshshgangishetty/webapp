package com.workforce.webapp.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;

public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {


    UserDetails findByActivationKey(String activationkey); //activationKey

    UserDetails findByPhone(String phone); //phone

    UserDetails findByToken(String token);

    UserDetails findByLoginAndPassword(String login, String md5);

    UserDetails findByEntityKeyAndRole(long etky, Userrole role);

}
