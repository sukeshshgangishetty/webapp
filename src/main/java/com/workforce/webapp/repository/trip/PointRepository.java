package com.workforce.webapp.repository.trip;

import com.workforce.webapp.enums.trip.PointType;
import com.workforce.webapp.model.trip.Point;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointRepository extends JpaRepository<Point, String> {

    Point findByClientId(String clientId);

    Point findByNameAndType(String name, PointType type);
}
