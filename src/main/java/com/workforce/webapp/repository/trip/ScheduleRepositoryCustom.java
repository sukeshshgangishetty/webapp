package com.workforce.webapp.repository.trip;

import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.trip.ScheduleAllocation;

import java.util.List;
import java.util.Map;

public interface ScheduleRepositoryCustom {
    Map<TripStatus, Integer> getTripsStats();

    Map<TripStatus, Integer> getScheduleAllocStats(String scheduleId, Long exeId);

    Map<TripStatus, Integer> getScheduleVisitStats(String scheduleAllocId);

    List<Long> getExeIdsFromAlloc(String scheduleId, String tripId);

    List<ScheduleAllocation> getAllocByExeIdAndTripId(Long exeId, String tripId);
}
