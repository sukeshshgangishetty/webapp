package com.workforce.webapp.repository.trip;

import com.workforce.webapp.model.trip.Trip;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TripRepository extends JpaRepository<Trip, String> {
    List<Trip> findAllByCreatedBy(String token);
}

