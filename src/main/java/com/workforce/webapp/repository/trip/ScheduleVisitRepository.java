package com.workforce.webapp.repository.trip;

import com.workforce.webapp.model.trip.ScheduleVisit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ScheduleVisitRepository extends JpaRepository<ScheduleVisit, String> {
    //List<ScheduleVisit> findAllByScheduleId(String scheduleId);
    //void deleteAllByScheduleId(String scheduleId);
    List<ScheduleVisit> findAllByScheduleAllocationId(String id);

    ScheduleVisit findBySequenceAndScheduleAllocationId(Integer sequence, String allocId);
}
