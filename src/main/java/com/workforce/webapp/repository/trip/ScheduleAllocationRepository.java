package com.workforce.webapp.repository.trip;


import com.workforce.webapp.model.trip.ScheduleAllocation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ScheduleAllocationRepository extends JpaRepository<ScheduleAllocation, String> {
    List<ScheduleAllocation> findAllByAssignedTo(Long exdId);

    List<ScheduleAllocation> findAllByAssignedToAndScheduleId(Long exdId, String scheduleId);

    List<ScheduleAllocation> findAllByScheduleId(String scheduleId);
}
