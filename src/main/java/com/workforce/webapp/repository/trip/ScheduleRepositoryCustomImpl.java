package com.workforce.webapp.repository.trip;

import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.trip.Schedule;
import com.workforce.webapp.model.trip.ScheduleAllocation;
import com.workforce.webapp.model.trip.ScheduleVisit;
import com.workforce.webapp.utils.Util;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScheduleRepositoryCustomImpl implements ScheduleRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public Map<TripStatus, Integer> getTripsStats() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createQuery(Tuple.class);
        Root<Schedule> root = criteriaQuery.from(Schedule.class);

        criteriaQuery.groupBy(root.get("status"));
        criteriaQuery.multiselect(root.get("status"), criteriaBuilder.count(root));

        return convertTuplesToMap(criteriaQuery);
    }

    public List<Long> getExeIdsFromAlloc(String scheduleId, String tripId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createQuery(Tuple.class);
        Root<ScheduleAllocation> root = criteriaQuery.from(ScheduleAllocation.class);
        criteriaQuery.multiselect(root.get("assignedTo"));

        if (!Util.isNullOrEmpty(scheduleId))
            criteriaQuery.where(criteriaBuilder.equal(root.get("schedule").get("id"), scheduleId));

        if (!Util.isNullOrEmpty(tripId))
            criteriaQuery.where(criteriaBuilder.equal(root.get("schedule").get("trip").get("id"), tripId));

        List<Tuple> tuples = entityManager.createQuery(criteriaQuery).getResultList();
        List<Long> ids = new ArrayList<>();
        for (Tuple tuple : tuples) {
            ids.add((Long) tuple.get(0));
        }
        return ids;
    }

    @Override
    public List<ScheduleAllocation> getAllocByExeIdAndTripId(Long exeId, String tripId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ScheduleAllocation> criteriaQuery = criteriaBuilder.createQuery(ScheduleAllocation.class);
        Root<ScheduleAllocation> root = criteriaQuery.from(ScheduleAllocation.class);
        List<Predicate> predicateList = new ArrayList<>();

        if (!Util.isNullOrEmpty(exeId)) {
            predicateList.add((criteriaBuilder.equal(root.get("assignedTo"), exeId)));
        }

        if (!Util.isNullOrEmpty(tripId)) {
            predicateList.add((criteriaBuilder.equal(root.get("schedule").get("trip").get("id"), tripId)));
        }
        criteriaQuery.where(criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()])));
        List<ScheduleAllocation> entities = entityManager.createQuery(criteriaQuery).getResultList();
        return entities.size() > 0 ? entities : null;
    }


    @Override
    public Map<TripStatus, Integer> getScheduleAllocStats(String scheduleId, Long exeId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createQuery(Tuple.class);
        Root<ScheduleAllocation> root = criteriaQuery.from(ScheduleAllocation.class);

        criteriaQuery.groupBy(root.get("status"));
        criteriaQuery.multiselect(root.get("status"), criteriaBuilder.count(root));

        if (!Util.isNullOrEmpty(scheduleId))
            criteriaQuery.where(criteriaBuilder.equal(root.get("schedule").get("id"), scheduleId));

        if (!Util.isNullOrEmpty(exeId))
            criteriaQuery.where(criteriaBuilder.equal(root.get("assignedTo"), exeId));

        return convertTuplesToMap(criteriaQuery);
    }


    @Override
    public Map<TripStatus, Integer> getScheduleVisitStats(String scheduleAllocId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createQuery(Tuple.class);
        Root<ScheduleVisit> root = criteriaQuery.from(ScheduleVisit.class);

        criteriaQuery.groupBy(root.get("status"));
        criteriaQuery.multiselect(root.get("status"), criteriaBuilder.count(root));

        if (!Util.isNullOrEmpty(scheduleAllocId))
            criteriaQuery.where(criteriaBuilder.equal(root.get("scheduleAllocation").get("id"), scheduleAllocId));

        return convertTuplesToMap(criteriaQuery);
    }


    private Map convertTuplesToMap(CriteriaQuery criteriaQuery) {
        List<Tuple> tuples = entityManager.createQuery(criteriaQuery).getResultList();
        Map<TripStatus, Integer> map = new HashMap<>();
        for (Tuple tuple : tuples) {
            Integer i = (int) (long) tuple.get(1);
            map.put((TripStatus) tuple.get(0), i);
        }
        return map;
    }

}
