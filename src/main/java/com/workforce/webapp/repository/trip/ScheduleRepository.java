package com.workforce.webapp.repository.trip;

import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.trip.Schedule;
import com.workforce.webapp.model.trip.Trip;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ScheduleRepository extends JpaRepository<Schedule, String>, ScheduleRepositoryCustom {
    List<Schedule> findAllByTripId(String tripId);

    Integer countAllByTripId(String tripId);

    Integer countAllByTripIdAndStatus(String tripId, TripStatus status);

    Schedule findTopByTripIdAndStatusAndExpStrTymAfter(String tripId, TripStatus status, Long date);
}
