package com.workforce.webapp.repository.trip;

import com.workforce.webapp.model.trip.TripPoint;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TripPointRepository extends JpaRepository<TripPoint, String> {
    List<TripPoint> findAllByTripIdOrderBySequenceAsc(String tripId);
}
