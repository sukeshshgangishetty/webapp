package com.workforce.webapp.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.workforce.webapp.model.CurrentTrack;
import com.workforce.webapp.projection.LoginDetails;


@Repository
public interface CurrentTrackRepository extends JpaRepository<CurrentTrack, Long> {


    @Query("SELECT u FROM  CurrentTrack u where u.etyky= :key and u.cttype= :ty ORDER BY ctid DESC ")
    List<CurrentTrack> findLastCurrentTrackByEkey(@Param("key") long key, @Param("ty") int ty);

    @Query(value = "SELECT startedat FROM wft.current_track where etyky=:etky and date(startedat)=(:date)", nativeQuery = true)
        //SELECT startedat as loginsAt FROM wft.current_track where etyky=16 and date(startedat) ='2018-07-24';
    List<Date> findStartedatByEtykyAndStartedat(@Param("etky") long etky, @Param("date") Date date);

    List<LoginDetails> findAllByEtykyAndCttypeOrderByCtidDesc(long etyky, int cttype);

}
