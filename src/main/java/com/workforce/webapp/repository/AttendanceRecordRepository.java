package com.workforce.webapp.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.AttendanceRecord;

public interface AttendanceRecordRepository extends JpaRepository<AttendanceRecord, Long> {

    Optional<AttendanceRecord> findOneByAttOnAndEmpEmpId(Date attOn, long empid);

    List<AttendanceRecord> findByEmpEmpId(long empId);

    List<AttendanceRecord> findByAttOnBetweenAndEmpEmpId(Date attFrom, Date attTo, long empid);

}
