package com.workforce.webapp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.CRMCoupon;

public interface CrmProductCouponRepository extends JpaRepository<CRMCoupon, Long> {

    Optional<CRMCoupon> findByName(String name);

    Optional<CRMCoupon> findByCouponCode(String code);

    List<CRMCoupon> findByProductCrmpid(long crmpid);

}
