package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.CrmUser;


public interface CrmUserRepository extends JpaRepository<CrmUser, Long> {

    CrmUser findByPhone(String phone);

}
