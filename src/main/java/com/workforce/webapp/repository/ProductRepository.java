package com.workforce.webapp.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.Product;
import com.workforce.webapp.model.ProductCategory;
import com.workforce.webapp.model.UserDetails;


public interface ProductRepository extends JpaRepository<Product, Long> {


    List<Product> findAllByUser(UserDetails user);

    List<Product> findAllByOrg(Organization org);

    List<Product> findAllByPcatAndOrg(ProductCategory pcat, Organization org);

    Product findByPid(long pid);

    @Query(value = "select product_pid from wft.order_particulars where order_orderid in (SELECT orderid FROM wft.order2 where org_id = ?2) group by product_pid Order by sum(qty) desc limit ?1", nativeQuery = true)
    List<BigInteger> findTopProducts(@Param("n") long n, @Param("orgid") long ordid);
}