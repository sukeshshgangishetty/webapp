package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.DispatchList;
import com.workforce.webapp.model.DispatchListStatus;
import com.workforce.webapp.model.Organization;

public interface DispatchListRepository extends JpaRepository<DispatchList, Long> {

    List<DispatchList> findByOrg(Organization org);

    List<DispatchList> findByDpstatusAndOrg(DispatchListStatus status, Organization org);
}
