package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.OrderDetails;
import com.workforce.webapp.model.OrderParticulars;


public interface OrderParticularRepository extends JpaRepository<OrderParticulars, Long> {

    List<OrderParticulars> findAllByOrder(OrderDetails order);

    OrderParticulars findById(long opid);

}
