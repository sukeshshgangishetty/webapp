package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.AttendanceActivity;

public interface AttendanceActivityRepository extends JpaRepository<AttendanceActivity, Long> {

    List<AttendanceActivity> findByAttendenceRecId(long attRecId);

    List<AttendanceActivity> findByAttendenceRecIdOrderByActionTimeAsc(long attRecId);


}
