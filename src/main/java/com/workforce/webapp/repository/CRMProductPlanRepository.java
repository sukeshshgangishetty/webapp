package com.workforce.webapp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.CRMProductPlan;

public interface CRMProductPlanRepository extends JpaRepository<CRMProductPlan, Long> {

    Optional<CRMProductPlan> findByName(String name);

    List<CRMProductPlan> findByProductCrmpid(long crmpid);

}
