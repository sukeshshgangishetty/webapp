package com.workforce.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.LeaveTypes;

public interface LeaveTypesRepository extends JpaRepository<LeaveTypes, Long> {


}
