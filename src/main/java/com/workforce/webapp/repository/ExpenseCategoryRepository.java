package com.workforce.webapp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.ExpensesCategory;

public interface ExpenseCategoryRepository extends JpaRepository<ExpensesCategory, Long> {

    Optional<ExpensesCategory> findOneByCategoryName(String catName);
}
