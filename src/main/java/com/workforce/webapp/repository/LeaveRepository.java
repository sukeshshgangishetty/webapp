package com.workforce.webapp.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.LeaveApplication;
import com.workforce.webapp.model.LeaveStatus;

public interface LeaveRepository extends JpaRepository<LeaveApplication, Long> {

    List<LeaveApplication> findByAppliedByEmpId(long empid);

    List<LeaveApplication> findByAppliedByReportingEmpId(long empid);

    List<LeaveApplication> findByAppliedByEmpIdAndStatusAndLeaveFromIsLessThanEqualAndLeaveToIsGreaterThanEqual(long empid,
                                                                                                                LeaveStatus status, Date checkDate, Date checkDate2);

    List<LeaveApplication> findByAppliedByEmpIdAndStatusAndLeaveFromIsGreaterThanEqualAndLeaveToIsLessThanEqual(long empid,
                                                                                                                LeaveStatus status, Date checkDate, Date checkDate2);


}
