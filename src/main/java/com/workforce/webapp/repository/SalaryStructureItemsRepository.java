package com.workforce.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.SalaryStructureItems;

public interface SalaryStructureItemsRepository extends JpaRepository<SalaryStructureItems, Long> {

}
