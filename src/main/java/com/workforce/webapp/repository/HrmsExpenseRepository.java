package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.workforce.webapp.model.HrmsExpenses;
import com.workforce.webapp.model.UserDetails;

public interface HrmsExpenseRepository extends JpaRepository<HrmsExpenses, Long>, JpaSpecificationExecutor<HrmsExpenses> {

    List<HrmsExpenses> findByCatExpCatId(long expcatid);

    List<HrmsExpenses> findByCreatedBy(UserDetails user);
//	List<HrmsExpenses> findBy

}
