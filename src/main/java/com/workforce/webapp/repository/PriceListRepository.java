package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.PriceList;
import com.workforce.webapp.model.Product;

public interface PriceListRepository extends JpaRepository<PriceList, Long> {


    List<PriceList> findByOrg(Organization org);

    List<PriceList> findByProduct(Product product);

    PriceList findOneByPlid(long plid);
}
