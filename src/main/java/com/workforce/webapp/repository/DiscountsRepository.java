package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.Discount;
import com.workforce.webapp.model.Organization;

public interface DiscountsRepository extends JpaRepository<Discount, Long> {
    List<Discount> findByOrg(Organization org);
}
