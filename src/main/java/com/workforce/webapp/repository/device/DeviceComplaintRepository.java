package com.workforce.webapp.repository.device;

import com.workforce.webapp.model.device.Device;
import com.workforce.webapp.model.device.DeviceComplaint;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceComplaintRepository extends JpaRepository<DeviceComplaint, String> {

    List<DeviceComplaint> findAllByOrgId(Long id);

    List<DeviceComplaint> findAllByDevice(Device device);
}
