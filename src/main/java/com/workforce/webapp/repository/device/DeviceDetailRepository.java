package com.workforce.webapp.repository.device;

import com.workforce.webapp.model.device.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.device.DeviceDetail;

import java.util.List;


public interface DeviceDetailRepository extends JpaRepository<DeviceDetail, String> {

    //Optional<DeviceDetail> findOneByDevice(TrackingDevice device);

    //@Query(value="SELECT * FROM wft.device_details where device_id in (select id from wft.tracking_device where assigned_to_id =:orgid)",nativeQuery=true)
    //<DeviceDetail> findAllByOrg(@Param("orgid") long entityKey);

    //Optional<DeviceDetail> findByDevice(TrackingDevice device);

    List<DeviceDetail> findAllByOrgId(Long Id);
}
