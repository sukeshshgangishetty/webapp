package com.workforce.webapp.repository.device;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.device.Device;


public interface DeviceRepository extends JpaRepository<Device, String> {

    //List<TrackingDevice> findAllByCreatedBy(UserDetails user);
    //List<TrackingDevice> findAllByAssignedTo(Organization org);
    //Optional<Device> findByDeviceNumber(String deviceNo);

    List<Device> findAllByOrgId(Long Id);

}
