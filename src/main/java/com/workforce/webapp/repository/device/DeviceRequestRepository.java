package com.workforce.webapp.repository.device;

import com.workforce.webapp.model.device.DeviceRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceRequestRepository extends JpaRepository<DeviceRequest, String> {

    List<DeviceRequest> findAllByOrgId(Long orgId);
}
