package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.LeaveBalance;
import com.workforce.webapp.model.LeaveTypes;

public interface LeaveBalanceRepository extends JpaRepository<LeaveBalance, Long> {

    List<LeaveBalance> findByEmployeeEmpId(long empid);

    List<LeaveBalance> findByEmployeeEmpIdAndLeaveType(long empid, LeaveTypes leaveTypes);
    //

}
