package com.workforce.webapp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.PayrollItem;
import com.workforce.webapp.model.PayrollItemType;

public interface PayrollItemRepository extends JpaRepository<PayrollItem, Long> {

    Optional<PayrollItem> findByName(String name);

    Optional<PayrollItem> findByNameAndType(String name, PayrollItemType type);

}
