package com.workforce.webapp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.EmployeeSalaryStructure;

public interface SalaryStructureRepository extends JpaRepository<EmployeeSalaryStructure, Long> {

    Optional<EmployeeSalaryStructure> findByEmpEmpId(long empid);
}
