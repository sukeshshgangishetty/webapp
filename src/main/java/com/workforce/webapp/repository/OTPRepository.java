package com.workforce.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.workforce.webapp.model.OTP;


@Repository
public interface OTPRepository extends JpaRepository<OTP, Long> {

    OTP findByValue(int value);

    OTP findByPhone(String phone);

    OTP findByPhoneAndOtpfor(String phone, int otpfor);

}
