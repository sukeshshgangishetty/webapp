package com.workforce.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.EmployeeOboardingDetails;

public interface EmployeeOboardingDetailsRepository extends JpaRepository<EmployeeOboardingDetails, Long> {

}
