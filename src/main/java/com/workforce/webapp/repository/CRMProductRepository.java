package com.workforce.webapp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.CRMProduct;

public interface CRMProductRepository extends JpaRepository<CRMProduct, Long> {

    Optional<CRMProduct> findByName(String name);

}
