package com.workforce.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.workforce.webapp.model.Organization;


public interface OrganizationRepository extends JpaRepository<Organization, Long> {

    Organization findOneByContactphone(String phone);

    Organization findOneById(long entityKey);

}
