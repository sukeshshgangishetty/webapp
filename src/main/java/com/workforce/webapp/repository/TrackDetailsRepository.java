package com.workforce.webapp.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.w3c.dom.stylesheets.LinkStyle;

import com.workforce.webapp.model.TrackDetails;


@Repository
public interface TrackDetailsRepository extends JpaRepository<TrackDetails, Long> {


    @Query("SELECT u FROM  TrackDetails u where u.ctrk= :key ORDER BY TID DESC ")
//LIMIT 1
    List<TrackDetails> findLastTrackByEkey(@Param("key") long key);


    List<TrackDetails> findByCtrk(long ctrk);
    //select * from track_details where ctrk in (select ctid from wft.current_track where date(startedat)="2018-06-22" and etyky=5 and cttype=2);

    @Query(value = "select * from track_details  where ctrk in (select ctid from wft.current_track  where date(startedat)=:date and etyky=:etky and cttype=:ttype) order by updated_at", nativeQuery = true)
    List<TrackDetails> findByDateAndEkey(@Param("date") Date date, @Param("etky") long etky, @Param("ttype") int ttype);

    //List <TrackDetails> findByDateAndEkeyOrderByUpdatedAtDesc(@Param("date") Date date,@Param("etky") long etky,@Param("ttype") int ttype);


    @Query(value = "SELECT * FROM wft.track_details where ctrk= :ctrk and date(updated_at)= (:udate)", nativeQuery = true)
    List<TrackDetails> findByDateAndEkey(@Param("ctrk") long ctrk, @Param("udate") String udate);

}
