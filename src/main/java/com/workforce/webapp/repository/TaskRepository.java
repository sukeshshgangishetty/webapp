package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByOrg(Organization org);

}
