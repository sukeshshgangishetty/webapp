package com.workforce.webapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.Executive;
import com.workforce.webapp.model.Organization;

public interface ExecutiveRepository extends JpaRepository<Executive, Long> {

    List<Executive> findByOrgId(Long orgId);

    List<Executive> findByOrg(Organization org);

    Executive findByPhone(String phone);

    Executive findByExeId(Long exeId);

    List<Executive> findCountByOrg(Organization org);

    Long countByOrg(Organization org);

}
