package com.workforce.webapp.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.workforce.webapp.model.OrderDetails;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;


public interface OrderRepository extends JpaRepository<OrderDetails, Long> {

    List<OrderDetails> findAllByOrg(Organization org);

    List<OrderDetails> findAllByOrderCollectedAndCreatedatBetween(UserDetails user, Date start, Date end);

    List<OrderDetails> findAllByOrderCollected(UserDetails user);

    List<OrderDetails> findAllByCreatedatBetweenAndOrg(Date date, Date date2, Organization org);

    List<OrderDetails> findAllByOrdertoAndOrg(Client client, Organization org);

    List<OrderDetails> findAllByOrderto(Client cilent);

    OrderDetails findByOrderid(long oid);

    @Query(value = "SELECT ifnull(sum(total),0) FROM wft.order2 where orderto_id=? and orstatus=1", nativeQuery = true)
    Double sumOfClientActiveOrders(long cid);

    @Query(value = "SELECT count(*) from wft.order2 where orderto_id=?", nativeQuery = true)
    Integer findAllByOrderto(long cid);

    @Query(value = "SELECT count(*) from wft.order2 where orstatus=?2 and orderto_id=?1", nativeQuery = true)
    Integer findAllByOrdertoAndOrstatus(long cid, int status);


}
