package com.workforce.webapp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.workforce.webapp.model.CRMProductAddOn;

public interface CRMProductAddOnRepository extends JpaRepository<CRMProductAddOn, Long> {

    Optional<CRMProductAddOn> findByName(String name);

    List<CRMProductAddOn> findByProductCrmpid(long crmpid);

    Optional<CRMProductAddOn> findByAddonCode(String code);

}
