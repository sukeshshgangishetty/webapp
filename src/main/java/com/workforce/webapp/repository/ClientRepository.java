package com.workforce.webapp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.workforce.webapp.model.UserDetails;


public interface ClientRepository extends JpaRepository<Client, Long> {

    Client findByPhone(String phone);

    List<Client> findAllByOrgId(long orgid);

    Optional<Client> findOneByIdAndOrgId(long id, long orgid);

    Long countByOrgId(long orgid);

    List<Client> findAllByAddedBy(UserDetails user);

    List<Client> findAllByAssignedTo(long assignedto);

    Client findOneById(long id);
//
//	@Query(value = "SELECT * FROM ("
//			+ "(SELECT `order2`.orderid as id, 1 as `type`, "
//			+ "`order2`.total as amount, `order2`.createdat as timestmp FROM `order2` where order2.orderto_id=?1 and order2.orstatus=1 "
//			+ "and year(createdat) = ?2 and month(createdat)= ?3) "
//			+ "UNION ALL (SELECT `payment`.payid AS id,2 as `type`, `payment`.amount as amount,"
//			+ " `payment`.collected_at as timestmp FROM payment where payment.from_id=?1 and payment.`status` = 1 "
//			+ "and year(collected_at) = ?2 and month(collected_at)= ?3)) results ORDER BY timestmp ASC", nativeQuery = true)
//	List<Object> findClientLedgerEntry(long cid,int year, int month);

}
