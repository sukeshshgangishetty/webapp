package com.workforce.webapp.utils;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.imageio.ImageIO;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;

public class RestUtils {

    public static final int CLIENT_UPDATE_APPROVE = -1;
    public static final int CLIENT_UPDATE_REJECT = -2;
    public static final int CLIENT_UPDATE_REJECTED_UNDO = -3;
    public static final int LEAVE_START_MONTH = 4;

    public static final DecimalFormat df2 = new DecimalFormat(".##");

    public static Map map(Object... args) {
        Map map = new HashMap();
        for (int i = 0; i < args.length; i += 2) {
            map.put(args[i], args[i + 1]);
        }
        return map;
    }

    //

    public static double distance(double lat1, double lat2, double lon1, double lon2) {
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);


        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2), 2);

        double c = 2 * Math.asin(Math.sqrt(a));

        double r = 6371;

        return (c * r);
    }


    public static boolean isAuthorized(String username, String pswd) {
        // TODO Auto-generated method stub
        if ((username.equals("abhi") || username.equals("sukesh") || username.equals("tanuja") || username.equals("ranveer")) && pswd.equals("$neem!!t")) {
            return true;
        } else {
            return false;
        }
    }

    public static String getActivationKey() {
        // TODO Auto-generated method stub
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String md5(String pass) {
        String generatedPassword = null;

        // Create MessageDigest instance for MD5
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(pass.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //Add password bytes to digest

        return generatedPassword;
    }


    public static String getUserLocation(String lat, String lon) {
        String readUserFeed = readUserLocationFeed(lat, lon);
        try {
            JSONObject Strjson = new JSONObject(readUserFeed);
            String disname = Strjson.getString("display_name");
            if (!disname.isEmpty()) {
                return disname;
            } else {
                return "Location not found";
            }
        } catch (Exception e) {
            return "Location Error";
        }
    }

    public static String readUserLocationFeed(String lat, String lon) {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(
                "https://workforce.eorbapp.com/geolocation?format=json&lat=" + lat + "&lon=" + lon);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                //logger.error("Error");
                //Log.e(ReverseGeocode.class.toString(), "Failed to download file");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }


    public static String Base64ToImage(String data, String filename, String path) {

        try {
            String base64Image = data.split(",")[1];
            byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));

            // write the image to a file

            File directory = new File(path);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            File outputfile = new File(path + filename);
            if (!outputfile.exists()) {
                outputfile.getParentFile().mkdirs();
                outputfile.createNewFile();
            }

            ImageIO.write(img, "png", outputfile);
            return outputfile.getAbsolutePath();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static Date getFirstDateOfMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }


}

