package com.workforce.webapp.utils;

public class Constant {


    /******* Paths Start **************/

//	public static String FolderPath = new AppExtension().getFolderPath();
//	public static final String FILE_UPLOAD_ERROR = "File Upload Error";
//	public static String FolderPathClaim = FolderPath + "data/";
//	public static String DatabasePathClaim = "data/";
    /******* Paths End **************/


    // Common Messages
    public static String NOT_FOUND = "Resource Not Found";
    public static String DEPENDENCY_FAILED = "ERROR ON OTHER MICORSERVICE";
    public static String SUCCESS = "Success";
    public static String SUCCESS_ADDED = "Successfully Added";
    public static String SUCCESS_UPDATED = "Successfully Updated";
    public static String SUCCESS_RECEIVE = "Successfully Fetched";
    public static String SUCCESS_DELETED = "Successfully Deleted";
    public static String SUCCESS_STATUS = "Status Successfully Updated";
    public static String SUCCESS_DOWNLOADED = "Successfully Downloaded";

    public static String NOT_AUTHORIZED = "Not Authorized to do this operation";
    public static String ERROR = "Internal Server Error";
    public static String MSG_PHONE_NUMBER_MISSING = "Phone Number is missing";
    public static String MESSAGE_LOGOUT = "Logout Successfully";
    public static String DELETION_ERROR = "Not Deleted!!";
    public static String EMAIL_ID_ALREADY_EXIST = "The email address already exists";
    public static String MOBILE_NO_ALREADY_EXIST = "The Mobile no already exists";
    public static String NAME_ALREADY_EXIST = "Name already exists";
    public static String FAILURE = "Failure";
    public static String USER_WRONG_OTP = "Verification code is not Correct";
    public static String SESSION_EXPIRED = "Verification link is expired. Try again";
    public static String COMPANY_REGISTERED = "Company Registered Successfully";
    public static String WRONG_COMPANY_ID = "Company Id is not Correct";
    public static String STATUS_CHANGED = "Status Changed Successfully";
    public static String NULL_FIELDS = "Some fields are Null";
    public static String NULL_VALU = "Please Enter The Same Text";
    public static String SECTOR_EVAL_EMPTY = "This sector evaluation attribute are not available";
    public static String APP_FORM_EMPTY = "There is no approved form of selected Sector and Sub Unit respectively";

    public static String FORM_SAVED = "Form Successfully Saved";
    public static String FORM_SUBMITTED = "Form Successfully Submitted and Moved To Submitted";
    public static String FORM_APPROVED = "Form Successfully Approved and Moved To Evaluation Sheet";
    public static String FORM_RETURNED = "Form Successfully Sent Back For Correction";
    public static String FORM_DRAFT = "Form Successfully Moved to Draft";
    public static String FORM_REJECTED = "Form Successfully Rejected and Moved To Rejected";

    /** Status Ends **/

    /******* Paths Start **************/

    public static String DatabasePathEval = "data/eval/";
    /******* Paths End **************/

    /************** ACL Starts *********************/
    public static final String ROLE_AD = "ROLE_AD";
    public static final String ROLE_US = "ROLE_US";
    /*************** state management Starts *******/
    public static final String STATE_REQ = "STATE_REQUEST";
    public static final String STATE_APPRVD = "STATE_APPROVED";
    public static final String STATE_DISSPRVD = "STATE_DISSAPROVED";

    public static final String REQUESTED = "REQUEST SENT SUCCESSFULLY";
    public static final String APPROVED = "REQUEST APPROVED SUCCESSFULLY";
    public static final String DISSAPROVED = "REQUEST DISSAPROVED SUCCESSFULLY";


    /**
     * UserDetails Starts
     **/
    public static String ROLE_NOT_EXIST = "Some Roles are missing.You can Save form in draft only.";
    public static String USER_ADDED = "User Successfully Added";
    public static String USER_UPDATED = "User Successfully Updated";
    public static String USER_DUPLICATE = "Duplicate Entry for Phone Number";
    public static String USER_DISABLE_FAIL = "User can't be disabled because its last user as sysadmin";
    public static String USER_PASSWORD_CHANGED = "Password Changed Successfully";
    public static String USER_WRONG_FIVE_PASSWORD = "Your Password Can Not Same As Last Five password";
    public static String USER_WRONG_PASSWORD = "Password is not Correct";
    public static String MAIL_SENT_FOR_PASSWORD_GENERATION = "Password generation link has been sent on your email id";
    public static String USER_WRONG_USERNAME_PASSWORD = "User Name or Password is not Correct";
    public static String USER_NAME_ALREADY_EXIST = "User Name already exists";
    public static String USER_ALREADY_EXIST = "You Are already Registered Password generation link has been sent on your email id";
    // User Login
    public static String USER_LOGIN_SUCCESS = "Welcome User";
    public static String USER_LOGIN_AUTH_FAILED = "You typed wrong Mobile number or Password";
    public static String USER_NOT_EXIST = "User or mailId doesn't exist";
    //	public static String NAME_ALREADY_EXIST = "Sector already exists";
    public static String USER_WRONG_VERIFCODE = "You typed wrong Verification code";
    public static String NOT_VERIFIDE = "User doesn't Verified please verify your email first";
    /** UserDetails Ends **/

    /**
     * Role Starts
     **/
    public static String ROLE_ADDED = "Role Successfully Added";
    public static String ROLE_UPDATED = "Role Successfully Updated";
    public static String ROLE_NAME_DUPLICATE = "Same Role Name already exist to this user";
    public static String ROLE_TYPE_UNIT_DUPLICATE = "Same User can not have active role in more than 1 unit";
    public static String ROLE_DISABLE_FAIL = "Role can't be disabled because its last role of sysadmin";
    public static String ROLE_DUPLICATE_SYS_ADMIN = "System admin already exist so misc data can't be inserted";
    public static String ROLE_USER_WRONG_UNIT = "User doesn't exist in selected unit";

    public static String ROLE_UNIT_ADMIN_EXIST = "Unit Admin already exist";
    public static String ROLE_VERIFIER1_EXIST = "Verifier already exist";
    public static String ROLE_VERIFIER2_EXIST = "Approver already exist";
    public static String ROLE_APPROVER_EXIST = "Approver Already exist";
    public static String ROLE_VERIFIER1_DOES_NOT_EXIST = "Verifier does not exist";
    public static String ROLE_VERIFIER2_DOES_NOT_EXIST = "Approver does not exist";
    public static String ROLE_APPROVER_DOES_NOT_EXIST = "Approver does not exist";

    /** Role Ends **/

    /************** ACL Ends *********************/


    public static String DatabasePathPromMate = "data/promMate/";
    public static String DatabasePathForm = "data/form/";
    public static String DatabasePathGallery = "data/gallery/";
    public static String DatabasePathSlider = "data/slider/";
    public static String DatabasePathAboutNeca = "data/aboutNeca/";
    public static String DatabasePathCircular = "data/circular/";
    public static String SUCCESS_FILE_UPLOADED = "File Successfully Uploaded";

    public static String ALREADY_PRESENT = "ALREADY PRESENT";

    // Admin
    public static String ADMIN_SIZE = "You cannot create more than two admin at a time.";
    public static String ADMIN_DELETE_SIZE = "You cannot delete the admin because at a time atleast one admin should be present.";

    public static String MODULE_EXIST = "Please add atleast one Module";
    public static String OWNER_FREE = "Shop Owner Free";
    public static String OWNER_NOT_FREE = "Shop Owner Not Free";

    public static String CHANGE_ACTIVE_STATUS_SUCCESS = " is active now.";

    public static String CHANGE_DEACTIVE_STATUS_SUCCESS = " is deactive now.";

    public static String QTY_EXEED = "Quantity exedded.";

    public static String DUPLICATE_SERVICE_NUMBER = "You cannot use this service number because this service number is already associated to another user .";

}

