package com.workforce.webapp.utils;

public class OTPRequestPurpose {

    public final static int ORG_REGISTRATION = 1;

    public final static int OTHERS = 2;

    public final static int EXPORT_DATA = 3;

    public final static int RESET_PASSWORD = 4;

    public final static int FORGET_PASSWORD = 5;


}
