package com.workforce.webapp.projection;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public interface LoginDetails {


    @JsonProperty("logid")
    long getCtid();

    @JsonProperty("loginat")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    Date getStartedat();

    String getImei();

    String getManufacturer();

    String getModel();

    String getOsversion();


}
