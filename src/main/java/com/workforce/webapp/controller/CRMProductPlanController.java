package com.workforce.webapp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.CRMProductPlan;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.CRMProductPlanRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@CrossOrigin
@EnableAutoConfiguration
@RestController

public class CRMProductPlanController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    CRMProductPlanRepository crmProdplanRepo;

    @PostMapping("/crmproductplan")
    ResponseEntity<?> addCrmProductPlan(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                        @RequestBody CRMProductPlan productPlan) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.CRM_ADMIN)) {
                Optional<CRMProductPlan> exitDepo = crmProdplanRepo.findByName(productPlan.getName());
                if (exitDepo.isPresent()) {
                    return new ResponseEntity<Response>(new Response("Product plan Already Exists"),
                            HttpStatus.ALREADY_REPORTED);
                } else {
                    // ProductCategory pcategory = new ProductCategory(, new
                    // Date(), user, org);
                    productPlan.setCreatedAt(new Date());

                    productPlan = crmProdplanRepo.save(productPlan);

                    return ResponseEntity.ok(productPlan);
                }

            } else {

                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {

            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);

        }

    }

    @GetMapping("/crmproductplan")
    ResponseEntity<?> getCrmProductPlans(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {

                List<CRMProductPlan> deps = crmProdplanRepo.findAll();
                return ResponseEntity.ok(deps);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);

            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/crmproductplan/{id}")
    ResponseEntity<?> getProductPlanById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {
                Optional<CRMProductPlan> dep = crmProdplanRepo.findById(id);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }


    @GetMapping("/crmproduct/{crmpid}/crmproductplan")
    ResponseEntity<?> getProductPlanByProductId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                @PathVariable long crmpid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {
                List<CRMProductPlan> dep = crmProdplanRepo.findByProductCrmpid(crmpid);
                return ResponseEntity.ok(dep);

            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

}


