package com.workforce.webapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.FilterItem;
import com.workforce.webapp.dto.FilterType;
import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.Discount;
import com.workforce.webapp.model.Executive;
import com.workforce.webapp.model.Location;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.Task;
import com.workforce.webapp.model.TaskAccessibility;
import com.workforce.webapp.model.TaskPriority;
import com.workforce.webapp.model.TaskStatus;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.TaskRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@RestController
@CrossOrigin
public class TaskController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    TaskRepository taskRepo;

    @Autowired
    OrganizationRepository orgRepo;

    @Autowired
    ExecutiveRepository exeRepo;

    @GetMapping("/task")
    ResponseEntity<Response> getAllTasks(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            List<Task> tasks;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Organization org = orgRepo.findOneById(user.getEntityKey());
                tasks = taskRepo.findByOrg(org);
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Organization org = exeRepo.findById(user.getEntityKey()).get().getOrg();
                tasks = taskRepo.findByOrg(org);
            } else {
                return ResponseEntity.notFound().build();
            }

            List<FilterItem> filters = new ArrayList<>();
            filters.add(new FilterItem("title", FilterType.TEXT));
            filters.add(new FilterItem("description", FilterType.TEXT));
            filters.add(new FilterItem("expstartDate", FilterType.DATE));
            return new ResponseEntity<Response>(new Response("Found Results", tasks, filters, HttpStatus.OK),
                    HttpStatus.OK);

        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/task")
    ResponseEntity<Response> addNewTask(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestBody Task task) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Organization org;
                if (user.getRole().equals(Userrole.ORGANIZATION)) {
                    System.out.println("get 1");
                    org = orgRepo.findOneById(user.getEntityKey());
                } else {
                    Optional<Executive> exeop = exeRepo.findById(user.getEntityKey());
                    System.out.println("get 2");
                    if (exeop.isPresent()) {
                        Executive exe = exeop.get();
                        if (exe.getOrg() != null) {
                            org = exe.getOrg();
                        } else {
                            // org missing
                            return new ResponseEntity<Response>(new Response("Identities might be misplaced, Contact developer ", HttpStatus.NOT_ACCEPTABLE),
                                    HttpStatus.NOT_ACCEPTABLE);
                        }
                    } else {
                        // executive not available
                        return new ResponseEntity<Response>(new Response("Inconsistance of Role, contact developer", HttpStatus.NOT_ACCEPTABLE),
                                HttpStatus.NOT_ACCEPTABLE);
                    }
                }
                task.setOrg(org);
                task.setStatus(TaskStatus.CREATED);
                task = taskRepo.save(task);
                System.out.println("get 3");
                return new ResponseEntity<Response>(new Response("Saved task successfully", task, HttpStatus.OK),
                        HttpStatus.OK);
            } else {

                return new ResponseEntity<Response>(new Response("No enough Permission"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }


    @PostMapping("/task/{taskid}/status/{status}")
    ResponseEntity<Response> updateTaskStatus(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long taskid, @PathVariable TaskStatus status) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Organization org;
                if (user.getRole().equals(Userrole.ORGANIZATION)) {
                    System.out.println("get 1");
                    org = orgRepo.findOneById(user.getEntityKey());
                } else {
                    Optional<Executive> exeop = exeRepo.findById(user.getEntityKey());
                    System.out.println("get 2");
                    if (exeop.isPresent()) {
                        Executive exe = exeop.get();
                        if (exe.getOrg() != null) {
                            org = exe.getOrg();
                        } else {
                            // org missing
                            return new ResponseEntity<Response>(new Response("Identities might be misplaced, Contact developer ", HttpStatus.NOT_ACCEPTABLE),
                                    HttpStatus.NOT_ACCEPTABLE);
                        }
                    } else {
                        // executive not available
                        return new ResponseEntity<Response>(new Response("Inconsistance of Role, contact developer", HttpStatus.NOT_ACCEPTABLE),
                                HttpStatus.NOT_ACCEPTABLE);
                    }
                }

                Optional<Task> taskop = taskRepo.findById(taskid);
                if (taskop.isPresent()) {
                    Task task = taskop.get();
                    if (task.getOrg() != null && task.getOrg().getId() == org.getId()) {
                        task.setStatus(status);
                        taskRepo.save(task);
                        return new ResponseEntity<Response>(new Response("Status Update Successful", HttpStatus.OK),
                                HttpStatus.OK);
                    } else {
                        // data inconsistance
                        return new ResponseEntity<Response>(new Response("Data Inconsistance, contact Admin", HttpStatus.EXPECTATION_FAILED),
                                HttpStatus.OK);
                    }


                } else {
                    // task not found
                    return new ResponseEntity<Response>(new Response("Data not found, Try again", HttpStatus.NOT_ACCEPTABLE),
                            HttpStatus.NOT_ACCEPTABLE);
                }


            } else {

                return new ResponseEntity<Response>(new Response("No enough Permission"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }


    @GetMapping("/task/{tid}")
    ResponseEntity<Response> getTaskbyId(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long tid) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            List<Discount> discounts;
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                try {
                    Task task = taskRepo.getOne(tid);
                    // if(task!=null && task.getOrg().getId() ==
                    // user.getEntityKey()){
                    return new ResponseEntity<Response>(new Response("Found Results", task, HttpStatus.OK),
                            HttpStatus.OK);
                    // }else{
                    // return new ResponseEntity<Response>(new Response("No
                    // Results Found "), HttpStatus.NO_CONTENT);

                    // }
                } catch (EntityNotFoundException e) {
                    return new ResponseEntity<Response>(new Response("No Results Found"), HttpStatus.NO_CONTENT);

                }

            } else {
                return new ResponseEntity<Response>(new Response("no enough permissions"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/task/sam")
    Task getSampleTask() {
        Task task = new Task();
        task.setTid(1);
        task.setAddress("12-11-11, LB Nagar, Hyderabad");
        task.setAccessibility(TaskAccessibility.GROUP);
        List<String> attachment = new ArrayList<String>();
        attachment.add("/uploads/task/1234212312.jpg");
        attachment.add("/uploads/task/1334443334.jpg");
        attachment.add("/uploads/task/4333445664.jpg");
        task.setAttachments(attachment);
        List<Long> exes = new ArrayList<Long>();
        exes.add(1L);
        exes.add(2L);
        Client client = new Client("XYZ ptv ltd", "7878457845", "asd@dfds.csc", new Location(), null, 0L, "XYZ", null,
                null, null);
        task.setClient(client);
        task.setCreatedAt(new Date());
        task.setExecutives(exes);
        task.setDescription("Sample Task description");
        task.setExpEndDate(new Date());
        task.setExpstartDate(new Date());
        task.setNeedReminder(false);
        Organization org = new Organization();
        task.setOrg(org);
        task.setOthers("Bank of Baroda");
        task.setPriority(TaskPriority.HIGH);
        task.setReminderTime(new Date());
        task.setTitle("Task Title");
        task.setUpdatedAt(new Date());
        task.setCreatedAt(new Date());
        return task;
    }

}