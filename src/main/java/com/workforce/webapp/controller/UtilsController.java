package com.workforce.webapp.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.constants.TemplateIdentifier;
import com.workforce.webapp.dto.MailRequest;
import com.workforce.webapp.dto.MailResponse;
import com.workforce.webapp.service.EmailService;

@RestController
public class UtilsController {


    @Autowired
    private EmailService emailService;

    @PostMapping("/sendemail")
    public MailResponse sendEmail(@RequestBody MailRequest request) {
        Map<String, Object> model = new HashMap<>();
        model.put("name", request.getName());
        model.put("url", "https://workforce.eorbapp.com/reset.html");
        return emailService.sendEmail(request, model, TemplateIdentifier.ACTIVATION_MAIL);

    }
}
