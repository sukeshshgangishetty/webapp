package com.workforce.webapp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.workforce.webapp.criteria.SearchOperation;
import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.ExpensesCategory;
import com.workforce.webapp.model.HrmsExpenses;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.ExpenseCategoryRepository;
import com.workforce.webapp.repository.HrmsExpenseRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.specifications.ExpensesSpecification;
import com.workforce.webapp.specificationsbuilder.ExpenseSpecificationsBuilder;
import com.workforce.webapp.specificationsbuilder.GenericSpecificationsBuilder;

@CrossOrigin
@EnableAutoConfiguration
@RestController
public class ExpenseController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    ExpenseCategoryRepository expCatRepo;

    @Autowired
    HrmsExpenseRepository expnRepo;

    @PostMapping("/expenses/category")
    ResponseEntity<?> AddExpenseCategory(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                         @RequestBody ExpensesCategory expCat) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION) && expCat != null) {
                Optional<ExpensesCategory> exitDepo = expCatRepo.findOneByCategoryName(expCat.getCategoryName());
                if (exitDepo.isPresent()) {
                    return new ResponseEntity<Response>(new Response("Department Name Already Exists"),
                            HttpStatus.ALREADY_REPORTED);
                } else {
                    // ProductCategory pcategory = new ProductCategory(, new
                    // Date(), user, org);
                    expCat.setCategoryName(expCat.getCategoryName().toUpperCase());
                    expCat.setCreatedAt(new Date());
                    expCat = expCatRepo.save(expCat);
                    return ResponseEntity.ok(expCat);
                }

            } else {

                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {

            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);

        }

    }

    @GetMapping("/expenses/category")
    ResponseEntity<?> getExpensesCategory(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                List<ExpensesCategory> deps = expCatRepo.findAll();
                return ResponseEntity.ok(deps);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/expenses/category/{id}")
    ResponseEntity<?> getDepartmentById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                Optional<ExpensesCategory> dep = expCatRepo.findById(id);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @PostMapping("/expenses")
    ResponseEntity<?> AddExpenses(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestBody HrmsExpenses exp) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if ((user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE))
                    && exp.getCat() != null) {
                Optional<ExpensesCategory> exitCat = expCatRepo.findById(exp.getCat().getExpCatId());
                if (exitCat.isPresent()) {
                    if (exp.getExpid() != 0) {
                        exp = expnRepo.save(exp);
                        return ResponseEntity.ok(exp);
                    } else {
                        exp.setCreatedAt(new Date());
//						if(exp.getCreatedBy()!=null) {
//							
//						}						
                        exp.setCreatedBy(user);
                        exp = expnRepo.save(exp);
                        return ResponseEntity.ok(exp);
                    }
                } else {
                    return new ResponseEntity<Response>(new Response("Expenses Category Does not Exists"),
                            HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                return new ResponseEntity<Response>(
                        new Response("Date is in correct, Make sure department is available"),
                        HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/expenses")
    ResponseEntity<?> getExpenses(@RequestHeader(value = "X-AUTH-TOKEN") String token) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {

                List<HrmsExpenses> emps = expnRepo.findAll();
                return ResponseEntity.ok(emps);

            } else if (user.getRole().equals(Userrole.HRMS_EMPLOYEE) && user.isReportingManager()) {
                //
                List<HrmsExpenses> emps = expnRepo.findAll();
                return ResponseEntity.ok(emps);

            } else if (user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                List<HrmsExpenses> emps = expnRepo.findByCreatedBy(user);
                return ResponseEntity.ok(emps);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/expenses/{id}")
    ResponseEntity<?> getExpensesById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Optional<HrmsExpenses> emp = expnRepo.findById(id);
                if (emp.isPresent()) {
                    return ResponseEntity.ok(emp.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else if (user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                Optional<HrmsExpenses> emp = expnRepo.findById(id);
                if (emp.isPresent()) {

//					HrmsExpenses exp = emp.get();
//					if( user.isReportingManager() && exp.get ){
//						
//					}

                    return ResponseEntity.ok(emp.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {

                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/expenses/category/id/{expid}")
    ResponseEntity<?> getExpensesByCategoryId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                              @PathVariable long expid) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                List<HrmsExpenses> emps = expnRepo.findByCatExpCatId(expid);
                return new ResponseEntity<Response>(new Response("Found details", emps), HttpStatus.NOT_ACCEPTABLE);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/expenses/filter")
    ResponseEntity<?> getExpensesByFilter(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                          @RequestParam(value = "query") String query) {

        Specification<HrmsExpenses> spec = resolveSpecification(query);
        try {
            return ResponseEntity.ok(expnRepo.findAll(spec));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Response>(new Response("Unable to parse the query string"),
                    HttpStatus.EXPECTATION_FAILED);
            // TODO: handle exception
        }

    }

    @RequestMapping(method = RequestMethod.GET, value = "/expenses/spec")
    @ResponseBody
    public List<HrmsExpenses> findAllBySpecification(@RequestParam(value = "search") String search) {
        ExpenseSpecificationsBuilder builder = new ExpenseSpecificationsBuilder();
        String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
        Pattern pattern = Pattern.compile("(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)(\\w+?)(\\p{Punct}?),");
        Matcher matcher = pattern.matcher(search + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(4), matcher.group(3), matcher.group(5));
        }

        Specification<HrmsExpenses> spec = builder.build();
        return expnRepo.findAll(spec);
    }

    @GetMapping(value = "/expenses/espec")
    @ResponseBody
    public List<HrmsExpenses> findAllByOrPredicate(@RequestParam(value = "search") String search) {
        Specification<HrmsExpenses> spec = resolveSpecification(search);
        return expnRepo.findAll(spec);
    }

    @GetMapping(value = "/expenses/spec/adv")
    @ResponseBody
    public List<HrmsExpenses> findAllByAdvPredicate(@RequestParam(value = "search") String search) {
        Specification<HrmsExpenses> spec = resolveSpecificationFromInfixExpr(search);
        return expnRepo.findAll(spec);
    }

    protected Specification<HrmsExpenses> resolveSpecificationFromInfixExpr(String searchParameters) {
        CriteriaParser parser = new CriteriaParser();
        GenericSpecificationsBuilder<HrmsExpenses> specBuilder = new GenericSpecificationsBuilder<>();
        return specBuilder.build(parser.parse(searchParameters), ExpensesSpecification::new);
    }

    protected Specification<HrmsExpenses> resolveSpecification(String searchParameters) {

        ExpenseSpecificationsBuilder builder = new ExpenseSpecificationsBuilder();

        String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
        Pattern pattern = Pattern
                .compile("(\\p{Punct}?)(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)(\\w+?)(\\p{Punct}?),");
        Matcher matcher = pattern.matcher(searchParameters + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(5), matcher.group(4),
                    matcher.group(6));
        }
        return builder.build();
    }

    // API - WRITE

    @RequestMapping(method = RequestMethod.POST, value = "/users")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody HrmsExpenses resource) {
        Preconditions.checkNotNull(resource);
        expnRepo.save(resource);
    }

}
