package com.workforce.webapp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.EmployeeOboardingDetails;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.EmployeeOboardingDetailsRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@RestController
@CrossOrigin
@EnableAutoConfiguration
public class EmployeeOnboardingController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    EmployeeOboardingDetailsRepository empOnDetRepo;

    @PostMapping("/employeeOnboarding")
    ResponseEntity<?> AddEmployeeOnboardingDetails(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                   @RequestBody EmployeeOboardingDetails dep) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION) && dep != null) {

                empOnDetRepo.save(dep);
                return ResponseEntity.ok(dep);
            } else {
                return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {

            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/employeeOnboarding")
    ResponseEntity<?> getEmployeeOnboarding(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (!user.getRole().equals(Userrole.ORGANIZATION) && !user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
            List<EmployeeOboardingDetails> deps = empOnDetRepo.findAll();
            return ResponseEntity.ok(deps);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/employeeOnboarding/{id}")
    ResponseEntity<?> getEmployeeOnboardingDetailsById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                Optional<EmployeeOboardingDetails> dep = empOnDetRepo.findById(id);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

}
