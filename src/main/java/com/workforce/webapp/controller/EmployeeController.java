package com.workforce.webapp.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.EmployeeDetails;
import com.workforce.webapp.dto.ReportingMangerMeta;
import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.Department;
import com.workforce.webapp.model.Employee;
import com.workforce.webapp.model.LeaveBalance;
import com.workforce.webapp.model.LeaveTypes;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.UserStatus;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.DepartmentRepository;
import com.workforce.webapp.repository.EmployeeRepository;
import com.workforce.webapp.repository.LeaveBalanceRepository;
import com.workforce.webapp.repository.LeaveRepository;
import com.workforce.webapp.repository.LeaveTypesRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.utils.RestUtils;

@RestController
@CrossOrigin
@EnableAutoConfiguration
public class EmployeeController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    DepartmentRepository departRepo;

    @Autowired
    LeaveTypesRepository lTypeRepo;

    @Autowired
    LeaveBalanceRepository lBalRepo;

    @Autowired
    private LeaveRepository leaveRepo;

    @Autowired
    EmployeeRepository empRepo;

    @PostMapping("/employee")
    ResponseEntity<?> AddEmployee(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestBody Employee emp) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION) && emp.getDepartment() != null) {
                Optional<Department> exitDepo = departRepo.findById(emp.getDepartment().getDepartmentid());
                if (exitDepo.isPresent()) {
                    if (emp.getEmpId() != 0) {
                        emp = empRepo.save(emp);

                        updateReportingManagerStatus(emp);
                        emp = empRepo.save(emp);

                    } else {
                        UserDetails existExeOp = userRepo.findByPhone(emp.getPhone());
                        if (existExeOp != null) {
                            return new ResponseEntity<Response>(
                                    new Response("User Already exist with " + emp.getPhone()),
                                    HttpStatus.NOT_ACCEPTABLE);
                        }
                        emp.setCreatedAt(new Date());
                        try {
                            emp = empRepo.save(emp);
                        } catch (Exception e) {

                            // UK_evds90lpbarcgk03300ms7ty7
                            if (e.getLocalizedMessage().contains("UK_buf2qp04xpwfp5qq355706h4a")) {
                                return new ResponseEntity<Response>(
                                        new Response("Employee already exists with " + emp.getPhone()),
                                        HttpStatus.NOT_ACCEPTABLE);

                            } else if (e.getLocalizedMessage().contains("UK_evds90lpbarcgk03300ms7ty7")) {
                                return new ResponseEntity<Response>(
                                        new Response("Employee already exists with " + emp.getComempid()),
                                        HttpStatus.NOT_ACCEPTABLE);

                            } else {
                                return new ResponseEntity<Response>(
                                        new Response("Exception: " + e.getLocalizedMessage()),
                                        HttpStatus.EXPECTATION_FAILED);
                            }
                            // UK_buf2qp04xpwfp5qq355706h4a

                            // TODO: handle exception
                        }
                        List<LeaveTypes> lTypes = lTypeRepo.findAll();
                        for (LeaveTypes ltype : lTypes) {
                            Calendar c = Calendar.getInstance();
                            int cm = c.get(Calendar.MONTH);
                            double b = ltype.getDefaultValue();

                            if (cm > RestUtils.LEAVE_START_MONTH) {

                                b = b - ((cm - RestUtils.LEAVE_START_MONTH) * b / 12);

                            } else if (cm < RestUtils.LEAVE_START_MONTH) {
                                b = (RestUtils.LEAVE_START_MONTH - cm) * b / 12;
                            }
                            lBalRepo.save(new LeaveBalance(ltype, b, b, emp));
                        }
                        updateReportingManagerStatus(emp);
                        emp = empRepo.save(emp);
                        UserDetails newexe = new UserDetails();
                        newexe.setRole(Userrole.HRMS_EMPLOYEE);
                        newexe.setUsername(emp.getfName() + " " + emp.getLName());
                        newexe.setPhone(emp.getPhone());
                        newexe.setEntityKey(emp.getEmpId());
                        newexe.setStatus(UserStatus.PENDING_SELF_ACTIVATION);
                        newexe.setLogin(emp.getPhone());
                        newexe.setPassword(RestUtils.md5(emp.getPhone()));
                        newexe.setEmail(emp.getEmail());
                        String userActivation = "";
                        while (userActivation.equals("")) {
                            userActivation = RestUtils.getActivationKey();
                            if (userRepo.findByActivationKey(userActivation) == null) {
                                System.out.println("Activation key=" + userActivation);
                                newexe.setActivationKey(userActivation);
                            } else {
                                userActivation = "";
                            }
                        }

                        try {
                            userRepo.save(newexe);
                        } catch (Exception e) {
                            if (e.getMessage().contains("sdf")) {
                                return new ResponseEntity<Response>(new Response("Unique values failed"),
                                        HttpStatus.NOT_ACCEPTABLE);
                            } else {
                                return new ResponseEntity<Response>(
                                        new Response("Exception: " + e.getLocalizedMessage()),
                                        HttpStatus.EXPECTATION_FAILED);
                            }
                            // TODO: handle exception
                        }
                        // Dear ##name##, use your phone number as username and
                        // password for eorbapp.
                        String msg = "Dear " + newexe.getUsername()
                                + ", use your phone number as username and password for HRMS @"
                                + " https://bit.ly/2J9ui1Q -Thanks, Call @ 8886668273, Neemiit";
                        // String msg= "Executive account created for
                        // "+org.getOrgname()+"\n login with your phone number
                        // as uname and pswd";
                        System.out.println("Message=" + msg);
                        SmsServiceImpl.sendSms(newexe.getPhone(), msg);
                    }

                    return ResponseEntity.ok(emp);
                } else {
                    return new ResponseEntity<Response>(new Response("Department Does not Exists"),
                            HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                return new ResponseEntity<Response>(
                        new Response("Date is in correct, Make sure department is available"),
                        HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    private void updateReportingManagerStatus(Employee emp) {
        // TODO Auto-generated method stub

        if (emp.getReportingEmpId() != 0) {

            UserDetails reportingUser = userRepo.findByEntityKeyAndRole(emp.getReportingEmpId(),
                    Userrole.HRMS_EMPLOYEE);
            if (reportingUser != null) {

                reportingUser.setReportingManager(true);
                userRepo.save(reportingUser);

                emp.setReportingEmp(empRepo.getOne(emp.getReportingEmpId()));

            }
        }

    }

    @GetMapping("/employee")
    ResponseEntity<?> getEmployees(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {

                List<Employee> emps = empRepo.findAll();
                return ResponseEntity.ok(emps);
            } else if (user.getRole().equals(Userrole.HRMS_EMPLOYEE) && user.isReportingManager()) {

                List<Employee> emps = empRepo.findByReportingEmpId(user.getEntityKey());
                return ResponseEntity.ok(emps);

            } else if (user.getRole().equals(Userrole.HRMS_EMPLOYEE) && !user.isReportingManager()) {

                Optional<Employee> smo = empRepo.findById(user.getEntityKey());
                EmployeeDetails emp = new EmployeeDetails();
                try {
                    emp.setDetails(smo.get());
                    Employee repEmp = empRepo.getOne(smo.get().getReportingEmpId());
                    emp.setReportingManger(new ReportingMangerMeta(repEmp.getEmpId(),
                            repEmp.getfName() + " " + repEmp.getLName(), repEmp.getDepartment()));
                } catch (NoSuchElementException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                } catch (EntityNotFoundException e) {
                    // TODO: handle exception
                    emp.setReportingManger(null);
                }
                return ResponseEntity.ok(emp);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/employee/{id}")
    ResponseEntity<?> getEmployeeById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                Optional<Employee> emp = empRepo.findById(id);
                if (emp.isPresent()) {
                    return ResponseEntity.ok(emp.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/employee/department/{depid}")
    ResponseEntity<?> getEmployeesByDepartmentId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                 @PathVariable long depid) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION)
                    || (user.getRole().equals(Userrole.HRMS_EMPLOYEE) && user.isReportingManager())) {
                List<Employee> emps = empRepo.findByDepartmentDepartmentid(depid);
                return new ResponseEntity<Response>(new Response("Found details", emps), HttpStatus.OK);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/employee/leave/{empid}")
    ResponseEntity<?> getLeavesByEmpId(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long empid) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            return ResponseEntity.ok(leaveRepo.findByAppliedByEmpId(empid));
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/employee/leavebalance/{empid}")
    ResponseEntity<?> getLeaveBalanceByEmpId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @PathVariable long empid) {
        System.out.println("Data 1");
        // System.out.println("Data 1");
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            System.out.println("Data 2");
            List<LeaveBalance> lBalance = lBalRepo.findByEmployeeEmpId(empid);
            System.out.println("Data 3");
            return ResponseEntity.ok(lBalance);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/employee/profile")
    ResponseEntity<?> getEmployeeProfile(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {

                Optional<Employee> smo = empRepo.findById(user.getEntityKey());
                EmployeeDetails emp = new EmployeeDetails();
                try {

                    emp.setDetails(smo.get());
                    Employee repEmp = empRepo.getOne(smo.get().getReportingEmpId());
                    emp.setReportingManger(new ReportingMangerMeta(repEmp.getEmpId(),
                            repEmp.getfName() + " " + repEmp.getLName(), repEmp.getDepartment()));

                } catch (NoSuchElementException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                } catch (EntityNotFoundException e) {
                    // TODO: handle exception
                    emp.setReportingManger(null);

                }
                return ResponseEntity.ok(emp);

            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/employee/leavebalance")
    ResponseEntity<?> getLeaveBalance(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            List<LeaveBalance> bals;
            if (user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                bals = lBalRepo.findByEmployeeEmpId(user.getEntityKey());
                return ResponseEntity.ok(bals);
            } else {
                return new ResponseEntity<Response>(new Response("Invalid request, try using /{empid} API"),
                        HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

}
