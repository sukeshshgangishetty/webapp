package com.workforce.webapp.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.EmployeeSalaryStructure;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.PayrollItem;
import com.workforce.webapp.model.SalaryStructureItems;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.PayrollItemRepository;
import com.workforce.webapp.repository.SalaryStructureItemsRepository;
import com.workforce.webapp.repository.SalaryStructureRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@CrossOrigin
@EnableAutoConfiguration
@RestController
public class PayrollController {

    @Autowired
    private UserDetailsRepository userRepo;

    @Autowired
    private PayrollItemRepository payRItemRepo;

    @Autowired
    private SalaryStructureRepository salSRepo;

    @Autowired
    private SalaryStructureItemsRepository salItemRepo;

    @GetMapping("/payrolls")
    ResponseEntity<?> getAllPayroll(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                    @RequestParam(required = false) int month, @RequestParam(required = false) int year) {
        return null;
    }

    @GetMapping("/payrolls/{id}")
    ResponseEntity<?> getPayrollById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        return null;
    }

    @GetMapping("/payrolls/employee/{eid}")
    ResponseEntity<?> getPayrollByEmployeeId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @PathVariable long eid, @RequestParam(required = false) int month,
                                             @RequestParam(required = false) int year) {
        return null;
    }

    @PostMapping("/payrolls/item")
    ResponseEntity<?> addPayrollItem(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                     @RequestBody PayrollItem dep) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION) && dep != null) {
                Optional<PayrollItem> exitDepo = payRItemRepo.findByNameAndType(dep.getName(), dep.getType());
                if (exitDepo.isPresent()) {

                    if (dep.getPayrollItemId() != 0) {
                        payRItemRepo.save(dep);
                        return ResponseEntity.ok(dep);
                    }

                    return new ResponseEntity<Response>(new Response("Item Already Exists"),
                            HttpStatus.ALREADY_REPORTED);
                } else {
                    // ProductCategory pcategory = new ProductCategory(, new
                    // Date(), user, org);
                    dep.setName(dep.getName().toUpperCase());
                    dep.setCreatedAt(new Date());
                    payRItemRepo.save(dep);
                    return ResponseEntity.ok(dep);
                }

            } else {

                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {

            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);

        }

    }

    @GetMapping("/payroll/item")
    ResponseEntity<?> getPayrollITems(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (!user.getRole().equals(Userrole.ORGANIZATION)) {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
            List<PayrollItem> deps = payRItemRepo.findAll();
            return ResponseEntity.ok(deps);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/payroll/item/{id}")
    ResponseEntity<?> getPayrollItemById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Optional<PayrollItem> dep = payRItemRepo.findById(id);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @PostMapping("/payroll/salaryStructure")
    ResponseEntity<?> addSalaryStructureForEmployee(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                    @RequestBody EmployeeSalaryStructure sStructure) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {

                if (sStructure.getEmp() != null) {

                    Optional<EmployeeSalaryStructure> exeSalStructure = salSRepo
                            .findByEmpEmpId(sStructure.getEmp().getEmpId());

                    if (exeSalStructure.isPresent()) {
                        if (exeSalStructure.get().getEssId() == sStructure.getEssId()) {
                            // update
                            if (sStructure.getSalaryItems() != null && !sStructure.getSalaryItems().isEmpty()) {

                                Set<SalaryStructureItems> salItems = new HashSet<>();
                                Iterator<SalaryStructureItems> it = sStructure.getSalaryItems().iterator();
                                while (it.hasNext()) {
                                    salItems.add(salItemRepo.save(it.next()));
                                }
                                sStructure.setSalaryItems(salItems);
                                sStructure.setAddedBy(user);
                                sStructure = salSRepo.save(sStructure);
                                return ResponseEntity.ok(sStructure);

                            } else {
                                // salary structure missing
                                return new ResponseEntity<Response>(new Response("Missing Salary Structure ITems Data"),
                                        HttpStatus.NOT_ACCEPTABLE);
                            }

                        } else {
                            // already exists
                            return new ResponseEntity<Response>(new Response("Salary Structure is already created"),
                                    HttpStatus.NOT_ACCEPTABLE);
                        }
                    } else {

                        // add
                        if (sStructure.getSalaryItems() != null && !sStructure.getSalaryItems().isEmpty()) {

                            Set<SalaryStructureItems> salItems = new HashSet<>();

                            for (SalaryStructureItems item : sStructure.getSalaryItems()) {

                                salItems.add(salItemRepo.save(item));

                            }
                            sStructure.setSalaryItems(salItems);
                            sStructure.setAddedBy(user);
                            sStructure = salSRepo.save(sStructure);
                            return ResponseEntity.ok(sStructure);

                        } else {
                            // salary structure missing
                            return new ResponseEntity<Response>(new Response("Missing Salary Structure ITems Data"),
                                    HttpStatus.NOT_ACCEPTABLE);
                        }

                    }

                } else {
                    return new ResponseEntity<Response>(new Response("Missing Employee Data"),
                            HttpStatus.NOT_ACCEPTABLE);
                }

            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/payroll/salaryStructure/{id}")
    ResponseEntity<?> getsalaryStructureById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Optional<EmployeeSalaryStructure> dep = salSRepo.findById(id);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/payroll/salaryStructure")
    ResponseEntity<?> getAllSalaryStructures(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (!user.getRole().equals(Userrole.ORGANIZATION)) {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
            List<EmployeeSalaryStructure> deps = salSRepo.findAll();
            return ResponseEntity.ok(deps);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/payroll/salaryStructure/employee/{empid}")
    ResponseEntity<?> getsalaryStructureByEmployeeId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                     @PathVariable long empid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Optional<EmployeeSalaryStructure> dep = salSRepo.findByEmpEmpId(empid);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

}
