package com.workforce.webapp.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.workforce.webapp.constants.TemplateIdentifier;
import com.workforce.webapp.dto.MailRequest;
import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.OTP;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.OrganizationStatus;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.UserStatus;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.service.EmailService;
import com.workforce.webapp.utils.RestUtils;

@CrossOrigin
@RestController
@EnableAutoConfiguration
public class OrganizationController {

    @Autowired
    private OTPRepository otpRepo;

    @Autowired
    private OrganizationRepository orgRepo;

    @Autowired
    private UserDetailsRepository userRepo;

    @Autowired
    private EmailService emailService;

    long port = 587;

    String username = "test1.neemiit@gmail.com";

    String password = "Neem!!T12";

    String host = "smtp.gmail.com";

    private static final Logger logger = LoggerFactory.getLogger(OrganizationController.class);

    public boolean logRequest(String name) {
        logger.info("Api request received for " + name);
        return true;
    }

    @PostMapping("/register")
    public ResponseEntity<Response> addOrganization(@RequestBody Organization organization,
                                                    @RequestParam(value = "otp") int otp) {
        System.out.println("Organization=" + organization);
        OTP ot = otpRepo.findByValue(otp);
        logger.info("Otp:" + ot);
        if (ot != null && ot.getPhone().equals(organization.getContactphone())) {
            Organization existingOrganization = orgRepo.findOneByContactphone(organization.getContactphone());
            System.out.println("dfd= " + organization.getContactphone());
            organization.setStatus(OrganizationStatus.ACTIVE);
            orgRepo.save(organization);
            UserDetails newOrg = new UserDetails();
            newOrg.setRole(Userrole.ORGANIZATION);
            newOrg.setUsername(organization.getContactname());
            newOrg.setEmail(organization.getContactemail());
            newOrg.setPhone(organization.getContactphone());
            newOrg.setEntityKey(organization.getId());
            newOrg.setStatus(UserStatus.PENDING_SELF_ACTIVATION);
            newOrg.setLogin(organization.getContactphone());
            String userActivation = "";
            while (userActivation.equals("")) {
                userActivation = RestUtils.getActivationKey();
                if (userRepo.findByActivationKey(userActivation) == null) {
                    System.out.println("Activation key=" + userActivation);
                    newOrg.setActivationKey(userActivation);
                } else {
                    userActivation = "";
                }
            }
            userRepo.save(newOrg);
            otpRepo.deleteById(ot.getId());
            Map<String, Object> model = new HashMap<>();
            model.put("name", organization.getContactname());

            ///activation/5e2d55c3b4274044858aed01ada097cf
            model.put("url", "http://hrm.eorbapp.com/activation/" + userActivation);
            MailRequest req = new MailRequest();
            req.setName(organization.getContactemail());
            req.setMailfrom("sukeshkumar.gdh@gmail.com");
            req.setMailto(organization.getContactemail());
            req.setSubject("Eorb Workforce Registration");
            try {
                emailService.sendEmail(req, model, TemplateIdentifier.ACTIVATION_MAIL);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return new ResponseEntity<Response>(
                    new Response("Registration Sucessful Check your mail for futher details", HttpStatus.OK),
                    HttpStatus.OK);

        } else {
            return new ResponseEntity<Response>(
                    new Response("OTP verification needed to add organization", HttpStatus.NOT_ACCEPTABLE),
                    HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @PostMapping("/org/{orgid}/status")
    public ResponseEntity<Response> changeOrgStatus(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long orgid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null && (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER))) {

            Organization org = orgRepo.getOne(orgid);
            if (org != null) {

                if (org.getStatus().equals(OrganizationStatus.ACTIVE)) {

                    org.setStatus(OrganizationStatus.INACTIVE);
                } else {
                    org.setStatus(OrganizationStatus.ACTIVE);
                }
                org = orgRepo.save(org);
                return new ResponseEntity<Response>(new Response("Status changed successfully", org, HttpStatus.OK),
                        HttpStatus.OK);

            } else {
                return new ResponseEntity<Response>(new Response("Organization details not found", org, HttpStatus.NOT_FOUND),
                        HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorized", HttpStatus.UNAUTHORIZED),
                    HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/org")
    public ResponseEntity<Response> getAllOrgaizations(@RequestHeader("X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null && (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.TRACKINGDEVICE_VENDOR) || user.getRole().equals(Userrole.CRM_USER))) {
            return new ResponseEntity<Response>(new Response("Found data", orgRepo.findAll(), HttpStatus.OK),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorized", HttpStatus.UNAUTHORIZED),
                    HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/org/{orgid}")
    public ResponseEntity<?> getAllOrgaizations(@PathVariable long orgid, @RequestHeader("X-AUTH-TOKEN") String token) {
        Optional<Organization> org = orgRepo.findById(orgid);
        if (org.isPresent()) {
            return ResponseEntity.ok(org.get());
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/org/sample", method = RequestMethod.GET)
    public Organization getSampleOrg() {
        Organization org = new Organization("Neemiit Ptv Ltd", "Sukesh", "Sukesh@gmail.com", "789658412", null,
                "Hyderabad", new Date());
        return org;
    }

}
