package com.workforce.webapp.controller;

import com.workforce.webapp.enums.client.RequestStatus;
import com.workforce.webapp.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class ClientNewController {

    private @Autowired
    ClientService clientService;

    @GetMapping("/client/category")
    public ResponseEntity getAllCategories(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(clientService.getAllCategories(token));
    }

    @GetMapping("/client/category/{id}")
    public ResponseEntity getCategoryById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                          @PathVariable(name = "id") String categoryId) {
        return ResponseEntity.ok(clientService.getCategoryById(token, categoryId));
    }

    @PostMapping(value = "/client/category")
    public ResponseEntity createCategory(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                         @Valid @RequestBody CategoryDto categoryDto) {
        return ResponseEntity.ok(clientService.createCategory(token, null, categoryDto));
    }

    @PutMapping(value = "/client/category/{id}")
    public ResponseEntity updateCategory(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                         @PathVariable(name = "id") String categoryId,
                                         @Valid @RequestBody CategoryDto categoryDto) {
        return ResponseEntity.ok(clientService.createCategory(token, categoryId, categoryDto));
    }

    @DeleteMapping("/client/category/{id}")
    public ResponseEntity deleteCategory(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                         @PathVariable(name = "id") String categoryId) {
        clientService.deleteCategory(token, categoryId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/client")
    public ResponseEntity getAllClient(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(clientService.getAllClient(token));
    }

    @GetMapping("/client/{id}")
    public ResponseEntity getClientById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                        @PathVariable(name = "id") String clientId) {
        return ResponseEntity.ok(clientService.getClientById(token, clientId));
    }

    @PostMapping(value = "/client")
    public ResponseEntity createClient(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                       @Valid @RequestBody ClientDto clientDto) {
        return ResponseEntity.ok(clientService.createClient(token, null, clientDto));
    }

    @PutMapping(value = "/client/{id}")
    public ResponseEntity updateClient(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                       @PathVariable(name = "id") String clientId,
                                       @Valid @RequestBody ClientDto clientDto) {

        return ResponseEntity.ok(clientService.updateClient(token, clientId, clientDto));
    }

    @PatchMapping(value = "/client/request/{id}")
    public ResponseEntity clientRequestStatus(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                              @PathVariable(name = "id") String requestId,
                                              @RequestParam(name = "status") RequestStatus status) {
        clientService.clientRequestStatus(token, requestId, status);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/client/{clientId}/contactDetails")
    public ResponseEntity addClientContactDetails(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                  @PathVariable(name = "clientId") String clientId,
                                                  @Valid @RequestBody ContactDetailDto contactDetailDto) {
        return ResponseEntity.ok(clientService.addClientContactDetail(token, clientId, contactDetailDto, null));
    }

    @PutMapping(value = "/client/{clientId}/contactDetails")
    public ResponseEntity updateClientContactDetails(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                     @PathVariable(name = "clientId") String clientId,
                                                     @Valid @RequestBody ContactDetailDto contactDetailDto) {
        return ResponseEntity.ok(clientService.addClientContactDetail(token, clientId, contactDetailDto, "Update"));
    }

    @PostMapping(value = "/client/{clientId}/kycDetails")
    public ResponseEntity addClientKYCDetails(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                              @PathVariable(name = "clientId") String clientId,
                                              @Valid @RequestBody KycDetailDto kycDetailDto) {
        return ResponseEntity.ok(clientService.addClientKYCDetail(token, clientId, kycDetailDto, null));
    }


    @PutMapping(value = "/client/{clientId}/kycDetails")
    public ResponseEntity updateClientKYCDetails(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                 @PathVariable(name = "clientId") String clientId,
                                                 @Valid @RequestBody KycDetailDto kycDetailDto) {
        return ResponseEntity.ok(clientService.addClientKYCDetail(token, clientId, kycDetailDto, "Update"));
    }

    @PostMapping(value = "/client/{clientId}/address")
    public ResponseEntity addClientAddress(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                           @PathVariable(name = "clientId") String clientId,
                                           @Valid @RequestBody AddressDto addressDto) {
        return ResponseEntity.ok(clientService.addClientAddress(token, clientId, addressDto));
    }

    @PutMapping(value = "/client/address/{addId}")
    public ResponseEntity updateClientAddress(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                              @PathVariable(name = "addId") String addId,
                                              @Valid @RequestBody AddressDto addressDto) {
        return ResponseEntity.ok(clientService.updateClientAddress(token, addId, addressDto));
    }

    @PatchMapping(value = "/client/address/{addId}")
    public ResponseEntity addressPrimaryStatus(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                               @PathVariable(name = "addId") String addId) {
        return ResponseEntity.ok(clientService.addressPrimaryStatus(token, addId));
    }

    @PatchMapping(value = "/client/accDetail/{accId}")
    public ResponseEntity accDetailPrimaryStatus(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                 @PathVariable(name = "accId") String accId) {
        return ResponseEntity.ok(clientService.accDetailPrimaryStatus(token, accId));
    }

    @PostMapping(value = "/client/{clientId}/accDetail")
    public ResponseEntity addClientAccDetail(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @PathVariable(name = "clientId") String clientId,
                                             @Valid @RequestBody AccountDetailDto accountDetailDto) {
        return ResponseEntity.ok(clientService.addClientAccDetail(token, clientId, accountDetailDto));
    }

    @PutMapping(value = "/client/accDetail/{accId}")
    public ResponseEntity updateClientAccDetail(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                @PathVariable(name = "accId") String accId,
                                                @Valid @RequestBody AccountDetailDto accountDetailDto) {
        return ResponseEntity.ok(clientService.updateClientAccDetails(token, accId, accountDetailDto));
    }

    @DeleteMapping("/client/address/{addId}")
    public ResponseEntity deleteAddress(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                        @PathVariable(name = "addId") String addId) {
        clientService.deleteAddress(token, addId);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/client/accDetail/{accId}")
    public ResponseEntity deleteAccDetail(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                          @PathVariable(name = "accId") String accId) {
        clientService.deleteAccDetail(token, accId);
        return ResponseEntity.ok().build();
    }

}
