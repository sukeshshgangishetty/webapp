/*
package com.workforce.webapp.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.dto.TrackDetailsDto;
import com.workforce.webapp.dto.TrackHistoryRequestDto;
import com.workforce.webapp.dto.TrackHistoryResponseDto;
import com.workforce.webapp.model.track.CurrentTrack;
import com.workforce.webapp.model.device.DeviceDetail;
import com.workforce.webapp.model.Executive;
import com.workforce.webapp.model.Location;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.TrackDetails;
import com.workforce.webapp.model.TrackDeviceStatus;
import com.workforce.webapp.model.TrackStatus;
import com.workforce.webapp.model.TrackType;
import com.workforce.webapp.model.device.TrackingDevice;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.CurrentTrackRepository;
import com.workforce.webapp.repository.DeviceDetailsRepository;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.TrackDetailsRepository;
import com.workforce.webapp.repository.TrackingDeviceRepo;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.utils.RestUtils;


@CrossOrigin
@EnableAutoConfiguration
@RestController
@RequestMapping("/track")
public class TrackController {
	private static final Logger logger = LoggerFactory.getLogger(TrackController.class);

	private final int typeTrack = 0;
	private final int IMEI = 1;
	private final int serviceProvider = 2;
	private final int signalStrength = 3;
	private final int ignation = 4;
	private final int time = 5; // if !-1
	private final int date = 6;
	private final int lat = 7;
	private final int lng = 8;
	private final int altitude = 9;
	private final int speed = 10;

	@Autowired
	TrackDetailsRepository trackRepo;

	@Autowired
	UserDetailsRepository userRepo;

	@Autowired
	OrganizationRepository orgRepo;

	@Autowired
	ExecutiveRepository exeRepo;

	@Autowired
	CurrentTrackRepository ctRepo;

	@Autowired
	TrackingDeviceRepo tdRepo;



	@Autowired
	DeviceDetailsRepository ddRepo;

	// @RequestMapping(value="/ac", method = RequestMethod.POST, consumes =
	// MediaType.APPLICATION_JSON_VALUE,produces = "application/json;
	// charset=utf-8")
	@GetMapping("/atd")
	public ResponseEntity<Response> addLoc(@RequestParam Double l, @RequestParam Double ln, @RequestParam Double b, @RequestParam Long k,
			@RequestParam int type) {
		logger.info("App Bat recieved bat" + b + " lat=" + l + " lng= " + ln + "key=" + k);
		if (k != null && k > 0) {
			CurrentTrack ct = null;
			if (type == TrackType.TRACKING_DEVICE) {

				Optional<TrackingDevice> tds = tdRepo.findByDevicenumber("" + k);
				if (tds.isPresent()) {
					TrackingDevice td = tds.get();
					if (td.getStatus().equals(TrackDeviceStatus.ASSIGNED)) {
						ct = tds.get().getCt();
						Location loc = new Location(l, ln, "");
						td.setLastUpdatedAt(new Date());
						td.setLocCurrent(loc);
						tdRepo.save(td);
					} else {
						return new ResponseEntity<Response>(new Response("Device is " + td.getStatus(),HttpStatus.NOT_ACCEPTABLE),HttpStatus.NOT_ACCEPTABLE);
					}

				} else {
					return new ResponseEntity<Response>(new Response("Unrecognized source",HttpStatus.NOT_ACCEPTABLE),HttpStatus.NOT_ACCEPTABLE);
				}

			} else if (type == TrackType.EXECUTIVE_DEVICE_MOBILE) {
//				ct = ctRepo.findOne(k);
			} else {
				return new ResponseEntity<Response>(new Response("Unrecognized source",HttpStatus.NOT_ACCEPTABLE),HttpStatus.NOT_ACCEPTABLE);
			}
			if (ct != null) {
				if (ct.getTstat().equals(TrackStatus.TRACK_STARTED)) {

					try {

						String address = RestUtils.getUserLocation("" + l, "" + ln);
						Location loc = new Location(l, ln, address);// RestUtils.getUserLocation(""+l,
																	// ""+ln)
						ct.setCurrentLoc(loc);
						ct.setCurrbat(b);
						if (ct.getTrackUpdatedAt() == null
								|| ((new Date()).getTime() - ct.getTrackUpdatedAt().getTime()) > 1000 * 60 * 5) {
							TrackDetails td = new TrackDetails(ct.getEtyky(), loc, b, ct.getCtid());
							trackRepo.save(td);

							*/
/* checking Geo Fencing voilation *//*


							if (type == TrackType.EXECUTIVE_DEVICE_MOBILE
									&& ct.getCttype() == TrackType.EXECUTIVE_DEVICE_MOBILE) {

//								Executive exe = exeRepo.findOne(ct.getEtyky());
//								GeoFencing fence = exe.getFeanc();
//								System.out.println("Fence = " + fence);
//								if (fence != null && fence.getPoints() != null && !fence.getPoints().isEmpty()) {
//									System.out.println("points size = " + fence.getPoints().size());
//									if (!RestUtils.isPointInPolygon(fence.getPoints(), loc)) {
//										System.out.println("INPoly");
//										Optional<User> orgData = userRepo.findByEntityKeyAndRole(exe.getOrg().getId(),
//												Userrole.ORGANIZATION);
//										if (orgData.isPresent() && orgData.get().getFbTok() != null) {
//											System.out.println(" send note");
//											Firebase.sendNotification(orgData.get().getFbTok(),
//													exe.getName() + "(" + exe.getPhone() + ") is out of boundery ",
//													"Boundery Voilation Alert");
//										} else {
//											System.out.println(" note not send");
//										}
//									} else {
//										System.out.println(" not IN Poly");
//									}
//
//								}
							}
							ct.setTrackUpdatedAt(new Date());
						}
						ctRepo.save(ct);

						return new ResponseEntity<Response>(new Response("Location Updated",HttpStatus.OK),HttpStatus.OK);

					} catch (NullPointerException ex) {
//						return RestUtils.map("Error", "yes", "Message", "Location not found");
						return new ResponseEntity<Response>(new Response("Location not found",HttpStatus.NOT_ACCEPTABLE),HttpStatus.NOT_ACCEPTABLE);
					}

				} else {
					return new ResponseEntity<Response>(new Response("Track already stopped",HttpStatus.NOT_ACCEPTABLE),HttpStatus.NOT_ACCEPTABLE);
				}

			} else {

				return new ResponseEntity<Response>(new Response("CT Not found",HttpStatus.NOT_ACCEPTABLE),HttpStatus.NOT_ACCEPTABLE);

			}
		} else {

			return new ResponseEntity<Response>(new Response("CT not Found",HttpStatus.NOT_ACCEPTABLE),HttpStatus.NOT_ACCEPTABLE);

		}
	}

	// @RequestMapping(value="/ac", method = RequestMethod.POST, consumes =
	// MediaType.APPLICATION_JSON_VALUE,produces = "application/json;
	// charset=utf-8")
	@GetMapping("/atddd")
	public ResponseEntity<?> addData(@RequestParam String data) {
		String[] values = data.split(",");
		logger.info("Data Recieved" + data);
		if (values != null && (values.length == 11 || values.length == 6)) {

			return ResponseEntity.badRequest().build();
		} else {

			return ResponseEntity.badRequest().build();
		}

	}

	@GetMapping("/td")
	public Map getLoc(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
		UserDetails user = userRepo.findByToken(token);
		if (user != null) {
			if (user.getRole().equals(Userrole.ORGANIZATION)) {

				Organization org = orgRepo.getOne(user.getEntityKey());
				List<Executive> exes = exeRepo.findByOrg(org);
				// logger.info("Track Executives Size="+exes.size());
				List<TrackDetailsDto> tracks = new ArrayList<>();
				for (Executive ex : exes) {
					TrackDetailsDto tdd = new TrackDetailsDto();
					List<CurrentTrack> ct = ctRepo.findLastCurrentTrackByEkey(ex.getExeId(),
							TrackType.EXECUTIVE_DEVICE_MOBILE);
					if (ct != null) {
						CurrentTrack ctt = ct.size() > 0 ? ct.get(0) : null;
						if (ctt != null) {
							*/
/*
 * List<TrackDetails> t =
 * trackRepo.findLastTrackByEkey(ctt.getCtid());
 * if(t!=null){
 * tdd.setTrack(t.size()>0?t.get(0):null); }
 *//*

							tdd.setCurrentTrack(ctt);
							tdd.setName(ex.getName());
							tracks.add(tdd);
						}
					}
				}
				List<TrackingDevice> devices = tdRepo.findAllByAssignedTo(org);
				System.out.println("Devices=" + devices != null ? "" + devices.size() : "null");
				for (TrackingDevice device : devices) {
					TrackDetailsDto tdd = new TrackDetailsDto();
					List<CurrentTrack> ct = ctRepo.findLastCurrentTrackByEkey(device.getId(),
							TrackType.TRACKING_DEVICE);
					if (ct != null) {
						CurrentTrack ctt = ct.size() > 0 ? ct.get(0) : null;
						if (ctt != null) {
							*/
/*
 * List<TrackDetails> t =
 * trackRepo.findLastTrackByEkey(ctt.getCtid());
 * if(t!=null){
 * tdd.setTrack(t.size()>0?t.get(0):null); }
 *//*

							tdd.setCurrentTrack(ctt);
							Optional<DeviceDetail> odd = ddRepo.findByDevice(device);
							if (odd.isPresent() && odd.get().getDriver() != null) {
								DeviceDetail dd = odd.get();
								tdd.setName(dd.getDriver()
										+ (dd.getVehicleNo() != null ? "(" + dd.getVehicleNo() + ")" : ""));
							} else {
								tdd.setName("Device" + device.getId());
							}
							tracks.add(tdd);
						}
					}
				}
				return RestUtils.map("Error", "No", "Tracks", tracks);
			} else {
				return RestUtils.map("Error", "Yes", "Message", "Unauthorised");
			}
		} else {
			return RestUtils.map("Error", "Yes", "Message", "Unauthorised");
		}
	}

	@GetMapping("/td/{trackkey}")
	public Map getTrackkey(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long trackkey) {

		UserDetails user = userRepo.findByToken(token);
		if (user != null && user.getRole().equals(Userrole.ORGANIZATION)) {

			List<TrackDetails> locations = trackRepo.findByCtrk(trackkey);
			return RestUtils.map("Error", "No", "Locations", locations);

		} else {
			return RestUtils.map("Error", "Yes", "Message", "Unauthorised");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = { "/tdhis" })
	public ResponseEntity<?> getTrackDetailsHistory(@RequestHeader(value = "X-AUTH-TOKEN") String token,
			@RequestBody TrackHistoryRequestDto thrdto) {

		logger.info("Date Given:" + thrdto);
		UserDetails user = userRepo.findByToken(token);
		if (user != null && user.getRole().equals(Userrole.ORGANIZATION)) {
			Executive exe = exeRepo.getOne(thrdto.getModelid());

			if (thrdto != null && thrdto.getModelid() != 0 && thrdto.getTtype() != 0) {
				if (thrdto.getFrom() == null || thrdto.getTo() == null) {

					thrdto.setFrom(RestUtils.getFirstDateOfMonth());
					thrdto.setTo(new Date());
				}
				if (thrdto.getTtype() == TrackType.EXECUTIVE_DEVICE_MOBILE) {
//					if (exe.getOrg().getId() == user.getEntityKey()) {
//						List<Attendance> atts = null;
//						// if(thrdto.getFrom()!=null && thrdto.getTo()!=null){
//						atts = attRepo.findAllByEntitykeyAndAttendanceBetween(thrdto.getModelid(), thrdto.getFrom(),
//								thrdto.getTo());
//						// }else{
//						// atts =
//						// attRepo.findAllByEntitykey(thrdto.getModelid());
//						// }
//						if (!atts.isEmpty()) {
//							List<TrackHistoryResponseDto> tracks = new ArrayList<>();
//							for (Attendance att : atts) {
//								List<TrackDetails> track = trackRepo.findByDateAndEkey(att.getAttendance(),
//										exe.getExeid(), thrdto.getTtype());
//								tracks.add(new TrackHistoryResponseDto(att.getAttendance(), track));
//							}
//							return ResponseEntity.ok(tracks);
//						} else {
//							return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NO_CONTENT);
//						}
//					} else {
//						return new ResponseEntity<Response>(new Response("UnKnown"), HttpStatus.NOT_ACCEPTABLE);
//					}

					return new ResponseEntity<Response>(new Response("Development under progress"), HttpStatus.NOT_ACCEPTABLE);
				} else if (thrdto.getTtype() == TrackType.TRACKING_DEVICE) {
					List<TrackHistoryResponseDto> tracks = new ArrayList<>();
					TrackingDevice trd = tdRepo.getOne(thrdto.getModelid());
					if (trd != null && trd.getStatus().equals(TrackDeviceStatus.ASSIGNED)
							&& trd.getAssignedTo().getId() == user.getEntityKey()) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

						Date dateHealper = trd.getCt() != null ? trd.getCt().getStartedat() : null;
						Date now = new Date();
						if (dateHealper != null) {
							// if(thrdto.getFrom()!=null &&
							// thrdto.getTo()!=null){
							dateHealper = dateHealper.getTime() < thrdto.getFrom().getTime() ? thrdto.getFrom()
									: dateHealper;
							now = now.getTime() < thrdto.getTo().getTime() ? now : thrdto.getTo();
							// }

							// Date dateHealper = startDate;
							while (now.getTime() - dateHealper.getTime() > -1) {
								List<TrackDetails> track = trackRepo.findByDateAndEkey(trd.getCt().getCtid(),
										sdf.format(dateHealper));
								tracks.add(new TrackHistoryResponseDto(dateHealper, track));
								dateHealper = new Date(dateHealper.getTime() + 86400000);

							}
							return ResponseEntity.ok(tracks);
						} else {
							return new ResponseEntity<Response>(new Response("Track Not Started"),
									HttpStatus.NO_CONTENT);
						}
					} else {
						return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NO_CONTENT);
					}
				} else {
					return new ResponseEntity<Response>(new Response("Invalid Type"), HttpStatus.NOT_ACCEPTABLE);
				}
			} else {
				return new ResponseEntity<Response>(new Response("Missing Data"), HttpStatus.NO_CONTENT);
			}
		} else {
			return new ResponseEntity<Response>(new Response("UNAUTHORIZED"), HttpStatus.UNAUTHORIZED);
		}
	}

	@RequestMapping("/atdd")
	ResponseEntity<?> addDeviceData(@RequestParam("d") String d) {
		String[] values = d.split(",");

		// if()
		// if(values!=null && values.length>5){
		int type = Integer.parseInt(values[typeTrack]);
		Optional<TrackingDevice> tds = tdRepo.findByDevicenumber("" + values[IMEI]);
		if (tds.isPresent()) {
			CurrentTrack ct = null;
			TrackingDevice td = tds.get();
			if (td.getStatus().equals(TrackDeviceStatus.ASSIGNED)) {
				ct = tds.get().getCt();
				if (ct != null) {
					if (ct.getTstat().equals(TrackStatus.TRACK_STARTED)) {
						if (!values[time].equals("-1")) {
							try {

								String address = RestUtils.getUserLocation("" + values[lat], "" + values[lng]);
								Location loc = new Location(Double.parseDouble(values[lat]),
										Double.parseDouble(values[lng]), address);// RestUtils.getUserLocation(""+l,

								ct.setCurrentLoc(loc);
								ct.setCurrbat(Double.parseDouble(values[signalStrength]));
								if (ct.getTrackUpdatedAt() == null
										|| ((new Date()).getTime() - ct.getTrackUpdatedAt().getTime()) > 1000 * 60
												* 5) {
									TrackDetails td2 = new TrackDetails(ct.getEtyky(), loc,
											Double.parseDouble(values[signalStrength]), ct.getCtid());
									trackRepo.save(td2);
									*/
/* checking Geo Fencing voilation *//*

									ct.setTrackUpdatedAt(new Date());
								}
								ctRepo.save(ct);
								return ResponseEntity.ok().build();
								// return
								// RestUtils.map("Error","No","Message","Location
								// Updated");
							} catch (NullPointerException ex) {
								return new ResponseEntity<Response>(
										new Response("Track already stopped",ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR
											),
										HttpStatus.INTERNAL_SERVER_ERROR);
							}
						} else {
							ct.setCurrentLoc(null);
							ct.setTrackUpdatedAt(new Date());
							ctRepo.save(ct);
							return ResponseEntity.ok().build();
						}
					} else {
						return  new ResponseEntity<Response>(new Response("Track already stopped",HttpStatus.IM_USED),
							HttpStatus.IM_USED);
					}
				} else {
//					return new ResponseEntity<Response>(new Response(HttpStatus.IM_USED.value(),
//							"Current Track Not Found", null, "Check the status of the device", null),
//							HttpStatus.IM_USED);

					return  new ResponseEntity<Response>(new Response("Check the status of the device",HttpStatus.IM_USED),
							HttpStatus.IM_USED);
					// return no CurrentTrack Found
				}
			} else {
//				return new ResponseEntity<Response>(new Response(HttpStatus.IM_USED.value(),
//						"Device is " + td.getStatus(), null, "Check device status", null), HttpStatus.IM_USED);
				// return RestUtils.map("Error","yes","Message","Device is
				// "+td.getStatus());

				return  new ResponseEntity<Response>(new Response("Device is " + td.getStatus()+" Check device status",HttpStatus.IM_USED),
						HttpStatus.IM_USED);			}
		} else {
//			return new ResponseEntity<Response>(
//					new Response(HttpStatus.IM_USED.value(), "Track Not found", null, "Track already stopped", null),
//					HttpStatus.IM_USED);

			return  new ResponseEntity<Response>(new Response("Track Not found or Track already stopped",HttpStatus.IM_USED),
					HttpStatus.IM_USED);
			// return RestUtils.map("Error","Yes","Message","Unrecognised
			// Source");
		}
		// DevicesRequest dr = new DevicesRequest(1, 5, null,
		// orgRepo.findOne(2L),"Need High Accurate devices irrespective of
		// Price", true, new Date());

		// return ResponseEntity.ok().build();
	}

//	@GetMapping("/executive/{eid}/logininfo")
//	ResponseEntity<?> getExecutiveLoginInfo(@RequestHeader(value = "X-AUTH-TOKEN") String token,
//			@PathVariable long eid) {
//		User user = userRepo.findByToken(token);
//		if (user != null) {
//			if (user.getRole().equals(Userrole.ORGANIZATION)) {
//				Executive exe = exeRepo.findOne(eid);
//				if (exe != null && exe.getOrg().getId() == user.getEntityKey()) {
//					List<LoginDetails> details = ctRepo.findAllByEtykyAndCttypeOrderByCtidDesc(eid, 1);
//					return ResponseEntity.ok(details);
//				} else {
//					return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
//				}
//			} else {
//				return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
//			}
//		} else {
//			return new ResponseEntity<Response>(new Response("UNAUTHORIZED"), HttpStatus.UNAUTHORIZED);
//		}
//	}

//	@PostMapping("/executive/{eid}/{ctid}/bulkloc")
//	ResponseEntity<?> uploadBulkData(@RequestBody String data, @PathVariable long eid, @PathVariable long ctid) {
//
//		CurrentTrack ct = ctRepo.findOne(ctid);
//		if (ct != null) {
//			if (ct.getEtyky() == eid) {
//				if (ct.getTstat().equals(TrackStatus.TRACK_STARTED)) {
//
//				} else {
//					// not allowed to update
//				}
//			} else {
//				// inconsistant data
//			}
//
//		} else {
//			// Not found
//		}
//		return null;
//
//	}
//
//	@GetMapping("/planet/getLoc/{key}")
//	Map getPlanetKey(@PathVariable long key) {
//
//		CurrentTrack ct = ctRepo.findOne(key);
//		if (ct != null) {
//			@SuppressWarnings("rawtypes")
//			Map res = new HashMap<>();
//			res.put("lat", ct.getCurrentLoc().getLat());
//			res.put("log", ct.getCurrentLoc().getLng());
//			res.put("dateTime", ct.getTrackUpdatedAt());
//			return RestUtils.map("Error", "yes", "Loc", res);
//		} else {
//			return RestUtils.map("Error", "yes", "Message", "No Content Found");
//		}
//
//	}

}
*/
