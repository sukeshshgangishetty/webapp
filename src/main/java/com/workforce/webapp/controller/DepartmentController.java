package com.workforce.webapp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.Department;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.DepartmentRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@RestController
@CrossOrigin
@EnableAutoConfiguration
public class DepartmentController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    DepartmentRepository departRepo;

//	private static final Logger logger = LogManager.getLogger(DepartmentController.class);

    @PostMapping("/department")
    ResponseEntity<?> AddDepartment(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestBody Department dep) {
        UserDetails user = userRepo.findByToken(token);
//		logger.info("Request to add department");
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION) && dep != null) {
                Optional<Department> exitDepo = departRepo.findByDepartmentName(dep.getDepartmentName());
                if (exitDepo.isPresent()) {
                    if (exitDepo.get().getDepartmentid() == dep.getDepartmentid()) {
                        if (dep.getHod() == null || dep.getHod().getEmpId() == 0) {
                            dep.setHod(null);
                        }
                        departRepo.save(dep);
                        return ResponseEntity.ok(dep);
                    } else {
                        return new ResponseEntity<Response>(new Response("Department Name Already Exists"),
                                HttpStatus.ALREADY_REPORTED);
                    }
                } else {
                    // ProductCategory pcategory = new ProductCategory(, new
                    // Date(), user, org);

                    if (dep.getHod() == null || dep.getHod().getEmpId() == 0) {
                        dep.setHod(null);
                    }
                    dep.setDepartmentName(dep.getDepartmentName().toUpperCase());
                    dep.setCreatedAt(new Date());
                    departRepo.save(dep);
                    return ResponseEntity.ok(dep);
                }

            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {

            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);

        }

    }

    @GetMapping("/department")
    ResponseEntity<?> getDepartments(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
//		logger.info("Request to get all department");
        if (user != null) {
            if (!user.getRole().equals(Userrole.ORGANIZATION) && !user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
            List<Department> deps = departRepo.findAll();
            return ResponseEntity.ok(deps);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/department/{id}")
    ResponseEntity<?> getDepartmentById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
//		logger.info("Request to get department by Id: " + id);
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                Optional<Department> dep = departRepo.findById(id);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

}
