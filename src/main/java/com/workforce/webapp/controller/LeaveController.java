package com.workforce.webapp.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.Employee;
import com.workforce.webapp.model.LeaveApplication;
import com.workforce.webapp.model.LeaveBalance;
import com.workforce.webapp.model.LeaveStatus;
import com.workforce.webapp.model.LeaveTypes;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.EmployeeRepository;
import com.workforce.webapp.repository.LeaveBalanceRepository;
import com.workforce.webapp.repository.LeaveRepository;
import com.workforce.webapp.repository.LeaveTypesRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.utils.RestUtils;

import io.swagger.annotations.Api;

@CrossOrigin
@EnableAutoConfiguration
@RestController
@Api(value = "onlinestore", description = "Operations releated to leaves are performed here")
public class LeaveController {

    @Autowired
    private UserDetailsRepository userRepo;

    @Autowired
    private EmployeeRepository empRepo;

    @Autowired
    private LeaveTypesRepository lTypeRepo;

    @Autowired
    private LeaveBalanceRepository lBalanceRepo;

    @Autowired
    private LeaveRepository leaveRepo;

    @PostMapping("/leaveType")
    ResponseEntity<?> addLeaveType(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestBody LeaveTypes lType,
                                   @RequestParam(value = "addToExist", defaultValue = "false") boolean addToexist) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {

                try {
                    lType = lTypeRepo.save(lType);

                    if (addToexist && lType.getLtId() == 0) {
                        List<Employee> emps = empRepo.findAll();
                        for (Employee emp : emps) {
                            Calendar c = Calendar.getInstance();
                            int cm = c.get(Calendar.MONTH);
                            double b = lType.getDefaultValue();

                            if (cm > RestUtils.LEAVE_START_MONTH) {
                                b = b - ((cm - RestUtils.LEAVE_START_MONTH) * b / 12);
                            } else if (cm < RestUtils.LEAVE_START_MONTH) {
                                b = (cm - RestUtils.LEAVE_START_MONTH) * b / 12;
                            }

                            lBalanceRepo.save(new LeaveBalance(lType, b, b, emp));
                        }
                    }
                    return ResponseEntity.ok(lType);
                } catch (Exception e) {

                    if (e.getLocalizedMessage().contains("UK_x5crfuqo0cky0mu1q1erqx4")) {
                        return new ResponseEntity<Response>(new Response("Seems Leave type already exists"),
                                HttpStatus.NOT_ACCEPTABLE);
                    } else {
                        return new ResponseEntity<Response>(new Response("" + e.getLocalizedMessage()),
                                HttpStatus.EXPECTATION_FAILED);
                    }
                    // TODO: handle exception
                }

            } else {
                return new ResponseEntity<Response>(new Response("No Enough permission to create leave type"),
                        HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/leaveType")
    ResponseEntity<?> getLeaveTypes(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            List<LeaveTypes> leaveTypes = lTypeRepo.findAll();
            return ResponseEntity.ok(leaveTypes);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/leaveType/{ltid}")
    ResponseEntity<?> getLeaveTypeById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long ltid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Optional<LeaveTypes> leaveTypeOp = lTypeRepo.findById(ltid);
            if (leaveTypeOp.isPresent()) {
                return ResponseEntity.ok(leaveTypeOp.get());
            } else {
                return ResponseEntity.notFound().build();
            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/leave")
    ResponseEntity<?> requestLeave(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                   @RequestBody LeaveApplication leave) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            System.out.println("LeaveController.requestLeave(): " + leave);

            if (user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                Optional<Employee> empOp;
                if (leave.getAppliedBy() == null) {
                    empOp = empRepo.findById(user.getEntityKey());
                } else {
                    empOp = empRepo.findById(leave.getAppliedBy().getEmpId());
                }
                if (empOp.isPresent()) {
                    Employee emp = empOp.get();
                    if (leave.getLeaveid() == 0) {

                        List<LeaveApplication> leavesOnFromDate = new ArrayList<>(), leavesOnToDate = new ArrayList<>(),
                                leavesInBetween = new ArrayList<>();
                        try {

                            leavesOnFromDate = leaveRepo
                                    .findByAppliedByEmpIdAndStatusAndLeaveFromIsLessThanEqualAndLeaveToIsGreaterThanEqual(
                                            emp.getEmpId(), LeaveStatus.APPROVED, leave.getLeaveFrom(),
                                            leave.getLeaveFrom());
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }
                        try {

                            leavesOnToDate = leaveRepo
                                    .findByAppliedByEmpIdAndStatusAndLeaveFromIsLessThanEqualAndLeaveToIsGreaterThanEqual(
                                            emp.getEmpId(), LeaveStatus.APPROVED, leave.getLeaveTo(),
                                            leave.getLeaveTo());
                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                        try {
                            leavesInBetween = leaveRepo
                                    .findByAppliedByEmpIdAndStatusAndLeaveFromIsGreaterThanEqualAndLeaveToIsLessThanEqual(
                                            emp.getEmpId(), LeaveStatus.APPROVED, leave.getLeaveFrom(),
                                            leave.getLeaveTo());

                        } catch (Exception e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                        if (leavesOnFromDate.isEmpty() && leavesOnToDate.isEmpty() && leavesInBetween.isEmpty()) {
                            leave.setAppliedBy(empOp.get());
                            leave.setReportingManager(empOp.get().getReportingEmp());
                            leave.setCreatedBy(user);
                            leave.setStatus(LeaveStatus.APPLIED);
                            System.out.println("Testing 1");
                            return ResponseEntity.ok(leaveRepo.save(leave));

                        } else {

                            return new ResponseEntity<Response>(
                                    new Response("Seems we already have approved leaves in selected Date range",
                                            HttpStatus.CONFLICT),
                                    HttpStatus.CONFLICT);

                        }
                    } else {
                        try {
                            LeaveApplication exLeave = leaveRepo.getOne(leave.getLeaveid());

                            if (!exLeave.getStatus().equals(LeaveStatus.APPROVED)
                                    && leave.getStatus().equals(LeaveStatus.APPROVED)) {

                                List<LeaveApplication> leavesOnFromDate = leaveRepo
                                        .findByAppliedByEmpIdAndStatusAndLeaveFromIsLessThanEqualAndLeaveToIsGreaterThanEqual(
                                                emp.getEmpId(), LeaveStatus.APPROVED, leave.getLeaveFrom(),
                                                leave.getLeaveFrom());

                                List<LeaveApplication> leavesOnToDate = leaveRepo
                                        .findByAppliedByEmpIdAndStatusAndLeaveFromIsLessThanEqualAndLeaveToIsGreaterThanEqual(
                                                emp.getEmpId(), LeaveStatus.APPROVED, leave.getLeaveTo(),
                                                leave.getLeaveTo());

                                List<LeaveApplication> leavesInBetween = leaveRepo
                                        .findByAppliedByEmpIdAndStatusAndLeaveFromIsGreaterThanEqualAndLeaveToIsLessThanEqual(
                                                emp.getEmpId(), LeaveStatus.APPROVED, leave.getLeaveFrom(),
                                                leave.getLeaveTo());

                                if (leavesOnFromDate.isEmpty() && leavesOnToDate.isEmpty()
                                        && leavesInBetween.isEmpty()) {

                                    List<LeaveBalance> leaveBalances = lBalanceRepo
                                            .findByEmployeeEmpIdAndLeaveType(emp.getEmpId(), exLeave.getLeaveType());
                                    if (!leaveBalances.isEmpty()) {

                                        LeaveBalance bal = leaveBalances.get(0);

                                        long leaveDays = (leave.getLeaveFrom().getTime() - leave.getLeaveTo().getTime())
                                                / (1000 * 60 * 60 * 24);

                                        leaveDays = leaveDays + 1;

                                        if (bal.getLeaveBalance() < leaveDays) {
                                            // unable to approve

                                            return new ResponseEntity<Response>(
                                                    new Response("Leaves not available to approve",
                                                            HttpStatus.NOT_ACCEPTABLE),
                                                    HttpStatus.NOT_ACCEPTABLE);

                                        } else {

                                            double lbal = bal.getLeaveBalance() - leaveDays;
                                            bal.setLeaveBalance(lbal);
                                            lBalanceRepo.save(bal);
                                            leave.setAvailableLeaves(bal.getLeaveBalance());

                                            return ResponseEntity.ok(leaveRepo.save(leave));

                                            // approve
                                        }

                                    } else {
                                        return new ResponseEntity<Response>(
                                                new Response("No Available leaves", HttpStatus.NOT_ACCEPTABLE),
                                                HttpStatus.NOT_ACCEPTABLE);
                                    }

                                } else {
                                    return new ResponseEntity<Response>(
                                            new Response("Seems we already have approved leaves in selected Date range",
                                                    HttpStatus.CONFLICT),
                                            HttpStatus.CONFLICT);
                                }

                                // deduct leaves
//								if (exLeave.getLeaveType().getLtId()   ) {
//
//								}

                            } else if (!exLeave.getStatus().equals(LeaveStatus.CANCELLED)
                                    && leave.getStatus().equals(LeaveStatus.CANCELLED)) {

                                //
//								LeaveBalance bal = leaveBalances.get(0);
//
//								long leaveDays = (leave.getLeaveFrom().getTime() - leave.getLeaveTo().getTime())
//										/ (1000 * 60 * 60 * 24);

                                return ResponseEntity.ok(leaveRepo.save(leave));

                            } else {
                                return ResponseEntity.ok(leaveRepo.save(leave));

                            }

                        } catch (EntityNotFoundException e) {
                            // TODO: handle exception
                            return new ResponseEntity<Response>(new Response("Leave Details missing in server"),
                                    HttpStatus.EXPECTATION_FAILED);
                        }
                    }
                } else {
                    // no emp found with the user
                    return new ResponseEntity<Response>(new Response("Employee Details missing with Data"),
                            HttpStatus.EXPECTATION_FAILED);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);

            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/leave/status")
    ResponseEntity<?> approveLeave(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                   @RequestBody LeaveApplication leave) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                Optional<Employee> empOp;
                if (leave.getAppliedBy() == null) {
                    empOp = empRepo.findById(user.getEntityKey());
                } else {
                    empOp = empRepo.findById(leave.getAppliedBy().getEmpId());
                }
                if (empOp.isPresent()) {
                    leave.setAppliedBy(empOp.get());
                    leave.setReportingManager(empOp.get().getReportingEmp());
                    leave.setCreatedBy(user);
                    return ResponseEntity.ok(leaveRepo.save(leave));
                } else {
                    // no emp found with the user
                    return new ResponseEntity<Response>(new Response("Employee Details missing with Data"),
                            HttpStatus.EXPECTATION_FAILED);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/leave")
    ResponseEntity<?> getAllLeaves(@RequestHeader(value = "X-AUTH-TOKEN") String token) {

        UserDetails user = userRepo.findByToken(token);

        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION)) {

                return ResponseEntity.ok(leaveRepo.findAll());

            } else if (user.getRole().equals(Userrole.HRMS_EMPLOYEE) && user.isReportingManager()) {

                List<LeaveApplication> emps = leaveRepo.findByAppliedByReportingEmpId(user.getEntityKey());
                return ResponseEntity.ok(emps);

            } else {

                List<LeaveApplication> emps = leaveRepo.findByAppliedByEmpId(user.getEntityKey());
                return ResponseEntity.ok(emps);

            }
        } else {

            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/leave/{id}")
    ResponseEntity<?> getLeaveById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            LeaveApplication emps = leaveRepo.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid leave Id:" + id));
            return ResponseEntity.ok(emps);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/leave/existsam")
    ResponseEntity<?> checkLeaveExistsOnGivenDate(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                  @RequestParam("empid") long empid, @RequestParam("checkDate") Date checkDate,
                                                  @RequestParam("status") LeaveStatus status) {
//		UserDetails user = userRepo.findByToken(token);
        if (token.equals("123456789")) {
            List<LeaveApplication> leaves = leaveRepo
                    .findByAppliedByEmpIdAndStatusAndLeaveFromIsLessThanEqualAndLeaveToIsGreaterThanEqual(empid, status,
                            checkDate, checkDate);
            return ResponseEntity.ok(leaves);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

}
