package com.workforce.webapp.controller;

import com.workforce.webapp.dto.executive.ExecutiveDto;
import com.workforce.webapp.service.ExecutiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@CrossOrigin
@EnableAutoConfiguration
@RestController
@RequestMapping("/api/v1")
public class ExecutiveController {

    private @Autowired
    ExecutiveService service;

	/*@Value("${attachments-folder}")
	private String attachmentsFolder;*/

    @PostMapping(value = "/exe")
    public ResponseEntity createExecutive(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                          @Valid @RequestBody ExecutiveDto executiveDto) {
        return ResponseEntity.ok(service.createExecutive(token, null, executiveDto));
    }


    @PutMapping(value = "/exec/{id}")
    public ResponseEntity updateExecutive(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                          @PathVariable(name = "id") Long exeId,
                                          @Valid @RequestBody ExecutiveDto executiveDto) {
        return ResponseEntity.ok(service.createExecutive(token, exeId, executiveDto));
    }

    @GetMapping("/exec")
    public ResponseEntity getAllExecs(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(service.getAllExecs(token));
    }

    @GetMapping("/exe/{id}")
    public ResponseEntity getExecById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                      @PathVariable(name = "id") Long execId) {
        return ResponseEntity.ok(service.getExecById(token, execId));
    }

    @GetMapping("/exe/profile")
    public ResponseEntity getExeProfile(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(service.getExeProfile(token));
    }

	/*@PostMapping("/executives")
	public ResponseEntity addExecutive(@RequestHeader(value = "X-AUTH-TOKEN") String token,
									   @RequestBody Executive executive) {

		System.out.println("Student =" + executive.toString());
		UserDetails user = userRepo.findByToken(token);
		System.out.print(user.toString());
		if (user != null && user.getRole().equals(Userrole.ORGANIZATION)) {
			*//*
     * if(executive.getExeid() !=0){
     *
     * }else{
     *
     * }
     *//*
			Executive existingExe = exeRepo.findByPhone(executive.getPhone());
			if (existingExe == null) {
				Organization org = orgRepo.findOneById(user.getEntityKey());
				if (org != null) {

					executive.setOrg(org);
					exeRepo.save(executive);
					UserDetails newexe = new UserDetails();
					newexe.setRole(Userrole.USER_EXECUTIVE);
					newexe.setUsername(executive.getName());
					newexe.setPhone(executive.getPhone());
					newexe.setEntityKey(executive.getExeid());
					newexe.setStatus(UserStatus.PENDING_SELF_ACTIVATION);
					newexe.setLogin(executive.getPhone());
					newexe.setPassword(RestUtils.md5(executive.getPhone()));
					newexe.setEmail(executive.getEmail());
					String userActivation = "";
					while (userActivation.equals("")) {
						userActivation = RestUtils.getActivationKey();
						if (userRepo.findByActivationKey(userActivation) == null) {
							System.out.println("Activation key=" + userActivation);
							newexe.setActivationKey(userActivation);
						} else {
							userActivation = "";
						}
					}
					userRepo.save(newexe);
					// Dear ##name##, use your phone number as username and
					// password for eorbapp.
					String msg = "Dear " + newexe.getUsername()
							+ ", use your phone number as username and password for eorbapp @"
							+ " https://bit.ly/2J9ui1Q -Thanks, Call @ 8886668273, Neemiit";
					// String msg= "Executive account created for
					// "+org.getOrgname()+"\n login with your phone number
					// as uname and pswd";
					System.out.println("Message=" + msg);
					SmsServiceImpl.sendSms(newexe.getPhone(), msg); // "success";//

					// String body ="Hi "+newexe.getUsername()+",<br>"+"<i
					// style='color:#B51F93'>Greetings from
					// "+org.getOrgname()+"..!!!</i><br><br>Kindly click the
					// below link for activating you account<br>"
					// +
					// "http://192.168.0.102:8082/activateAccount?activationKey="+userActivation;
					// MailUtils.sendMail(newexe.getEmail(), "Erob
					// Alert.!!!Account Successfully Created",
					// userActivation);

					return ResponseEntity.ok(executive);
					// return
					// RestUtils.map("Error","No","Executive",executive,"Message",executive.getName()+"
					// sucessfully added");

				} else {
					return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null, null,
							"Organization details not found", null), HttpStatus.NOT_ACCEPTABLE);
					//

					// return RestUtils.map("Error","Yes","Message","");
				}

			} else {
				if (executive.getExeid() != 0) {

					executive.setOrg(orgRepo.findOneById(user.getEntityKey()));
					UserDetails exeu = userRepo.findByEntityKeyAndRole(executive.getExeid(),
							Userrole.USER_EXECUTIVE);
					if (exeu!=null) {
//						UserDetails exeu = exeuser.get();
						exeu.setEmail(executive.getEmail());
						exeu.setUsername(executive.getName());
						try {
							userRepo.save(exeu);
						} catch (Exception e) {
							return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null,
									null, "" + e.getMessage(), null), HttpStatus.NOT_ACCEPTABLE);
							// TODO: handle exception
						}
					} else {
						return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null, null,
								"Executive details not found", null), HttpStatus.NOT_ACCEPTABLE);
					}
					try {
						exeRepo.save(executive);
						return ResponseEntity.ok(executive);
					} catch (DataIntegrityViolationException ex) {
						ex.printStackTrace();
						System.out.println("cause:" + ex.getCause().getCause().getMessage() + "; cause local:"
								+ ex.getCause().getLocalizedMessage());
						String msg = ex.getCause().getCause().getMessage();
						System.out.println("Msg:" + msg);
						if (msg != null && msg.contains("imglogo")) {
							String name = "/executiveimage/" + new Date().getTime() + ".JPG";
							String img = RestUtils.Base64ToImage(executive.getImglogo(), name, attachmentsFolder);
							if (img != null) {
								executive.setImglogo(name);
							} else {
								executive.setImglogo("");
							}
							exeRepo.save(executive);
							return ResponseEntity.ok(executive);
						} else if (msg != null && msg.contains("aadhaar_number")) {
							return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null,
									null, "Aadhaar Number is not valid", null), HttpStatus.NOT_ACCEPTABLE);
						} else {
							return new ResponseEntity<Response>(
									new Response(HttpStatus.NOT_ACCEPTABLE, null, null, msg, null),
									HttpStatus.NOT_ACCEPTABLE);

						}

					}
				} else {
					return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null, null,
							"Executive with phonenumber already exists !!", null), HttpStatus.NOT_ACCEPTABLE);

				}
			}
		} else {
			return new ResponseEntity<Response>(
					new Response(HttpStatus.UNAUTHORIZED, null, null, "Not Authorised...!!", null),
					HttpStatus.UNAUTHORIZED);
		}
	}



	@GetMapping("/org/executives")
	public ResponseEntity<?> getExecutives(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
		UserDetails user = userRepo.findByToken(token);
		if (user != null) {

			List<Executive> executives = exeRepo.findByOrg(orgRepo.findOneById(user.getEntityKey()));
			System.out.println("Count:" + exeRepo.countByOrg(orgRepo.findOneById(user.getEntityKey())));
			return ResponseEntity.ok(executives);

		} else {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		}

	}

	@GetMapping("/org/executives/{id}")
	public ResponseEntity<?> getExecutive(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
		UserDetails user = userRepo.findByToken(token);
		if (user != null) {

			Optional<Executive> executiveop = exeRepo.findById(id);
			if (executiveop.isPresent() && (executiveop.get().getOrg().getId() == user.getEntityKey())) {
				return ResponseEntity.ok(executiveop.get());
			} else {
				return new ResponseEntity<String>("Not Allowed", HttpStatus.NOT_ACCEPTABLE);
			}

		} else {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		}

	}

	@Scheduled(cron = "0 0 8 * * *")
	void sendLoginNotification() {
		List<String> regids = userRepo.findAllFbToksByRole(Userrole.USER_EXECUTIVE.toString());
		if (regids != null && regids.size() > 0) {
			Firebase.sendNotificationToMultiple(regids,
					"Every day is a fresh new start.. So just paint the canvas of your life with beautiful days and delightful memories.. Greetings from E-Orb",
					"Good Morning..!! Login alert");
			System.out.println("Sent Notification");
		} else {
			System.out.println("Unable to Send Notification Role:" + Userrole.USER_EXECUTIVE.toString());
		}
	}

	@Scheduled(cron = "0 30 8 * * *")
	void sendLoginNotificationtaeight() {
		List<String> regids = userRepo.findAllFbToksByRole(Userrole.USER_EXECUTIVE.toString());
		if (regids != null && regids.size() > 0) {
			Firebase.sendNotificationToMultiple(regids,
					"Every day is a fresh new start.. So just paint the canvas of your life with beautiful days and delightful memories.. Greetings from E-Orb",
					"Good Morning..!! Login alert");
			System.out.println("Sent Notification");
		} else {
			System.out.println("Unable to Send Notification Role:" + Userrole.USER_EXECUTIVE.toString());
		}
	}

	@Scheduled(cron = "0 0 9 * * *")
	void sendLoginNotificationAtNine() {
		List<String> regids = userRepo.findAllFbToksByRole(Userrole.USER_EXECUTIVE.toString());
		if (regids != null && regids.size() > 0) {
			Firebase.sendNotificationToMultiple(regids,
					"Every day is a fresh new start.. So just paint the canvas of your life with beautiful days and delightful memories.. Greetings from E-Orb",
					"Good Morning..!! Login Warning");
			System.out.println("Sent Notification");
		} else {
			System.out.println("Unable to Send Notification Role:" + Userrole.USER_EXECUTIVE.toString());
		}
	}

	@GetMapping("/executives/sam")
	public Executive getSampleExecutive() {
		Address add = new Address("#4, Balamrai", "Paradise", "Telangana", "500061");
		Executive exe = new Executive("Thelidhu", "7845845812",
				"http://www.suitdoctors.com/wp-content/uploads/2016/11/dummy-man-570x570.png", add, null,
				"Sample@gmail.com", new Date(), "AP10 AJ 9999", "ESFF12ZX", "2018SVERA", new Date(), "123212321231",
				"12AJ12K432", "Nothing", "Good At work");
		return exe;
	}*/

}
