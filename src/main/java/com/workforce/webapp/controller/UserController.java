package com.workforce.webapp.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.LoginDto;
import com.workforce.webapp.dto.PasswordResetDto;
import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.OTP;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.UserStatus;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.utils.RestUtils;

@CrossOrigin
@EnableAutoConfiguration
@RestController
public class UserController {

    /**
     * mail.host="smtp.gmail.com" mail.password="Neem!!T12" mail.port=587
     * mail.username="test1.neemiit@gmail.com"
     */

    long port = 587;

    String username = "test1.neemiit@gmail.com";

    String password = "Neem!!T12";

    String host = "smtp.gmail.com";

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    OrganizationRepository orgRepo;

    @Autowired
    OTPRepository otpRepo;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/activate", method = RequestMethod.GET)
    public ResponseEntity<?> activateAccount(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @RequestParam String activationKey, @RequestParam String pass) {
        UserDetails user = userRepo.findByActivationKey(activationKey);
        if (user != null) {
            if (user.getStatus().equals(UserStatus.PENDING_SELF_ACTIVATION)) {

                String password = RestUtils.md5(pass);
                if (password != null) {
                    user.setUsername(user.getPhone());
                    user.setPassword(password);
                    user.setStatus(UserStatus.ACTIVE);
                    userRepo.save(user);

                    return new ResponseEntity<Response>(
                            new Response("Hi " + user.getUsername() + ", Your account Activated sucessfully",
                                    HttpStatus.OK),
                            HttpStatus.OK);
                } else {
                    return new ResponseEntity<Response>(
                            new Response("Sorry, Unable to activate account", HttpStatus.NOT_ACCEPTABLE),
                            HttpStatus.NOT_ACCEPTABLE);

                }
            } else {
                return new ResponseEntity<Response>(new Response(
                        "Hi " + user.getUsername()
                                + ", your account is already activated kindly login with username and password",
                        HttpStatus.CONTINUE), HttpStatus.CONTINUE);

            }
        }
        return new ResponseEntity<Response>(new Response("Unable to fetch your details", HttpStatus.NOT_ACCEPTABLE),
                HttpStatus.NOT_ACCEPTABLE);

    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ResponseEntity<Response> login(@RequestBody LoginDto login) {
        // @RequestParam String username,@RequestParam String pswd
        System.out.println("Date For login:" + login.toString());
//    	if(login!=null){
        String username = login.getUn();
        String pswd = login.getPs();
        UserDetails user = userRepo.findByLoginAndPassword(username, RestUtils.md5(pswd));
        System.out.println("User:" + login.getUn() + ",Ps:" + login.getPs());
        if (user != null) {

            String token = null;
            while (token == null) {
                token = RestUtils.getActivationKey();
                UserDetails tUser = userRepo.findByToken(token);
                if (tUser != null) {
                    token = null;
                }
            }
            System.out.println("Token for " + user.getUsername() + " is " + token + " size " + token.length());
            user.setToken(token);
            user.setTokenGeneratedAt();
            if (login.getFt() != null) {
                user.setFbTok(login.getFt());
            }
            userRepo.save(user);
            System.out.println("Login request for usename: " + username + " and pswd: " + pswd + " results are valid");
            Map userm = new HashMap<>();
            userm.put("details", user);
            userm.put("token", user.getToken());
            return new ResponseEntity<Response>(new Response("Successfully logged in", userm, HttpStatus.OK),
                    HttpStatus.OK);
        } else {
            System.out
                    .println("Login request for usename: " + username + " and pswd: " + pswd + " results are invalid");
            return new ResponseEntity<Response>(
                    new Response("Username or password incorrect", user, HttpStatus.UNAUTHORIZED),
                    HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResponseEntity<?> logout(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            logger.info("User=" + user.toString());
            user.setToken(null);
            user.setFbTok(null);
            userRepo.save(user);
            System.out.println("Sucessfully logged out " + token);
            return ResponseEntity.ok().build();
        } else {
            return new ResponseEntity<Response>(new Response("Unable to fetch details"), HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/cpass", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ResponseEntity<?> changePassword(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                            @RequestBody Map data) {

        UserDetails user = userRepo.findByToken(token);
        System.out.println("Data =" + data.toString());
        if (user != null) {
            if (user.getPassword().equals(RestUtils.md5((String) data.get("oldp")))) {
                user.setPassword(RestUtils.md5((String) data.get("newp")));
                user.setToken(RestUtils.getActivationKey());
                userRepo.save(user);
                return ResponseEntity.ok().build();
            } else {
                return new ResponseEntity<Response>(new Response("Incorrect Password"), HttpStatus.NOT_ACCEPTABLE);
            }
        }
        return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/reset", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ResponseEntity<?> resetPassword(@RequestBody PasswordResetDto reset) {
        UserDetails user = userRepo.findByToken(reset.getPhone());
        System.out.println("Data =" + reset.toString());
        if (user != null) {
            OTP otp = otpRepo.findByPhone(reset.getPhone());
            if (otp != null && otp.getValue() == reset.getOtp()) {
                user.setPassword(RestUtils.md5((String) user.getPhone()));
                user.setToken(RestUtils.getActivationKey());
                userRepo.save(user);
                return ResponseEntity.ok().build();
            } else {
                return new ResponseEntity<Response>(new Response("Incorrect otp"), HttpStatus.NOT_ACCEPTABLE);
            }
        }
        return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);

    }

}
