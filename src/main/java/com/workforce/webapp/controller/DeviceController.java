package com.workforce.webapp.controller;

import com.workforce.webapp.dto.device.DeviceComplaintDto;
import com.workforce.webapp.dto.device.DeviceDetailDto;
import com.workforce.webapp.dto.device.DeviceDto;
import com.workforce.webapp.dto.device.DeviceRequestDto;
import com.workforce.webapp.repository.*;
import com.workforce.webapp.repository.device.DeviceDetailRepository;
import com.workforce.webapp.repository.device.DeviceRepository;
import com.workforce.webapp.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class DeviceController {


    private @Autowired
    DeviceService service;

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    DeviceRepository tdRepo;

    @Autowired
    OrganizationRepository orgRepo;

    @Autowired
    CurrentTrackRepository ctRepo;

    @Autowired
    DeviceDetailRepository ddRepo;


    @PostMapping(value = "/device")
    public ResponseEntity addDevice(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                    @Valid @RequestBody DeviceDto deviceDto) {
        return ResponseEntity.ok(service.addOrUpdateDevice(token, null, deviceDto));
    }

    @PutMapping(value = "/device/{id}")
    public ResponseEntity updateDevice(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                       @PathVariable(name = "id") String deviceId,
                                       @Valid @RequestBody DeviceDto deviceDto) {
        return ResponseEntity.ok(service.addOrUpdateDevice(token, deviceId, deviceDto));
    }

    @GetMapping("/device")
    public ResponseEntity getAllDevices(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(service.getAllDevices(token));
    }

    @GetMapping("/device/org")
    public ResponseEntity getAllDeviceByOrg(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(service.getAllDeviceByOrg(token));
    }

    @GetMapping("/device/detail/{id}")
    public ResponseEntity getAllDeviceDetailById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                 @PathVariable(name = "id") String devDetId) {
        return ResponseEntity.ok(service.getAllDeviceDetailById(token, devDetId));
    }

    @PatchMapping(value = "/device/{deviceId}/org/{orgId}")
    public ResponseEntity assignOrgToDevice(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                            @PathVariable(name = "deviceId") String deviceId,
                                            @PathVariable(name = "orgId") Long orgId) {
        service.assignOrgToDevice(token, deviceId, orgId);
        return ResponseEntity.ok().build();
    }


    @PostMapping(value = "/device/detail/{id}")
    public ResponseEntity addDeviceDetail(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                          @PathVariable(name = "id") String devDetId,
                                          @Valid @RequestBody DeviceDetailDto deviceDetailDto) {
        return ResponseEntity.ok(service.addDeviceDetail(token, devDetId, deviceDetailDto));
    }

    @PostMapping(value = "/device/request")
    public ResponseEntity createDeviceRequest(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                              @Valid @RequestBody DeviceRequestDto deviceRequestDto) {
        return ResponseEntity.ok(service.deviceRequest(token, null, deviceRequestDto));
    }

    @PutMapping(value = "/device/request/{id}")
    public ResponseEntity updateDeviceRequest(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                              @PathVariable(name = "id") String requestId,
                                              @Valid @RequestBody DeviceRequestDto deviceRequestDto) {
        return ResponseEntity.ok(service.deviceRequest(token, requestId, deviceRequestDto));
    }


    @PostMapping(value = "/device/detail/{id}/complaint")
    public ResponseEntity createDeviceComplaint(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                @PathVariable(name = "id") String devDetId,
                                                @Valid @RequestBody DeviceComplaintDto deviceComplaintDto) {
        return ResponseEntity.ok(service.deviceComplaint(token, devDetId, null, deviceComplaintDto));
    }

    @PutMapping(value = "/device/complaint/{id}")
    public ResponseEntity updateDeviceComplaint(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                @PathVariable(name = "id") String complaintId,
                                                @Valid @RequestBody DeviceComplaintDto deviceComplaintDto) {
        return ResponseEntity.ok(service.deviceComplaint(token, null, complaintId, deviceComplaintDto));
    }


    @PatchMapping(value = "/device/complaint/{id}")
    public ResponseEntity cancelDeviceComplaint(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                @PathVariable(name = "id") String complaintId) {
        service.cancelDeviceComplaint(token, complaintId);
        return ResponseEntity.ok().build();
    }


    @GetMapping("/device/org/complaint")
    public ResponseEntity getAllDevCompByOrg(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(service.getAllDevCompByOrg(token));
    }

    @GetMapping("/device/{id}/complaint")
    public ResponseEntity getAllDevCompByDevId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                               @PathVariable(name = "id") String devDetId) {
        return ResponseEntity.ok(service.getAllDevCompByDevId(token, devDetId));
    }

    @GetMapping("/device/complaint/{id}")
    public ResponseEntity getDevCompByCompId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @PathVariable(name = "id") String complaintId) {
        return ResponseEntity.ok(service.getDevCompByCompId(token, complaintId));
    }


    @GetMapping("/device/org/request")
    public ResponseEntity getAllDevReqByOrg(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(service.getAllDevReqByOrg(token));
    }

    @GetMapping("/device/request/{id}")
    public ResponseEntity getDevReqByReqId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                           @PathVariable(name = "id") String requestId) {
        return ResponseEntity.ok(service.getDevReqByReqId(token, requestId));
    }

    @PatchMapping(value = "/device/request/{id}")
    public ResponseEntity cancelDeviceRequest(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                              @PathVariable(name = "id") String requestId) {
        service.cancelDeviceRequest(token, requestId);
        return ResponseEntity.ok().build();
    }

	/*@PostMapping("/device/map")
	public ResponseEntity<?> mapDeviceToOrg(@RequestHeader(value = "X-AUTH-TOKEN") String token,
			@RequestBody DeviceMappingDto dmap) {

		UserDetails user = userRepo.findByToken(token);
		if (user != null && user.getRole().equals(Userrole.TRACKINGDEVICE_VENDOR)) {

			Device td = tdRepo.getOne(dmap.getDeviceid());
			if (td.getStatus().equals(TrackDeviceStatus.ENROLLED)) {

				if (dmap.getDdid()== 0) {
					Organization org = orgRepo.getOne(dmap.getOrgid());
					td.setAssignedTo(org);
					td.setOrgid(org.getId());
					td.setOrgname(org.getOrgname());
					td.setStatus(TrackDeviceStatus.ASSIGNED);
					CurrentTrack ct = new CurrentTrack(TrackType.TRACKING_DEVICE, td.getId());
					ct = ctRepo.save(ct);
					td.setCt(ct);
					tdRepo.save(td);
					DeviceDetail dd = new DeviceDetail("Device(" + dmap.getVehicleNo() + ")", dmap.getVehicleNo(),
							null, "N/A", td);
					ddRepo.save(dd);
					return new ResponseEntity<Response>(new Response("data saved",dd), HttpStatus.OK);

				} else {

					Optional<DeviceDetail> ddop = ddRepo.findById(dmap.getDdid());
					if (ddop.isPresent()) {

						DeviceDetail d = ddop.get();
						Device old = d.getDevice();
						if(old!=null){
							td.setAssignedTo(old.getAssignedTo());
							td.setOrgid(old.getOrgid());
							td.setOrgname(old.getOrgname());
							old.setStatus(TrackDeviceStatus.ENROLLED);
							old.setAssignedTo(null);
							old.setOrgid(0);
							old.setOrgname(null);
							tdRepo.save(old);

						}
						d.setDevice(td);
						d = ddRepo.save(d);
						td.setStatus(TrackDeviceStatus.ASSIGNED);
						tdRepo.save(td);
						return new ResponseEntity<Response>(new Response("data Updated",d), HttpStatus.OK);
					} else {
						return new ResponseEntity<Response>(new Response("invalid ddid",HttpStatus.NOT_ACCEPTABLE), HttpStatus.NOT_ACCEPTABLE);


					}

				}

			} else {
				return new ResponseEntity<Response>(new Response("Assignment is not allowed now",HttpStatus.NOT_ACCEPTABLE), HttpStatus.NOT_ACCEPTABLE);
			}
		} else {

			return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@PostMapping("/device/meta")
	public ResponseEntity<?> addDeviceMeta(@RequestHeader(value = "X-AUTH-TOKEN") String token,
			@RequestBody DeviceDetail dd) {

		UserDetails user = userRepo.findByToken(token);
		if (user != null) {
			if (user.getRole().equals(Userrole.ORGANIZATION)) {

				try {
					Device td = tdRepo.getOne(dd.getDevice().getId());
					if (td.getAssignedTo().getId() == user.getEntityKey()) {
						ddRepo.save(dd);
						dd.setDevice(td);

						return ResponseEntity.ok(dd);

					} else {
						return new ResponseEntity<Response>(new Response("No Enough rights"),
								HttpStatus.NOT_ACCEPTABLE);
					}
				} catch (NullPointerException ex) {
					ex.printStackTrace();
					return new ResponseEntity<Response>(new Response("Device Data Cannot be null"),
							HttpStatus.INTERNAL_SERVER_ERROR);

				} catch (Exception ex) {
					ex.printStackTrace();
					return new ResponseEntity<Response>(new Response("" + ex.getMessage()),
							HttpStatus.INTERNAL_SERVER_ERROR);

				}
			} else {
				return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
			}
		} else {
			return new ResponseEntity<Response>(new Response("NOT AUTHORISED"), HttpStatus.UNAUTHORIZED);
		}
	}

	@GetMapping("/device/meta")
	public ResponseEntity<?> getAllDeviceMeta(@RequestHeader(value = "X-AUTH-TOKEN") String token) {

		UserDetails user = userRepo.findByToken(token);
		if (user != null) {
			if (user.getRole().equals(Userrole.ORGANIZATION)) {
				List<DeviceDetail> devices = ddRepo.findAllByOrg(user.getEntityKey());
				return ResponseEntity.ok(devices);
			} else if (user.getRole().equals(Userrole.TRACKINGDEVICE_VENDOR)) {
				List<DeviceDetail> devices = ddRepo.findAll();
				return ResponseEntity.ok(devices);
			} else {
				return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
			}
		} else {
			return new ResponseEntity<Response>(new Response("UNAUTHORIZED"), HttpStatus.UNAUTHORIZED);
		}

	}

	@GetMapping("/device/meta/{did}")
	public ResponseEntity<?> getDeviceMetaByDeviceId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
			@PathVariable long did) {

		UserDetails user = userRepo.findByToken(token);
		if (user != null) {
			if (user.getRole().equals(Userrole.ORGANIZATION)) {
				Device td = tdRepo.getOne(did);
				if (td.getAssignedTo().getId() == user.getEntityKey()) {
					Optional<DeviceDetail> devices = ddRepo.findOneByDevice(td);
					if (!devices.isPresent()) {
						return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NO_CONTENT);
					} else {
						return ResponseEntity.ok(devices.get());
					}
				} else {
					return new ResponseEntity<Response>(new Response("No Enough Rights"), HttpStatus.NOT_ACCEPTABLE);
				}
			} else {
				return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
			}
		} else {
			return new ResponseEntity<Response>(new Response("UNAUTHORIZED"), HttpStatus.UNAUTHORIZED);
		}
	}


	@PostMapping("/device")
	public ResponseEntity enrollDevice(@RequestHeader(value = "X-AUTH-TOKEN") String token,
									   @RequestBody Device td) {

		UserDetails user = userRepo.findByToken(token);
		if (user.getRole().equals(Userrole.TRACKINGDEVICE_VENDOR)) {
			if (td.getId() == 0) {

				td.setStatus(TrackDeviceStatus.ENROLLED);
				td.setCreatedby(user);
				td.setLocCurrent(new Location());
				tdRepo.save(td);
				return ResponseEntity.ok().body(td);
			} else {
				try {

					Device device = tdRepo.getOne(td.getId());
					if (td.getNextRenewal() != null) {
						device.setNextRenewal(td.getNextRenewal());
					}
					if (td.getLastrenewaled() != null) {
						device.setLastrenewaled(td.getLastrenewaled());
					}
					if (td.getImei() != null) {
						device.setImei(td.getImei());
					}
					if (td.getDevicenumber() != null) {
						device.setDevicenumber(td.getDevicenumber());
					}
					device = tdRepo.save(device);
					return ResponseEntity.ok().body(device);
				} catch (EntityNotFoundException e) {
					// TODO: handle exception
					return new ResponseEntity<Response>(new Response("Unable to update try again later, Invalid: ID",
							HttpStatus.UNPROCESSABLE_ENTITY), HttpStatus.UNPROCESSABLE_ENTITY);
				}
			}
		} else {
			return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
		}
	}*/


	/*@GetMapping("/device")
	public ResponseEntity<?> getEnrolledDevices(@RequestHeader(value = "X-AUTH-TOKEN") String token) {

		UserDetails user = userRepo.findByToken(token);
		if (user.getRole().equals(Userrole.TRACKINGDEVICE_VENDOR)) {

			List<Device> tds = tdRepo.findAll();
			return new ResponseEntity<List<Device>>(tds, HttpStatus.OK);

		} else {
			return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
		}

	}

	@GetMapping("/device/org")
	public ResponseEntity<?> getEnrolledDevicesByOrg(@RequestHeader(value = "X-AUTH-TOKEN") String token) {

		UserDetails user = userRepo.findByToken(token);
		if (user != null && user.getRole().equals(Userrole.TRACKINGDEVICE_VENDOR)) {

			List<Device> tds = tdRepo.findAllByCreatedby(user);
			return new ResponseEntity<List<Device>>(tds, HttpStatus.OK);

		} else {

			return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
		}

	}

	@GetMapping("/org/devices")
	public ResponseEntity<?> getAssignedDevicesByOrg(@RequestHeader(value = "X-AUTH-TOKEN") String token) {

		UserDetails user = userRepo.findByToken(token);
		if (user != null && user.getRole().equals(Userrole.ORGANIZATION)) {

			List<Device> tds = tdRepo.findAllByAssignedTo(orgRepo.getOne(user.getEntityKey()));
			return new ResponseEntity<List<Device>>(tds, HttpStatus.OK);

		} else {

			return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
		}

	}*/



	/*@GetMapping("/device/sam")
	public Device getTDSam() {

		return new Device(new Date(), new UserDetails(), "125632541", "8974585623", new Date(), new Date(),
				"MAG1", TrackDeviceStatus.ENROLLED, null, new Date());

	}*/










	/*



    @PostMapping("/vendor")
    public ResponseEntity<?> regVendor(@RequestBody Vendor vendor){

        venRepo.save(vendor);
        User newVen = new User();
        newVen.setRole(Userrole.TRACKINGDEVICE_VENDOR);
        newVen.setUsername(vendor.getContactname());
        newVen.setEmail(vendor.getEmail());
        newVen.setPhone(vendor.getContactPhone());
        newVen.setEntityKey(vendor.getId());
        newVen.setStatus(UserStatus.PENDING_SELF_ACTIVATION);
        newVen.setLogin(vendor.getContactPhone());
        newVen.setPassword(RestUtils.md5(vendor.getContactPhone()));
        String userActivation="";
        while(userActivation.equals("")){
            userActivation=RestUtils.getActivationKey();
            if(userRepo.findByActivationKey(userActivation)==null){
                System.out.println("Activation key="+userActivation );
                newVen.setActivationKey(userActivation);
            }else{
                userActivation="";
            }
        }
        userRepo.save(newVen);
        return ResponseEntity.ok().build();

    }

    @GetMapping("/vendor/sam")
    public Vendor getVendorSam(){

        return new Vendor("Magni5","Srinivas",new
                Date(),"90989909","magni5@gmail.com");

    }

	 @PostMapping("device/request")
	 ResponseEntity<?>
	 requestForDevices(@RequestHeader(value="X-AUTH-TOKEN")String
	 token,@RequestBody DevicesRequest dReq){

	 User user = userRepo.findByToken(token);
	 if(user!=null){
	 if(user.getRole().equals(Userrole.ORGANIZATION)){
	 if(dReq!=null){
	 if(dReq.getDevicecount()>0 && !dReq.getRequiredBy().before(new Date())){
	 dReq.setRequestedby(user);
	 dReq.setOrg(orgRepo.findOne(user.getEntityKey()));
	 drRepo.save(dReq);

	 return ResponseEntity.ok(dReq);

	 }else{
	 return new ResponseEntity<Response>(new Response("Invalid
	 Count/Date"),HttpStatus.BAD_REQUEST);
	 }
	 }else{
	 return new ResponseEntity<Response>(new Response("No Data
	 Found"),HttpStatus.NO_CONTENT);

	 }

	 }else{
	 return new ResponseEntity<Response>(new Response("Not
	 Allowed"),HttpStatus.NOT_ACCEPTABLE);
	 }

	 }else{
	 return new ResponseEntity<Response>(new
	 Response("UNAUTHORIZED"),HttpStatus.UNAUTHORIZED);
	 }


	 }

	 @GetMapping("device/request")
	 ResponseEntity<?>
	 getAllrequestsForDevices(@RequestHeader(value="X-AUTH-TOKEN")String
	 token){

	 User user = userRepo.findByToken(token);
	 if(user!=null){
	 if(user.getRole().equals(Userrole.ORGANIZATION)){

	 return
	 ResponseEntity.ok(drRepo.findAllByOrg(orgRepo.findOne(user.getEntityKey())));
	 }else{
	 return new ResponseEntity<Response>(new Response("Not
	 Allowed"),HttpStatus.NOT_ACCEPTABLE);
	 }

	 }else{
	 return new ResponseEntity<Response>(new
	 Response("UNAUTHORIZED"),HttpStatus.UNAUTHORIZED);
	 }

	 }

	@GetMapping("device/meta/sam")
	ResponseEntity<?> getSampleDeviceMeta() {
		TrackingDevice td = new TrackingDevice();
		td.setId(2);
		DeviceDetail dd = new DeviceDetail(1L, "Sukesh", "TS 10 AP 8979", "2017GHJ1230", "7845784125", td);
		return ResponseEntity.ok(dd);
	}

	 @GetMapping("device/request/sam")
	 ResponseEntity<?> getSampleDeviceRequest(){
	 DevicesRequest dr = new DevicesRequest(1, 5, null,
	 orgRepo.findOne(2L),"Need High Accurate devices irrespective of Price",
	 true, new Date());

	 return ResponseEntity.ok(dr);
	 }
*/


}
