package com.workforce.webapp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.CRMProductAddOn;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.CRMProductAddOnRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@CrossOrigin
@EnableAutoConfiguration
@RestController

public class CRMProductAddOnController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    CRMProductAddOnRepository crmProdaddOnRepo;

    @PostMapping("/crmproductaddon")
    ResponseEntity<?> addCrmProductAddOn(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                         @RequestBody CRMProductAddOn productAddOn) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.CRM_ADMIN)) {
                Optional<CRMProductAddOn> exitDepo = crmProdaddOnRepo.findByAddonCode(productAddOn.getName());
                if (exitDepo.isPresent()) {

                    if (productAddOn.getCrmpaddOn() != 0
                            && exitDepo.get().getCrmpaddOn() == productAddOn.getCrmpaddOn()) {
                        productAddOn = crmProdaddOnRepo.save(productAddOn);
                        return ResponseEntity.ok(productAddOn);

                    } else {
                        return new ResponseEntity<Response>(new Response("Product Add on Already Exists"),
                                HttpStatus.ALREADY_REPORTED);
                    }

                } else {
                    // ProductCategory pcategory = new ProductCategory(, new
                    // Date(), user, org);
                    productAddOn.setCreatedAt(new Date());
                    productAddOn = crmProdaddOnRepo.save(productAddOn);
                    return ResponseEntity.ok(productAddOn);
                }

            } else {

                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {

            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);

        }

    }

    @GetMapping("/crmproductaddon")
    ResponseEntity<?> getCrmProductAddOns(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {

                List<CRMProductAddOn> deps = crmProdaddOnRepo.findAll();
                return ResponseEntity.ok(deps);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);

            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/crmproductaddon/{id}")
    ResponseEntity<?> getProductAddOnById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {
                Optional<CRMProductAddOn> dep = crmProdaddOnRepo.findById(id);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/crmproduct/{crmpid}/crmproductaddon")
    ResponseEntity<?> getProductAddOnByProductId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                 @PathVariable long crmpid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {
                List<CRMProductAddOn> dep = crmProdaddOnRepo.findByProductCrmpid(crmpid);
                return ResponseEntity.ok(dep);

            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

}
