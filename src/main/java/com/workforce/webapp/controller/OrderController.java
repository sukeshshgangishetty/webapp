package com.workforce.webapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.OrderDto;
import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.OrderDetails;
import com.workforce.webapp.model.OrderParticulars;
import com.workforce.webapp.model.OrdersStatus;
import com.workforce.webapp.model.PriceList;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.OrderParticularRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.PriceListRepository;
import com.workforce.webapp.repository.ProductRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@RestController
@EnableAutoConfiguration
@CrossOrigin
public class OrderController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    ExecutiveRepository exeRepo;

    @Autowired
    ClientRepository clientRepo;

    @Autowired
    OrganizationRepository orRepo;

    @Autowired
    ProductRepository prRepo;

    @Autowired
    PriceListRepository prlRepo;

    @Autowired
    OrderRepository orderRepo;


    @Autowired
    OrderParticularRepository orderperRepo;

    @PostMapping("/order")
    ResponseEntity<?> placeOrder(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestBody OrderDto order) {
        UserDetails user = userRepo.findByToken(token);
        System.out.println("OrderDetails" + order.toString());
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                System.out.println(order.toString());
                OrderDetails order2 = order.getOrder();
                System.out.println(order2);
                order2.setOrderCollected(user);
                order2.setOrg(orRepo.findOneById(user.getEntityKey()));
                order2.setCreatedat(new Date());
                order2.setOrstatus(OrdersStatus.ADDED);
                orderRepo.save(order2);
                for (OrderParticulars orderp : order.getParticulars()) {

                    orderp.setOrder(order2);
                    orderperRepo.save(orderp);
                }
                return ResponseEntity.ok(order);
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                OrderDetails order2 = order.getOrder();
                System.out.println(order2);
                order2.setOrderCollected(user);
                // orRepo.findOne(user.getEntityKey())
                order2.setOrg(exeRepo.findById(user.getEntityKey()).get().getOrg());
                order2.setCreatedat(new Date());
                if (order.getParticulars() != null && order.getParticulars().size() > 0) {
                    order2.setOrstatus(OrdersStatus.ADDED);

                    Client client = clientRepo.findOneById(order2.getOrderto().getId());
                    if (client != null) {
                        order2.setOrderto(client);
                    } else {
                        return new ResponseEntity<Response>(new Response("INSUFFICENT DATA"),
                                HttpStatus.PARTIAL_CONTENT);
                    }
                    order2 = orderRepo.save(order2);
                    for (OrderParticulars orderp : order.getParticulars()) {

                        orderp.setOrder(order2);
                        PriceList price = prlRepo.getOne(orderp.getPricelist().getPlid());
                        double tax = price.getProduct().getTax();
                        orderp.setTax(tax);
                        orderp.setAmount((orderp.getRate() + ((orderp.getRate() * tax) / 100)) * orderp.getQty());
                        orderperRepo.save(orderp);
                    }
//					Optional<UserDetails> fbtok = userRepo.findByEntityKeyAndRole(
//							exeRepo.findOne(user.getEntityKey()).getOrg().getId(), Userrole.ORGANIZATION);
//					if (fbtok.isPresent()) {
//						Firebase.sendTaskNotification(
//								fbtok.get().getFbTok(), "Order for Rs." + order2.getTotal() + " collected "
//										+ user.getUsername() + " from:" + order2.getOrderto().getClientname(),
//								"New Order Collection");
//						try {
//							enoteRepo.save(new EorbNotification("New Order of Rs." + order2.getTotal(),
//									" collected " + user.getUsername() + " from:" + order2.getOrderto().getClientname(),
//									order2.getOrderid(), fbtok.get(), "Orders"));
//						} catch (Exception ex) {
//							ex.printStackTrace();
//						}
//					}
                    return ResponseEntity.ok(order);
                } else {
                    return new ResponseEntity<Response>(new Response("Order Particulars can't be empty"),
                            HttpStatus.BAD_REQUEST);
                }
            } else if (user.getRole().equals(Userrole.USER_CLIENT) && user.getActivated()) {
                OrderDetails order2 = order.getOrder();
                System.out.println(order2);
                order2.setOrderCollected(user);
                // orRepo.findOne(user.getEntityKey())
                order2.setOrg(orRepo.findOneById(clientRepo.findOneById(user.getEntityKey()).getOrgId()));
                order2.setCreatedat(new Date());
                if (order.getParticulars() != null && order.getParticulars().size() > 0) {
                    order2.setOrstatus(OrdersStatus.ADDED);
                    Client client = clientRepo.findOneById(user.getEntityKey());
                    if (client != null) {
                        order2.setOrderto(client);
                    } else {
                        return new ResponseEntity<Response>(new Response("INSUFFICENT DATA"),
                                HttpStatus.PARTIAL_CONTENT);
                    }
                    order2 = orderRepo.save(order2);
                    for (OrderParticulars orderp : order.getParticulars()) {

                        orderp.setOrder(order2);
                        PriceList price = prlRepo.getOne(orderp.getPricelist().getPlid());
                        double tax = price.getProduct().getTax();
                        orderp.setTax(tax);
                        orderp.setAmount((orderp.getRate() + ((orderp.getRate() * tax) / 100)) * orderp.getQty());
                        orderperRepo.save(orderp);
                    }
//					Optional<User> fbtok = userRepo.findByEntityKeyAndRole(
//							client.getOrgId(), Userrole.ORGANIZATION);
//					if (fbtok.isPresent()) {
//						Firebase.sendTaskNotification(
//								fbtok.get().getFbTok(), "Order for Rs." + order2.getTotal() + " collected "
//										+ user.getUsername() + " from:" + order2.getOrderto().getClientname(),
//								"New Order Collection");
//						try {
//							enoteRepo.save(new EorbNotification("New Order of Rs." + order2.getTotal(),
//									" collected " + user.getUsername() + " from:" + order2.getOrderto().getClientname(),
//									order2.getOrderid(), fbtok.get(), "Orders"));
//						} catch (Exception ex) {
//							ex.printStackTrace();
//						}
//					}
                    return ResponseEntity.ok(order);
                } else {
                    return new ResponseEntity<Response>(new Response("Order Particulars can't be empty"),
                            HttpStatus.BAD_REQUEST);
                }
            } else {
                return new ResponseEntity<Response>(new Response("NOT ALLOWED"), HttpStatus.NOT_ACCEPTABLE);

            }
        } else {
            return new ResponseEntity<Response>(new Response("UNAUTHORIZED"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/order")
    ResponseEntity<?> getAllOrders(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                List<OrderDetails> orders = orderRepo.findAllByOrg(orRepo.findOneById(user.getEntityKey()));
                if (orders.size() > 0) {
                    List<OrderDto> ordersDetail = new ArrayList<>();
                    for (OrderDetails order : orders) {
                        List<OrderParticulars> particulars = orderperRepo.findAllByOrder(order);
                        System.out.println("Date:" + (order.getCreatedat()).getTime());
                        ordersDetail.add(new OrderDto(order, particulars));
                    }
                    return ResponseEntity.ok(ordersDetail);
                } else {
                    return new ResponseEntity<Response>(new Response("No Orders Found"), HttpStatus.NO_CONTENT);
                }
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                List<OrderDetails> orders = orderRepo.findAllByOrderCollected(user);
                return ResponseEntity.ok(orders);
            } else if (user.getRole().equals(Userrole.USER_CLIENT)) {
                List<OrderDetails> orders = orderRepo.findAllByOrderto(clientRepo.findOneById(user.getEntityKey()));
                return ResponseEntity.ok(orders);
            } else {
                return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("UNAUTHORIZED"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/order/{id}")
    ResponseEntity<?> getOrderById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user.getRole().equals(Userrole.ORGANIZATION)) {
            OrderDetails order = orderRepo.findByOrderid(id);
            if (order != null) {
                if (order.getOrg().getId() == user.getEntityKey()) {
                    return ResponseEntity.ok(new OrderDto(order, orderperRepo.findAllByOrder(order)));
                } else {
                    return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                return new ResponseEntity<Response>(new Response("NOT FOUND"), HttpStatus.NOT_FOUND);
            }

        } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
            OrderDetails order = orderRepo.findByOrderid(id);
            if (order != null) {
                if (order.getOrderCollected().getUserid() == user.getUserid()) {
                    return ResponseEntity.ok(new OrderDto(order, orderperRepo.findAllByOrder(order)));
                } else {
                    return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
                }

            } else {
                return new ResponseEntity<Response>(new Response("NOT FOUND"), HttpStatus.NOT_FOUND);
            }

        } else if (user.getRole().equals(Userrole.USER_CLIENT) && user.getActivated()) {

            OrderDetails order = orderRepo.findByOrderid(id);
            if (order != null) {
                if (order.getOrderto().getId() == user.getEntityKey()) {
                    return ResponseEntity.ok(new OrderDto(order, orderperRepo.findAllByOrder(order)));
                } else {
                    return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                return new ResponseEntity<Response>(new Response("NOT FOUND"), HttpStatus.NOT_FOUND);

            }
        } else {
            return new ResponseEntity<Response>(new Response("UNAUTHORIZED"), HttpStatus.UNAUTHORIZED);
        }
    }

//	@GetMapping("/order/executive/{exeid}/start/{sdate}/end/{edate}")
//	ResponseEntity<?> getTasksByExecutiveAndBetweenDates(@RequestHeader(value = "X-AUTH-TOKEN") String token,
//			@PathVariable long exeid, @PathVariable long sdate, @PathVariable long edate) {
//
//		User user = userRepo.findByToken(token);
//		if (user != null) {
//			if (user.getRole().equals(Userrole.ORGANIZATION)) {
//				if (exeid == 0) {
//
//					List<Order2> orders = orderRepo.findAllByCreatedatBetweenAndOrg(new Date(sdate), new Date(edate),
//							orRepo.findOne(user.getEntityKey()));
//					List<OrderDto> orderDetails = new ArrayList<>();
//					for (Order2 order : orders) {
//						orderDetails.add(new OrderDto(order, orderperRepo.findAllByOrder(order)));
//					}
//
//					return ResponseEntity.ok(orderDetails);
//				} else {
//					Executive exe = exeRepo.findOne(exeid);
//					if (exe != null && exe.getOrg().getId() == user.getEntityKey()) {
//						Optional<User> userExe = userRepo.findByEntityKeyAndRole(exe.getExeid(),
//								Userrole.USER_EXECUTIVE);
//						if (userExe.isPresent()) {
//							Date strt = new Date(sdate);
//							System.out.println("strt:" + strt.getTime() + " todate:" + strt.toString());
//							Date end = new Date(edate);
//
//							System.out.println("end:" + end.getTime() + " todate:" + end.toString() + "user:"
//									+ userExe.get().getUserid());
//
//							List<Order2> orders = orderRepo.findAllByOrderCollectedAndCreatedatBetween(userExe.get(),
//									new Date(sdate), new Date(edate));
//							List<OrderDto> orderDetails = new ArrayList<>();
//							for (Order2 order : orders) {
//								orderDetails.add(new OrderDto(order, orderperRepo.findAllByOrder(order)));
//							}
//
//							return ResponseEntity.ok(orderDetails);
//
//						} else {
//							return new ResponseEntity<Response>(new Response("Not Found 1"), HttpStatus.NOT_FOUND);
//
//						}
//
//					} else {
//						return new ResponseEntity<Response>(new Response("Not Found 2"), HttpStatus.NOT_FOUND);
//					}
//				}
//			} else {
//				return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
//			}
//		} else {
//			return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
//		}
//	}

    @PostMapping(value = "/order/{oid}/status/{status}")
    ResponseEntity<?> changeStatusoFOrder(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long oid,
                                          @PathVariable OrdersStatus status) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                OrderDetails order = orderRepo.findByOrderid(oid);
                if (order.getOrg().getId() == user.getEntityKey()) {

                    if (order.getOrstatus().equals(OrdersStatus.ACCEPTED)) {
                        return new ResponseEntity<Response>(new Response("Operation Not Allowed"), HttpStatus.LOCKED);
                    } else {
                        Client client = order.getOrderto();
                        order.setOrstatus(status);
                        if (status.equals(OrdersStatus.ACCEPTED)) {
                            order.setApprovedat(new Date());
                            String msg = "Dear " + client.getClientname() + ", Your Order of RS." + order.getTotal()
                                    + " has been " + "recievied by " + order.getOrg().getOrgname() + " at "
                                    + (new Date()) + " \n Greetings for E-ORB";
                            SmsServiceImpl.sendSms(client.getPhone(), msg);
                        }
                        orderRepo.save(order);

                        return ResponseEntity.ok().build();
                    }
                } else {
                    return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("UNAUTHORIZED"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/order/{oid}")
    ResponseEntity<?> updateOrder(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long oid,
                                  @RequestBody List<OrderParticulars> orderparts) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                OrderDetails order = orderRepo.findByOrderid(oid);
                if (order != null && order.getOrg().getId() == user.getEntityKey()) {
                    if (order.getOrstatus().equals(OrdersStatus.ON_HOLD)
                            || order.getOrstatus().equals(OrdersStatus.ADDED)) {
                        if (orderparts != null && !orderparts.isEmpty()) {
                            double total = 0.0;
                            for (OrderParticulars orderPart : orderparts) {
                                OrderParticulars orfpart = orderperRepo.findById(orderPart.getId());
                                if (orfpart != null && orfpart.getOrder().getOrderid() == oid) {
                                    orfpart.setQty(orderPart.getQty());
                                    double itemtot = orfpart.getQty() * orfpart.getRate()
                                            + ((orfpart.getQty() * orfpart.getRate() * orfpart.getTax()) / 100);
                                    orfpart.setAmount(itemtot);
                                    orfpart = orderperRepo.save(orfpart);
                                    total = total + itemtot;
                                } else {
                                    // order particular and database not matched
                                    return new ResponseEntity<Response>(new Response(HttpStatus.NO_CONTENT,
                                            "Details Mismatched", null, "Given details are inconsistent", null),
                                            HttpStatus.NO_CONTENT);
                                }
                            }

                            order.setTotal(total);
                            order.setApprovedat(new Date());

                            order.setOrstatus(OrdersStatus.ACCEPTED);
                            orderRepo.save(order);
                            Client client = order.getOrderto();

                            String msg = "Dear " + client.getClientname() + ", Your Order of RS." + order.getTotal()
                                    + " has been " + "Updated by " + order.getOrg().getOrgname() + " at "
                                    + (new Date()) + " \n Greetings for E-ORB";
                            SmsServiceImpl.sendSms(client.getPhone(), msg);

                            // OK
                            return ResponseEntity.ok().build();
                        } else {
                            // unable to fetch data
                            // int status, String error, String exception,
                            // String message, String path
                            return new ResponseEntity<Response>(new Response(HttpStatus.NO_CONTENT,
                                    "Unable to fetch Data", null, "Given details are not found", null),
                                    HttpStatus.NO_CONTENT);
                        }
                    } else {
                        // already approved
                        return new ResponseEntity<Response>(new Response(HttpStatus.NOT_MODIFIED,
                                "Opertaion Not allowed", null, "No changes are accepted now", null),
                                HttpStatus.NOT_MODIFIED);

                    }
                } else {
                    // unable to get data
                    return new ResponseEntity<Response>(new Response(HttpStatus.NOT_FOUND, "Data Mismatched",
                            null, "Data is not valid", null), HttpStatus.NOT_FOUND);

                }
            } else {
                // operation not allowed
                return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE,
                        "Insufficent Permissions", null, "You do not have sufficent permission", null),
                        HttpStatus.NO_CONTENT);
            }
        } else {
            // not found
            return new ResponseEntity<Response>(new Response("Not Authorised"), HttpStatus.UNAUTHORIZED);
        }
    }

//	@GetMapping("/order/updatesam")
//	ResponseEntity<?> sampleOrde2r() {
//
//		OrderDetails order = new OrderDetails();
//		order.setOrderCollected(userRepo.findOne(2L));
//		order.setOrg(orRepo.findOne(2L));
//		order.setTotal(25.200);
//		order.setOrderid(1);
//		order.setCreatedat(new Date());
//		order.setOrderto(clientRepo.findOne(2L));
//		order.setRemarks("This is sample order");
//		List<OrderParticulars> particulars = new ArrayList<>();
//		particulars.add(new OrderParticulars(order, 10, 1));
//		particulars.add(new OrderParticulars(order, 20, 2));
//		particulars.add(new OrderParticulars(order, 30, 3));
//		particulars.add(new OrderParticulars(order, 10, 4));
//		return ResponseEntity.ok(particulars);
//	}

    @GetMapping("/order/sam")
    ResponseEntity<?> sampleOrder() {
        OrderDetails order = new OrderDetails();
        order.setOrderCollected(userRepo.findById(1L).get());
        order.setOrg(orRepo.findById(2L).get());
        order.setTotal(25.200);
        order.setOrderid(1);
        order.setCreatedat(new Date());
        order.setOrderto(clientRepo.findOneById(1L));
        order.setRemarks("This is sample order");
        List<OrderParticulars> particulars = new ArrayList<>();
        particulars.add(new OrderParticulars(order, prlRepo.getOne(2L), 10, 1000.00, 100));
        particulars.add(new OrderParticulars(order, prlRepo.getOne(1L), 15, 1200.00, 800));
        particulars.add(new OrderParticulars(order, prlRepo.getOne(3L), 20, 1000.00, 50));
        return ResponseEntity.ok(new OrderDto(order, particulars));
    }


}
