package com.workforce.webapp.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.ClientDto;
import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.ClientAccoutStatus;
import com.workforce.webapp.model.Location;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.UserStatus;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.utils.RestUtils;


@CrossOrigin
@EnableAutoConfiguration
@RestController

public class ClientController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    ClientRepository clientRepo;


    @Autowired
    OrganizationRepository orgRepo;

    @Autowired
    ExecutiveRepository exeRepo;


    @Value("${attachments-folder}")
    private String attachmentsFolder;

    @GetMapping(value = "/client")
    ResponseEntity<?> getAllClients(@RequestHeader("X-AUTH-TOKEN") String token,
                                    @RequestParam(required = false) Integer prps) {
        System.out.println("Prps=" + prps);
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            List<Client> clients;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                clients = clientRepo.findAllByOrgId(user.getEntityKey());

            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                if (prps != null && prps == 2) {
                    clients = clientRepo.findAllByOrgId(exeRepo.findById(user.getEntityKey()).get().getOrg().getId());
                } else {
                    clients = clientRepo.findAllByAssignedTo(user.getEntityKey());
                }
                // OrgId(exeRepo.findOne(user.getEntityKey()).getOrg().getId());
            } else {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().body(clients);

        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping(value = "/client")
    ResponseEntity<?> addNewClient(@RequestHeader("X-AUTH-TOKEN") String token, @RequestBody Client client) {

        // System.out.println("Client="+client);

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                client.setOrgId(user.getEntityKey());
                if (client.getId() == null || client.getId() <= 0) {
                    client.setAddedBy(user);
                }
                if (client.getImage() != null && !client.getImage().endsWith(".JPG")) {
                    String name = "/clientimage/" + new Date().getTime() + ".JPG";
                    String img = RestUtils.Base64ToImage(client.getImage(), name, attachmentsFolder);
                    if (img != null) {
                        client.setImage(name);

                    } else {
                        client.setImage("");
                    }
                }

                clientRepo.save(client);

                return ResponseEntity.ok().body(client);

            }

//			else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
//				
//				client.setOrgId(exeRepo.findOne(user.getEntityKey()).getOrg().getId());
//
//				client.setAddedBy(user);
//				client.setAssignedTo(user.getEntityKey());
//				if (client.getImage() != null && !client.getImage().endsWith(".JPG")) {
//
//					String name = "/clientimage/" + new Date().getTime() + ".JPG";
//					String img = RestUtils.Base64ToImage(client.getImage(), name, attachmentsFolder);
//					if (img != null) {
//						client.setImage(name);
//
//					} else {
//						client.setImage("");
//					}
//				}
//				clientRepo.save(client);
//				System.out.println("Client=" + client);
//				Optional<User> fbtok = userRepo.findByEntityKeyAndRole(
//						exeRepo.findOne(user.getEntityKey()).getOrg().getId(), Userrole.ORGANIZATION);
//				if (fbtok.isPresent()) {
//					Firebase.sendTaskNotification(fbtok.get().getFbTok(),
//							"Client " + client.getCompanyName() + " addedby <i>" + user.getUsername() + "</i>",
//							"New Client Registration");
//				}
//				try {
//					long orgid = exeRepo.findOne(user.getEntityKey()).getOrg().getId();
//					Optional<User> notifUser = userRepo.findByEntityKeyAndRole(orgid, Userrole.ORGANIZATION);
//					if (notifUser.isPresent()) {
//						enoteRepo.save(new EorbNotification("New Client:" + client.getClientname(),
//								user.getUsername() + " added new client", notifUser.get(), "clients"));
//					}
//				} catch (Exception ex) {
//					ex.printStackTrace();
//				}
//
//				return ResponseEntity.ok().body(client);
//
//			} 

            else {

                return ResponseEntity.notFound().build();

            }

        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

//	@PostMapping(value = "/client/update/{cid}")
//	ResponseEntity<?> updateNewClient(@RequestHeader("X-AUTH-TOKEN") String token, @PathVariable long cid,
//			@RequestBody ClientUpdateRequest clientUpdate) {
//
//		User user = userRepo.findByToken(token);
//		if (user != null) {
//			if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
//				System.out.println("Old Client data :" + clientRepo.findOne(cid).toString());
//				Client client = clientRepo.findOne(cid);
//				System.out.println("Date got: for client" + cid + ", with data:" + clientUpdate.toString());
//				if (client.getOrgId() == exeRepo.findOne(user.getEntityKey()).getOrg().getId()) {
//					Optional<ClientUpdateRequest> cur = cuRepo.findByCid(cid);
//					if (cur.isPresent()) {
//						clientUpdate.setId(cur.get().getId());
//					}
//					clientUpdate.setCid(cid);
//					clientUpdate.setUpdateRequestedBy(user);
//
//					clientUpdate = cuRepo.save(clientUpdate);
//					client.setUpdateState(clientUpdate.getId());
//					clientRepo.save(client);
//					Optional<User> fbtok = userRepo.findByEntityKeyAndRole(
//							exeRepo.findOne(user.getEntityKey()).getOrg().getId(), Userrole.ORGANIZATION);
//					if (fbtok.isPresent()) {
//						Firebase.sendTaskNotification(fbtok.get().getFbTok(), "Update Request for Client "
//								+ client.getCompanyName() + "  is made by " + user.getUsername(),
//								"Client Update Request");
//					}
//					try {
//						long orgid = exeRepo.findOne(user.getEntityKey()).getOrg().getId();
//						Optional<User> notifUser = userRepo.findByEntityKeyAndRole(orgid, Userrole.ORGANIZATION);
//						if (notifUser.isPresent()) {
//							enoteRepo
//									.save(new EorbNotification("Required Updation for Client:" + client.getClientname(),
//											user.getUsername() + " updated client", notifUser.get(), "clients"));
//						}
//					} catch (Exception ex) {
//						ex.printStackTrace();
//					}
//
//					return ResponseEntity.ok().build();
//
//				} else {
//					return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
//				}
//
//			} else {
//				return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
//			}
//		} else {
//			return new ResponseEntity<Response>(new Response("Not Authorized"), HttpStatus.UNAUTHORIZED);
//		}
//
//	}

    @PostMapping("/client/activatelogin/{cid}")
    ResponseEntity<?> createClientLogin(@RequestHeader("X-AUTH-TOKEN") String token, @PathVariable long cid) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null && user.getRole().equals(Userrole.ORGANIZATION)) {
            Client client = clientRepo.findOneById(cid);

            System.out.print("Client OrgId:" + client.getOrgId());
            System.out.print("EtKey:" + user.getEntityKey());

            if (client != null && client.getOrgId() == user.getEntityKey()) {
                // venkat add == insterd of != @ above line
                // abhinay user_executive to user_client
                UserDetails newClient = new UserDetails();
                newClient.setRole(Userrole.USER_CLIENT);
                newClient.setUsername(client.getClientname());
                newClient.setPhone(client.getPhone());
                newClient.setEntityKey(client.getId());
                newClient.setStatus(UserStatus.PENDING_SELF_ACTIVATION);
                // chnged from user to client in below lines
                newClient.setLogin(client.getPhone());
                newClient.setActivated(true);
                newClient.setPassword(RestUtils.md5(client.getPhone()));
                newClient.setEmail(client.getMail());
                String userActivation = "";
                while (userActivation.equals("")) {
                    userActivation = RestUtils.getActivationKey();
                    if (userRepo.findByActivationKey(userActivation) == null) {
                        System.out.println("Activation key=" + userActivation);
                        newClient.setActivationKey(userActivation);
                    } else {
                        userActivation = "";
                    }
                }
                userRepo.save(newClient);
                client.setAccStatus(ClientAccoutStatus.ACCOUNT_IN_ACTIVE);
                clientRepo.save(client);
                // Dear ##name##, use your phone number as username and password
                // for eorbapp.
                String msg = "Dear " + newClient.getUsername()
                        + ", use your phone number as username and password for eorbapp @"
                        + " https://bit.ly/2J9ui1Q -Thanks, Call @ 8886668273, Neemiit";
                // String msg= "Executive account created for
                // "+org.getOrgname()+"\n login with your phone number as uname
                // and pswd";
                System.out.println("Message=" + msg);
                SmsServiceImpl.sendSms(newClient.getPhone(), msg); // "success";//
                return new ResponseEntity<Response>(new Response("Login Account Created Sucessfully"), HttpStatus.OK);
            } else {

                return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {
            return new ResponseEntity<Response>(new Response("Not Authorized"), HttpStatus.UNAUTHORIZED);
        }

    }

    @PostMapping("/client/changeAccountStatus/{cid}")
    ResponseEntity<?> deactivateLogin(@RequestHeader("X-AUTH-TOKEN") String token, @PathVariable long cid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null && user.getRole().equals(Userrole.ORGANIZATION)) {
            UserDetails clientUser = userRepo.findByEntityKeyAndRole(cid, Userrole.USER_CLIENT);//				UserDetails clientUser = clientUserOp.get();
            if (clientUser != null) {
                Client client = clientRepo.findOneById(clientUser.getEntityKey());
                if (client.getOrgId() == user.getEntityKey()) {
                    clientUser.setActivated(!clientUser.getActivated());
                    userRepo.save(clientUser);
                    if (clientUser.getActivated()) {
                        client.setAccStatus(ClientAccoutStatus.ACCOUNT_IN_ACTIVE);
                    } else {
                        client.setAccStatus(ClientAccoutStatus.ACCOUNT_IN_DEACTIVE);
                    }
                    clientRepo.save(client);

                    return new ResponseEntity<Response>(new Response("Login Account Deactivated Sucessfully"),
                            HttpStatus.OK);
                } else {
                    return new ResponseEntity<Response>(new Response("No Account found"),
                            HttpStatus.NOT_ACCEPTABLE);
                }


            } else {
                return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Not Authorized"), HttpStatus.UNAUTHORIZED);
        }
    }

//	@GetMapping(value = "/client/update/{cid}")
//	ResponseEntity<?> getUpdateReqest(@RequestHeader("X-AUTH-TOKEN") String token, @PathVariable long cid) {
//
//		User user = userRepo.findByToken(token);
//		if (user != null) {
//			if (user.getRole().equals(Userrole.USER_EXECUTIVE) || user.getRole().equals(Userrole.ORGANIZATION)) {
//				// System.out.println("Old Client data
//				// :"+clientRepo.findOne(cuid).toString());
//				// Client client = clientRepo.findOne(cid);
//				Optional<ClientUpdateRequest> cur = cuRepo.findByCid(cid);
//				if (cur.isPresent()) {
//					return ResponseEntity.ok(cur.get());
//				} else {
//					return ResponseEntity.notFound().build();
//				}
//
//			} else {
//				return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
//			}
//		} else {
//			return new ResponseEntity<Response>(new Response("Not Authorized"), HttpStatus.UNAUTHORIZED);
//		}
//
//	}

//	@PostMapping(value = "/client/update/{cuid}/action/{action}")
//	ResponseEntity<?> performActionOnReqest(@RequestHeader("X-AUTH-TOKEN") String token, @PathVariable long cuid,
//			@PathVariable int action) {
//		UserDetails user = userRepo.findByToken(token);
//		if (user != null) {
//			if (user.getRole().equals(Userrole.USER_EXECUTIVE) || user.getRole().equals(Userrole.ORGANIZATION)) {
//				// System.out.println("Old Client data
//				// :"+clientRepo.findOne(cuid).toString());
//				// Client client = clientRepo.findOne(cid);
//
//				Optional<ClientUpdateRequest> cur = cuRepo.findByCid(cuid);
//				if (cur.isPresent()) {
//					ClientUpdateRequest cu = cur.get();
//					Client client;
//
//					switch (action) {
//					case RestUtils.CLIENT_UPDATE_APPROVE:
//
//						client = clientRepo.findOne(cuid);
//						client.setClientname(cu.getClientname());
//						client.setPhone(cu.getPhone());
//						client.setMail(cu.getMail());
//						client.setLocation(cu.getLocation());
//						client.setIncorporatedAt(cu.getIncorporatedAt());
//						client.setOfficeNumber(cu.getOfficeNumber());
//						client.setGstNo(cu.getGstNo());
//						client.setRemarks(cu.getRemarks());
//						client.setImage(cu.getImage());
//						client.setUpdateState(-1);
//
//						clientRepo.save(client);
//						try {
//							enoteRepo.save(new EorbNotification("Update request Accepted",
//									"Client (" + cur.get().getClientname() + ")'s details are updated",
//									cur.get().getUpdateRequestedBy(), "clients"));
//						} catch (Exception ex) {
//							ex.printStackTrace();
//						}
//						Firebase.sendTaskNotification(cur.get().getUpdateRequestedBy().getFbTok(),
//								"Update Request for Client CID #" + cuid + "  is APPROVED", "Client Update Request");
//						break;
//					case RestUtils.CLIENT_UPDATE_REJECT:
//						client = clientRepo.findOne(cuid);
//						client.setUpdateState(-2);
//						clientRepo.save(client);
//						try {
//							enoteRepo.save(new EorbNotification("Update request Rejected",
//									"Client (" + cur.get().getClientname() + ")'s details are not updated",
//									cur.get().getUpdateRequestedBy(), "clients"));
//						} catch (Exception ex) {
//							ex.printStackTrace();
//						}
//						Firebase.sendTaskNotification(cur.get().getUpdateRequestedBy().getFbTok(),
//								"Update Request for Client CID #" + cuid + "  is REJECTED", "Client Update Request");
//
//						break;
//					case RestUtils.CLIENT_UPDATE_REJECTED_UNDO:
//						client = clientRepo.findOne(cuid);
//						client.setUpdateState(cu.getId());
//						clientRepo.save(client);
//						Firebase.sendTaskNotification(cur.get().getUpdateRequestedBy().getFbTok(),
//								"Update Request for Client CID #" + cuid + "  is REJECTION UNDONE",
//								"Client Update Request");
//						break;
//
//					default:
//						return ResponseEntity.notFound().build();
//
//					}
//					return ResponseEntity.ok(cur);
//				} else {
//					return ResponseEntity.notFound().build();
//				}
//
//			} else {
//				return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
//			}
//		} else {
//			return new ResponseEntity<Response>(new Response("Not Authorized"), HttpStatus.UNAUTHORIZED);
//		}
//
//	}

    @GetMapping(value = "/client/{id}")
    ResponseEntity<?> getClietById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                   @PathVariable(value = "id") long cid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            long orgid = 0;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {

                orgid = user.getEntityKey();
            }
//			else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
//
//				orgid = exeRepo.findOne(user.getEntityKey()).getOrg().getId();
//			} 
            else {
                return new ResponseEntity<Response>(new Response("NotAllwed"), HttpStatus.NOT_ACCEPTABLE);
            }
            Client client = clientRepo.findOneById(cid);
            if (client != null) {
                if (client.getOrgId() == orgid) {
                    ClientDto clientDto = new ClientDto();
                    clientDto.setFromClient(client);
//					double closingbal = payRepo.getClosingBalance(client.getId(), client.getId(), client.getId());
//					List<Order2> orders = orRepo.findAllByOrdertoAndOrg(client, orgRepo.findOne(client.getOrgId()));
//					List<Payment> payments = payRepo.findAllByFrom(client);
//					clientDto.setClosingBalance(closingbal);
//					clientDto.setOrderCount(orders != null ? orders.size() : 0);
//					clientDto.setPaymentsCount(payments != null ? payments.size() : 0);
//					clientDto.setTotalOrderAmount(
//							Double.parseDouble(RestUtils.df2.format(orRepo.sumOfClientActiveOrders(cid))));
//					clientDto.setTotalPaymentAmount(
//							Double.parseDouble(RestUtils.df2.format(payRepo.sumOfClientApprovedPayments(cid))));
                    clientDto.setAccStatus(client.getAccStatus());
                    return ResponseEntity.ok().body(clientDto);

                } else {
                    return new ResponseEntity<Response>(new Response("NotAllwed"), HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                return new ResponseEntity<Response>(new Response("NotAllwed"), HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);
        }
    }

//	@GetMapping(value = "/client/profile")
//	ResponseEntity<?> getClientProfile(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
//
//		UserDetails user = userRepo.findByToken(token);
//		if (user != null && user.getRole().equals(Userrole.USER_CLIENT)) {
//
//			Client client = clientRepo.findOne(user.getEntityKey());
//			if (client != null) {
//				ClientDto clientDto = new ClientDto();
//				clientDto.setFromClient(client);
//				double closingbal = payRepo.getClosingBalance(client.getId(), client.getId(), client.getId());
//				List<Order2> orders = orRepo.findAllByOrdertoAndOrg(client, orgRepo.findOne(client.getOrgId()));
//				List<Payment> payments = payRepo.findAllByFrom(client);
//				clientDto.setClosingBalance(closingbal);
//				clientDto.setOrderCount(orders != null ? orders.size() : 0);
//				clientDto.setPaymentsCount(payments != null ? payments.size() : 0);
//				clientDto.setTotalOrderAmount(
//						Double.parseDouble(RestUtils.df2.format(orRepo.sumOfClientActiveOrders(client.getId()))));
//				clientDto.setTotalPaymentAmount(
//						Double.parseDouble(RestUtils.df2.format(payRepo.sumOfClientApprovedPayments(client.getId()))));
//				clientDto.setAccStatus(client.getAccStatus());
//				return ResponseEntity.ok().body(clientDto);
//			} else {
//				return new ResponseEntity<Response>(new Response("Data Mismathed"), HttpStatus.NOT_ACCEPTABLE);
//			}
//		} else {
//			return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);
//		}
//	}

//	@GetMapping(value = "/client/ledger/{year}/{month}")
//	ResponseEntity<?> getClientLedger(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable int year,
//			@PathVariable int month) {
//		Date date = new Date();
//		Calendar calendar = new GregorianCalendar();
//		calendar.setTime(date);
//		int curyear = calendar.get(Calendar.YEAR);
//
//		System.out.println("No Data Found " + curyear);
//		if (year <= curyear) {
//			User user = userRepo.findByToken(token);
//			if (user != null && user.getRole().equals(Userrole.USER_CLIENT)) {
//				long cid = 0;
//				// && user.getRole().equals(Userrole.USER_CLIENT)
//				Client client = clientRepo.findOne(user.getEntityKey());
//				if (client != null) {
//					List<Object> entries = clientRepo.findClientLedgerEntry(client.getId(), year, month);
//					if (entries.isEmpty()) {
//						System.out.println("No Data Found");
//						return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NO_CONTENT);
//					}
//					ClientLedgerDto ledger = new ClientLedgerDto(0, entries);
//					System.out.println("ok" + entries);
//					return ResponseEntity.ok(ledger);
//
//				} else {
//					System.out.println("Invalid Data of client");
//					return new ResponseEntity<Response>(new Response("Invalid Data of client"),
//							HttpStatus.NOT_ACCEPTABLE);
//				}
//			} else {
//				System.out.println("UnAuthorised");
//				return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);
//			}
//		} else {
//			System.out.println("Invalid Data of date");
//			return new ResponseEntity<Response>(new Response("Invalid Data of date"), HttpStatus.NOT_ACCEPTABLE);
//		}
//	}

//	@GetMapping(value = "/client/ledger/{cid}/{year}/{month}")
//	ResponseEntity<?> getClientLedger(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long cid,
//			@PathVariable int year, @PathVariable int month) {
//		Date date = new Date();
//		Calendar calendar = new GregorianCalendar();
//		calendar.setTime(date);
//		int curyear = calendar.get(Calendar.YEAR);
//		System.out.println("No Data Found " + curyear);
//		if (year <= curyear) {
//			User user = userRepo.findByToken(token);
//			if (user != null) {
//				// && user.getRole().equals(Userrole.USER_CLIENT)
//				Client client = clientRepo.findOne(cid);
//				if (client != null) {
//					if ((user.getRole().equals(Userrole.ORGANIZATION) && user.getEntityKey() == client.getOrgId())) {
//
//					} else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
//						Executive exe = exeRepo.findOne(user.getEntityKey());
//						if (exe != null && (exe.getOrg().getId() == client.getOrgId())) {
//
//						} else {
//							//
//							return new ResponseEntity<Response>(new Response("Invalid Data of date"),
//									HttpStatus.NOT_ACCEPTABLE);
//						}
//					} else {
//						return new ResponseEntity<Response>(new Response("Invalid Data of date"),
//								HttpStatus.NOT_ACCEPTABLE);
//					}
//
//					List<Object> entries = clientRepo.findClientLedgerEntry(client.getId(), year, month);
//					if (entries.isEmpty()) {
//						System.out.println("No Data Found");
//						return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NO_CONTENT);
//					}
//					ClientLedgerDto ledger = new ClientLedgerDto(500, entries);
//					System.out.println("ok" + entries);
//					return ResponseEntity.ok(ledger);
//
//				} else {
//					System.out.println("Invalid Data of client");
//					return new ResponseEntity<Response>(new Response("Invalid Data of client"),
//							HttpStatus.NOT_ACCEPTABLE);
//				}
//			} else {
//				System.out.println("UnAuthorised");
//				return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);
//			}
//		} else {
//			System.out.println("Invalid Data of date");
//			return new ResponseEntity<Response>(new Response("Invalid Data of date"), HttpStatus.NOT_ACCEPTABLE);
//		}
//	}

//	@GetMapping(value = "/client/dashboard")
//	ResponseEntity<?> clientDashboard(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
//		User user = userRepo.findByToken(token);
//		if (user != null && user.getRole().equals(Userrole.USER_CLIENT)) {
//			Client client = clientRepo.findOne(user.getEntityKey());
//			if (client != null) {
//				int to = orRepo.findAllByOrderto(user.getEntityKey());
//				int tao = orRepo.findAllByOrdertoAndOrstatus(user.getEntityKey(), 1);
//				int tp = payRepo.findAllByFromCount(user.getEntityKey());
//				Map<String, Object> cDr = new HashMap<>();
//				cDr.put("totalOrders", to);
//				cDr.put("totalAcceptedOrders", tao);
//				cDr.put("totalPayments", tp);
//
//				Organization org = orgRepo.findOne(client.getOrgId());
//				if (org != null) {
//					cDr.put("cpLogo", org.getLogoUrl());
//					cDr.put("cpName", org.getOrgname());
//
//				} else {
//					cDr.put("cpLogo", "");
//					cDr.put("cpName", "");
//				}
//				cDr.put("companyName", client.getCompanyName());
//				cDr.put("clientName", client.getClientname());
//				cDr.put("phone", client.getPhone());
//				return ResponseEntity.ok(cDr);
//			} else {
//				return null;
//			}
//
//		} else {
//			return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);
//		}
//	}
//
//	@GetMapping(value = "/client/export")
//	ResponseEntity<?> exportClients(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
//		User user = userRepo.findByToken(token);
//		if (user != null) {
//			if (user.getRole().equals(Userrole.ORGANIZATION)) {
//				List<Client> clients = clientRepo.findAllByOrgId(user.getEntityKey());
//				String fileName = "/export/client.xls";
//				File file = ExportToExcelService.exportClientsToExcel(clients, attachmentsFolder + fileName, "");
//				MultiValueMap<String, String> headers = new HttpHeaders();
//				headers.add("Content-Type", "application/octet-stream");
//				if (file != null) {
//					try {
//						enoteRepo.save(new EorbNotification("Export Sucessfull",
//								"Export request of your client is completed", user, "clients"));
//					} catch (Exception ex) {
//						ex.printStackTrace();
//					}
//					return new ResponseEntity<String>(fileName, headers, HttpStatus.OK);
//				} else {
//					return new ResponseEntity<String>("Unable to get data", headers, HttpStatus.INTERNAL_SERVER_ERROR);
//				}
//			} else {
//				return new ResponseEntity<Response>(new Response("Not Allowed"), HttpStatus.NOT_ACCEPTABLE);
//			}
//		} else {
//			return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);
//		}
//		// Client("Sukesh","994989800","kjdhfk@kdfjhd.com",new
//		// Location(17.8979,78.80980,"Golnaka ekkama home"),"/sample.jpg",0);
//	}

    @GetMapping(value = "/client/sam")
    Client getSamClients() {
        Client c = new Client("Sukesh", "994989800", "kjdhfk@kdfjhd.com",
                new Location(17.8979, 78.80980, "Golnaka ekkama home"), "/sample.jpg", 0, "XYZ pvt Ltd", new Date(),
                "27676767", "Lies on hills");
        c.setGstNo("hkjhkjhkjj");
        return c;
        // Client("Sukesh","994989800","kjdhfk@kdfjhd.com",new
        // Location(17.8979,78.80980,"Golnaka ekkama home"),"/sample.jpg",0);
    }

//	@PostMapping(value = "/client/bulkaction")
//	ResponseEntity<?> bulkAction(@RequestHeader(value = "X-AUTH-TOKEN") String token,@RequestBody MultipleClientStatus mcs) {
//		if (mcs.getClientids() != null && mcs.getAction() > 0) {
//			System.out.print("Action:" + mcs.getAction());
//			for (Integer in : mcs.getClientids()) {
//				System.out.print(in + ",");
//			}
//			return ResponseEntity.ok().build();
//		}else{
//			return ResponseEntity.badRequest().build();
//		}
//
//		
//	}

}
