package com.workforce.webapp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.CRMProductAlldetails;
import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.CRMProduct;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.CRMProductAddOnRepository;
import com.workforce.webapp.repository.CRMProductPlanRepository;
import com.workforce.webapp.repository.CRMProductRepository;
import com.workforce.webapp.repository.CrmProductCouponRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@CrossOrigin
@EnableAutoConfiguration
@RestController

public class CRMProductController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    CRMProductRepository crmProdRepo;

    @Autowired
    CRMProductAddOnRepository crmProdAddonRepo;

    @Autowired
    CRMProductPlanRepository crmProdPlanRepo;


    @Autowired
    CrmProductCouponRepository crmProdCouponRepo;

    @PostMapping("/crmproduct")
    ResponseEntity<?> addCrmProduct(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                    @RequestBody CRMProduct product) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.CRM_ADMIN)) {
                Optional<CRMProduct> exitDepo = crmProdRepo.findByName(product.getName());
                if (exitDepo.isPresent()) {
                    return new ResponseEntity<Response>(new Response("Department Name Already Exists"),
                            HttpStatus.ALREADY_REPORTED);
                } else {
                    // ProductCategory pcategory = new ProductCategory(, new
                    // Date(), user, org);
                    product.setCreatedAt(new Date());
                    product = crmProdRepo.save(product);
                    return ResponseEntity.ok(product);
                }

            } else {

                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {

            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);

        }

    }

    @GetMapping("/crmproduct")
    ResponseEntity<?> getCrmProducts(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {

                List<CRMProduct> deps = crmProdRepo.findAll();
                return ResponseEntity.ok(deps);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);

            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/crmproduct/{id}")
    ResponseEntity<?> getCRMProductById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {
                Optional<CRMProduct> dep = crmProdRepo.findById(id);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/crmproduct/{id}/alldata")
    ResponseEntity<?> getCRMProductAllDataById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                               @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {
                Optional<CRMProduct> dep = crmProdRepo.findById(id);
                if (dep.isPresent()) {

                    CRMProductAlldetails allDetails = new CRMProductAlldetails();

                    long crmpid = dep.get().getCrmpid();
                    allDetails.setDetails(dep.get());
                    allDetails.setCoupons(crmProdCouponRepo.findByProductCrmpid(crmpid));
                    allDetails.setAddons(crmProdAddonRepo.findByProductCrmpid(crmpid));
                    allDetails.setPlans(crmProdPlanRepo.findByProductCrmpid(crmpid));
                    return ResponseEntity.ok(allDetails);

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

}
