package com.workforce.webapp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.CRMCoupon;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.CrmProductCouponRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@CrossOrigin
@EnableAutoConfiguration
@RestController

public class CRMProductCouponController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    CrmProductCouponRepository crmProdCouponRepo;

    @PostMapping("/crmproductcoupon")
    ResponseEntity<?> addCrmProductCoupon(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                          @RequestBody CRMCoupon productCoupon) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.CRM_ADMIN)) {
                Optional<CRMCoupon> exitDepo = crmProdCouponRepo.findByCouponCode(productCoupon.getCouponCode());
                if (exitDepo.isPresent()) {
                    return new ResponseEntity<Response>(new Response("Coupon code Already Exists"),
                            HttpStatus.ALREADY_REPORTED);
                } else {
                    // ProductCategory pcategory = new ProductCategory(, new
                    // Date(), user, org);
                    productCoupon.setCreatedAt(new Date());

                    try {
                        productCoupon = crmProdCouponRepo.save(productCoupon);
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                        return new ResponseEntity<Response>(new Response(e.getLocalizedMessage()),
                                HttpStatus.EXPECTATION_FAILED);
                    }
                    return ResponseEntity.ok(productCoupon);
                }

            } else {

                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {

            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);

        }

    }

    @GetMapping("/crmproductcoupon")
    ResponseEntity<?> getCrmProductCoupons(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {

                List<CRMCoupon> deps = crmProdCouponRepo.findAll();
                return ResponseEntity.ok(deps);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);

            }
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/crmproductcoupon/{id}")
    ResponseEntity<?> getProductAddOnById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {
                Optional<CRMCoupon> dep = crmProdCouponRepo.findById(id);
                if (dep.isPresent()) {
                    return ResponseEntity.ok(dep.get());

                } else {
                    return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }


    @GetMapping("/crmproduct/{crmpid}/crmproductcoupon")
    ResponseEntity<?> getProductCouponByProductId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                  @PathVariable long crmpid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.CRM_ADMIN) || user.getRole().equals(Userrole.CRM_USER)) {
                List<CRMCoupon> dep = crmProdCouponRepo.findByProductCrmpid(crmpid);
                return ResponseEntity.ok(dep);

            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

}
