package com.workforce.webapp.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.Product;
import com.workforce.webapp.model.ProductCategory;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.ProductCategoryRepository;
import com.workforce.webapp.repository.ProductRepository;
import com.workforce.webapp.repository.UserDetailsRepository;


@RestController
@CrossOrigin
@EnableAutoConfiguration
public class ProductController {


    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    ProductRepository proRepo;

    @Autowired
    OrganizationRepository orgRepo;

    @Autowired
    ProductCategoryRepository pcatRepo;

    @Autowired
    ExecutiveRepository exeRepo;

    @Value("${attachments-folder}")
    private String attachmentsFolder;

    @PostMapping(value = "/product")
    ResponseEntity<?> addProduct(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestBody Product product) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                product.setUser(user);
                product.setOrg(orgRepo.findOneById(user.getEntityKey()));
                ProductCategory pcat = product.getPcat();
//				System.out.println(pcat.toString());  50100172384912 HDFC000460
                if (pcat != null && pcat.getCatid() != 0) {
                    Optional<ProductCategory> cat = pcatRepo.findByCatidAndOrg(pcat.getCatid(), orgRepo.findOneById(user.getEntityKey()));
                    if (cat.isPresent()) {
                        pcat = cat.get();
                        product.setPcat(pcat);
                    } else {
                        pcat = null;
                    }
                } else {
                    pcat = null;
                }
                proRepo.save(product);
                return ResponseEntity.ok(product);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/product")
    ResponseEntity<?> getProductsByOrg(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        System.out.println("GET PRODUCT by " + token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                org = orgRepo.findOneById(user.getEntityKey());
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                org = exeRepo.getOne(user.getEntityKey()).getOrg();
            }
//			else if(user.getRole().equals(Userrole.USER_CLIENT)){
//				Client client = cliRepo.findOne(user.getEntityKey());
//				if(client!=null){
//					org= orgRepo.findOne(client.getOrgId());
//				}else{
//					return new ResponseEntity<Response>(new Response("Client not found"),HttpStatus.NOT_ACCEPTABLE);
//				}
//				
//			}
            else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
            if (org != null) {

                List<Product> products = proRepo.findAllByOrg(org);
                return ResponseEntity.ok(products);
            } else {
                return new ResponseEntity<Response>(new Response("Invalid details"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }
    }


    @GetMapping("/category/{cid}/product")
    ResponseEntity<?> getProductsByCategory(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long cid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;

            if (user.getRole().equals(Userrole.ORGANIZATION) && cid > 0) {
                org = orgRepo.findOneById(user.getEntityKey());
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE) && cid > 0) {
                org = exeRepo.getOne(user.getEntityKey()).getOrg();
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
            ProductCategory pcat = pcatRepo.findOneByCatid(cid);
            List<Product> products = proRepo.findAllByPcatAndOrg(pcat, org);
            return ResponseEntity.ok(products);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }


    @GetMapping("/product/{pid}")
    ResponseEntity<?> getProductById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long pid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            long orgid = 0;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                orgid = user.getEntityKey();
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                orgid = exeRepo.getOne(user.getEntityKey()).getOrg().getId();
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

            Optional<Product> product = proRepo.findById(pid);
            if (product.isPresent() && product.get().getOrg().getId() == orgid) {
                return ResponseEntity.ok(product.get());
            } else {
                return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);
            }


        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    /***
     * Add new Product Category
     * @param token
     * @param pcat
     * @return
     */


    @PostMapping("/product/category")
    ResponseEntity<?> AddProductCategory(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestBody ProductCategory pcat) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION) && pcat != null) {
                org = orgRepo.findOneById(user.getEntityKey());
                if (pcatRepo.findByCatnameAndOrg(pcat.getCatname(), org).isPresent()) {
                    return new ResponseEntity<Response>(new Response("Catagory Name Already Exists"), HttpStatus.ALREADY_REPORTED);
                } else {
                    //ProductCategory pcategory = new ProductCategory(, new Date(), user, org);
                    pcat.setCatname(pcat.getCatname().toUpperCase());
                    pcat.setCreateddate(new Date());
                    pcat.setOrg(org);
                    pcat.setUser(user);
                    pcatRepo.save(pcat);
                    return ResponseEntity.ok(pcat);
                }

            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/product/category")
    ResponseEntity<?> getProductCategories(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                org = orgRepo.findOneById(user.getEntityKey());
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                org = exeRepo.getOne(user.getEntityKey()).getOrg();
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
            List<ProductCategory> pcats = pcatRepo.findAllByOrg(org);
            return ResponseEntity.ok(pcats);
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }


    @GetMapping("/product/category/{cid}")
    ResponseEntity<?> getProductCategoryById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long cid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            Organization org = null;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                org = orgRepo.findOneById(user.getEntityKey());
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                org = exeRepo.getOne(user.getEntityKey()).getOrg();
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }
            Optional<ProductCategory> pcats = pcatRepo.findOneByCatidAndOrg(cid, org);
            if (pcats.isPresent()) {
                return ResponseEntity.ok(pcats.get());

            } else {
                return new ResponseEntity<Response>(new Response("No Data Found"), HttpStatus.NOT_FOUND);

            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }


    @GetMapping("/product/top/{n}")
    ResponseEntity<?> getProductsByOrg(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long n) {
        UserDetails user = userRepo.findByToken(token);
        System.out.println("GET Top" + n + " PRODUCTs by " + token);
        if (user != null) {
            Organization orgid = null;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                List<BigInteger> pids = proRepo.findTopProducts(n, user.getEntityKey());
                List<Product> products = new ArrayList<>();
                for (BigInteger l : pids) {
                    Optional<Product> p = proRepo.findById(l.longValue());
                    if (p.isPresent()) {
                        products.add(p.get());
                    }

                }
                return ResponseEntity.ok(products);
            } else {
                return new ResponseEntity<Response>(new Response("Now Allowed"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorised"), HttpStatus.UNAUTHORIZED);
        }

    }

//	@GetMapping("/product/export")
//	ResponseEntity<?> exportExpenditures(@RequestHeader(value="X-AUTH-TOKEN") String token){
//		UserDetails user = userRepo.findByToken(token);
//		if(user!=null){
//			if(user.getRole().equals(Userrole.ORGANIZATION)){
//				List<Product> procs = proRepo.findAllByOrg(orgRepo.findOne(user.getEntityKey()));
//				if(procs!=null && procs.size()>0){
//					String[] values = {"product Id","Name","Category","Amount","Units","Tax","Taxable","Quantity Per Case","Image","Part Number","HSN/SAC"};
//					List<String[]> data = new ArrayList<>();
//					for(Product pro:procs){
//						data.add(pro.getValuesArray("http://192.168.0.107:88/workforcetracking/uploads/"));	
//					}
//					ExportData dat = new ExportData(values, data,attachmentsFolder+"/export/product.xls");
//					File f= ExportToExcelService.exportDataToExcel(dat);
//					MultiValueMap<String, String> headers = new HttpHeaders();
//					headers.add("Content-Type", "application/octet-stream");
//					return new  ResponseEntity<String>("/export/"+f.getName(),headers,HttpStatus.OK);
//					
//				}else{
//					return new ResponseEntity<Response>(new Response("No Expenditures Found"),HttpStatus.NO_CONTENT);
//				}
//				
//			}else{
//				return new ResponseEntity<Response>(new Response("Not Allowed"),HttpStatus.NOT_ACCEPTABLE);
//			}
//		}else{
//			return new ResponseEntity<Response>(new Response("Unauthorized"),HttpStatus.UNAUTHORIZED);
//		}
//	}
//
//	
//	@RequestMapping(value = "/sampleuploads", method = RequestMethod.POST,
//		    consumes = {"multipart/form-data"})
//		@ResponseBody
//		public ResponseEntity<?> executeSampleService(
//		        @RequestPart("properties") @Valid  CommentDto comment,
//		        @RequestPart("file") @Valid @NotNull MultipartFile[] uploads) {
//		    return ResponseEntity.ok(comment);
//		}
//	
//		
//	
//	@RequestMapping(value="/product/sam")
//	Product getProductSam(){
//		return new Product("Surfexcel",pcatRepo.findOne(1L),90.100,"qtys","451245", 
//				"123652", 5, true, userRepo.findOne(2L), 
//				new Date(), null, 100, "https://www.countrycheeseandmore.com/wp-content/uploads/2017/02/dummy-cat.png");
//				//Product("Surfexcel",null,90.100,"qtys",100,null);
//	}

}
