package com.workforce.webapp.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.Discount;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@CrossOrigin
@RestController
public class DiscountsController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    DiscountsRepository dsRepo;

    @Autowired
    OrganizationRepository orgRepo;

    @Autowired
    ExecutiveRepository exeRepo;

    @GetMapping("/discount")
    ResponseEntity<Response> getAllDiscounts(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            List<Discount> discounts;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Organization org = orgRepo.findOneById(user.getEntityKey());
                discounts = dsRepo.findByOrg(org);
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Organization org = orgRepo.findOneById(exeRepo.findById(user.getEntityKey()).get().getOrg().getId());
                discounts = dsRepo.findByOrg(org);
            } else {
                return ResponseEntity.notFound().build();
            }
            return new ResponseEntity<Response>(new Response("Found Results", discounts, HttpStatus.OK), HttpStatus.OK);
//			ResponseEntity.ok().body(discounts);

        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/discount")
    ResponseEntity<Response> addNewDiscounts(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @RequestBody Discount discount) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Organization org = orgRepo.findOneById(user.getEntityKey());
                discount.setOrg(org);
                discount = dsRepo.save(discount);
                return new ResponseEntity<Response>(
                        new Response("Saved discount successfully", discount, HttpStatus.OK), HttpStatus.OK);
            } else {

                return new ResponseEntity<Response>(new Response("No enough Permission"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    // @PostMapping("/discount/{dsid}/validate")
    // ResponseEntity<Response>
    // validateDispatchList(@RequestHeader(value="X-AUTH-TOKEN") String
    // token,@PathVariable long dlid){
    // return ResponseEntity.ok().build();
    // }

    @GetMapping("/discount/{dsid}")
    ResponseEntity<Response> getDiscouintbyId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                              @PathVariable long dsid) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            List<Discount> discounts;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                try {
                    Discount dis = dsRepo.getOne(dsid);
                    if (dis != null && dis.getOrg().getId() == user.getEntityKey()) {
                        return new ResponseEntity<Response>(new Response("Found Results", dis, HttpStatus.OK), HttpStatus.OK);
                    } else {
                        return new ResponseEntity<Response>(new Response("No Found Results"), HttpStatus.NO_CONTENT);

                    }
                } catch (EntityNotFoundException e) {
                    return new ResponseEntity<Response>(new Response("No Found Results"), HttpStatus.NO_CONTENT);

                }

            } else {
                return new ResponseEntity<Response>(new Response("no enough permissions"), HttpStatus.NOT_ACCEPTABLE);
            }
//			ResponseEntity.ok().body(discounts);

        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

}
