package com.workforce.webapp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.Address;
import com.workforce.webapp.model.CrmUser;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.UserStatus;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.CrmUserRepository;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.utils.RestUtils;


@CrossOrigin
@EnableAutoConfiguration
@RestController
public class CrmUserController {


    @Autowired
    private CrmUserRepository crmURepo;

    @Autowired
    private UserDetailsRepository userRepo;


    @Value("${attachments-folder}")
    private String attachmentsFolder;

    @SuppressWarnings("rawtypes")
    @PostMapping("/crmuser")
    public ResponseEntity<?> addCrmUser(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                        @RequestBody CrmUser crmuser) {

        System.out.println("Student =" + crmuser.toString());
        UserDetails user = userRepo.findByToken(token);
        System.out.print(user.toString());
        if (user != null && user.getRole().equals(Userrole.CRM_ADMIN)) {

            CrmUser existingCu = crmURepo.findByPhone(crmuser.getPhone());
            if (existingCu == null) {

                crmURepo.save(crmuser);
                UserDetails newexe = new UserDetails();
                newexe.setRole(Userrole.CRM_USER);
                newexe.setUsername(crmuser.getName());
                newexe.setPhone(crmuser.getPhone());
                newexe.setEntityKey(crmuser.getCrmusid());
                newexe.setStatus(UserStatus.PENDING_SELF_ACTIVATION);
                newexe.setLogin(crmuser.getPhone());
                newexe.setPassword(RestUtils.md5(crmuser.getPhone()));
                newexe.setEmail(crmuser.getEmail());
                String userActivation = "";
                while (userActivation.equals("")) {
                    userActivation = RestUtils.getActivationKey();
                    if (userRepo.findByActivationKey(userActivation) == null) {
                        System.out.println("Activation key=" + userActivation);
                        newexe.setActivationKey(userActivation);
                    } else {
                        userActivation = "";
                    }
                }
                userRepo.save(newexe);
                // Dear ##name##, use your phone number as username and
                // password for eorbapp.
                String msg = "Dear " + newexe.getUsername()
                        + ", use your phone number as username and password for Nemiit CRM  @"
                        + " https://bit.ly/2J9ui1Q -Thanks, Call @ 8886668273, Neemiit";
                // String msg= "Executive account created for
                // "+org.getOrgname()+"\n login with your phone number
                // as uname and pswd";
                System.out.println("Message=" + msg);
                SmsServiceImpl.sendSms(newexe.getPhone(), msg); // "success";//

                // String body ="Hi "+newexe.getUsername()+",<br>"+"<i
                // style='color:#B51F93'>Greetings from
                // "+org.getOrgname()+"..!!!</i><br><br>Kindly click the
                // below link for activating you account<br>"
                // +
                // "http://192.168.0.102:8082/activateAccount?activationKey="+userActivation;
                // MailUtils.sendMail(newexe.getEmail(), "Erob
                // Alert.!!!Account Successfully Created",
                // userActivation);

                return ResponseEntity.ok(crmuser);
                // return
                // RestUtils.map("Error","No","Executive",executive,"Message",executive.getName()+"
                // sucessfully added");

//				} else {
//					return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null, null,
//							"Organization details not found", null), HttpStatus.NOT_ACCEPTABLE);
//					//
//
//					// return RestUtils.map("Error","Yes","Message","");
//				}

            } else {
                if (crmuser.getCrmusid() != 0) {

                    UserDetails exeu = userRepo.findByEntityKeyAndRole(crmuser.getCrmusid(),
                            Userrole.CRM_USER);
                    if (exeu != null) {
//						UserDetails exeu = exeuser.get();
                        exeu.setEmail(crmuser.getEmail());
                        exeu.setUsername(crmuser.getName());
                        try {
                            userRepo.save(exeu);
                        } catch (Exception e) {
                            return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null,
                                    null, "" + e.getMessage(), null), HttpStatus.NOT_ACCEPTABLE);

                            // TODO: handle exception
                        }
                    } else {
                        return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null, null,
                                "Executive details not found", null), HttpStatus.NOT_ACCEPTABLE);
                    }
                    try {
                        crmURepo.save(crmuser);
                        return ResponseEntity.ok(crmuser);
                    } catch (DataIntegrityViolationException ex) {
                        ex.printStackTrace();
                        System.out.println("cause:" + ex.getCause().getCause().getMessage() + "; cause local:"
                                + ex.getCause().getLocalizedMessage());
                        String msg = ex.getCause().getCause().getMessage();
                        System.out.println("Msg:" + msg);
                        if (msg != null && msg.contains("imglogo")) {
                            String name = "/executiveimage/" + new Date().getTime() + ".JPG";
                            String img = RestUtils.Base64ToImage(crmuser.getImglogo(), name, attachmentsFolder);
                            if (img != null) {
                                crmuser.setImglogo(name);
                            } else {
                                crmuser.setImglogo("");
                            }
                            crmURepo.save(crmuser);
                            return ResponseEntity.ok(crmuser);
                        } else if (msg != null && msg.contains("aadhaar_number")) {
                            return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null,
                                    null, "Aadhaar Number is not valid", null), HttpStatus.NOT_ACCEPTABLE);
                        } else {
                            return new ResponseEntity<Response>(
                                    new Response(HttpStatus.NOT_ACCEPTABLE, null, null, msg, null),
                                    HttpStatus.NOT_ACCEPTABLE);

                        }

                    }
                } else {
                    return new ResponseEntity<Response>(new Response(HttpStatus.NOT_ACCEPTABLE, null, null,
                            "Executive with phonenumber already exists !!", null), HttpStatus.NOT_ACCEPTABLE);

                }
            }
        } else {

            return new ResponseEntity<Response>(
                    new Response(HttpStatus.UNAUTHORIZED, null, null, "Not Authorised...!!", null),
                    HttpStatus.UNAUTHORIZED);
        }

    }


    @GetMapping("/crmuser")
    public ResponseEntity<?> getCrmUsers(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            List<CrmUser> crmusers = crmURepo.findAll();
            return ResponseEntity.ok(crmusers);

        } else {
            return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/crmuser/{id}")
    public ResponseEntity<?> getCrmUser(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long id) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            Optional<CrmUser> crmuop = crmURepo.findById(id);
            if (crmuop.isPresent()) {
                return ResponseEntity.ok(crmuop.get());
            } else {
                return new ResponseEntity<String>("Not Allowed", HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
        }

    }

    //	@Scheduled(cron = "0 0 8 * * *")
//	void sendLoginNotification() {
//		List<String> regids = userRepo.findAllFbToksByRole(Userrole.USER_EXECUTIVE.toString());
//		if (regids != null && regids.size() > 0) {
//			Firebase.sendNotificationToMultiple(regids,
//					"Every day is a fresh new start.. So just paint the canvas of your life with beautiful days and delightful memories.. Greetings from E-Orb",
//					"Good Morning..!! Login alert");
//			System.out.println("Sent Notification");
//		} else {
//			System.out.println("Unable to Send Notification Role:" + Userrole.USER_EXECUTIVE.toString());
//		}
//	}
//
//	@Scheduled(cron = "0 30 8 * * *")
//	void sendLoginNotificationtaeight() {
//		List<String> regids = userRepo.findAllFbToksByRole(Userrole.USER_EXECUTIVE.toString());
//		if (regids != null && regids.size() > 0) {
//			Firebase.sendNotificationToMultiple(regids,
//					"Every day is a fresh new start.. So just paint the canvas of your life with beautiful days and delightful memories.. Greetings from E-Orb",
//					"Good Morning..!! Login alert");
//			System.out.println("Sent Notification");
//		} else {
//			System.out.println("Unable to Send Notification Role:" + Userrole.USER_EXECUTIVE.toString());
//		}
//	}
//
//	@Scheduled(cron = "0 0 9 * * *")
//	void sendLoginNotificationAtNine() {
//		List<String> regids = userRepo.findAllFbToksByRole(Userrole.USER_EXECUTIVE.toString());
//		if (regids != null && regids.size() > 0) {
//			Firebase.sendNotificationToMultiple(regids,
//					"Every day is a fresh new start.. So just paint the canvas of your life with beautiful days and delightful memories.. Greetings from E-Orb",
//					"Good Morning..!! Login Warning");
//			System.out.println("Sent Notification");
//		} else {
//			System.out.println("Unable to Send Notification Role:" + Userrole.USER_EXECUTIVE.toString());
//		}
//	}
//
    @GetMapping("/CrmUser/sam")
    public CrmUser getSampCrmUser() {
        Address add = new Address("#4, Balamrai", "Paradise", "Telangana", "500061");
        CrmUser cmUser = new CrmUser("Thelidhu", "7845845812",
                "http://www.suitdoctors.com/wp-content/uploads/2016/11/dummy-man-570x570.png", add, null,
                new Date(), "AP10 AJ 9999", "ESFF12ZX", "2018SVERA", new Date(), "123212321231",
                "12AJ12K432", "Nothing", "Good At work");
        return cmUser;
    }

}
