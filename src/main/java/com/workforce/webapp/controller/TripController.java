package com.workforce.webapp.controller;

import com.workforce.webapp.dto.trip.TripDto;
import com.workforce.webapp.dto.trip.ScheduleDto;
import com.workforce.webapp.enums.trip.TripStatus;
import com.workforce.webapp.model.Location;
import com.workforce.webapp.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class TripController {

    private @Autowired
    TripService service;

    @GetMapping("/trip/stats")
    public ResponseEntity getTripsStats(@RequestParam(name = "exeId", required = false) Long exeId) {
        return ResponseEntity.ok(service.getTripsStats(exeId));
    }

    @GetMapping("/trip")
    public ResponseEntity getAllTrips(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(service.getAllTrips(token));
    }

    @GetMapping("/trip/{id}")
    public ResponseEntity getTripById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                      @PathVariable(name = "id") String tripId) {
        return ResponseEntity.ok(service.getTripById(token, tripId));
    }

    @PostMapping(value = "/trip")
    public ResponseEntity createTrip(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                     @Valid @RequestBody TripDto tripDto) {
        return ResponseEntity.ok(service.createTrip(token, null, tripDto));
    }

    @PutMapping(value = "/trip/{id}")
    public ResponseEntity updateTrip(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                     @PathVariable(name = "id") String tripId,
                                     @Valid @RequestBody TripDto tripDto) {
        return ResponseEntity.ok(service.updateTrip(token, tripId, tripDto));
    }

    @PostMapping(value = "/trip/schedule")
    public ResponseEntity createTripSchedule(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @Valid @RequestBody ScheduleDto scheduleDto) {
        return ResponseEntity.ok(service.createTripSchedule(token, null, scheduleDto));
    }


    @PutMapping(value = "/trip/schedule/{id}")
    public ResponseEntity updateTripSchedule(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @PathVariable(name = "id") String scheduleId,
                                             @Valid @RequestBody ScheduleDto scheduleDto) {
        return ResponseEntity.ok(service.updateTripSchedule(token, scheduleId, scheduleDto));
    }

    @PatchMapping(value = "/trip/schedule/{id}")
    public ResponseEntity tripScheduleStatus(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                             @PathVariable(name = "id") String scheduleAllocId,
                                             @RequestParam(name = "status") TripStatus status) {
        service.scheduleTripStatus(token, scheduleAllocId, status);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/trip/schedule")
    public ResponseEntity getScheduleAllocByExe(@RequestHeader(value = "X-AUTH-TOKEN") String token) {
        return ResponseEntity.ok(service.getScheduleAllocByExc(token));
    }


    @GetMapping("/trip/schedule/{schId}")
    public ResponseEntity getSchAllocByOrgWithSchId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                    @PathVariable(name = "schId") String schId) {
        return ResponseEntity.ok(service.getScheduleAllocByOrg(token, schId, null));
    }

    @GetMapping("/trip/schedule/exec/{exeId}")
    public ResponseEntity getSchAllocByOrgWithExeId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                    @PathVariable(name = "exeId") Long exeId) {
        return ResponseEntity.ok(service.getScheduleAllocByOrg(token, null, exeId));
    }

    @GetMapping("/trip/{tripId}/scheduleAlloc/{exeId}")
    public ResponseEntity getAllocByExeIdAndTripId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                   @PathVariable(name = "tripId") String tripId,
                                                   @PathVariable(name = "exeId") Long exeId) {
        return ResponseEntity.ok(service.getAllocByExeIdAndTripId(token, tripId, exeId));
    }

    @GetMapping("/trip/scheduleAlloc/{id}/visits")
    public ResponseEntity getScheduleVisits(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                            @PathVariable(name = "id") String scheduleAllocId) {
        return ResponseEntity.ok(service.getScheduleVisit(token, scheduleAllocId));
    }

    @PatchMapping(value = "/trip/schedule/visit/{schVisitId}")
    public ResponseEntity scheduleVisitStatus(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                              @PathVariable(name = "schVisitId") String schVisitId,
                                              @RequestParam(name = "status") TripStatus status,
                                              @Valid @RequestBody Location location) {
        service.scheduleVisitStatus(token, schVisitId, status, location);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/trip/points")
    public ResponseEntity getAllPoints() {
        return ResponseEntity.ok(service.getAllPoints());
    }


    @DeleteMapping("/trip/schedule/{id}")
    public ResponseEntity delete(@PathVariable(name = "id") String scheduleId) {
        service.delete(scheduleId);
        return ResponseEntity.ok().build();
    }

}
