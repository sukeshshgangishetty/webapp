package com.workforce.webapp.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.AttendenceData;
import com.workforce.webapp.dto.AttendenceDataDto;
import com.workforce.webapp.dto.AttendenceTableData;
import com.workforce.webapp.dto.EmployeeMeta;
import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.AttendanceAction;
import com.workforce.webapp.model.AttendanceActivity;
import com.workforce.webapp.model.AttendanceIndication;
import com.workforce.webapp.model.AttendanceNotation;
import com.workforce.webapp.model.AttendanceRecord;
import com.workforce.webapp.model.AttendanceRegularization;
import com.workforce.webapp.model.AttendanceSource;
import com.workforce.webapp.model.AttendenceCurrentStatus;
import com.workforce.webapp.model.Employee;
import com.workforce.webapp.model.LeaveApplication;
import com.workforce.webapp.model.LeaveStatus;
import com.workforce.webapp.model.RegularizationAction;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.AttendanceActivityRepository;
import com.workforce.webapp.repository.AttendanceRecordRepository;
import com.workforce.webapp.repository.AttendenceRegularizationRepository;
import com.workforce.webapp.repository.EmployeeRepository;
import com.workforce.webapp.repository.LeaveRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@CrossOrigin
@EnableAutoConfiguration
@RestController
@RequestMapping("/attendance")
public class AttendanceController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    AttendanceActivityRepository attActionRepo;

    @Autowired
    AttendanceRecordRepository attRepo;

    @Autowired
    private LeaveRepository leaveRepo;

    @Autowired
    EmployeeRepository empRepo;

    @Autowired
    AttendenceRegularizationRepository attRegRepo;

    @GetMapping("")
    ResponseEntity<?> getAttendance(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestParam int month,
                                    @RequestParam int year) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {

                AttendenceTableData attTabData = new AttendenceTableData();
                attTabData.setAttendanceMonth(month);
                attTabData.setAttendanceYear(year);
                List<AttendenceData> attData = new ArrayList<AttendenceData>();
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.MONTH, month - 1);
                cal.set(Calendar.YEAR, year);

                Date begining, end;

                Calendar calendar = cal;
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                setTimeToBeginningOfDay(calendar);
                begining = calendar.getTime();
                Calendar calendarb = cal;
                calendarb.set(Calendar.DAY_OF_MONTH, calendarb.getActualMaximum(Calendar.DAY_OF_MONTH));
                setTimeToEndofDay(calendarb);
                end = calendarb.getTime();

                System.out.println("Data: Begning" + begining + ", end:" + end);

                if (user.getRole().equals(Userrole.ORGANIZATION)) {
                    List<Employee> emps = empRepo.findAll();
                    for (Employee emp : emps) {

                        AttendenceData atData = new AttendenceData();
                        atData.setEmployee(new EmployeeMeta(emp.getfName() + emp.getLName(), emp.getDesignation(), null,
                                emp.getEmpId()));
                        atData.setPresentdays(2.5);
                        List<AttendanceRecord> recs = attRepo.findByAttOnBetweenAndEmpEmpId(begining, end,
                                emp.getEmpId());
                        atData.setData(recs);
                        attData.add(atData);
                    }

                } else if (user.getRole().equals(Userrole.HRMS_EMPLOYEE) && user.isReportingManager()) {

                    List<Employee> emps = empRepo.findByReportingEmpId(user.getEntityKey());
                    for (Employee emp : emps) {

                        AttendenceData atData = new AttendenceData();
                        atData.setEmployee(new EmployeeMeta(emp.getfName() + emp.getLName(), emp.getDesignation(), null,
                                emp.getEmpId()));
                        atData.setPresentdays(2.5);
                        List<AttendanceRecord> recs = attRepo.findByAttOnBetweenAndEmpEmpId(begining, end,
                                emp.getEmpId());
                        atData.setData(recs);
                        attData.add(atData);
                    }
                } else {

                    Employee emp = empRepo.getOne(user.getEntityKey());
                    AttendenceData atData = new AttendenceData();
                    atData.setEmployee(new EmployeeMeta(emp.getfName() + emp.getLName(), emp.getDesignation(), null,
                            emp.getEmpId()));
                    atData.setPresentdays(2.5);
                    List<AttendanceRecord> recs = attRepo.findByAttOnBetweenAndEmpEmpId(begining, end, emp.getEmpId());
                    atData.setData(recs);
                    attData.add(atData);

                }

                attTabData.setAttendancedata(attData);
                return ResponseEntity.ok(attTabData);
            } else {
                return new ResponseEntity<Response>(new Response("No enough permissions"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {

            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    private void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private void setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

    @GetMapping("/employee/{empid}")
    ResponseEntity<?> getAttendanceByEmployeeId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                @PathVariable long empid, @RequestParam(required = false) Date attOn) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                if (attOn == null) {
                    List<AttendanceRecord> attendanceRecords = attRepo.findByEmpEmpId(empid);
                    return ResponseEntity.ok(attendanceRecords);

                } else {

                    Optional<AttendanceRecord> attOp = attRepo.findOneByAttOnAndEmpEmpId(attOn, empid);
                    if (attOp.isPresent()) {

                        List<AttendanceActivity> activities = attActionRepo
                                .findByAttendenceRecId(attOp.get().getAttRecId());
                        return ResponseEntity.ok(new AttendenceDataDto(attOp.get(), activities));

                    } else {
                        return new ResponseEntity<Response>(new Response("No Data found"), HttpStatus.NO_CONTENT);
                    }

                }

            } else {
                return new ResponseEntity<Response>(new Response("No enough permissions"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {

            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }

    }

    @GetMapping("/{attid}/action")
    ResponseEntity<?> getAttendanceActionByAttendanceId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                        @PathVariable long attid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {

                List<AttendanceActivity> activities = attActionRepo.findByAttendenceRecId(attid);
                return ResponseEntity.ok(activities);

            } else {
                return new ResponseEntity<Response>(new Response("No enough permissions"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {

            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/action")
    ResponseEntity<?> addAttendenceAction(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                          @RequestBody AttendanceActivity action) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {

                if (action.getSource() != null) {

                    AttendanceRecord att;
                    Optional<AttendanceRecord> existRecord = attRepo.findOneByAttOnAndEmpEmpId(new Date(),
                            user.getEntityKey());
                    if (existRecord.isPresent()) {
                        att = existRecord.get();
                    } else {
                        att = new AttendanceRecord();
                        att.setAttOn(new Date());
                        att.setEmp(new Employee(user.getEntityKey()));
                        att.setPunchIn(new Date());
                        att.setAddedBy(user);
                        att.setNotation(AttendanceNotation.PRESENT);
                        att.setIndication(AttendanceIndication.ON_TIME);
                        att = attRepo.save(att);

                    }

                    AttendanceActivity attAct = new AttendanceActivity();
                    attAct.setAction((att.getCurrStatus() == null
                            || att.getCurrStatus().equals(AttendenceCurrentStatus.NOT_AVAILABLE))
                            ? AttendanceAction.PUNCH_IN
                            : AttendanceAction.PUNCH_OUT);

                    if (attAct.getAction().equals(AttendanceAction.PUNCH_OUT)) {
                        att.setPunchOut(new Date());
                    }
                    attAct.setActionAt(new Date());
                    attAct.setReason(action.getReason());
                    attAct.setActionTime(new Date());
                    attAct.setAttendenceRecId(att.getAttRecId());
                    attAct.setSource(action.getSource());
                    attAct = attActionRepo.save(attAct);

                    att.setCurrStatus((att.getCurrStatus() == null
                            || att.getCurrStatus().equals(AttendenceCurrentStatus.NOT_AVAILABLE))
                            ? AttendenceCurrentStatus.AVAILABLE
                            : AttendenceCurrentStatus.NOT_AVAILABLE);
                    att = attRepo.save(att);
                    return new ResponseEntity<Response>(new Response("Successfully " + attAct.getAction() + ""),
                            HttpStatus.OK);
                } else {

                    // missing source
                    return new ResponseEntity<Response>(new Response("Source is missing"), HttpStatus.NOT_ACCEPTABLE);

                }

            } else {

                return new ResponseEntity<Response>(new Response("No enough permissions"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {

            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }

    }

    @PostMapping("/regularization")
    ResponseEntity<?> addAttendanceReqularization(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                  @RequestBody AttendanceRegularization regularization) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {

                if (regularization.getRegAction().equals(RegularizationAction.ADD_ACTION)) {

                    AttendanceRecord att;
                    Optional<AttendanceRecord> existRecord = attRepo.findOneByAttOnAndEmpEmpId(new Date(),
                            user.getEntityKey());
                    if (existRecord.isPresent()) {
                        att = existRecord.get();
                    } else {
                        att = new AttendanceRecord();
                        att.setAttOn(regularization.getRegularizationDate());
                        att.setEmp(regularization.getEmp());
                        att.setPunchIn(regularization.getActionTime());
                        att.setAddedBy(user);
                        att = attRepo.save(att);

                    }
                    AttendanceActivity attAct = new AttendanceActivity();
                    attAct.setAction(regularization.getAction());
                    attAct.setActionAt(att.getAttOn());
                    attAct.setActionTime(regularization.getActionTime());
                    attAct.setAttendenceRecId(att.getAttRecId());
                    attAct.setSource(AttendanceSource.MANUAL);
                    attAct = attActionRepo.save(attAct);

                } else if (regularization.getRegAction().equals(RegularizationAction.MODIFY_ACTION)) {
                    try {
                        AttendanceActivity activity = attActionRepo.getOne(regularization.getRefId());
                        activity.setAction(regularization.getAction());
                        activity.setActionTime(regularization.getActionTime());
                        attActionRepo.save(activity);
                    } catch (EntityNotFoundException e) {
                        return new ResponseEntity<Response>(new Response("invalid Reference Id"),
                                HttpStatus.NOT_ACCEPTABLE);

                        // TODO: handle exception
                    }
                }
                regularization.setRegularizedBy(user);
                attRegRepo.save(regularization);
                return ResponseEntity.ok(regularization);

            } else {
                return new ResponseEntity<Response>(new Response("No enough permissions"), HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/regularization")
    ResponseEntity<?> getRegularizations(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestParam int month,
                                         @RequestParam int year) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                return ResponseEntity.ok(attRegRepo.findAll());
            } else {
                return new ResponseEntity<Response>(new Response("No enough permissions"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {

            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/regularization/{regid}")
    ResponseEntity<?> getRegularizationById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                            @PathVariable long regid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                try {
                    return ResponseEntity.ok(attRegRepo.getOne(regid));
                } catch (EntityNotFoundException e) {
                    // TODO: handle exception
                    return ResponseEntity.notFound().build();

                }

            } else {
                return new ResponseEntity<Response>(new Response("No enough permissions"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {

            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/regularization/employee/{empid}")
    ResponseEntity<?> getRegularizationbyEmployeeId(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                                    @PathVariable long empid, @RequestParam int month, @RequestParam int year) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.HRMS_EMPLOYEE)) {
                List<AttendanceRegularization> regularizations = attRegRepo.findByEmpEmpId(empid);
                return ResponseEntity.ok(regularizations);

            } else {
                return new ResponseEntity<Response>(new Response("No enough permissions"), HttpStatus.NOT_ACCEPTABLE);

            }

        } else {

            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/attendence/generation")
    ResponseEntity<?> attendenceGeneration(@RequestHeader(value = "X-AUTH-TOKEN") String token) {

        if (token.contains("asdasdasdasd")) {
            List<Employee> employess = empRepo.findAll();
            System.out.println("Att gen Data: 1");
            if (!employess.isEmpty()) {
                System.out.println("Att gen Data: 2");

                for (Employee emp : employess) {
                    System.out.println("Att gen Data: 3, emp =" + emp);

                    Optional<AttendanceRecord> attRecOp = attRepo.findOneByAttOnAndEmpEmpId(new Date(), emp.getEmpId());
                    if (attRecOp.isPresent()) {

                        AttendanceRecord attRec = attRecOp.get();
                        List<AttendanceActivity> activities = attActionRepo
                                .findByAttendenceRecIdOrderByActionTimeAsc(attRec.getAttRecId());
                        System.out.println("Att gen Data: 4,  date:" + (new Date()) + ", datta" + activities);

                        if (activities.isEmpty()) {
                            System.out.println("Att gen Data: 5");

                            // no records found
                            attRec.setActionRequired(true);
                            attRec.setActionMessage(
                                    "No Activity is recorded, this might be due to system generated record");
                            attRec.setNotation(AttendanceNotation.ACTION_REQUIRED);
                            attRec.setIndication(AttendanceIndication.ON_TIME);

                        } else {
                            //
                            double production;

                            if (activities.size() == 1) {

                                System.out.println("Att gen Data: 8, data is not sufficent");

                                // data is insufficient

                            } else {
                                System.out.println("Att gen Data: 9, found data");

                                Date checkIn = null, checkOut = null;

                                for (AttendanceActivity a : activities) {

                                    System.out.println("Activity:" + a);

                                    // get 1st check in

                                    if (a.getAction().equals(AttendanceAction.PUNCH_IN) && checkIn == null) {

                                        checkIn = a.getActionTime();
                                        attRec.setPunchIn(a.getActionTime());

                                    }

                                    // get last check out

                                    if (a.getAction().equals(AttendanceAction.PUNCH_OUT)) {

                                        checkOut = a.getActionTime();
                                        attRec.setPunchOut(a.getActionTime());

                                    }

                                }

                                if (attRec.getPunchIn() == null || attRec.getPunchOut() == null) {
                                    attRec.setActionRequired(true);
                                    attRec.setActionMessage("Actions not performed correctly");
                                    attRec.setIndication(AttendanceIndication.EARLY_LOGOUT);
                                    attRec.setNotation(AttendanceNotation.ACTION_REQUIRED);
                                    // inconsistance in check in and out, missing checkin/checkout action
                                } else {
                                    // last check out lesser than 1st check in
                                    System.out.println("Att gen Data: 10, PunchIn:" + checkIn + " punchOut:" + checkOut
                                            + ", production = "
                                            + (checkOut.getTime() - checkIn.getTime()) / (1000 * 60));

                                    production = (checkOut.getTime() - checkIn.getTime()) / (1000 * 60);

                                    if (checkOut.getTime() > checkIn.getTime()) {
                                        attRec.setActionRequired(false);
                                        if (production < 500) {
                                            if (production < 300) {
                                                attRec.setIndication(AttendanceIndication.ON_TIME);
                                                attRec.setNotation(AttendanceNotation.FIRST_HALF);
                                            } else {
                                                attRec.setIndication(AttendanceIndication.EARLY_LOGOUT);
                                                attRec.setNotation(AttendanceNotation.PRESENT);
                                            }
                                        } else if (production > 560) {
                                            attRec.setIndication(AttendanceIndication.OVERTIME);
                                            attRec.setNotation(AttendanceNotation.PRESENT);

                                        } else {
                                            attRec.setIndication(AttendanceIndication.ON_TIME);
                                            attRec.setNotation(AttendanceNotation.PRESENT);

                                        }
                                        attRec.setProduction(production);
                                        if (production > 560) {
                                            attRec.setOverTime((long) (production - 560));
                                        } else {
                                            attRec.setOverTime(0);
                                        }

                                    } else {
                                        // Inconsistency in check in and out
                                        attRec.setActionRequired(true);
                                        attRec.setNotation(AttendanceNotation.PRESENT);
                                        attRec.setIndication(AttendanceIndication.ON_TIME);
                                        attRec.setActionMessage("Inconsistency in check in and out");
                                    }
                                }

                            }
                            System.out.println("Att gen Data: 6");

                        }
                        attRepo.save(attRec);

                    } else {
                        // attendence record not present,
                        // need to check if approved leave is present

                        List<LeaveApplication> ApprovedLeavesOnDate = leaveRepo
                                .findByAppliedByEmpIdAndStatusAndLeaveFromIsLessThanEqualAndLeaveToIsGreaterThanEqual(
                                        emp.getEmpId(), LeaveStatus.APPROVED, new Date(), new Date());
                        AttendanceRecord att = new AttendanceRecord();
                        att.setAttOn(new Date());
                        att.setEmp(new Employee(emp.getEmpId()));
                        att.setActionRequired(true);
                        att.setProduction(0);
                        att.setIndication(AttendanceIndication.ON_TIME);
                        if (ApprovedLeavesOnDate.isEmpty()) {
                            att.setNotation(AttendanceNotation.ABSENT);
                            att.setActionMessage("This is system generated Record, since no action is recorded");
                        } else {
                            att.setNotation(AttendanceNotation.LEAVE);
                            att.setActionMessage("" + ApprovedLeavesOnDate.get(0).getLeaveid());
                        }

                        // need add new record and need to give alert indication

                        att = attRepo.save(att);
                        System.out.println("Att gen Data: 7");
                    }

                }

            }

            // jira@neemiit.com
            // NU(L_(dgFnu7
            return ResponseEntity.ok().build();
        } else {
            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }

    }

}
