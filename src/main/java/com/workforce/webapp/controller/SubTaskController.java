package com.workforce.webapp.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.SubTask;
import com.workforce.webapp.model.Task;
import com.workforce.webapp.model.TaskPriority;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.DispatchListRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.SubTaskRepository;
import com.workforce.webapp.repository.TaskRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@CrossOrigin
@RestController
@RequestMapping("/task")
public class SubTaskController {

    @Autowired
    DispatchListRepository dispRepo;

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    OrderRepository orderRepo;

    @Autowired
    OrganizationRepository orgRepo;

    @Autowired
    TaskRepository taskRepo;

    @Autowired
    SubTaskRepository subTRepo;

    @GetMapping("/{tid}/sub")
    ResponseEntity<Response> getAllSuntasks(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                            @PathVariable long tid) {

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE)) {

                Optional<Task> existTaskop = taskRepo.findById(tid);
                if (existTaskop.isPresent()) {

                    Task exTask = existTaskop.get();
                    List<SubTask> subtasks = subTRepo.findByTask(exTask);
                    return new ResponseEntity<Response>(new Response("Found results", subtasks, HttpStatus.OK),
                            HttpStatus.OK);
                } else {
                    // task not found
                    return new ResponseEntity<Response>(new Response("Invalid main task details"),
                            HttpStatus.NOT_ACCEPTABLE);
                }

            } else {
                // not acceptable to use by user
                return new ResponseEntity<Response>(new Response("No enough permissions to access this"),
                        HttpStatus.NOT_ACCEPTABLE);
            }

        } else {

            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/{tid}/sub")
    ResponseEntity<Response> addSubtask(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long tid,
                                        @RequestBody SubTask subtask) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE)) {

                Optional<Task> existTaskop = taskRepo.findById(tid);
                if (existTaskop.isPresent()) {
                    Task exTask = existTaskop.get();
                    subtask.setTask(exTask);
                    subtask = subTRepo.save(subtask);
                    return new ResponseEntity<Response>(new Response("Added Subtask", subtask, HttpStatus.OK),
                            HttpStatus.OK);
                } else {
                    // task not found
                    return new ResponseEntity<Response>(new Response("Invalid main task details"),
                            HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                // not acceptable to use by user
                return new ResponseEntity<Response>(new Response("No enough permissions to access this"),
                        HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/{tid}/sub/{stid}")
    ResponseEntity<Response> getSubtaskById(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long tid,
                                            @PathVariable long stid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {

            if (user.getRole().equals(Userrole.ORGANIZATION) || user.getRole().equals(Userrole.USER_EXECUTIVE)) {

                Optional<SubTask> existSubTaskop = subTRepo.findById(stid);
                if (existSubTaskop.isPresent()) {

                    SubTask exSubTask = existSubTaskop.get();
                    if (exSubTask.getTask() != null && exSubTask.getTask().getTid() == tid) {
                        return new ResponseEntity<Response>(new Response("Added Subtask", exSubTask, HttpStatus.OK),
                                HttpStatus.OK);

                    } else {
                        // invalid task id// sub task task not found
                        return new ResponseEntity<Response>(new Response("Invalid sub task details"),
                                HttpStatus.NOT_ACCEPTABLE);

                    }
                } else {
                    // sub task task not found
                    return new ResponseEntity<Response>(new Response("Invalid main task details"),
                            HttpStatus.NOT_ACCEPTABLE);
                }

            } else {
                // not acceptable to use by user
                return new ResponseEntity<Response>(new Response("No enough permissions to access this"),
                        HttpStatus.NOT_ACCEPTABLE);
            }

        } else {
            return new ResponseEntity<Response>(new Response("Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
    }


    @GetMapping("/sub/sam")
    SubTask getSampleSubTask() {
        Set<String> attach = new HashSet<>();
        attach.add("attachments/task/subtask/1234423232.JPG");
        attach.add("attachments/task/subtask/123442322232.PNG");
        attach.add("attachments/task/subtask/123442323232.PDF");
        SubTask subTask = new SubTask("Sample sub task", "this is just for referance purpose", new Client(), "Any Description if client is NULL", new Date(), new Date(), TaskPriority.HIGH, attach, new Date(), null);


        return subTask;
    }

}