package com.workforce.webapp.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.Organization;
import com.workforce.webapp.model.PriceList;
import com.workforce.webapp.model.Product;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.ExecutiveRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.PriceListRepository;
import com.workforce.webapp.repository.ProductRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@RestController
@CrossOrigin
@RequestMapping("/pricelist")
public class PriceListController {


    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    PriceListRepository pListRepo;


    @Autowired
    OrganizationRepository orgRepo;


    @Autowired
    ExecutiveRepository exeRepo;


    @Value("${attachments-folder}")
    private String attachmentsFolder;

    @Autowired
    private ProductRepository prodRepo;

    @GetMapping(value = "/")
    ResponseEntity<?> getPriceList(@RequestHeader("X-AUTH-TOKEN") String token,
                                   @RequestParam(required = false) Integer prps) {
        System.out.println("Prps=" + prps);
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            List<PriceList> priceList;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Organization org = orgRepo.findOneById(user.getEntityKey());
                priceList = pListRepo.findByOrg(org);
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                Organization org = orgRepo.findOneById(exeRepo.findById(user.getEntityKey()).get().getOrg().getId());
                priceList = pListRepo.findByOrg(org);
            } else {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().body(priceList);

        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping(value = "/")
    ResponseEntity<?> addPriceToList(@RequestHeader("X-AUTH-TOKEN") String token, @RequestBody PriceList price) {

        // System.out.println("Client="+client);

        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            if (user.getRole().equals(Userrole.ORGANIZATION)) {
                Organization org = orgRepo.findOneById(user.getEntityKey());
                price.setOrg(org);
                price = pListRepo.save(price);
                return ResponseEntity.ok().body(price);
            } else {

                return new ResponseEntity<Response>(new Response("No enough Permission"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorized"), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/{id}")
    ResponseEntity<?> getPriceListById(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                       @PathVariable(value = "id") long plid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            long orgid = 0;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {

                orgid = user.getEntityKey();
            } else {
                return new ResponseEntity<Response>(new Response("NotAllwed"), HttpStatus.NOT_ACCEPTABLE);
            }
            Optional<PriceList> priceop = pListRepo.findById(plid);
            if (priceop.isPresent()) {
                PriceList price = priceop.get();
                if (price.getOrg().getId() == orgid) {
                    return new ResponseEntity<Response>(new Response("Found details", price, HttpStatus.OK), HttpStatus.OK);
                } else {
                    return new ResponseEntity<Response>(new Response("NotAllwed"), HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                return new ResponseEntity<Response>(new Response("NotAllwed"), HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);
        }
    }


    @GetMapping(value = "/product/{pid}")
    ResponseEntity<?> getPricelistByProduct(@RequestHeader(value = "X-AUTH-TOKEN") String token,
                                            @PathVariable(value = "pid") long pid) {
        UserDetails user = userRepo.findByToken(token);
        if (user != null) {
            long orgid = 0;
            if (user.getRole().equals(Userrole.ORGANIZATION)) {

                orgid = user.getEntityKey();
            } else if (user.getRole().equals(Userrole.USER_EXECUTIVE)) {
                orgid = exeRepo.findById(user.getEntityKey()).get().getOrg().getId();
            } else {
                return new ResponseEntity<Response>(new Response("NotAllwed"), HttpStatus.NOT_ACCEPTABLE);
            }

            Optional<Product> productop = prodRepo.findById(pid);
            if (productop.isPresent()) {

                Product product = productop.get();
                if (product.getOrg().getId() == orgid) {
                    List<PriceList> priceList = pListRepo.findByProduct(product);
                    return new ResponseEntity<Response>(new Response("Found details", priceList, HttpStatus.OK), HttpStatus.OK);

                } else {
                    // no enough permissions
                    return new ResponseEntity<Response>(new Response("NotAllwed"), HttpStatus.NOT_ACCEPTABLE);

                }

            } else {
                // product not found
                return new ResponseEntity<Response>(new Response("Product not found"), HttpStatus.NOT_ACCEPTABLE);

            }
        } else {
            return new ResponseEntity<Response>(new Response("UnAuthorised"), HttpStatus.UNAUTHORIZED);
        }
    }


    @GetMapping(value = "/sam")
    PriceList getSamPrice() {
        PriceList pl = new PriceList(null, 12.50, 11.00, 1.5, 10, null, new Date(), new Date());
        return pl;
    }


}
