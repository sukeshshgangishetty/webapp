package com.workforce.webapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.repository.DispatchListRepository;
import com.workforce.webapp.repository.OrganizationRepository;
import com.workforce.webapp.repository.UserDetailsRepository;

@CrossOrigin
@RestController
public class DispatchListController {


    @Autowired
    DispatchListRepository dispRepo;

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    OrderRepository orderRepo;

    @Autowired
    OrganizationRepository orgRepo;

    @GetMapping("/dispatchlist")
    ResponseEntity<Response> getAllDispatchList(@RequestHeader(value = "X-AUTH-TOKEN") String token) {

        return ResponseEntity.ok().build();
    }

    @PostMapping("/dispatchlist")
    ResponseEntity<Response> generateDispatchList(@RequestHeader(value = "X-AUTH-TOKEN") String token, @RequestBody List<Long> orderids) {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/dispatchlist/{dlid}/validate")
    ResponseEntity<Response> validateDispatchList(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long dlid) {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/dispatchlist/{dlid}")
    ResponseEntity<Response> getDispatchListbyId(@RequestHeader(value = "X-AUTH-TOKEN") String token, @PathVariable long dlid) {
        return ResponseEntity.ok().build();
    }
}
