package com.workforce.webapp.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workforce.webapp.dto.Response;
import com.workforce.webapp.model.UserDetails;
import com.workforce.webapp.model.Userrole;
import com.workforce.webapp.repository.UserDetailsRepository;
import com.workforce.webapp.utils.OTPRequestPurpose;

@CrossOrigin
@RestController
@EnableAutoConfiguration
public class OTPController {

    @Autowired
    UserDetailsRepository userRepo;

    @Autowired
    OTPRepository otpRepo;

    private static final Logger logger = LoggerFactory.getLogger(OTPController.class);

    public boolean logRequest(String name) {

        logger.info("Api request received for " + name);
        return true;
    }

    @RequestMapping(value = "/otp", method = RequestMethod.POST)
    ResponseEntity<?> otp(HttpServletRequest httpServletRequest, @RequestParam String phone, @RequestParam int prps,
                          @RequestHeader(required = false) String token) {
        logRequest("Otp request for" + phone + " from " + prps);

        if (prps == OTPRequestPurpose.ORG_REGISTRATION) {
            UserDetails user = userRepo.findByPhone(phone + "");
            if (user == null) {
                OTP resend = otpRepo.findByPhone(phone + "");
                if (resend == null) {
                    while (true) {
                        int val = ((int) (Math.random() * 9000) + 1000);
                        OTP otp = otpRepo.findByValue(val);
                        if (otp == null) {
                            OTP newOtp = new OTP(phone + "", val);
                            newOtp.setGeneratedat(new Date());
                            otpRepo.save(newOtp);
                            String msg = "OTP: " + val
                                    + " for registring at eOrb\nHave a good day..!! Keep Smileing..!! - Neemiiit,Call @ 8886668273";
                            String smsresult = SmsServiceImpl.sendSms(phone + "", msg); // "success";//
                            return new ResponseEntity<Response>(
                                    new Response("OTP sent to mobile number", HttpStatus.OK), HttpStatus.OK);

                        }
                    }
                } else {

                    String msg = "OTP: " + resend.getValue()
                            + " for registring at eOrb\nHave a good day..!! Keep Smileing..!!";
                    SmsServiceImpl.sendSms(phone + "", msg + "");
                    return new ResponseEntity<Response>(new Response("OTP resended to mobile number", HttpStatus.OK),
                            HttpStatus.OK);
                }

            } else {
                return new ResponseEntity<Response>(
                        new Response("Phone number already registred procced to login", HttpStatus.CONFLICT),
                        HttpStatus.CONFLICT);
            }
        } else if (prps == OTPRequestPurpose.EXPORT_DATA) {

        } else if (prps == OTPRequestPurpose.FORGET_PASSWORD) {
            UserDetails user = userRepo.findByPhone(phone);
            if (user != null) {
                OTP resend = otpRepo.findByPhoneAndOtpfor(phone, OTPRequestPurpose.RESET_PASSWORD);
                if (resend == null) {
                    while (true) {
                        int val = ((int) (Math.random() * 9000) + 1000);
                        OTP otp = otpRepo.findByValue(val);
                        if (otp == null) {
                            OTP newOtp = new OTP(phone + "", val, prps, "Password reset of the user");
                            otpRepo.save(newOtp);
                            String msg = "Hi " + user.getUsername() + "OTP: " + val
                                    + " for Reseting your password\n Note: This action will set your phone number as password, Kindly change to the desired one - Neemiiit,Call @ 8887778273";
                            String smsresult = SmsServiceImpl.sendSms(phone + "", msg); // "success";//
                            if (smsresult.equalsIgnoreCase("error")) {
                                return new ResponseEntity<Response>(
                                        new Response("Unable to send OTP... try again", HttpStatus.EXPECTATION_FAILED),
                                        HttpStatus.EXPECTATION_FAILED);

                            }
                            return new ResponseEntity<Response>(
                                    new Response("OTP sent to mobile number", HttpStatus.OK), HttpStatus.OK);
                        }
                    }
                } else {
                    String msg = "Hi " + user.getUsername() + "OTP: " + resend.getValue()
                            + " for Reseting your password\n Note: This action will set your phone number as password, Kindly change to the desired one - Neemiiit,Call @ 8887778273";
                    String smsresult = SmsServiceImpl.sendSms(phone + "", msg); // "success";//
                    if (smsresult.equalsIgnoreCase("error")) {
                        return new ResponseEntity<Response>(
                                new Response("Unable to resend OTP... try again", HttpStatus.EXPECTATION_FAILED),
                                HttpStatus.EXPECTATION_FAILED);
                    }
                    return new ResponseEntity<Response>(
                            new Response("Otp resent successfully", HttpStatus.EXPECTATION_FAILED),
                            HttpStatus.EXPECTATION_FAILED);
                }
            } else {
                return new ResponseEntity<Response>(new Response("illegal platform", HttpStatus.NOT_ACCEPTABLE),
                        HttpStatus.NOT_ACCEPTABLE);
            }

        } else if (prps == OTPRequestPurpose.RESET_PASSWORD) {
            System.out.println("user_agent:" + httpServletRequest.getHeader("User-Agent"));
            String userAgent = httpServletRequest.getHeader("User-Agent");
            UserDetails user = userRepo.findByPhone(phone);
            if (user != null) {
                if (userAgent != null && (userAgent.length() > 30 && user.getRole().equals(Userrole.ORGANIZATION))
                        || (userAgent.length() == 10 && user.getRole().equals(Userrole.USER_EXECUTIVE))) {

                    OTP resend = otpRepo.findByPhoneAndOtpfor(phone, OTPRequestPurpose.RESET_PASSWORD);
                    if (resend == null) {
                        while (true) {
                            int val = ((int) (Math.random() * 9000) + 1000);
                            OTP otp = otpRepo.findByValue(val);
                            if (otp == null) {
                                OTP newOtp = new OTP(phone + "", val, prps, "Password reset of the user");
                                otpRepo.save(newOtp);
                                String msg = "Hi " + user.getUsername() + "OTP: " + val
                                        + " for Reseting your password\n Note: This action will set your phone number as password, Kindly change to the desired one - Neemiiit,Call @ 8887778273";
                                String smsresult = SmsServiceImpl.sendSms(phone + "", msg); // "success";//
                                if (smsresult.equalsIgnoreCase("error")) {
                                    return new ResponseEntity<Response>(new Response("Unable to send OTP... try again",
                                            HttpStatus.EXPECTATION_FAILED), HttpStatus.EXPECTATION_FAILED);

                                }
                                return new ResponseEntity<Response>(
                                        new Response("OTP sent to mobile number", HttpStatus.OK), HttpStatus.OK);
                            }
                        }
                    } else {
                        String msg = "Hi " + user.getUsername() + "OTP: " + resend.getValue()
                                + " for Reseting your password\n Note: This action will set your phone number as password, Kindly change to the desired one - Neemiiit,Call @ 8887778273";
                        String smsresult = SmsServiceImpl.sendSms(phone + "", msg); // "success";//
                        if (smsresult.equalsIgnoreCase("error")) {
                            return new ResponseEntity<Response>(
                                    new Response("Unable to resend OTP... try again", HttpStatus.EXPECTATION_FAILED),
                                    HttpStatus.EXPECTATION_FAILED);
                        }
                        return new ResponseEntity<Response>(
                                new Response("Otp resent successfully", HttpStatus.EXPECTATION_FAILED),
                                HttpStatus.EXPECTATION_FAILED);
                    }
                } else {
                    return new ResponseEntity<Response>(new Response("illegal platform", HttpStatus.NOT_ACCEPTABLE),
                            HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                return new ResponseEntity<Response>(
                        new Response("Unable to reset password, Unknown phone number", HttpStatus.NOT_ACCEPTABLE),
                        HttpStatus.NOT_ACCEPTABLE);
            }

        }
        return null;
    }

    @RequestMapping(value = "/validateOtp", method = RequestMethod.POST)
    ResponseEntity<Response> validateOtp(@RequestParam String phone, @RequestParam int otp) {
        logRequest("Otp validation request for" + phone + " with " + otp);

        OTP exotp = otpRepo.findByPhone(phone);
        if (exotp != null) {
            if (exotp.getValue() == otp) {
                return new ResponseEntity<Response>(new Response("Valid OTP", HttpStatus.OK), HttpStatus.OK);
            } else {
                return new ResponseEntity<Response>(new Response("Invalid OTP", HttpStatus.NOT_ACCEPTABLE),
                        HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            return new ResponseEntity<Response>(
                    new Response("OTP not available try again later", HttpStatus.NOT_ACCEPTABLE),
                    HttpStatus.NOT_ACCEPTABLE);
        }
    }

}
